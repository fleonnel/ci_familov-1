<script type="text/javascript">
    $.growl({title: "Growl", message: "The kitten is awake!", duration: 10000, style: 'warning'});
    $.growl.error({message: "The kitten is attacking!"});
    $.growl.notice({message: "The kitten is cute!"});
    $.growl.warning({message: "The kitten is ugly!"});
</script>
$(document).ready(function () {
    if ($(".truncate2").length) {
        $(".truncate2").dotdotdot({
            watch: 'window',
            wrap: 'letter'
        });
    }
    if ($(".select2_demo_3").length) {
        $(".select2_demo_3").select2({
            //placeholder: "",
            allowClear: true
        });
    }
});
$(document).on("click", ".status_change", function (e) {
});

function photoPreview(input,class_name,default_img) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+class_name).attr('src', e.target.result);
            //$('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }else{
        $('.'+class_name).attr('src', default_img);
    }
}
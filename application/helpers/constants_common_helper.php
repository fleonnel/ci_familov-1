<?php
define('CONSTANT_HELPER', 'Yupppeee !');
if (!defined('SESSION_FLASH')) define('SESSION_FLASH', 'session_flash');
if (!defined('ROOT_DIR_ADMIN')) define('ROOT_DIR_ADMIN', 'admin/');
if (!defined('EMAIL_HEADER_TITLE')) define('EMAIL_HEADER_TITLE', 'Familov');
define('N_A', 'N/A');

/* FOR FLASH MESSAGE */
define('FLASH_STATUS_SUCCESS', 'S');
define('FLASH_STATUS_ERROR', 'E');
define('FLASH_STATUS_WARNING', 'W');
define('FLASH_STATUS_INFO', 'I');
define('FLASH_TYPE', 'FLASH_TYPE');
define('FLASH_TOAST', 'TOAST');
define('FLASH_HTML', 'HTML');
define('FLASH_STATUS', 'FLASH_STATUS');
define('FLASH_MESSAGE', 'FLASH_MESSAGE');

define('STATUS_TRUE', TRUE);
define('STATUS_FALSE', FALSE);
/* FOR FLASH MESSAGE END */

/* FOR NOTIFICATION MESSAGE */
define('ERROR', 'Error !');
define('SUCCESS', 'Success !');
define('INFO', 'Information !');
define('WARNING', 'Warning !');
define('DATA_TABLE_ERROR', 'Something went wrong !');
define('MESSAGE_LOADING', 'Photo Uploading. Please Wait...!');
define('MSG_DATA_NOT_FOUND', 'Data Not Found!');
define('MSG_DATA_FOUND', 'Data Found!');
/* FOR NOTIFICATION MESSAGE END */

/* FOR SWEET ALERT NOTIFICATION MESSAGE */
define('SWEET_ALERT_WARNING', 'warning');
define('SWEET_ALERT_INFO', 'info');
define('SWEET_ALERT_SUCCESS', 'success');
define('SWEET_ALERT_ERROR', 'error');
define('SWEET_ALERT_CONFIRMATION_COLOR', '#DD6B55');
define('SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT', 'Yes');
define('SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT', 'No');
define('CONFIRMATION_MESSAGE_FOR_STATUS_TITLE', 'Are you sure want change status?');
#define('CONFIRMATION_MESSAGE_FOR_STATUS_UPDATE_TITLE', 'Are you sure to change \n #OLD_STATUS# \n Status To #NEW_STATUS# ?');
define('CONFIRMATION_MESSAGE_FOR_STATUS_UPDATE_TITLE', 'Do you want to change #ORDER_NO# to #NEW_STATUS# ?');
define('CONFIRMATION_MESSAGE_FOR_PRODUCT_STATUS_UPDATE_TITLE', 'Are you sure to change \n #OLD_STATUS# \n Status To #NEW_STATUS# ?');
#define('CONFIRMATION_MESSAGE_FOR_AVAILABLE_STATUS_UPDATE_TITLE', 'Are you sure to change \n #OLD_AVAILABLE# \n Status To #NEW_AVAILABLE# ?');
define('CONFIRMATION_MESSAGE_FOR_AVAILABLE_STATUS_UPDATE_TITLE', 'Do you want to change #PRODUCT_NAME# to #NEW_AVAILABLE# ?');
define('CONFIRMATION_MESSAGE_FOR_STATUS_TEXT', '');
define('CONFIRMATION_MESSAGE_FOR_DELETE_TITLE', 'Are you sure want delete it?');
define('CONFIRMATION_MESSAGE_FOR_DUPLICATE_TITLE', 'Are you sure want duplicate it?');
define('CONFIRMATION_MESSAGE_FOR_DELETE_TEXT', '');
define('CONFIRMATION_MESSAGE_FOR_SET_DEFAULT', 'Are you sure want set as default contact?');
/* FOR SWEET ALERT NOTIFICATION MESSAGE END */

/* HTML CLASS Start */
define('COLOR_S', 'S');
define('COLOR_E', 'E');
define('COLOR_G', 'G');
define('COLOR_I', 'I');
define('COLOR_W', 'W');
define('COLOR_P', 'P');
define('COLOR_D', 'D');
/* HTML CLASS End */



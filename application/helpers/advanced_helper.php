<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function change_filename($path, $name)
{

    $actual_name = pathinfo($name, PATHINFO_FILENAME);
    $original_name = $actual_name;
    $extension = pathinfo($name, PATHINFO_EXTENSION);
    $i = 1;

    while (file_exists($path . $actual_name . "." . $extension)) {
        $actual_name = (string)$original_name . $i;
        $name = $actual_name . "." . $extension;
        $i++;
    }
    return $name;

}

function bill_date_diff($date1, $date2)
{
    $date1 = date_create($date1);
    $date2 = date_create($date2);
    $diff = date_diff($date1, $date2);
    return ((int)$diff->format('%a')) + 1;
}

function edit_display($fieldname, $array, $key = '', $default = '')
{
    if ($key == '') {
        $key = $fieldname;
    }
    if (isset($_REQUEST[$fieldname])) {
        return $_REQUEST[$fieldname];
    } else {
        if (is_object($array)) {
            if (isset($array->$key)) {
                return stripslashes($array->$key);
            } else {
                return $default;
            }
        } else {
            if (isset($array[$key])) {

                return $array[$key];
            } else {
                return $default;
            }
        }


    }
}

function edit_mdisplay($fieldname, $array, $key = '', $default = '')
{
    if ($key == '') {
        $key = $fieldname;
    }
    if (isset($_REQUEST[$fieldname])) {
        return $_REQUEST[$fieldname];
    } else {
        if (isset($array[0]->$key)) {
            foreach ($array as $key1 => $value1) {
                $data[] = $value1->$fieldname;
            }
            return $data;
        } else {
            return $default;
        }
    }
}

function form_field($caption, $input, $required = '')
{
    ?>
    <div class="contentrow">
        <div class="datafield">
            <div class="fieldlable"><b><?php echo $caption ?></b>
                <?php if ($required != '') {
                    ?>
                    <span class="f_req">*</span>
                <?php
                } ?>
            </div>
            <div class="fieldcontrol">

                <?php

                echo $input;
                ?>

            </div>
            <div class="fieldrightlable"></div>
        </div>
    </div>

<?php
}


/* image_resize  */

function mix_upload($name, $path, $type = '')
{
    $filename = '';

    if (isset($_FILES[$name]['tmp_name']) && $_FILES[$name]['tmp_name'] != '') {

        /*
            File Type Audio
        */

        if ($type == 'audio') {
            if ($_FILES[$name]['type'] == 'audio/mpeg' || $_FILES[$name]['type'] == 'audio/mpg' || $_FILES[$name]['type'] == 'audio/mpeg3' || $_FILES[$name]['type'] == 'audio/mp3') {// Mime Type Audio
                $ext = pathinfo($_FILES[$name]['name']);
                $seoname = seo_url($ext['filename']);
                $seo_finalname = $seoname . '.' . $ext['extension'];
                $filename = $seo_finalname;
                if (move_uploaded_file($_FILES[$name]['tmp_name'], $path . $filename)) {
                    return $filename;
                }
            }
        }
        /*
            File Type Video
        */

        if ($type == 'video') {

            if ($_FILES[$name]['type'] == 'video/x-ms-wmv' || $_FILES[$name]['type'] == 'video/mpeg' || $_FILES[$name]['type'] == 'video/quicktime' || $_FILES[$name]['type'] == 'video/x-msvideo' || $_FILES[$name]['type'] == 'video/x-sgi-movie') {// Mime Type Video
                $ext = pathinfo($_FILES[$name]['name']);
                $seoname = seo_url($ext['filename']);
                $seo_finalname = $seoname . '.' . $ext['extension'];
                $filename = time() . $seo_finalname;
                if (move_uploaded_file($_FILES[$name]['tmp_name'], $path . $filename)) {
                    return $filename;
                }
            }
        } else {
            $ext = pathinfo($_FILES[$name]['name']);
            $seoname = seo_url($ext['filename']);
            $seo_finalname = $seoname . '.' . $ext['extension'];
            $filename = $seo_finalname;
            if (move_uploaded_file($_FILES[$name]['tmp_name'], $path . $filename)) {
                return $filename;
            }

        }


    }
}


function mix_upload_multiple($name, $path, $type = '')
{
    $filename = '';
    echo $name;
    if (isset($_FILES[$name]['tmp_name']) && $_FILES[$name]['tmp_name'] != '') {

        /*
            File Type Audio
        */
        $name = array();
        if ($type == 'audio') {

            $imagesize = sizeof($_FILES['audio']['tmp_name']);
            for ($i = 0; $i < $imagesize; $i++) {
                //if($_FILES[$name]['type'][$i]=='audio/mpeg' )
                //{// Mime Type Audio
                $ext = pathinfo($_FILES[$name]['name']);
                $seoname = seo_url($ext['filename']);
                $seo_finalname = $seoname . '.' . $ext['extension'];
                $filename = $seo_finalname;
                if (move_uploaded_file($_FILES[$name]['tmp_name'], $path . $filename)) {
                    //return $filename;

                    array_push($name, $filename);
                }
                //}
            }
        }

        /*
            File Type Video
        */

        if ($type == 'video') {
            for ($i = 0; $i < $size; $i++) {
                if ($_FILES[$name]['type'][$i] == 'video/x-ms-wmv' || $_FILES[$name]['type'][$i] == 'video/mpeg' || $_FILES[$name]['type'][$i] == 'video/quicktime' || $_FILES[$name]['type'][$i] == 'video/x-msvideo' || $_FILES[$name]['type'][$i] == 'video/x-sgi-movie') {// Mime Type Video
                    $ext = pathinfo($_FILES[$name]['name'][$i]);
                    $seoname = seo_url($ext['filename']);
                    $seo_finalname = $seoname . '.' . $ext['extension'];
                    $filename = $seo_finalname;
                    if (move_uploaded_file($_FILES[$name]['tmp_name'][$i], $path . $filename)) {
                        array_push($name, $filename);
                    }
                }
            }
        }
        var_dump($_FILES);
        die();
        if (!empty($name)) {
            return implode(",", $name);
        }


    }
}


function image_resize_product_multiple($name, $path, $totalimage, $oldimage = '', $ratio = "FALSE", $original = "full", $seonewname = '')
{

    $name = explode("_", $name);
    $position = $name[1];
    $name = $name[0];

    $filename = '';
    $CI =& get_instance();
    if (isset($_FILES[$name]['tmp_name'][$position]) && $_FILES[$name]['tmp_name'][$position] != '') {

        //base64_to_jpeg($_POST['baseimage'],category_image.'/full/'.$newimage);

        if ($_FILES[$name]['type'][$position] == 'image/jpeg' || $_FILES[$name]['type'][$position] == 'image/jpeg' || $_FILES[$name]['type'][$position] == 'image/gif' || $_FILES[$name]['type'][$position] == 'image/png' || $_FILES[$name]['type'][$position] == 'image/bmp') {
            //$newimage=time().$_FILES[$name]['name'];
            //$filename=$newimage;

            //$prefix=array('diamond-jewellery-','gold-jewellery-','diamond-','gold-');

            //$prefixtaken=array_rand($prefix);

            $ext = pathinfo($_FILES[$name]['name'][$position]);
            //	$seoname=seo_url($ext['filename']);
            $seoname = seo_url($ext['filename']);
            if ($seonewname != '') {
                $seoname = seo_url($seonewname);
            }
            //	$seo_finalname=$seoname.time().'.'.$ext['extension'];

            $seo_finalname = $seoname . '.' . $ext['extension'];
            $filename = $seo_finalname;


            /*
                    File Name new try by Dr. Anil
            */


            $filename = change_filename($path . $original . '/', $seo_finalname);
            /*

            */


            if (move_uploaded_file($_FILES[$name]['tmp_name'][$position], $path . $original . '/' . $filename)) {
                foreach ($totalimage as $key => $val) {
                    $size = explode(',', $val);

                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $path . $original . '/' . $filename,
                        'new_image' => $path . $key . '/' . $filename,
                        'create_thumb' => FALSE,
                        'maintain_ratio' => $ratio,
                        'width' => $size[0],
                        'height' => $size[1]
                    );
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();
                    $CI->image_lib->clear();
                    unset($config);

                }
            }//if(move_upl

        }//if($_FILES[$name]['type']=

        if ($oldimage != '') {
            if (file_exists($path . $original . "/" . $oldimage)) {
                @unlink($path . $original . "/" . $oldimage);
            }

            foreach ($totalimage as $key => $val) {
                if (file_exists($path . $key . "/" . $oldimage)) {
                    //	echo $path.$key."/".$oldimage;
                    @unlink($path . $key . "/" . $oldimage);
                }
            }
        }

    }//if(isset($_FILES
    if ($filename == '' && $oldimage != '') {
        return $oldimage;
    } else {
        return $filename;
    }
}


function image_resize($name, $path, $totalimage, $oldimage = '', $ratio = "FALSE", $original = "full", $seonewname = '')
{


    $filename = '';
    $CI =& get_instance();
    if (isset($_FILES[$name]['tmp_name']) && $_FILES[$name]['tmp_name'] != '') {

        //base64_to_jpeg($_POST['baseimage'],category_image.'/full/'.$newimage);

        if ($_FILES[$name]['type'] == 'image/jpeg' || $_FILES[$name]['type'] == 'image/jpeg' || $_FILES[$name]['type'] == 'image/gif' || $_FILES[$name]['type'] == 'image/png' || $_FILES[$name]['type'] == 'image/bmp') {
            //$newimage=time().$_FILES[$name]['name'];
            //$filename=$newimage;
            $ext = pathinfo($_FILES[$name]['name']);
            //	$seoname=seo_url($ext['filename']);
            $seoname = seo_url($ext['filename']);
            if ($seonewname != '') {
                $seoname = seo_url($seonewname);
            }

            //	$seo_finalname=$seoname.time().'.'.$ext['extension'];

            $seo_finalname = $seoname . '.' . $ext['extension'];
            $filename = $seo_finalname;


            /*
                    File Name new try by Dr. Anil
            */


            $filename = change_filename($path . $original . '/', $seo_finalname);
            /*

            */


            if (move_uploaded_file($_FILES[$name]['tmp_name'], $path . $original . '/' . $filename)) {
                $imageInfo = getimagesize($path . $original . '/' . $filename);
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $path . $original . '/' . $filename,
                    'new_image' => $path . $original . '/' . $filename,
                    'create_thumb' => FALSE,
                    'maintain_ratio' => $ratio,
                    'width' => $imageInfo[0] - 1,
                    'height' => $imageInfo[1]
                );
                $CI->image_lib->initialize($config);
                $CI->image_lib->resize();
                $CI->image_lib->clear();
                unset($config);

                foreach ($totalimage as $key => $val) {
                    $size = explode(',', $val);
                    if ($ratio == 'true') {

                        $imageInfo = getimagesize($path . $original . '/' . $filename);
                        if ($imageInfo[0] > $size[0]) {

                            $config = array(
                                'image_library' => 'gd2',
                                'source_image' => $path . $original . '/' . $filename,
                                'new_image' => $path . $key . '/' . $filename,
                                'create_thumb' => FALSE,
                                'maintain_ratio' => true,
                                'width' => $size[0]
                            );
                            $CI->image_lib->initialize($config);
                            $CI->image_lib->resize();
                            $CI->image_lib->clear();
                            unset($config);
                        } else {

                            if ($filename != '')
                                copy($path . $original . '/' . $filename, $path . $key . '/' . $filename);
                        }
                    } else {
                        $config = array(
                            'image_library' => 'gd2',
                            'source_image' => $path . $original . '/' . $filename,
                            'new_image' => $path . $key . '/' . $filename,
                            'create_thumb' => FALSE,
                            'maintain_ratio' => $ratio,
                            'width' => $size[0],
                            'height' => $size[1]
                        );
                        $CI->image_lib->initialize($config);
                        $CI->image_lib->resize();
                        $CI->image_lib->clear();
                        unset($config);
                    }


                }
            }//if(move_upl

        }//if($_FILES[$name]['type']=

        if ($oldimage != '') {
            if (file_exists($path . $original . "/" . $oldimage)) {
                @unlink($path . $original . "/" . $oldimage);
            }

            foreach ($totalimage as $key => $val) {
                if (file_exists($path . $key . "/" . $oldimage)) {
                    //	echo $path.$key."/".$oldimage;
                    @unlink($path . $key . "/" . $oldimage);
                }
            }
        }

    }//if(isset($_FILES
    if ($filename == '' && $oldimage != '') {
        return $oldimage;
    } else {
        return $filename;
    }
}

/* image_resize  */
function image_processing($name, $path, $totalimage, $original = "full", $ratio = "FALSE")
{
    if ($name == '')
        return;
    //var_dump(func_get_args());
    $CI =& get_instance();
    foreach ($totalimage as $key => $val) {
        //echo $path.$original.'/'.$name;
        if (file_exists($path . $original . '/' . $name)) {
            if (file_exists($path . $key . '/' . $name)) {
                @unlink($path . $key . '/' . $name);
            }
            $size = explode(',', $val);
            if ($ratio == 'true') {

                $imageInfo = getimagesize($path . $original . '/' . $name);
//							var_dump($imageInfo);
                if ($imageInfo[0] > $size[0]) {

                    //	echo "fullimage".$imageInfo[0].' resige to '.$size[0].' '.$name."<br>";
                    //echo $size[0];
                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $path . $original . '/' . $name,
                        'new_image' => $path . $key . '/' . $name,
                        'create_thumb' => FALSE,
                        'maintain_ratio' => true,
                        'width' => $size[0]
                    );
                } else {
                    //	echo "origional :".$path.$original.'/'.$name,$path.$key.'/'.$name."<br>";
                    //		unset($config);
                    if ($name != '')
                        copy($path . $original . '/' . $name, $path . $key . '/' . $name);
                }

                //	die();

            } else {
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $path . $original . '/' . $name,
                    'new_image' => $path . $key . '/' . $name,
                    'create_thumb' => FALSE,
                    'maintain_ratio' => $ratio,
                    'width' => $size[0],
                    'height' => $size[1]
                );

            }
            if (!empty($config)) {
                $CI->image_lib->initialize($config);
                $CI->image_lib->resize();
                $CI->image_lib->clear();
                unset($config);
            }
        }
    }
}


function image_resize_multiple($name, $path, $totalimage, $oldimages = '', $keep_images = '', $ratio = "FALSE", $original = "full")
{
    $filename = '';
    $array = array();
    $CI =& get_instance();
    if (isset($_FILES[$name]['tmp_name']) && $_FILES[$name]['tmp_name'] != '') {


        //base64_to_jpeg($_POST['baseimage'],category_image.'/full/'.$newimage);
        $imagesize = count($_FILES[$name]['tmp_name']);
        for ($i = 0; $i < $imagesize; $i++) {
            if ($_FILES[$name]['type'][$i] == 'image/jpeg' || $_FILES[$name]['type'][$i] == 'image/jpeg' || $_FILES[$name]['type'][$i] == 'image/gif' || $_FILES[$name]['type'][$i] == 'image/png' || $_FILES[$name]['type'][$i] == 'image/bmp') {
                $filename = time() . $_FILES[$name]['name'][$i];

                $array[] = $filename;

                if (move_uploaded_file($_FILES[$name]['tmp_name'][$i], $path . $original . '/' . $filename)) {


                    $imageInfo = getimagesize($path . $original . '/' . $filename);
                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $path . $original . '/' . $filename,
                        'new_image' => $path . $original . '/' . $filename,
                        'create_thumb' => FALSE,
                        'maintain_ratio' => $ratio,
                        'width' => $imageInfo[0] - 1,
                        'height' => $imageInfo[1]
                    );
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();
                    $CI->image_lib->clear();
                    unset($config);


                    foreach ($totalimage as $key => $val) {
                        $size = explode(',', $val);

                        $config = array(
                            'image_library' => 'gd2',
                            'source_image' => $path . $original . '/' . $filename,
                            'new_image' => $path . $key . '/' . $filename,
                            'create_thumb' => FALSE,
                            'maintain_ratio' => $ratio,
                            'width' => $size[0],
                            'height' => $size[1]
                        );
                        $CI->image_lib->initialize($config);
                        $CI->image_lib->resize();
                        $CI->image_lib->clear();
                        unset($config);

                    }
                }
            }
        }
    }
    if ($oldimages == '') {
        return implode(",", $array);
    } else {
        if (isset($_REQUEST[$keep_images])) {
            $oldimagesarray = array();
            foreach ($_REQUEST[$keep_images] as $row) {
                $array[] = $row;
                if ($oldimages != '') {
                    $oldimagesarray = explode(",", $oldimages);
                }
            }
            foreach ($oldimagesarray as $row) {
                if (!in_array($row, $_REQUEST[$keep_images])) {

                    /*
                        Delete Those Images
                    */
                    if (file_exists($path . $original . "/" . $row)) {
                        //		@unlink($path.$original."/".$row);
                    }

                    foreach ($totalimage as $key => $val) {
                        if (file_exists($path . $key . "/" . $row)) {
                            //		@unlink($path.$key."/".$row);
                        }
                    }


                }
            }
        } else {
            $oldimagesarray = explode(",", $oldimages);
            if (!empty($oldimages)) {
                foreach ($oldimagesarray as $row) {
                    if (file_exists($path . $original . "/" . $row)) {
                        //				@unlink($path.$original."/".$row);
                    }

                    foreach ($totalimage as $key => $val) {
                        if (file_exists($path . $key . "/" . $row)) {
                            //						@unlink($path.$key."/".$row);
                        }
                    }
                }
            }
        }
        if (!empty($array)) {
            return implode(",", $array);
        }

    }//else
}


/* image_resize multiple  */


function getTempName($name, $arrayindex = '')
{
    $array = array();

    if (is_numeric($arrayindex)) {
        $array['tmp_name'] = $_FILES[$name]['tmp_name'][$arrayindex];
        $array['name'] = $_FILES[$name]['name'][$arrayindex];
        $array['file_type'] = $_FILES[$name]['type'][$arrayindex];

    } else {
        $array['tmp_name'] = $_FILES[$name]['tmp_name'];
        $array['file_type'] = $_FILES[$name]['type'];
        $array['name'] = $_FILES[$name]['name'];
    }
    return $array;
}

function mix_media_upload($name, $path, $original = "full", $arrayindex = '')
{
    $filename = '';
    $CI =& get_instance();

    $fileinfo = getTempName($name, $arrayindex);
    $type = $fileinfo['file_type'];
    $tmp_name = $fileinfo['tmp_name'];
    $name = $fileinfo['name'];

    if (isset($tmp_name) && $tmp_name != '') {
        $ext = pathinfo($name);
        //	$seoname=seo_url($ext['filename']);
        $seoname = seo_url($ext['filename']);


        $seo_finalname = $seoname . '.' . $ext['extension'];
        $filename = $seo_finalname;


        $filename = change_filename($path . $original . '/', $seo_finalname);
        /*

        */

        if (move_uploaded_file($tmp_name, $path . $original . '/' . $filename)) {
            return $filename;
        } else {
            return '';
        }

    }

}

function image_resize_new($name, $path, $totalimage, $oldimage = '', $ratio = "FALSE", $original = "full", $seonewname = '', $arrayindex = '')
{
    $filename = '';
    $CI =& get_instance();

    $imageInfo = getTempName($name, $arrayindex);

    $type = $imageInfo['file_type'];
    $tmp_name = $imageInfo['tmp_name'];
    $name = $imageInfo['name'];


    if (isset($tmp_name) && $tmp_name != '') {

        //base64_to_jpeg($_POST['baseimage'],category_image.'/full/'.$newimage);

        if ($type == 'image/jpeg' || $type == 'image/jpeg' || $type == 'image/gif' || $type == 'image/png' || $type == 'image/bmp') {
            //$newimage=time().$_FILES[$name]['name'];
            //$filename=$newimage;
            $ext = pathinfo($name);
            //	$seoname=seo_url($ext['filename']);
            $seoname = seo_url($ext['filename']);
            if ($seonewname != '') {
                $seoname = seo_url($seonewname);
            }

            //	$seo_finalname=$seoname.time().'.'.$ext['extension'];

            $seo_finalname = $seoname . '.' . $ext['extension'];
            $filename = $seo_finalname;


            /*
                    File Name new try by Dr. Anil
            */


            $filename = change_filename($path . $original . '/', $seo_finalname);
            /*

            */


            if (move_uploaded_file($tmp_name, $path . $original . '/' . $filename)) {
                $imageInfo = getimagesize($path . $original . '/' . $filename);
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $path . $original . '/' . $filename,
                    'new_image' => $path . $original . '/' . $filename,
                    'create_thumb' => FALSE,
                    'maintain_ratio' => $ratio,
                    'width' => $imageInfo[0] - 1,
                    'height' => $imageInfo[1]
                );
                $CI->image_lib->initialize($config);
                $CI->image_lib->resize();
                $CI->image_lib->clear();
                unset($config);


                foreach ($totalimage as $key => $val) {
                    $size = explode(',', $val);

                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $path . $original . '/' . $filename,
                        'new_image' => $path . $key . '/' . $filename,
                        'create_thumb' => FALSE,
                        'maintain_ratio' => $ratio,
                        'width' => $size[0],
                        'height' => $size[1]
                    );
                    $CI->image_lib->initialize($config);
                    $CI->image_lib->resize();
                    $CI->image_lib->clear();
                    unset($config);

                }
            }//if(move_upl

        }//if($_FILES[$name]['type']=

        if ($oldimage != '') {
            if (file_exists($path . $original . "/" . $oldimage)) {
                @unlink($path . $original . "/" . $oldimage);
            }

            foreach ($totalimage as $key => $val) {
                if (file_exists($path . $key . "/" . $oldimage)) {
                    //	echo $path.$key."/".$oldimage;
                    @unlink($path . $key . "/" . $oldimage);
                }
            }
        }

    }//if(isset($_FILES
    if ($filename == '' && $oldimage != '') {
        return $oldimage;
    } else {
        return $filename;
    }
}

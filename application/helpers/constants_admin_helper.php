<?php
/***
* ROUTE
 *  D : For Directory
 *  C : For Controller
 *  M : For Model
 *  V : For View (Do not add slash)
 **/

/* Define Directory*/
if (!defined('ROOT_DIR_ADMIN')) define('ROOT_DIR_ADMIN', 'admin/');
if (!defined('ADMIN_D')) define('ADMIN_D', 'admin/');
if (!defined('HOME_D')) define('HOME_D', 'home/');
if (!defined('CUSTOMER_D')) define('CUSTOMER_D', 'customer/');
if (!defined('COUNTRY')) define('COUNTRY', 'country/');
if (!defined('CATEGORY')) define('CATEGORY', 'category/');
if (!defined('SHOP')) define('SHOP', 'shop/');
if (!defined('CITY')) define('CITY', 'city/');
if (!defined('PRODUCT')) define('PRODUCT', 'product/');
if (!defined('BANNER')) define('BANNER', 'banner/');
if (!defined('LABEL')) define('LABEL', 'label/');
if (!defined('CUSTOMER')) define('CUSTOMER', 'customer/');
if (!defined('ORDER')) define('ORDER', 'order/');
if (!defined('PROMOTION')) define('PROMOTION', 'promotion/');
if (!defined('CURRENCY')) define('CURRENCY', 'currency/');

if (!defined('SETTING_D')) define('SETTING_D', 'setting/');
if (!defined('PERMISSION_D')) define('PERMISSION_D', 'permission/');
if (!defined('EMAIL_TEMPLATE_D')) define('EMAIL_TEMPLATE_D', 'email_template/');

/* Define Controller */

if (!defined('ADMIN_C')) define('ADMIN_C', 'admin/');
if (!defined('HOME_C')) define('HOME_C', 'home/');
if (!defined('CUSTOMER_C')) define('CUSTOMER_C', 'customer/');
if (!defined('COUNTRY_C')) define('COUNTRY_C', 'country/');
if (!defined('DASHBOARD_C')) define('DASHBOARD_C', 'dashboard/');
if (!defined('SETTING_C')) define('SETTING_C', 'setting/');
if (!defined('PERMISSION_C')) define('PERMISSION_C', 'permission/');
if (!defined('EMAIL_TEMPLATE_C')) define('EMAIL_TEMPLATE_C', 'email_template/');

/* Home Directory Views */
if (!defined('LOGIN_V')) define('LOGIN_V','login_view');
if (!defined('FORGOT_PASSWORD_V')) define('FORGOT_PASSWORD_V','forgot_password_view');
if (!defined('DASHBOARD_V')) define('DASHBOARD_V','dashboard');
if (!defined('ACCESS_DENIED_V')) define('ACCESS_DENIED_V', 'view_access_denied');
if (!defined('DAY_OUT')) define('DAY_OUT', 'day_out');

/* Admin Directory Views */
if (!defined('LIST_ADMIN')) define('LIST_ADMIN', 'list_admin');
if (!defined('CRUD_ADMIN')) define('CRUD_ADMIN', 'crud_admin');
if (!defined('CHANGE_PASSWORD')) define('CHANGE_PASSWORD','change_password');
if (!defined('CHANGE_ADMIN_MANAGER_PASSWORD')) define('CHANGE_ADMIN_MANAGER_PASSWORD','change_admin_manager_password');
if (!defined('UPDATE_ADMIN')) define('UPDATE_ADMIN', 'update_admin');

/* Customer Directory Views */
if (!defined('LIST_CUSTOMER')) define('LIST_CUSTOMER', 'list_customer');
if (!defined('CRUD_CUSTOMER')) define('CRUD_CUSTOMER', 'crud_customer');
if (!defined('VIEW_CUSTOMER')) define('VIEW_CUSTOMER', 'view_customer');

/* Customer Contact Directory Views */
if (!defined('LIST_CUSTOMER_CONTACT')) define('LIST_CUSTOMER_CONTACT', 'list_customer_contact');
if (!defined('CRUD_CUSTOMER_CONTACT')) define('CRUD_CUSTOMER_CONTACT', 'crud_customer_contact');

/* Customer Services Directory Views */
if (!defined('LIST_CUSTOMER_SERVICE')) define('LIST_CUSTOMER_SERVICE', 'list_customer_service');
if (!defined('CRUD_CUSTOMER_SERVICE')) define('CRUD_CUSTOMER_SERVICE', 'crud_customer_service');

/* Country Directory Views */
if (!defined('LIST_COUNTRY')) define('LIST_COUNTRY', 'list_country');
if (!defined('CRUD_COUNTRY')) define('CRUD_COUNTRY', 'crud_country');

/* Category Directory Views */
if (!defined('LIST_CATEGORY')) define('LIST_CATEGORY', 'list_category');
if (!defined('CRUD_CATEGORY')) define('CRUD_CATEGORY', 'crud_category');

/* Product Directory Views */
if (!defined('LIST_PRODUCT')) define('LIST_PRODUCT', 'list_product');
if (!defined('CRUD_PRODUCT')) define('CRUD_PRODUCT', 'crud_product');

/* Banner Directory Views */
if (!defined('LIST_BANNER')) define('LIST_BANNER', 'list_banner');
if (!defined('CRUD_BANNER')) define('CRUD_BANNER', 'crud_banner');

/* Shop Directory Views */
if (!defined('LIST_SHOP')) define('LIST_SHOP', 'list_shop');
if (!defined('CRUD_SHOP')) define('CRUD_SHOP', 'crud_shop');

/* City Directory Views */
if (!defined('LIST_CITY')) define('LIST_CITY', 'list_city');
if (!defined('CRUD_CITY')) define('CRUD_CITY', 'crud_city');

/* Label Directory Views */
if (!defined('LIST_LABEL')) define('LIST_LABEL', 'list_label');
if (!defined('CRUD_LABEL')) define('CRUD_LABEL', 'crud_label');

/* Order Directory Views */
if (!defined('LIST_ORDER')) define('LIST_ORDER', 'list_order');
if (!defined('CRUD_ORDER')) define('CRUD_ORDER', 'crud_order');
if (!defined('VIEW_ORDER')) define('VIEW_ORDER', 'view_order');

/* Promotion Directory Views */
if (!defined('LIST_PROMOTION')) define('LIST_PROMOTION', 'list_promotion');
if (!defined('CRUD_PROMOTION')) define('CRUD_PROMOTION', 'crud_promotion');

/* Currency Directory Views */
if (!defined('LIST_CURRENCY')) define('LIST_CURRENCY', 'list_currency');
if (!defined('CRUD_CURRENCY')) define('CRUD_CURRENCY', 'crud_currency');

/* Permission Directory Views */
if (!defined('CRUD_PERMISSION')) define('CRUD_PERMISSION', 'crud_permission');

/* Email Template Directory Views */
if (!defined('LIST_EMAIL_TEMPLATE')) define('LIST_EMAIL_TEMPLATE', 'list_email_template');
if (!defined('CRUD_EMAIL_TEMPLATE')) define('CRUD_EMAIL_TEMPLATE', 'crud_email_template');
if (!defined('VIEW_EMAIL_TEMPLATE')) define('VIEW_EMAIL_TEMPLATE', 'view_email_template');

/* Setting Directory Views */
if (!defined('LIST_SETTING_GENERAL')) define('LIST_SETTING_GENERAL', 'list_setting_general');
if (!defined('CRUD_SETTING')) define('CRUD_SETTING', 'crud_setting');
/**
* Templating Section
**/
if (!defined('THEME_HEADER_SECTION')) define('THEME_HEADER_SECTION', 'template/admin/_header');
if (!defined('THEME_BREADCRUMB_SECTION')) define('THEME_BREADCRUMB_SECTION', 'template/admin/_breadcrumb');
if (!defined('THEME_LEFT_MENU_SECTION')) define('THEME_LEFT_MENU_SECTION', 'template/admin/_leftmenu');
if (!defined('THEME_RIGHT_MENU_SECTION')) define('THEME_RIGHT_MENU_SECTION', 'template/admin/_rightmenu');
if (!defined('THEME_FOOTER_SECTION')) define('THEME_FOOTER_SECTION', 'template/admin/_footer');
if (!defined('THEME_CSS_SECTION')) define('THEME_CSS_SECTION', 'template/admin/_css');
if (!defined('THEME_JS_SECTION')) define('THEME_JS_SECTION', 'template/admin/_js');

/**
* Message Section For Header, Flash, Notification, Pages.
*
*/

if (!defined('ADMIN_PAGE_TITLE')) define('ADMIN_PAGE_TITLE', 'Login :: Admin Panel');
if (!defined('LOGIN_PAGE_TITLE_ONE')) define('LOGIN_PAGE_TITLE_ONE', 'Familov');
if (!defined('LOGIN_PAGE_TITLE_TWO')) define('LOGIN_PAGE_TITLE_TWO', 'Admin Panel');
if (!defined('LOGIN_PAGE_SUB_TITLE')) define('LOGIN_PAGE_SUB_TITLE', 'Welcome !');
if (!defined('COMPANY_NAME_ONE')) define('COMPANY_NAME_ONE', 'Familov');
if (!defined('COMPANY_NAME_TWO')) define('COMPANY_NAME_TWO', '.com');
if (!defined('COMPANY_NAME_SHORT_NAME')) define('COMPANY_NAME_SHORT_NAME', 'F');
if (!defined('COPYRIGHT_TEXT')) define('COPYRIGHT_TEXT', 'Copyright © 2017');
if (!defined('VERSION_TEXT')) define('VERSION_TEXT', 'Version 2.0');
if (!defined('ADMIN_PANEL_FOOTER_ONE')) define('ADMIN_PANEL_FOOTER_ONE', 'Backend Brains');
if (!defined('ADMIN_PANEL_FOOTER_ONE_URL')) define('ADMIN_PANEL_FOOTER_ONE_URL', 'http://backendbrains.com');

if (!defined('EMAIL_HEADER_TITLE')) define('EMAIL_HEADER_TITLE', 'Familov');
# Global Section
if (!defined('MSG_ADD_SUCCESS')) define('MSG_ADD_SUCCESS', 'New Record Added Successfully !');
if (!defined('MSG_ADD_ERROR')) define('MSG_ADD_ERROR', 'New Record Added Failed !');
if (!defined('MSG_UPDATE_SUCCESS')) define('MSG_UPDATE_SUCCESS', 'Record Updated Successfully !');
if (!defined('MSG_UPDATE_ERROR')) define('MSG_UPDATE_ERROR', 'Record Update Failed !');
# Login Section
if (!defined('MSG_LOGIN_INVALID')) define('MSG_LOGIN_INVALID', '<strong>Access Denied !</strong><br> Invalid Username/Password');
if (!defined('MSG_FORGOT_PASSWORD_INVALID')) define('MSG_FORGOT_PASSWORD_INVALID', '<strong>Access Denied !</strong> Username/Email not exist.');
if (!defined('MSG_FORGOT_PASSWORD_SUCCESS')) define('MSG_FORGOT_PASSWORD_SUCCESS', 'We received your forgot password request.<br> We will send you mail on {EMAIL} with new password.');

if (!defined('MSG_ADMIN_LOGIN_SUCCESS')) define('MSG_ADMIN_LOGIN_SUCCESS', 'Login Successfully !');
if (!defined('MSG_ADMIN_LOGOUT_SUCCESS')) define('MSG_ADMIN_LOGOUT_SUCCESS', 'Logout Successfully !');
if (!defined('MSG_LOGOUT_EMPLOYEE_ERROR')) define('MSG_LOGOUT_EMPLOYEE_ERROR', 'Please close all the requests before the logout.');
if (!defined('MSG_DAY_OUT_EMPLOYEE_ERROR')) define('MSG_DAY_OUT_EMPLOYEE_ERROR', 'Please close all the requests before the day out.');
if (!defined('MSG_DAY_END_EMPLOYEE_SUCESS')) define('MSG_DAY_END_EMPLOYEE_SUCESS', 'Employee Day End Sucessfully.');
if (!defined('MSG_LOGIN_STATUS_INACTIVE')) define('MSG_LOGIN_STATUS_INACTIVE', '<strong>Access Denied !</strong><br> Your account is blocked. Please contact us for more info ' . @MAIL_HELLO );

#Admin Section
if (!defined('MSG_ADMIN_ADD_SUCCESS')) define('MSG_ADMIN_ADD_SUCCESS', 'New Admin Added Successfully !');
if (!defined('MSG_ADMIN_ADD_ERROR')) define('MSG_ADMIN_ADD_ERROR', 'New Admin Added Failed !');
if (!defined('MSG_ADMIN_UPDATE_SUCCESS')) define('MSG_ADMIN_UPDATE_SUCCESS', 'Admin Updated Successfully !');
if (!defined('MSG_ADMIN_UPDATE_ERROR')) define('MSG_ADMIN_UPDATE_ERROR', 'Admin Update Failed !');
if (!defined('MSG_ADMIN_PASSWORD_UPDATE_SUCCESS')) define('MSG_ADMIN_PASSWORD_UPDATE_SUCCESS', 'Password Updated Successfully !');
if (!defined('MSG_ADMIN_PASSWORD_UPDATE_ERROR')) define('MSG_ADMIN_PASSWORD_UPDATE_ERROR', 'Change Password Failed !');
if (!defined('MSG_ADMIN_PASSWORD_NOT_MATCH_ERROR')) define('MSG_ADMIN_PASSWORD_NOT_MATCH_ERROR', 'Current Password is invalid !');


/**
 * Database And section Status List.
 *
 */
#Table : admin
if (!defined('ADMIN_STATUS_ACTIVE')) define('ADMIN_STATUS_ACTIVE', 'Active');
if (!defined('ADMIN_STATUS_PENDING')) define('ADMIN_STATUS_PENDING', 'Pending');
if (!defined('ADMIN_STATUS_BLOCK')) define('ADMIN_STATUS_BLOCK', 'Block');
if (!defined('ADMIN_STATUS_INACTIVE')) define('ADMIN_STATUS_INACTIVE', 'Inactive');
if (!defined('ADMIN_STATUS_UPDATE_SUCCESS')) define('ADMIN_STATUS_UPDATE_SUCCESS', 'Status Updated Successfully !');
if (!defined('DELETE_USER')) define('DELETE_USER', 'Delete');
if (!defined('START')) define('START', 'Active');
if (!defined('STOP')) define('STOP', 'Inactive');
if (!defined('DUPLICATE_ENTRY')) define('DUPLICATE_ENTRY', 'Duplicate');

/* FOR STATUS OPERATION MESSAGE */
define('DATA_TABLE_ERROR_STATUS_UPDATE', 'Status Update Failed !');
define('DATA_TABLE_SUCCESS_STATUS_UPDATE_ACTIVE', 'Status Activated Successfully.');
define('DATA_TABLE_SUCCESS_STATUS_UPDATE_INACTIVE', 'Status Inactivated Successfully.');
define('DATA_TABLE_SUCCESS_STATUS_BLOCK', 'Status Blocked Successfully.');
define('MESSAGE_LAST_OPERATION_DISCARD', 'Discard Last Operation.');
define('MESSAGE_INFORMATION_NOT_AVAILABLE', 'Information Not Available.');
define('MESSAGE_DATA_AVAILABLE', 'No Data Available.');
/* FOR STATUS OPERATION MESSAGE END */

/* COMMON MASTER DATA SECTION MESSAGE */
define('MESSAGE_ADD_SUCCESS', 'Added Successfully.');
define('MESSAGE_ADD_ERROR', 'Failed To Add.');
define('MESSAGE_UPDATE_SUCCESS', 'Updated Successfully.');
define('MESSAGE_UPDATE_ERROR', 'Failed To Updated.');
define('MESSAGE_DELETE_SUCCESS', 'Deleted Successfully.');
define('MESSAGE_CHANGE_STATUS_SUCCESS', 'Status Change Successfully.');
define('MESSAGE_CHANGE_STATUS_ERROR', 'Failed To Change Status.');
define('MESSAGE_VIEW_ERROR', 'Failed To View Order.');
define('MESSAGE_DELETE_ERROR', 'Failed To Delete.');
define('MESSAGE_DELETE_ERROR_LINK', 'Failed To Delete.User is already map with other section.');
define('MESSAGE_DELETE_DEPARTMENT_ERROR_LINK', 'Failed To Delete, Department is already map with other section.');
define('MESSAGE_DELETE_SERVICE_ERROR_LINK', 'Failed To Delete, Service is already map with other section.');
define('MESSAGE_DELETE_CUSTOMER_SERVICE_ERROR_LINK', 'Failed To Delete, Customer Service is already map with other section.');
define('MESSAGE_ACCESS_DENIED', 'Access denied.');
define('MESSAGE_ACCESS_DENIED_ERROR_MSG', 'Access is denied. You have limited rights on system.');
define('PAYMENT_EUR', 'EUR');
define('PAYMENT_DOLLOR', 'USD');
define('MESSAGE_BILL_UPLOAD_SUCCESS', 'Bill Uploaded Successfully.');

#Table : Customer
if (!defined('CUSTOMER_STATUS_ACTIVE')) define('CUSTOMER_STATUS_ACTIVE', 'Active');
if (!defined('CUSTOMER_STATUS_IN_ACTIVE')) define('CUSTOMER_STATUS_IN_ACTIVE', 'Inactive');

define('MESSAGE_DELETE_SHOP', "You can't delete this shop because this shop has order(s).");
define('MESSAGE_DELETE_PRODUCT', "You can't delete this product because this product has order(s).");
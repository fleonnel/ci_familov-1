<?php
function _ci_profiler($mode = TRUE)
{
    $CI =& get_instance();
    $CI->output->enable_profiler($mode);
}
function _pre($array, $exit = 1)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    if ($exit == 1) {
        exit();
    }
}

function _e($str="Hello ! This is debugging. ", $exit = 1)
{
    echo "<br> " . $str;
    if ($exit == 1) {
        exit;
    }
}
if (!function_exists('admin_url')) {
    function admin_url($url = '')
    {
        return base_url(ROOT_DIR_ADMIN . $url);
    }
}

function _random_string($length = 8)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!_#^*';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    $randomString = str_shuffle($randomString);
    return $randomString;
}

function send_mail($details = array())
{
    //pre($details);die;
    $CI =& get_instance();
    $CI->load->library('email'); // load library
    $config = Array(
        'protocol' => 'mail', //mail, sendmail, smtp
        //'smtp_host' => 'ssl://smtp.gmail.com',
        //'smtp_port' => 465,
        //'smtp_user' => 'aditya.arbsoft@gmail.com',
        //'smtp_pass' => 'P@ssw0rd1234',
        'mailtype' => 'html',
        'charset' => 'utf-8' // utf-8, iso-8859-1
    );
    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");

    $CI->email->from($details['from'], 'Backedn Brains');
    $CI->email->to($details['to']);
    $CI->email->subject($details['subject']);
    $CI->email->message($details['message']);

    if ($CI->email->send()) {
        echo 'Your email was sent, thanks !';
    } else {
        show_error($CI->email->print_debugger());
    }

    return true;
}

if (!function_exists('sendemail')) {
    function sendemail($mail_from, $mail_to, $subject, $body, $cc = '')
    {
        $CI = &get_instance();
        $CI->load->library("email");
        $CI->email->set_mailtype("html");
        $CI->email->from($mail_from, 'RS');
        $CI->email->to($mail_to);
        $CI->email->cc($cc);
        $CI->email->subject($subject);
        $CI->email->message($body);
        $CI->email->send();
        $CI->email->print_debugger();
    }
}

function days_left($date)
{
    $future = strtotime($date);
    $now = time();
    $timeleft = $future - $now;
    return $daysleft = round((($timeleft / 24) / 60) / 60);
}


function day_diff_from_current($date1, $date2)
{
    //return $date1;
    $interval = $date1->diff($date2);
    //return $interval->h;die;

    //return pre($interval);die;
    if ($interval->invert == 1) {
        //return $interval->h;die;
        $diff = ' ';
        if ($interval->y != 0) {
            $diff .= $interval->y . ' Yr ';
        }
        if ($interval->m != 0) {
            $diff .= $interval->m . ' mnth ';
        }
        if ($interval->d != 0) {
            $diff .= $interval->d . ' d ';
        }
        if ($interval->h != 0) {
            $diff .= $interval->h . ' h ';
        }
        if ($interval->i != 0) {
            $diff .= $interval->i . ' m ';
        }
//if($interval->s!=0)
//{
//  $diff.=$interval->s.' sec ';
//}


        return $diff;
    } else {

        $diff = ' ';
        if ($interval->y != 0) {
            $diff .= $interval->y . ' Yr ';
        }
        if ($interval->m != 0) {
            $diff .= $interval->m . ' mnth ';
        }
        if ($interval->d != 0) {
            $diff .= $interval->d . ' d ';
        }
        if ($interval->h != 0) {
            $diff .= $interval->h . ' h ';
        }
        if ($interval->i != 0) {
            $diff .= $interval->i . ' m ';
        }
//if($interval->s!=0)
//{
//  $diff.=$interval->s.' sec ';
//}


        return $diff;
    }
}

function get_perticular_field_value($tablename, $filedname, $where = "")
{
    $CI =& get_instance();
    $str = "select " . $filedname . " from " . $tablename . " where 1=1 " . $where;
    $query = $CI->db->query($str);
    $record = "";
    if ($query->num_rows() > 0) {

        foreach ($query->result_array() as $row) {
            $field_arr = explode(" as ", $filedname);
            if (count($field_arr) > 1) {
                $filedname = $field_arr[1];
            } else {
                $filedname = $field_arr[0];
            }
            $record = $row[$filedname];
        }

    }
    return $record;

}

//  Function to get last  field  value from table
/////////////////////////////////////////////////////////////
function get_last_field_value($tablename, $filedname, $where = "")
{
    $CI =& get_instance();
    $str = "select " . $filedname . " from " . $tablename . " where 1=1 " . $where . " order by id desc limit 1";
    $query = $CI->db->query($str);
    $record = "";
    if ($query->num_rows() > 0) {

        foreach ($query->result_array() as $row) {
            $record = $row[$filedname];
        }

    }
    return $record;

}

/////////////////////////////////////////////////////////////
//  Function to get perticular field from table
/////////////////////////////////////////////////////////////
function get_perticular_count($tablename, $where = "")
{
    $CI =& get_instance();
    $str = "select * from " . $tablename . " where 1=1 " . $where;
    //echo $str;
    $query = $CI->db->query($str);
    $record = $query->num_rows();
    return $record;

}

function millisecsBetween($dateOne, $dateTwo, $abs = true)
{
    $func = $abs ? 'abs' : 'intval';
    return $func(strtotime($dateOne) - strtotime($dateTwo)) * 1000;
}

/////////////////////////////////////////////////////////////
//  Function to DATABASE DATE
/////////////////////////////////////////////////////////////
function database_date($date)
{
    $date_array = explode("/", $date);
    $new_date = $date_array[2] . '-' . $date_array[0] . '-' . $date_array[1];
    return $new_date;
}

/////////////////////////////////////////////////////////////
//  Function to VIEW DATE
/////////////////////////////////////////////////////////////
function view_date($date)
{
    $date_array = explode("-", $date);
    $new_date = $date_array[1] . '/' . $date_array[2] . '/' . $date_array[0];
    return $new_date;
}

///////////////////////create random password///////////////////////////
function get_random_password($chars_min = 6, $chars_max = 8, $use_upper_case = false, $include_numbers = false, $include_special_chars = false)
{
    $length = rand($chars_min, $chars_max);
    $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
    if ($include_numbers) {
        $selection .= "1234567890";
    }
    if ($include_special_chars) {
        $selection .= "!@\"#$%&[]{}?|";
    }

    $password = "";
    for ($i = 0; $i < $length; $i++) {
        $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
        $password .= $current_letter;
    }

    return $password;
}


function CreateNewRefToken($id, $typo)
{
    return strtoupper(($typo == 'Rent' ? 'R' : 'S') . substr(md5($id), 0, 17));
}
/* Directory Management Start */
function deleteNonEmptyDir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (filetype($dir . "/" . $object) == "dir") {
                    deleteNonEmptyDir($dir . "/" . $object);
                } else {
                    unlink($dir . "/" . $object);
                }
            }
        }
        reset($objects);
        rmdir($dir);
    }
}
/* Directory Management End */

    function put_space_after_comma($str) # Space After Comma
    {
        return  preg_replace('/(?<!\d),|,(?!\d{3})/', ', ', $str);
    }

    function db_datev($date){ # Explode Date by day, month and year
        list($day, $month, $year) = @split('[/.-]', $date);
        $added_date="$year-$month-$day";
        return $added_date; 
    }
    function get_last_segment() # get last Segment
    {   
        $CI =& get_instance();
        return $CI->uri->last_segment();    
    }

    function seo_url($string){
    $string = preg_replace("`\[.*\]`U","",$string);
    $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
    $string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
    return strtolower(trim($string, '-'));
    }


    function base64_to_jpeg($base64_string, $output_file) {
    $ifp = fopen($output_file, "wb"); 

    $data = explode(',', $base64_string);

    fwrite($ifp, base64_decode($data[1])); 
    fclose($ifp); 

    return $output_file; 
}

if ( ! function_exists('error_view'))
{
    function error_view($str)//for this application i used start and now not from next application user.by anil suhagiya
    {
        $split_error=explode(".",$str);
        $total=count($split_error);
        if($total>0)
        {
            $i=0;
            foreach($split_error as $row)
            {
                $i++;
                if($total==$i)
                {
                    echo $row;
                }
                else
                {
                    echo $row.'.' ."</br>";
                }
            }
            
        }
    }
}

if ( ! function_exists('encode_new'))
{
    function encode_new($str,$key='')
    {
        $CI =& get_instance();
        return $CI->encryptnew->encode($str,$key);
    }
}
if ( ! function_exists('decode_new'))
{
    function decode_new($str,$key='')
    {
        $CI =& get_instance();

        return $CI->encryptnew->decode($str,$key);
    }
}


if ( ! function_exists('encode_display'))
{
    function encode_display($str,$key='')
    {
        $CI =& get_instance();
        if($key=='')
        {
            $key='360';
        }
        return $CI->encryption->encode($str,$key);
    }
}
if ( ! function_exists('decode_display'))
{
    function decode_display($str,$key='')
    {
        $CI =& get_instance();
        if($key=='')
        {
            $key='360';
        }
        return $CI->encryption->decode($str,$key);
    }
}

if ( ! function_exists('post_display'))
{
    function post_display($str='',$data='')
    {
        if(isset($_POST[$str]))
        {
            return htmlspecialchars($_POST[$str]);
        }
        else
        {
            return htmlspecialchars($data);
        }

    }
}


if ( ! function_exists('display_limiter'))
{
    function display_limiter($str='',$end='0')
    {
        if (strlen($str) < $end)
        {
            //return $str;
            return '<span title="">'.htmlspecialchars($str).'</span>';
        }
        else
        {
            
            $str_sort=mb_substr($str,0,$end) ."..";
            return '<span title="'.mb_convert_encoding($str,'HTML-ENTITIES', 'UTF-8').'">'.htmlspecialchars($str_sort).'</span>';
        }
        
    }
}
if ( ! function_exists('select_radio'))
{
    function select_radio($name,$value,$default="",$set_value='')
    {
        if(isset($_POST[$name]) && $_POST[$name]==$value)
        {
            return 'checked="checked"';
        }
        else if($value==$set_value)
        {
            return 'checked="checked"';
        }
        else if(!isset($_POST[$name]) && $default!='' && $set_value=='')
        {
            return 'checked="checked"';
        }
        else
        {
            return '';
        }
    }
}
if ( ! function_exists('select_radio_edit'))
{
    function select_radio_edit($value,$set_value)
    {
        if($value==$set_value)
        {
            return 'checked="checked"';
        }
        else
        {
            return '';
        }
    }
}

if ( ! function_exists('display_label'))
{
    function display_label($array,$class)
    {?>
        <script src="js/jquery.inline-labels.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript">
        $('label.inline').inlineLabel();
                $(function() {
            });
        </script>
        <?php
        foreach($array as $key=>$value)
        {
            ?>
            <label for="<?php echo $key ?>" class="<?php echo $class ?>"><?php echo $value ?></label>
            <?php
        }
    }
}
if ( ! function_exists('select_display'))
{
    function select_display($array,$value='',$label='',$set_value='',$single='',$encode='')//if want to print key(as value),value(as label) than -> select_display($array,''); 
    {
        $str='';
        #$str.='<option value="" disabled selected>Select Option</option>';
        if($label=='')
        {
            $label=$value;
        }
        if($value!='')
        {
            foreach($array as $row)
            {
                if(is_object($row))
                {
                    if($row->$value==$set_value)
                    {
                        $str.='<option value="'.$row->$value.'" selected="selected">'.ucfirst($row->$label).'</option>';
                    }
                    else
                    {
                        $str.='<option value="'.$row->$value.'">'.ucfirst($row->$label).'</option>';
                    }
                }else
                {
                    if($row[$value]==$set_value)
                    {
                        $v=$encode!=''?encode_display($row[$value]):$row[$value];
                        $str.='<option value="'.$v.'" selected="selected">'.ucfirst($row[$label]).'</option>';
                    }
                    else
                    {
                        $v=$encode!=''?encode_display($row[$value]):$row[$value];                       
                        $str.='<option value="'.$v.'">'.ucfirst($row[$label]).'</option>';
                    }
                    
                }
            }
        }
        else
        {
            if($single!='')
            {
                foreach($array as $key)
                {
                    if($key==$set_value)
                    {
                        $str.="<option value=\"$key\" selected='selected'>$key</option>";
                    }
                    else
                    {
                        $str.="<option value=\"$key\">$key</option>";
                    }
                }
                
            }
            else
            {
                foreach($array as $key=>$value)
                {
                    if($key==$set_value)
                    {
                        
                        $str.="<option value='".$key."' selected='selected'>".$value."</option>";
                    }
                    else
                    {
                        $str.="<option value='".$key."'>".$value."</option>";
                    }
                }
                
            }
        }
        return stripslashes($str);
    }
}
if ( ! function_exists('select_display_join'))
{
    function select_display_join($array,$value='',$label=array(),$set_value='',$single='',$encode='')//if want to print key(as value),value(as label) than -> select_display($array,''); 
    {
        $str='';
        
        if($value!='')
        {
            foreach($array as $row)
            {
                if(is_object($row))
                {
                    $label_v="";
                        foreach ($label as $key_v) {
                            echo $label_v.=ucfirst($row->$key_v)." ";
                        }
                    if($row->$value==$set_value)
                    {
                        
                        $str.='<option value="'.$row->$value.'" selected="selected">'.$label_v.'</option>';
                    }
                    else
                    {
                        $str.='<option value="'.$row->$value.'">'.$label_v.'</option>';
                    }
                }else
                {
                    if($row[$value]==$set_value)
                    {
                        $v=$encode!=''?encode_display($row[$value]):$row[$value];
                        $str.='<option value="'.$v.'" selected="selected">'.ucfirst($row[$label]).'</option>';
                    }
                    else
                    {
                        $v=$encode!=''?encode_display($row[$value]):$row[$value];                       
                        $str.='<option value="'.$v.'">'.ucfirst($row[$label]).'</option>';
                    }
                    
                }
            }
        }
        else
        {
            if($single!='')
            {
                foreach($array as $key)
                {
                    if($key==$set_value)
                    {
                        $str.="<option value=\"$key\" selected='selected'>$key</option>";
                    }
                    else
                    {
                        $str.="<option value=\"$key\">$key</option>";
                    }
                }
                
            }
            else
            {
                foreach($array as $key=>$value)
                {
                    if($key==$set_value)
                    {
                        
                        $str.="<option value='".$key."' selected='selected'>".$value."</option>";
                    }
                    else
                    {
                        $str.="<option value='".$key."'>".$value."</option>";
                    }
                }
                
            }
        }
        return $str;
    }
}

if ( ! function_exists('select_mdisplay'))
{
    function select_mdisplay($array,$set_value='')//if want to print key(as value),value(as label) than -> select_display($array,''); 
    {
        $str='';
        foreach($array as $key=>$val)
        {
            $selected=in_array(trim($key,'!'),$set_value)?'selected="selected"':'';
            $str.='<option value="'.$key.'" '.$selected.'>'.ucfirst($val).'</option>';
           
        }
      //  }
        return $str;
    }
}
if ( ! function_exists('get_strting'))
{
    function get_strting($array,$index,$seprator)//array,index name of array,separator that we are used
    {
        $str='';
        foreach($array as $row)
        {
            $str.=$seprator .$row->$index;
        }
        $str=trim($str,$seprator);
        return $str;
    }
}
if ( ! function_exists('get_daydiff'))
{
    function get_daydiff($end_date,$today)//Last date and current date default otherwise passed date ex... get_daydiff('2012-09-22','2012-09-19');
    {
        if($today=='')
        {
            $today=date('Y-m-d');
        }
        $str = floor(strtotime($end_date)/(60*60*24)) - floor(strtotime($today)/(60*60*24));
        return $str;
    }
}
if ( ! function_exists('date_view'))
{
    function date_view($date,$format='',$default='')//Last date and current date default otherwise passed date ex ... add_date('2012-07-20','+','20','d');
    {
        if($date=='' && $default=='')
        {
            $date=date('Y-m-d');
            return date('d-m-Y',strtotime($date));          
        }elseif($date=='' && $default!='')
        {
            return '';
        }
        else
        {
            if($date=='0000-00-00')
            {
                return '';
            }
            return date('d-m-Y',strtotime($date));
        }
    }
}

if ( ! function_exists('date_timeview'))
{
    function date_timeview($date,$format='')//Lastdate and current date default otherwise passed date ex ... add_date('2012-07-20','+','20','d');
    {
        return date('d-M-Y h:i:s',strtotime($date));
    }
}
if ( ! function_exists('add_date'))
{
    function add_date($startdate,$do,$add,$what)//Lastdate and current date default otherwise passed date ex ... add_date('2012-07-20','+','20','d');
    {
        if(($what=='m' || $what=='d') &&($do=='+' ||$do=='-'))
        {
            if($what=='m')
            {
                $what='months';
            }
            else
            {
                $what='days';
            }
            $str=date('Y-m-d',strtotime("$do$add $what",strtotime($startdate)));    
            return $str;
        }
        else
        {
            return 'enter ("2012-07-20"),+/-,"no","m/d" ' ;
        }
    }
}
if ( ! function_exists('sort_field_select'))
{
    function sort_field_select($sort_field,$label)//Lastdate and current date default otherwise passed date ex ... add_date('2012-07-20','+','20','d');
    {
        if(isset($_POST['sort_action']) && $_POST['sort_action']=='DESC' && $_POST['sort_field']==$sort_field)
        {
        ?>
        <a id="sort_desc" onClick="javascript:sorting_data('<?php echo $sort_field; ?>','ASC')" style="color:#000;"><?php echo $label; ?><img border="0" src="images/sortdown.gif"/></a>            
        <?php
        }
        else if(isset($_POST['sort_action']) && $_POST['sort_action']=='ASC' && $_POST['sort_field']==$sort_field)
        {
        ?>
        <a id="sort_asc" onClick="javascript:sorting_data('<?php echo $sort_field; ?>','DESC');" style="color:#000;" ><?php echo $label; ?><img border="0" src="images/sortup.gif"/></a>
        <?php  
        }
        else
        {
        ?>
        <a id="sort_asc" onClick="javascript:sorting_data('<?php echo $sort_field; ?>','ASC');" style="color:#000;" ><?php echo $label; ?></a><?php  
        }
    }
}


if ( ! function_exists('show_form_field_error'))
{
    function show_form_field_error($error)
    {
        #$return ='<p class="form_field_error">'.form_error($error).'</p>';
        $return ='<span class="help-block ">'.form_error($error).'</span>';
        return $return;
    }
}
if (!function_exists('generate_unique_token')) {
    function  generate_unique_token($number)
    {
        $arr = array('a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0');
        $token = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $token .= $arr[$index];
        }
        return $token;
    }
}
if (!function_exists('get_current_date')) {
    function  get_current_date()
    {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d');
    }
}
if (!function_exists('get_current_date_time')) {
    function get_current_date_time()
    {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s');
    }
}
if (!function_exists('get_current_time')) {
    function get_current_time()
    {
        date_default_timezone_set('Asia/Kolkata');
        return date('H:i:s');
    }
}
if (!function_exists('truncate')) {
    function truncate($str, $len)
    {
        $tail = max(0, $len - 10);
        $trunk = substr($str, 0, $tail);
        $trunk .= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len - $tail))));
        return $trunk;
    }
}
if (!function_exists('generate_numeric_unique_token')) {
    function generate_numeric_unique_token($number)
    {
        $arr = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
        $token = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $token .= $arr[$index];
        }
        return $token;
    }
}
if (!function_exists('get_date_time_to_string')) {
    function get_date_time_to_string($DateTime)
    {
        return strtotime($DateTime);
    }
}

if (!function_exists('date_time')) {
    function date_time($text, $format = '', $time = '')
    {
        if ($text == "" || $text == "0000-00-00 00:00:00" || $text == "0000-00-00")
            return "---";
        switch ($format) {
            //us formate
            case "1":
                return date('M j, Y', strtotime($text));
                break;

            case "2":
                return date('M j, y  [G:i] ', strtotime($text));
                break;

            case "3":
                return date("M j, Y", $text);
                break;

            case "4":
                return date('Y,n,j,G,', $text) . intval(date('i', $text)) . ',' . intval(date('s', $text));
                break;

            case "5":
                return date('l, F j, Y', strtotime($text));
                break;

            case "6":
                return date('g:i:s', $text);
                break;

            case "7":
                return date('F j, Y  h:i A', strtotime($text));
                break;

            case "8":
                return date('Y-m-d', strtotime($text));
                break;
            case "9":
                return date('F j, Y', strtotime($text));
                break;
            case "10":
                return date('d/m/Y', strtotime($text));
                break;
            case "11":
                return date('m/d/y', strtotime($text));
                break;
            case "12":
                return date('H:i', strtotime($text));
                break;
            case "13":
                return date('F j, Y (H:i:s)', strtotime($text));
                break;
            case "14":
                return date('j-M-Y', strtotime($text));
                break;
            case "15":
                return date('D', strtotime($text));
                break;
            case "16":
                return date('d', strtotime($text));
                break;
            case "17":
                return date('M Y', strtotime($text));
                break;
            case "18":
                return date('h:i A', strtotime($text));
                break;
            case "19":
                return date('M j, Y', strtotime($text));
                break;
            case "20":
                return date('l,F d', strtotime($text));
                break;
            case "21":
                return date('m/d/y, l', strtotime($text));
                break;
            //Use below(22-23-24) date time format in whole site
            //For Time - 01:00 AM
            case "22":
                return date('h:i A', strtotime($text));
                break;
            //For date 10 Mar 2016
            case "23":
                return date('j M Y', strtotime($text));
                break;
            //For Date and Time  28 Mar 2016 01:00 AM
            case "24":
                return date('j M Y', strtotime($text)) . ' ' . date('h:i A', strtotime($time));
                break;
            //For DateTime type
            case "25":
                return date('j M Y  h:i A', strtotime($text));
                break;
            case "26":
                return date("jS M, Y", strtotime($text));
                break;
            case "27":
                return date("jS M, Y h:i:s a", strtotime($text));
                break;
            case "28":
                return date("jS M, Y h:i a", strtotime($text));
                break;
            default :
                return date('Y-m-d', strtotime($text));
                break;
        }
    }
}

/* custom helper code for campaign price with radio start */

function get_radiobutton_withprice($tablename, $filedname, $where = "")
{
    $CI =& get_instance();
 
    $query = "select * from $tablename where campaign_id='".$filedname."' and status=$where";
    $res = $CI->db->query($query);

     $record = "";
     $i=0;
     $str ="";
    if ($res->num_rows() > 0) {

        foreach ($res->result_array() as $row) {
    
                $record .='<tr><td><input type="radio" value="'.$row['days_number'].'" name="days" >'.$row['days_number'].' days</td>';
                if($i==1)
                {

                    $str ='<td ><span style="text-decoration: line-through;">$'.$row['price'].'</span><br/><span>$'.$row['special_price'].'</span></td></tr>';
                }
                $i++;
        }

    }
    return $record.$str;

}

/**
 * Use : For unset any key from Array
 * @param : $array is main array which wll use for remove key
 * @param : $keyArray is array with key name which  will remove from $array
 **/
function unset_array_keys($array, $keyArray = array())
{
    if (count($keyArray) > 0) {
        foreach ($keyArray as $key => $value) {
            unset($array[$value]);
        }
    }
    return $array;
}

/**
 * Use : For create new array using any key from $array
 * @param : $array is main array
 * @param : $keyArray is array with key name which will use for create new array from $array
 **/
function make_array_of_any_key($array, $keyArray = array())
{
    $newArray = array();
    if (count($keyArray) > 0) {
        foreach ($keyArray as $key_one => $value_one) {
            foreach ($array as $key_two => $value_two) {
                if (array_key_exists($value_one, $value_two)) {
                    $newArray[] = $value_two[$value_one];
                }
            }
        }
    }
    return $newArray;
}

if (!function_exists('get_extension')) {
    function get_extension($str)
    {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }
}

if (!function_exists('_array_to_obj')) {
    function _array_to_obj($array = array(), $type = 1)
    {
        if ($type == 1)
            return (object)$array;
        else
            return (array)$array;
    }
}
if (!function_exists('_form_validaiton')) {
    function _form_validaiton($key,$obj)
    {
        if(isset($obj->$key)===TRUE){
            return 1;
        }else{
            return 0;
        }
    }
}

if (!function_exists('view_button_email')) {
    function view_button_email($url=A_TAG_DISABLE,$type=COLOR_P, $btn_size='btn-md')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="'.$url.'" class="btn btn-success '.$btn_size.' "><i class="fa fa-eye"></i></a>
                   </div>';
            return $btn;
    }
}
/* Data Tables Start */
if (!function_exists('view_button')) {
    function view_button($p_key,$url=A_TAG_DISABLE,$type=COLOR_P, $btn_size='btn-md')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="'.$url.'" onClick="action_details('. $p_key.')"></a>
                         <button type="button" class="btn  btn-success ' . $btn_size . ' " onClick="action_details(' . $p_key . ')"><i class="fa fa-eye"></i>
                        </button>
                   </div>';
            return $btn;
    }
}
if (!function_exists('shop_button')) {
    function shop_button($url=A_TAG_DISABLE,$type=COLOR_P, $btn_size='btn-md')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="'.$url.'" class="btn '.$type.' '.$btn_size.' "><i class="fa fa-shopping-cart"></i></a>
                   </div>';
            return $btn;
    }
}
if (!function_exists('change_button')) {
    function change_button($p_key,$code,$old_status,$url=A_TAG_DISABLE,$type=COLOR_P, $btn_size='btn-xs')
    {
        $CI =& get_instance();
        //$type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<select class="form-control order_change" style="width:65px;" onchange="action_change(this.value,'. $p_key.',\''.$code.'\',\''.$old_status.'\')" id="'. $p_key.'" >
                                            <option value=""> Status</option>
                                            <option value="' . ORDER_STATUS_WAIT . '">Awaiting for payment</option>
                                            <option value="' . ORDER_STATUS_BUY . '">Payment accepted</option>
                                            <option value="' . ORDER_STATUS_IN_PROCESS . '">In Process</option>
                                            <option value="' . ORDER_STATUS_DELIVERED . '">Delivered</option>
                                            <option value="' . ORDER_STATUS_CANCEL . '">Cancel</option>
                                        </select>';
            return $btn;
    }
}

if (!function_exists('change_button_for_manager')) {
    function change_button_for_manager($p_key, $code, $old_status, $url = A_TAG_DISABLE, $type = COLOR_P, $btn_size = 'btn-xs')
    {
        $CI =& get_instance();
        //$type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<select class="form-control order_change" style="width:65px;" onchange="action_change(this.value,' . $p_key . ',\'' . $code . '\',\'' . $old_status . '\')" id="' . $p_key . '" >
                                            <option value="">Status</option>
                                            <option value="' . ORDER_STATUS_IN_PROCESS . '">In Process</option>
                                            <option value="' . ORDER_STATUS_DELIVERED . '">Delivered</option>
                                        </select>';
        return $btn;
    }
}

if (!function_exists('edit_button')) {
    function edit_button($url = A_TAG_DISABLE, $type = COLOR_I, $btn_size = 'btn', $icon = 'fa fa-edit')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="' . $url . '" class="btn btn-light  ' . $btn_size . ' "><i class="' . $icon . '"></i></a>
                   </div>';
        return $btn;
    }
}
if (!function_exists('delete_button')) {
    function delete_button($p_key, $label_name = DELETE_USER, $type = COLOR_E, $btn_size = 'btn-xs', $icon = 'fa fa-trash')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="' . A_TAG_DISABLE . '" class="btn ' . $type . ' ' . $btn_size . ' " onclick="action_delete(' . $p_key . ',\'' . $label_name . '\');"><i class="' . $icon . '"></i></a>
                   </div>';
        return $btn;
    }
}
if (!function_exists('stop_button')) {
    function stop_button($p_key, $label_name = STOP, $type = COLOR_E, $btn_size = 'btn-xs', $icon = 'fa fa-stop')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="' . A_TAG_DISABLE . '" class="btn ' . $type . ' ' . $btn_size . ' " onclick="action_delete(' . $p_key . ',\'' . $label_name . '\');"><i class="' . $icon . '"></i></a>
                   </div>';
        return $btn;
    }
}
if (!function_exists('start_button')) {
    function start_button($p_key, $label_name = START, $type = COLOR_E, $btn_size = 'btn-xs', $icon = 'fa fa-play')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="' . A_TAG_DISABLE . '" class="btn ' . $type . ' ' . $btn_size . ' " onclick="action_delete(' . $p_key . ',\'' . $label_name . '\');"><i class="' . $icon . '"></i></a>
                   </div>';
        return $btn;
    }
}

if (!function_exists('duplicate_button')) {
    function duplicate_button($p_key, $label_name = DUPLICATE_ENTRY, $type = COLOR_S, $btn_size = 'btn-xs', $icon = 'fa fa-clone')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group">
                        <a href="' . A_TAG_DISABLE . '" class="btn ' . $type . ' ' . $btn_size . ' " onclick="action_duplicate(' . $p_key . ',\'' . $label_name . '\');"><i class="' . $icon . '"></i></a>
                   </div>';
        return $btn;
    }
}
if (!function_exists('status_tag')) {
    function status_tag($label_name='',$type=COLOR_S)
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $status_tag = '<span class="label '.$type.' text-center">' . $label_name . '</span>';
        return $status_tag;
    }
}
if (!function_exists('action_status')) {
    function action_status($p_key='',$label_name='',$type='', $btn_size = 'btn-xs')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<li class="text-center"> <button type="button" class="btn   '.$type.' '.$btn_size.'" onclick="status_update(' . $p_key . ',\'' . $label_name . '\');">'.$label_name.'</button> </li>';
        return $btn;
    }
}
if (!function_exists('action_available')) {
    function action_available($p_key='',$label_name='',$type='', $btn_size = 'btn-xs')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<li class="text-center"> <button type="button" class="btn   '.$type.' '.$btn_size.'" onclick="availability_update(' . $p_key . ',\'' . $label_name . '\');">'.$label_name.'</button> </li>';
        return $btn;
    }
}
if (!function_exists('action_event')) {
    function action_event($label_name='',$type=COLOR_S,$p_key=0, $btn_size='btn-xs')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<li class="text-center"> <button type="button" class="btn btn-block  '.$type.' '.$btn_size.'" onclick="status_update(' . $p_key . ',\'' . $label_name . '\');">'.$label_name.'</button> </li>';
        return $btn;
    }
}
if (!function_exists('action_button')) {
    function action_button($action_html='',$type=COLOR_D, $btn_size='btn-xs')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<div class="btn-group ">
                            <button type="button" class="btn  '.$type.' '.$btn_size.' dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                ' . $action_html . '
                            </ul>
                        </div>';
        return $btn;
    }
}
if (!function_exists('set_as_default')) { #ticket System.
    function set_as_default($label_name = '', $type = COLOR_S, $p_key = 0, $ref_key = 0, $btn_size = 'btn-xs')
    {
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['btn'][$type];
        $btn = '<li class="text-center"> <button type="button" class="btn btn-block  ' . $type . ' ' . $btn_size . '" onclick="status_update(' . $p_key . ',' . $ref_key . ',\'' . $label_name . '\');">' . $label_name . '</button> </li>';
        return $btn;
    }
}
/* Data Tables End */

/* Get Lat Long Start */
if (!function_exists('get_lang_lat')) {
    function get_lang_lat($address)
    {
        $address = urlencode($address);
        $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . $address . "&sensor=true";
        $xml = simplexml_load_file($request_url);
        if ($xml->status && $xml->status == "OK") {
            return $xml->result->geometry->location;
        } else {
            return (object)array('lat' => '', 'lng' => '');
        }
    }
}
/* Get Lat Long End */
/* Email Template Start By Vikrant Parmar*/
if (!function_exists('email_template')) {
    function email_template($email_key = 'TEST_MAIL', $content_data = '', $base_url = '')
    {
        $CI =& get_instance();
        $CI->load->model('email_template_model');
        $mail_template_response = $CI->email_template_model->get_email_template($email_key, 1);
        if ($email_key == 'TEST_MAIL') {
            $content_data = new stdClass();
            $content_data->user_name = 'Vikrant P.';
            $content_data->contact_number = '+91 8000255245';
            $content_data->action_url = base_url();
            $content_data->sender_name = 'Backend Brains';
            $content_data->website_name = 'wwww.backendbrains.com';
            $content_data->varification_link = base_url().'verification';
            $content_data->url_link = base_url().'verification';
        } else {
            if ($base_url == '') {
                $content_data->var_site_url_admin = admin_url();
            } else {
                $content_data->var_site_url_admin = $base_url . ROOT_DIR_ADMIN;
            }
            #_e('asda');
            $content_data->var_site_name_admin = EMAIL_HEADER_TITLE;
        }
        #_pre($mail_template_response);
        $language = $CI->session->userdata('language');
        if(empty($language)){
            $language_code = 'fr';            
        }else{
            $language_code = $language->vLangCode;    
        }
        
        
        // _pre($language_code);
        if(count($mail_template_response)>0){
            if ($language_code != 'en') {
                $email_html = $mail_template_response->tEmailFormatDesc_FR;
                $email_subject = $mail_template_response->vEmailFormatSubject_FR;
            } else {
                $email_html = $mail_template_response->tEmailFormatDesc_EN;
                $email_subject = $mail_template_response->vEmailFormatSubject_EN;
            }
            foreach ($content_data as $key => $value) {
                $email_html = str_replace('{' . $key . '}', $value, $email_html);
                #$email_html = str_replace('{var_'.$key.'}', $value, $email_html);
            }
            foreach ($content_data as $key => $value) {
                $email_subject = str_replace('{' . $key . '}', $value, $email_subject);
            }
            $mail_template_response->email_html = $email_html;
            $mail_template_response->email_subject = $email_subject;
            return $mail_template_response;
        }else{
            return 0;
        }
    }
}
/* Email Template End */
if (!function_exists('url_encode')) {
    function url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }
}
if (!function_exists('url_decode')) {
    function url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
}
/* String Function Start By Vikrant Parmar.*/
if (!function_exists('_ucwords')) {
    function _ucwords($str = '')
    {
        if ($str != '') {
            $str = ucwords(strtolower($str));
        }
        return $str;
    }
}

if (!function_exists('string_replace')) { #used in Left menu section.
    function _str_repalace($old_string='',$new_string='',$main_string='')
    {
        return str_replace($old_string,$new_string,$main_string);
    }
}
if (!function_exists('string_prefix')) {
    function string_prefix($prefix_sting='',$main_string='',$delimiter='_')
    {
        return $prefix_sting.$delimiter.$main_string;
    }
}

if (!function_exists('text_limiter')) {
    function text_limiter($str = '', $start = 0, $end = 0)
    {
        if (strlen($str) < $end) {
            return $str;
        } else {
            $str = substr($str, 0, $end) . "...";
            return $str;
        }
    }
}

if (!function_exists('replace_with')) {
    function replace_with($str = '', $delimiter = "_")
    {
        $new_str = '';
        if ($str != '') {
            $new_str = preg_replace('/[-`~!@#$%\^&*()+={}[\]\\\\|;:\'",.><?\/ ]/', $delimiter, $str);
        }
        return $new_str;
    }
}
/* String Function End */

/* Notification Start By Vikrant Parmar.*/
if (!function_exists('_set_flashdata')) {
    /**
        $type = 1 for Toast, 2 for HTML
     */
    function _set_flashdata($msg_type = FLASH_STATUS_SUCCESS, $obj,$type = FLASH_TOAST)
    {
        $notification_msg = '';
        if(is_object($obj)){
            if(count($obj)>0){
                foreach($obj as $k=>$v){
                    $notification_msg .= ($notification_msg) ? "<br>".$v : $v;
                }
            }
        }else{
            $notification_msg  = $obj;
        }
        $CI =& get_instance();
        $CI->session->set_flashdata(FLASH_TYPE, $type);
        $CI->session->set_flashdata(FLASH_STATUS, $msg_type);
        $CI->session->set_flashdata(FLASH_MESSAGE, $notification_msg);
    }
}
if (!function_exists('_notification')) {
    function _notification($type = COLOR_S, $msg= 'Hello ! This is notification.')
    {
        $notification_html = '';
        $CI =& get_instance();
        $type = $CI->config->item('element_type')['notification'][$type];
        $notification_html .= '<div class="alert '.$type.' alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    '.$msg.'
                                </div>' ;
        return $notification_html;
    }
}
if (!function_exists('_notify')) {
    function _notify(){
        $CI =& get_instance();
        if (@$CI->session->flashdata(FLASH_MESSAGE) != '') {
            if(@$CI->session->flashdata(FLASH_TYPE) == FLASH_HTML){
                _e(_notification(@$CI->session->flashdata(FLASH_STATUS),@$CI->session->flashdata(FLASH_MESSAGE)),0);
            }else{
                if(@$CI->session->flashdata(FLASH_TYPE) == FLASH_TOAST){
                    _notification_js();
                }
            }
        }
    }
}
if (!function_exists('_notification_js')) {
    function _notification_js()
    {
        if (SIDE == 'front' || SIDE == 'Front') {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    <?php
                     $CI =& get_instance();
                        if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_SUCCESS){ ?>
                    toastr.success('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=SUCCESS?>', {positionClass: 'toast-top-full-width'});
                    <?php }else if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_ERROR){ ?>
                    toastr.error('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                    <?php   }else if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_WARNING){ ?>
                    toastr.warning('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=WARNING?>', {positionClass: 'toast-top-full-width'});
                    <?php   }else if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_INFO){ ?>
                    toastr.info('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=INFO?>', {positionClass: 'toast-top-full-width'});
                    <?php } ?>
                });
            </script>
        <?php } else { ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    <?php
                     $CI =& get_instance();
                        if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_SUCCESS){ ?>
                    toastr.success('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=SUCCESS?>', {positionClass: 'toast-top-right'});
                    <?php }else if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_ERROR){ ?>
                    toastr.error('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=ERROR?>', {positionClass: 'toast-top-right'});
                    <?php   }else if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_WARNING){ ?>
                    toastr.warning('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=WARNING?>', {positionClass: 'toast-top-right'});
                    <?php   }else if(@$CI->session->flashdata(FLASH_STATUS) == FLASH_STATUS_INFO){ ?>
                    toastr.info('<?=@$CI->session->flashdata(FLASH_MESSAGE)?>', '<?=INFO?>', {positionClass: 'toast-top-right'});
                    <?php } ?>
                });
            </script>
        <?php }

    }
}

/* Notification End */


/* HTML */

if (!function_exists('span_asterisk')) {
    function span_asterisk()
    {
        return '<span class="text-red">*</span>';
    }
}
if (!function_exists('_header_footer')) {
    function _header_footer($BODY_CONTENT = '')
    {
        $LOGO = '';
        $message = '';
        $CI =& get_instance();
        $message .= '
                <html>
                <head>
                <title>' . $CI->lang->line('MSG_MAIL_HEADER_TITLE') . ' </title>
                </head>
                <body>
                <div style="margin: 0;">
                    <table style="border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F6F9FC">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <center style="width: 100%;">
                                        <div style="font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; overflow: hidden; font-family: sans-serif;">&nbsp;</div>
                                        <div style="max-width: 600px;background-color:#FFF;margin-top:15px;">
                                            <table style="background-color:#FFF;max-width:600px;margin-top:20px;border-top-left-radius:6px;border-top-right-radius:6px;border-bottom:1px solid #f2f2f2;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left" bgcolor="#ea0b44">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: left; padding: 15px 15px 15px 25px; font-family: sans-serif; font-weight: bold; color: #525F7f; font-size: 30px;">
                                                                <a href="" style="margin-top: 30px"><img src="'.MAIN_URL.'uploads/images/familov-logo.png" alt="Familov.com" width="35%"></a>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="max-width:590px;border-bottom-left-radius:6px;border-bottom-right-radius:6px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="padding: 20px; font-family: sans-serif; line-height: 24px; color: #566985; font-size: 15px;">' . $BODY_CONTENT . '</td>
                                                                    </tr>
                                                                    <tr>
                                                                         <td>&nbsp;</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                           <table style="background-color:#F6F9FC;max-width:600px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left" bgcolor="#ea0b44">
                                                <tbody>
                                                    
                                                    <tr>
                                             
                                                        <td style="padding:20px;width:50%;font-size:11px;font-family:sans-serif;line-height:19px;text-align:left;color:#8898aa;min-height:100px;">2017-2018 © Familov.com </td>
                                                        <td style="padding:20px;width:50%;font-size:11px;font-family:sans-serif;line-height:19px;text-align:right;color:#8898aa;">Made with &#10084; im Saarland</td>

                                                    </tr>
                                                </tbody>
                                             </table>

                                      </div>
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="yj6qo">&nbsp;</div>
                    <div class="adL">&nbsp;</div>
                    </div>
                </body>
                </html>';
        return $message;
    }
}

/* Array & Object */
if (!function_exists('_array_obj')) {
    function _array_obj($data = '', $type = 'A', $place_holder = 'Option', $key_value = array('country_id', 'country_name'), $option = 0) #'A' for array and 'O' for Object return
    {
        if ($type == 'A') {
            $new_data = array();
        } else {
            $new_data = new stdClass();
        }
        if ($option == 0)
            $new_data[''] = 'Select ' . $place_holder;

        if ($data == '') {

        } else if (is_object($data)) {
            list($key, $value) = $key_value;
            foreach ($data as $k => $v) {
                $new_data[$v->$key] = $v->$value;
            }
        } else {
            list($key, $value) = $key_value;

            foreach ($data as $k => $v) {
                $new_data[$v->$key] = $v->$value;
            }
        }
        return $new_data;
    }
}


#Image Upload Start By Vikrant P. (Ph. No# 8000255245)
if (!function_exists('image_upload')) {
    function image_upload($files, $html_element, $path = 'uploads/temp', $new_file_name = 'test', $file_max_size = ATTACHMENT_MAX_FILE_SIZE)
    {
        $CI =& get_instance();
        $name = $files['name'];
        $size = $files['size'];
        $tmp = $files['tmp_name'];
        $ext = get_extension($name);
        if (strlen($name) > 0) {
            $valid_formats = $CI->config->item('valid_formats');
            #_e($ext,0);
            #_pre($valid_formats,0);
            if (in_array($ext, $valid_formats)) {
                #_e("in if");
                if ($size < (1024 * $file_max_size)) {
                    if (!is_dir($path))
                        mkdir($path, 0755);

                    $upload_result = bb_do_upload($html_element, $path, $new_file_name);
                    return $upload_result;
                } else {
                    return array('status' => 0, 'error' => 'Attachment size exceeded the maximum size.');
                }
            } else {
                #_e("in else");
                return array('status' => 0, 'error' => 'File format is not valid.');
            }
        } else {
            return array('status' => 0, 'error' => 'Image Name is blank.');
        }
    }
}
if (!function_exists('bb_do_upload')) {
    function bb_do_upload($element_name, $path, $new_name = '')
    {
        $CI =& get_instance();
        if ($new_name == '') {
            $new_name = time();
        }
        $config['file_name'] = $new_name;
        $config['upload_path'] = $path;
        $valid_formats = $CI->config->item('valid_formats');
        $config['allowed_types'] = implode("|", $valid_formats);//'gif|jpg|png|jpeg';
        #$config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        unset($config);
        if (!$CI->upload->do_upload($element_name)) {
            return array('error' => $CI->upload->display_errors(), 'status' => 0);
        } else {
            return array('status' => 1, 'upload_data' => $CI->upload->data());
        }
    }
}
if (!function_exists('bb_resize_image')) {
    function bb_resize_image($sourcePath, $desPath, $width = '300', $height = '300')
    {
        $CI =& get_instance();
        $CI->load->library('image_lib');
        $CI->image_lib->clear();
        $config['image_library'] = 'gd2';
        $config['source_image'] = $sourcePath;
        $config['new_image'] = $desPath;
        $config['quality'] = '100%';
        # $config['maintain_ratio'] = TRUE; #false if needed.
        $config['create_thumb'] = TRUE;
        #$config['maintain_ratio'] = true;
        $config['thumb_marker'] = '';
        $config['width'] = $width;
        $config['height'] = $height;
        $CI->image_lib->initialize($config);

        if ($CI->image_lib->resize())
            return true;
        return false;
    }
}

/** Auto Ticket Next Date Start
 * @return : next ticket date Periodicity type wise
 **/


if (!function_exists('auto_ticket_next_date')) {
    function auto_ticket_next_date($periodicity_type, $periodicity_value_first = '', $periodicity_value_second = '')
    {
        $CI =& get_instance();
        #$CI->load->library('image_lib');
        $periodicity_array = $CI->config->item('periodicity_value');
        $up_coming_date = 0;
        if (array_key_exists($periodicity_type, $periodicity_array)) {
            #TODO For Half Yearly
            if ($periodicity_type == 'Half Yearly') {
                if ($periodicity_value_first == 'First_Half') {
                    $dt_half_yearly = $periodicity_value_second . "-" . date('04-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_half_yearly . ' +6 months'));
                } else {
                    $dt_half_yearly = $periodicity_value_second . "-" . date('10-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_half_yearly . ' +6 months'));
                }
            } else if ($periodicity_type == 'Monthly') {
                $month = date('m', strtotime($periodicity_value_first));
                $year = $periodicity_value_second;
                $dt_month = $year . "-" . $month . "-01";
                $up_coming_date = date('Y-m-d', strtotime($dt_month . ' +1 months'));
            } else if ($periodicity_type == 'Quarterly') {
                if ($periodicity_value_first == 'Q1') { #April to June => 4 To 6
                    $dt_quarterly = $periodicity_value_second . "-" . date('04-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_quarterly . ' +3 months'));
                } else if ($periodicity_value_first == 'Q2') { #July TO September => 7 TO 9
                    $dt_quarterly = $periodicity_value_second . "-" . date('07-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_quarterly . ' +3 months'));
                } else if ($periodicity_value_first == 'Q3') { #October To December => 10 TO 12
                    $dt_quarterly = $periodicity_value_second . "-" . date('10-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_quarterly . ' +3 months'));
                } else if ($periodicity_value_first == 'Q4') { #January To March => 1 TO 3
                    $periodicity_value_second = $periodicity_value_second + 1;
                    $dt_quarterly = $periodicity_value_second . "-" . date('01-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_quarterly . ' +3 months'));
                }
            } else if ($periodicity_type == 'Yearly') {
                $year_array = explode("-", $periodicity_value_first);
                if (count($year_array) > 0) {
                    $dt_yearly = $year_array[1];
                    $dt_next = $dt_yearly + 1;
                    $dt_year = $dt_yearly . "-" . date('04-01');
                    $up_coming_date = date('Y-m-d', strtotime($dt_year));
                }
            }
        } else {
            $up_coming_date = 0;
        }
        return $up_coming_date;
    }
}

/* Auto Ticket Next Date End */


/** Rating Calculation
 * @return : rating %.
 **/


if (!function_exists('rating_calculation')) {
    function rating_calculation($rating_array = array(), $rank_total = 0)
    {
        $rank_percentage = array();
        for ($i = 1; $i <= 5; ++$i) {
            $var = $rating_array[$i];
            $count = $var;
            $percent = $count * 100 / $rank_total;
            for ($j = 1; $j <= 5; ++$j) {
                #echo $j <= $i ? "☆ " : "  ";
            }
            $rank_percentage[$i] = round($percent, 2);
            #printf("\t%2d (%5.2f%%)\n", $count, $percent,2);
            #_e('',0);
        }
        #_pre($rank_percentage);
        return $rank_percentage;
    }
}

/** Convert Price
 * @return : .
 **/
if (!function_exists('convert_price')) {
    function convert_price($price)
    {
        $CI =& get_instance();
        $currency = $CI->session->userdata('currency');
        $price = str_replace(',', '.', $price);
        #_pre($currency,1);
        $current_curr_code = $currency->vCode;
        $current_rate = $currency->rate;
        #_pre($price,0);
        $price = $current_rate * $price;
        $price = number_format((float)$price, 2);
        #_pre($price);
        return $price;
        #return str_replace('.', ',', $price);
    }
}
if (!function_exists('convert_price_restore')) {
    function convert_price_restore($price, $curr = 'USD')
    {
        $CI =& get_instance();
        if ($curr == 'USD') {
            $str = "SELECT * from currency where vCode = 'USD'";
            $currency = $CI->db->query($str)->row();
        } else {
            $str = "SELECT * from currency where vCode = 'EUR'";
            $currency = $CI->db->query($str)->row();
        }
        /*$currency = $CI->session->userdata('currency');
        $price = str_replace(',', '.', $price);
        $current_curr_code = $currency->vCode;
        $current_rate = $currency->rate;*/

        $price = str_replace(',', '.', $price);
        $current_curr_code = $currency->vCode;
        $current_rate = $currency->rate;
        #_pre($price,0);
        $price = $current_rate * $price;
        $price = number_format((float)$price, 2);
        #_pre($price);
        return $price;
        #return str_replace('.', ',', $price);
    }
}


if (!function_exists('convert_price_app')) {
    function convert_price_app($price, $site_curr = 'USD')
    {
        $CI =& get_instance();
        $str = "SELECT * from currency where vCode = '$site_curr'";
        $row = $CI->db->query($str)->row();
        if (count($row)) {
            $currency = $row;
        } else {
            $str = "select * from currency where eDefault='Yes'";
            $row = $CI->db->query($str)->row();
            $currency = $row;
        }
        $price = str_replace(',', '.', $price);
        #_pre($currency,0);
        $current_curr_code = $currency->vCode;
        $current_rate = $currency->rate;
        #_pre($price,0);
        $price = $current_rate * $price;
        $price = number_format((float)$price, 2);
        #_pre($price);
        return $price;
        #return str_replace('.', ',', $price);
    }
}


if (!function_exists('_number_format')) {
    function _number_format($price)
    {
        $price = number_format((float)$price, 2);
        return $price;
    }
}
if (!function_exists('set_currency')) {
    function set_currency()
    {
        $CI =& get_instance();
        $site_curr = $CI->session->userdata('site_curr');
        $str = "SELECT * from currency where vCode = '$site_curr'";
        $row = $CI->db->query($str)->row();
        if (count($row)) {
            $currency_data['currency'] = $row;
        } else {
            $str = "select * from currency where eDefault='Yes'";
            $row = $CI->db->query($str)->row();
            $CI->session->set_userdata('site_curr', $row->vCode);
            $currency_data['currency'] = $row;
        }
        $CI->session->set_userdata($currency_data);
        #if (!defined('CURRENCY_SYMBOL')) define('CURRENCY_SYMBOL', $row->vSymbol);
        #_pre($CI->session->userdata());
        $str = "select * from currency";
        $currency_list = $CI->db->query($str)->result();
        return $currency_list;
        #_pre($currency_list);

    }
}
if (!function_exists('set_language')) {
    function set_language()
    {
        $CI =& get_instance();
        $site_lang = $CI->session->userdata('site_lang');
        $str = "SELECT * from currency where vLanguage = '$site_lang'";
        $row = $CI->db->query($str)->row();
        if (count($row)) {
            $language_data['language'] = $row;
        } else {
            $str = "select * from currency where eDefault='Yes'";
            $row = $CI->db->query($str)->row();
            $CI->session->set_userdata('site_lang', $row->vLanguage);
            $language_data['language'] = $row;
        }
        $CI->session->set_userdata($language_data);
        #if (!defined('CURRENCY_SYMBOL')) define('CURRENCY_SYMBOL', $row->vSymbol);
        #_pre($CI->session->userdata());
        $str = "select * from currency";
        $currency_list = $CI->db->query($str)->result();
        return $currency_list;
        #_pre($currency_list);

    }
}

if (!function_exists('price_show')) {
    function price_show($price, $type = 1) #1 for '.' dot and 2 for ',' comma
    {
        if ($type == 1) {
            $price = str_replace(',', '.', $price);
            $price = number_format(($price), 2);
            $price = str_replace('.', ',', number_format(($price), 2));
        } else {
            $price = str_replace(',', '.', $price);
            $price = number_format(($price), 2);
            $price = str_replace('.', ',', number_format(($price), 2));
        }
        return $price;
    }
}
if (!function_exists('get_percentage')) {
    function get_percentage($total_amount, $special_amount)
    {
        #$total_amount = 250;
        #$discount_amount = 30;

        $percentage = 100 - (($special_amount / $total_amount) * 100);
        #$percentage = (($special_amount / $total_amount) * 100);
        #return $percentage;
        return '-' . number_format($percentage, 0);
    }
}
if (!function_exists('_set_dash')) {
    function _set_dash($str = '')
    {
        $str = trim($str);
        if ($str == '') {
            $str = '-';
        }
        return $str;
    }
}
if (!function_exists('_get_config')) {
    function total_days($date_order,$id){

                                    $color = '';
                                    $d1 = $date_order;//'2017-08-14 12:12:12';

                                    $d2 = date('Y-m-d H:i:s');
                                    $date1=date_create($d1);
                                    $date2=date_create($d2);
// _pre($d1, 0); _pre($d2, 0);
                                    $diff=date_diff($date1,$date2);
// _pre($diff, 0);
                                    // $days_count = $diff->format("%R%a");
                                    $days_count = $diff->format("%a");
                                    // _pre($days_count);
                                        #_e($days_count,0);
                                    if($days_count > 0 ){
                                        #_e($days_count);
                                        $days_count = $diff->format("%a");
                                        if($days_count >=2 && $days_count < 4 ){ // 2, 3
                                            #$color = '#FEB4B3';
                                            $color = 'light_pink';
                                            #_e("2 => days : => ".$days_count." color => ".$color. " ID => ".$id,0);

                                        }
                                        if($days_count >= 4){ // 5......
                                            #_e("4 => days : => ".$days_count." color => ".$color. " ID => ".$id,0);
                                            #$color = '#FFE4B0';
                                            $color = 'light_yellow';

                                        }
                                    }
                                    
                                    return $color;
                                }
}

/**
 * Dtabase Config value get
 **/

if (!function_exists('_get_config')) {
    function _get_config($key = '')
    {
        $CI =& get_instance();
        $str = "select * from setting where 1=1 AND `keys`= '" . $key . "'";
        $query = $CI->db->query($str);
        $record = "";
        if ($query->num_rows() > 0) {
            $config_data = $query->row();
            if (count($config_data) > 0) {
                return $config_data->value;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}
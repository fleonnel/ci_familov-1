<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**************************************
 *   Photo Management Helper.
 *    - Photo Upload
 *    - Photo Resize
 *
 **************************************/
error_reporting(0);
include APPPATH . 'libraries/imagelibraries/ImageResize.php';
if (!function_exists('test')) {
    function test($photo_for)
    {
        echo '<pre>';
        print_r($photo_for);
        echo '</pre>';
    }
}
if (!function_exists('photo_uploads')) {
    function photo_uploads($ID, $FILES, $PATH, $URL, $SIZE_ARRAY, $THUMB_PREFIX = 'thumb_', $OLD_PHOTO = '')
    {
        #echo "<br> Id => " . $ID;echo "<br> Path => ".$PATH; echo "<br> URL => ".$URL;exit;
        #$CI =& get_instance();
        #$CI->load->library('imagelibraries/imageResize');
        #$this->load->library('imagelibraries/ImageResize');

        $config_r["generate_image_file"] = true;
        $config_r["generate_thumbnails"] = true;
        $config_r["image_max_size"] = 1000; //Maximum image size (height and width)
        #$config["thumbnail_size"]  			= 150; //Thumbnails will be cropped to 200x200 pixels
        $config_r["thumbnail_prefix"] = $THUMB_PREFIX; //Normal thumb Prefix
        $config_r["destination_folder"] = $PATH;//'uploads/'; //upload directory ends with / (slash)
        $config_r["thumbnail_destination_folder"] = $PATH;//'uploads/'; //upload directory ends with / (slash)
        $config_r["upload_url"] = $URL;//"https://localhost/Demo/photo/ajax-image-upload-master/uploads/";
        $config_r["quality"] = 100; //jpeg quality
        $config_r["random_file_name"] = true; //randomize each file name
        $config_r["file_data"] = $FILES;
        $config_r["thumbnail_size"] = $SIZE_ARRAY;
        #test($config["thumbnail_size"]);
        #exit;
        $ImageResize = new ImageResize($config_r);
        #exit;
        $upload_folder_path = $PATH;
        $ImageResponse = '';
        if (!is_dir($upload_folder_path)) {
            $status_dir = mkdir($upload_folder_path, 0777);
        }
         try {
             $ImageResponse = $ImageResize->resize(); //initiate image resize
             $flag = PHOTO_UPLOAD_SUCCESS;
         } catch (Exception $e) {
             $flag = PHOTO_UPLOAD_ERROR;
             $message = $e->getMessage();
         }
        return array('flag' => $flag, 'message' => $message, "photo_array" => $ImageResponse);
    }
}
if (!function_exists('get_photos')) {
    function get_photos($TYPE = PROPERTY_IMAGE, $ID, $PHOTO = '', $SIZE = '', $COVER = 0)
    {
        #echo $Visible;exit;
        if ($TYPE == PROPERTY_IMAGE) {
            $remaining_path = $ID . "/";
            $photo_name = $ID . "_" . PROPERTY_PHOTO_THUMBNAIL_PREFIX . $SIZE . "_" . $PHOTO;
            $photo_name_cover = $PHOTO;
            $photo_path = PROPERTY_PHOTO_UPLOAD_PATH . $remaining_path;
            $URL = PROPERTY_PHOTO_UPLOAD_URL . $remaining_path;
            #$DefaultPhotoURL = $DEFAULT_STATUS;
            #_e($photo_path . $photo_name);
            if (!$COVER)
                $photo_url = is_file($photo_path . $photo_name) ? $URL . $photo_name : DEFAULT_PHOTO . $SIZE . PROPERTY_NO_IMG;
            else
                $photo_url = is_file($photo_path . $photo_name_cover) ? $URL . $photo_name_cover : DEFAULT_PHOTO_COVER . PROPERTY_NO_IMG_COVER;
            return $photo_url;
        }
    }
}
/****
 * For : Photo Upload with Resize Using inbuilt IMAGE functions
 * Author : Vikrant Parmar.
 **/
if (!function_exists('photo_upload_resize')) {
    function photo_upload_resize($FILES, $PATH, $URL, $new_width, $new_height, $new_file_name = '', $THUMB_PREFIX = 'thumb_', $OLD_PHOTO = '')
    {
        #echo "<br> Id => " . $ID;echo "<br> Path => ".$PATH; echo "<br> URL => ".$URL;exit;
        $upload_folder_path = $PATH;
        $file_data = $FILES['tmp_name'];
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = get_extension($name);
        $ext = strtolower($ext);
        $src = '';
        switch ($ext) {
            case ($ext == "jpg" || $ext == "jpeg"):
                #$uploadedfile = $_FILES['file']['tmp_name'];
                $src = imagecreatefromjpeg($file_data);
                break;
            case 'png':
                $src = imagecreatefrompng($file_data);
                break;
            default:
                $src = imagecreatefromgif($file_data);
            #break;
        }
        #echo $scr;

        list($width, $height) = getimagesize($file_data);
        $new_width_size = $new_width;//60;
        $newheight = ($height / $width) * $new_width_size;
        $tmp = imagecreatetruecolor($new_width_size, $newheight);
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $new_width_size, $newheight, $width, $height);

        if ($new_file_name == '') {
            $new_file_path = $PATH . $name;
            $new_file_name = $name;
        } else {
            $new_file_path = $PATH . $new_file_name . "." . $ext;
            $new_file_name = $new_file_name . "." . $ext;
        }
        #$filename = $PATH.$_FILES['file']['name'];
        imagejpeg($tmp, $new_file_path, 100);
        imagedestroy($src);
        imagedestroy($tmp);
        return array('status' => true, 'img_full_path' => $new_file_path, "img_name" => $new_file_name);
    }
}
/****
 * For : Photo Upload with Resize Using GD2
 * Author : Vikrant Parmar.
 **/
if (!function_exists('photo_upload_resize_gd2')) {
    function photo_upload_resize_gd2($FILES, $PATH, $URL, $new_width, $new_height, $new_file_name = '', $THUMB_PREFIX = 'thumb_', $OLD_PHOTO = '')
    {
        $CI =& get_instance();
        $CI->load->library('image_lib');
        $CI->image_lib->clear();
        $file_data = $FILES['tmp_name'];
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = get_extension($name);
        $ext = strtolower($ext);
        if ($new_file_name == '') {
            $new_file_path = $PATH . $name;
            $new_file_name = $name;
        } else {
            $new_file_path = $PATH . $new_file_name . "." . $ext;
            $new_file_name = $new_file_name . "." . $ext;
        }
        $config['image_library'] = 'gd2';
        $config['source_image'] = $FILES['tmp_name'];
        $config['new_image'] = $new_file_path;
        #$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = false;
        $config['width'] = $new_width;
        $config['height'] = $new_height;
        $config['overwrite'] = false;
        $config['remove_spaces'] = true;
        // Load the Library
        $CI->image_lib->initialize($config);
        // resize image
        #$CI->image_lib->resize(); // handle if there is any problem
        if (!$CI->image_lib->resize()) {
            $res_array = array('status' => false, "error" => $CI->image_lib->display_errors());
        } else {
            $res_array = array('status' => true, 'img_full_path' => $new_file_path, "img_name" => $new_file_name);
        }

        return $res_array;
    }
}
?>
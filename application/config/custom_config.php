<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//Here you can add valid file extensions.
$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "svg", "PNG", "JPG", "JPEG", "GIF", "BMP", "SVG");
# please upload file less than 5 mb. support file jpg、png、gif、bmp
$config['valid_formats'] = $valid_formats;
$ARRAY_NUMBERS = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
$config['array_numbers'] = $ARRAY_NUMBERS;
//Here you can add valid file extensions For valid Upload.
$ALLOWED_FILE_TYPES = array('image/png', 'image/gif', 'image/jpeg', 'image/jpg', 'image/pjpeg');
$config['allowed_file_types'] = $ALLOWED_FILE_TYPES;

$config['tbl_column_status_array'] = array(
    'admin_status' => array("Active" => "Activated", "Pending" => "Pending", 'Block' => 'Blocked'),
    'test' => array("Active" => "Activated", "Pending" => "Pending", 'Block' => 'Blocked'),
);

$config['element_type'] = array(
    'notification' => array("S" => "alert-success", "E" => "alert-danger", 'I' => 'alert-info', 'W' => 'alert-warning'),
    'btn' => array("S" => "btn-success", "E" => "btn-danger", 'I' => 'btn-info', 'W' => 'btn-warning', 'D' => 'btn-default', 'P' => 'btn-primary'),
    'label' => array("S" => "label-success", "E" => "label-danger", 'I' => 'label-info', 'W' => 'label-warning'),
);


$Periodicity = array(
    'One Time' => 'One Time',
    'Monthly' => 'Monthly',
    'Quarterly' => 'Quarterly',
    'Half Yearly' => 'Half Yearly',
    'Yearly' => 'Yearly',
);
$config['periodicity'] = $Periodicity;


$periodicity_yearly = array();
for ($i = 1991; $i <= (date('Y') + 1); $i++) {
    $temp = $i . '-' . ($i + 1);
    $periodicity_yearly[$temp] = $temp;
}
$yearly = array();
for ($i = 1991; $i <= (date('Y') + 1); $i++) {
    $temp = $i;
    $yearly[$temp] = $temp;
}
$config['yearly'] = $yearly;
$one_time = date('Y-m-d');
$periodicity_value = array(
    'One Time' => array('current_date' => $one_time),
    'Monthly' => array('January' => 'January', 'February' => 'February', 'March' => 'March', 'April' => 'April', 'May' => 'May', 'June' => 'June', 'July' => 'July', 'August' => 'August', 'September' => 'September', 'October' => 'October', 'November' => 'November', 'December' => 'December'),
    'Quarterly' => array('Q1' => 'Q1 (April To June)', 'Q2' => 'Q2 (July To September)', 'Q3' => 'Q3 (October To December)', 'Q4' => 'Q4 (January To March)'),
    'Half Yearly' => array('First_Half' => 'First Half (April To September)', 'Second_Half' => 'Second Half (October To March)'),
    'Yearly' => $periodicity_yearly,
);
$config['periodicity_value'] = $periodicity_value;

$config['ticket_status_list'] = array(
    1 => array("name" => "In Process", "method" => 'open', "message" => "Ticket is in process.", 'icon' => 'fa fa-refresh', 'color' => 'funkyradio-primary'),
    2 => array("name" => "Client-side Pending", "method" => 'client_pending', "message" => "Ticket is pending mode from client side.", 'icon' => 'fa fa-hourglass-half', 'color' => 'funkyradio-warning'),
    3 => array("name" => "Postpone", "method" => 'postpone', "message" => "Ticket is postpone.", 'icon' => 'fa  fa-calendar-times-o', 'color' => 'funkyradio-danger'),
    4 => array("name" => "Cancel", "method" => 'cancel', "message" => "Ticket is cancel.", 'icon' => 'fa fa-ban', 'color' => 'funkyradio-info'),
    5 => array("name" => "Complete", "method" => 'completed', "message" => "Ticket is complete.", 'icon' => 'fa fa-check-square-o', 'color' => 'funkyradio-success'), #from employee side
    6 => array("name" => "Close", "method" => 'close', "message" => "Ticket is close.", 'icon' => 'fa  fa-check', 'color' => 'funkyradio-success_s'), #client side feedback
);

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cron Array
 * "Key" : Cron NAME (Set Crone Name Here)
 * "CRON_NAME" : For Crone NAME (Set Crone NAME Here)
 * "CRON_URL" : For Crone URL (Set Crone URL Here)
 * "Description" Index : Define Crone Use (Set Crone Use Details Here)
 * "CRON_STATUS" : For Crone Status (Set Crone Status Here : Active, In Process,  InActive)
 **/
if (!defined('CRON_NAME')) define('CRON_NAME', 'CRON_NAME');
if (!defined('CRON_URL')) define('CRON_URL', 'CRON_URL');
if (!defined('CRON_DESCRIPTION')) define('CRON_DESCRIPTION', 'CRON_DESCRIPTION');
if (!defined('CRON_STATUS')) define('CRON_STATUS', 'CRON_STATUS');
if (!defined('CRON_STATUS_ACTIVE')) define('CRON_STATUS_ACTIVE', 'Active');
if (!defined('CRON_STATUS_INACTIVE')) define('CRON_STATUS_INACTIVE', 'Inactive');
if (!defined('CRON_STATUS_IN_PROCESS')) define('CRON_STATUS_IN_PROCESS', 'In Process');

if (!defined('CRON_BASE')) define('CRON_BASE', 'cron/');
$config['cron_list_array'] = array(
    'Crone Test' => array(CRON_NAME => "Crone Test", CRON_URL => CRON_BASE . "home/test", CRON_DESCRIPTION => "Crone Test use for testing the cron setup.", CRON_STATUS => CRON_STATUS_ACTIVE),
    'Crone Test One' => array(CRON_NAME => "Crone Test One", CRON_URL => CRON_BASE . "home/test_one", CRON_DESCRIPTION => "Crone Test use for testing the cron setup.", CRON_STATUS => CRON_STATUS_IN_PROCESS),
);
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|   ['hostname'] The hostname of your database server.
|   ['username'] The username used to connect to the database
|   ['password'] The password used to connect to the database
|   ['database'] The name of the database you want to connect to
|   ['dbdriver'] The database type. ie: mysql.  Currently supported:
                 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|   ['dbprefix'] You can add an optional prefix, which will be added
|                to the table name when using the  Active Record class
|   ['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|   ['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|   ['cache_on'] TRUE/FALSE - Enables/disables query caching
|   ['cachedir'] The path to the folder where cache files should be stored
|   ['char_set'] The character set used in communicating with the database
|   ['dbcollat'] The character collation used in communicating with the database
|                NOTE: For MySQL and MySQLi databases, this setting is only used
|                as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7.
|                There is an incompatibility in PHP with mysql_real_escape_string() which
|                can make your site vulnerable to SQL injection if you are using a
|                multi-byte character set and are running versions lower than these.
|                Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|   ['swap_pre'] A default table prefix that should be swapped with the dbprefix
|   ['autoinit'] Whether or not to automatically initialize the database.
|   ['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|                           - good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/
$active_group = 'default';
$query_builder = TRUE;
ini_set('max_execution_time', 300);
ini_set('upload_max_filesize', '10M');
ini_set('post_max_size', '10M');
ini_set('max_input_time', 300);

if ($_SERVER['HTTP_HOST'] == '103.21.58.248') {
    error_reporting(0);
    $db['default'] = array(
        'dsn'   => '',
        'hostname' => 'localhost',
        'username' => 'vcodeccr_familov',
        'password' => 'backendbrains@2017',
        'database' => 'vcodeccr_familov',
        'dbdriver' => 'mysqli',
        'dbprefix' => '',
        'pconnect' => FALSE,
        'db_debug' => (ENVIRONMENT !== 'production'),
        'cache_on' => FALSE,
        'cachedir' => '',
        'char_set' => 'utf8',
        'dbcollat' => 'utf8_general_ci',
        'swap_pre' => '',
        'encrypt' => FALSE,
        'compress' => FALSE,
        'stricton' => FALSE,
        'failover' => array(),
        'save_queries' => TRUE
    );
    if (!defined('MAIL_TEST_MODE')) define('MAIL_TEST_MODE', 'Live');
    # if (!defined('MAIL_TEST_MODE_TEST')) define('MAIL_TEST_MODE_TEST', 'Live');
    if (!defined('MAIL_TEST_EMAIL')) define('MAIL_TEST_EMAIL', 'parmarvikrantr@gmail.com');
    if (!defined('PROFILER_MODE')) define('PROFILER_MODE', FALSE);
    if (!defined('SIDE')) define('SIDE', 'admin');
    #if (!defined('MAIL_TEST_MODE_LIVE')) define('MAIL_TEST_MODE_LIVE', 'Live');
} else
    if ($_SERVER['HTTP_HOST'] == 'familov.com' || $_SERVER['HTTP_HOST'] == 'www.familov.com' ) {
        //error_reporting(0);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $db['default'] = array(
            'dsn' => '',
            'hostname' => 'localhost',
            'username' => 'familov_com_ci_familov',
            'password' => 'leo@2018#neW',
            'database' => 'familov_com_ci_familov',
            'dbdriver' => 'mysqli',
            'dbprefix' => '',
            'pconnect' => FALSE,
            'db_debug' => (ENVIRONMENT !== 'production'),
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
        if (!defined('MAIL_TEST_MODE')) define('MAIL_TEST_MODE', 'Live');
        #if (!defined('MAIL_TEST_MODE_TEST')) define('MAIL_TEST_MODE_TEST', 'Live');
        if (!defined('MAIL_TEST_EMAIL')) define('MAIL_TEST_EMAIL', 'parmarvikrantr@gmail.com');
        if (!defined('PROFILER_MODE')) define('PROFILER_MODE', FALSE);
        if (!defined('SIDE')) define('SIDE', 'admin');
        #if (!defined('MAIL_TEST_MODE_LIVE')) define('MAIL_TEST_MODE_LIVE', 'Live');
}
else {

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $db['default'] = array(
        'dsn'   => '',
        'hostname' => 'localhost',
        'username' => 'root',
        'password' => '',
        'database' => 'ci_familov',
        'dbdriver' => 'mysqli'  ,
        'dbprefix' => '',
        'pconnect' => FALSE,
        'db_debug' => TRUE, //(ENVIRONMENT !== 'development'),
        'cache_on' => FALSE,
        'cachedir' => '',
        'char_set' => 'utf8',
        'dbcollat' => 'utf8_general_ci',
        'swap_pre' => '',
        'encrypt' => FALSE,
        'compress' => FALSE,
        'stricton' => FALSE,
        'failover' => array(),
        'save_queries' => TRUE
    );
    if (!defined('MAIL_TEST_MODE')) define('MAIL_TEST_MODE', 'Test');
    #  if (!defined('MAIL_TEST_MODE_LIVE')) define('MAIL_TEST_MODE_LIVE', 'Test');
    if (!defined('MAIL_TEST_EMAIL')) define('MAIL_TEST_EMAIL', 'parmarvikrantr@gmail.com');
    if (!defined('PROFILER_MODE')) define('PROFILER_MODE', FALSE);
    if (!defined('SIDE')) define('SIDE', 'Front');
    #if (!defined('MAIL_TEST_MODE_TEST')) define('MAIL_TEST_MODE_TEST', 'Test');
    #_ci_profiler(TRUE);
}
#if (!defined('MAIL_TEST_EMAIL')) define('MAIL_TEST_EMAIL', 'parmarvikrantr@gmail.com');
if (!defined('MAIL_TEST_MODE_LIVE')) define('MAIL_TEST_MODE_LIVE', 'Test');
#if (!defined('MAIL_TEST_MODE')) define('MAIL_TEST_MODE', 'Test');
#if (!defined('MAIL_TEST_MODE_TEST')) define('MAIL_TEST_MODE_TEST', 'Test');
#echo MAIL_TEST_MODE_TEST;exit;
/* End of file database.php */
/* Location: ./application/config/database.php */
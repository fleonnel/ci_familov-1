<?php

class LanguageLoader
{
    function initialize()
    {
        $ci =& get_instance();
        $ci->load->helper('language');
        $siteLang = strtolower($ci->session->userdata('site_lang'));
        #_pre($siteLang);
        if ($siteLang) {
            $ci->lang->load('message', $siteLang);
        } else {
            $ci->lang->load('message', 'english');
        }
    }
}
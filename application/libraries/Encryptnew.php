<?php
class Encryptnew {

	#var $skey = 'sdfE$Gfer4Om&32V';
	var $skey = "*/@2[znu/U6,Qg-B";
 	var $encryptmode=MCRYPT_MODE_ECB;
	var $cipher=MCRYPT_RIJNDAEL_128;

    public  function encode($value,$encyptionkey=''){
 		if($encyptionkey!='') $this->skey=$encyptionkey;
	    if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size($this->cipher, $this->encryptmode);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt($this->cipher, $this->skey, $text,  $this->encryptmode, $iv);
        return trim($this->safe_b64encode($crypttext));
    }
 
    public  function safe_b64encode($string) {

        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
    public function decode($value,$encyptionkey = ''){
 		if($encyptionkey!='') $this->skey=$encyptionkey;
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size($this->cipher,  $this->encryptmode);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt($this->cipher, $this->skey, $crypttext,  $this->encryptmode, $iv);
        return trim($decrypttext);
    }
 
	public function safe_b64decode($string,$encyptionkey='') {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
}
?>
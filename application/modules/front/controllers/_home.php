<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->data['Page'] = 'Home';
        $this->render('/cms/home');
    }

    public function signup()
    {
        $arr['page'] = 'signup';
        $this->data['page'] = 'home';
        $this->render('/cms/signup');
    }

    public function homedata()
    {
        $arr['page'] = 'home';
        $this->load->view('pages/home', $arr);
    }

    public function about()
    {
        $arr['page'] = 'home';
        $this->load->view('pages/about', $arr);
    }

    public function contact()
    {
        $arr['page'] = 'home';
        $this->load->view('pages/contact', $arr);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
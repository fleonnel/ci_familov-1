<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends Front_Controller
{
    var $table = 'products';
    var $primary_key = 'product_id';
    var $dir = '';
    private $perPage = 48;
    private $perPageimage = 1;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("shop_model");
        $this->load->model("product_model");
        $this->load->model("shop_model");
        $this->load->model("category_model");
        $this->load->model("promotion_model");
        $this->load->model("customer_model");
        $this->load->model("order_model");
        $this->load->helper('string');

        #_pre(base64_decode('DQogICAgICAgICAgICAgICAgPGh0bWw+DQogICAgICAgICAgICAgICAgPGhlYWQ+DQogICAgICAgICAgICAgICAgPHRpdGxlPkZhbWlsb3YgPC90aXRsZT4NCiAgICAgICAgICAgICAgICA8L2hlYWQ+DQogICAgICAgICAgICAgICAgPGJvZHk+DQogICAgICAgICAgICAgICAgPGRpdiBzdHlsZT0ibWFyZ2luOiAwOyI+DQogICAgICAgICAgICAgICAgICAgIDx0YWJsZSBzdHlsZT0iYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTsiIGJvcmRlcj0iMCIgd2lkdGg9IjEwMCUiIGNlbGxzcGFjaW5nPSIwIiBjZWxscGFkZGluZz0iMCIgYmdjb2xvcj0iI0Y2RjlGQyI+DQogICAgICAgICAgICAgICAgICAgICAgICA8dGJvZHk+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQgdmFsaWduPSJ0b3AiPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGNlbnRlciBzdHlsZT0id2lkdGg6IDEwMCU7Ij4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPSJmb250LXNpemU6IDFweDsgbGluZS1oZWlnaHQ6IDFweDsgbWF4LWhlaWdodDogMHB4OyBtYXgtd2lkdGg6IDBweDsgb3ZlcmZsb3c6IGhpZGRlbjsgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7Ij4mbmJzcDs8L2Rpdj4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPSJtYXgtd2lkdGg6IDYwMHB4O2JhY2tncm91bmQtY29sb3I6I0ZGRjttYXJnaW4tdG9wOjE1cHg7Ij4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiNGRkY7bWF4LXdpZHRoOjYwMHB4O21hcmdpbi10b3A6MjBweDtib3JkZXItdG9wLWxlZnQtcmFkaXVzOjZweDtib3JkZXItdG9wLXJpZ2h0LXJhZGl1czo2cHg7Ym9yZGVyLWJvdHRvbToxcHggc29saWQgI2YyZjJmMjsiIGJvcmRlcj0iMCIgd2lkdGg9IjEwMCUiIGNlbGxzcGFjaW5nPSIwIiBjZWxscGFkZGluZz0iMCIgYWxpZ249ImxlZnQiIGJnY29sb3I9IiNlYTBiNDQiPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRib2R5Pg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIHN0eWxlPSJ0ZXh0LWFsaWduOiBsZWZ0OyBwYWRkaW5nOiAxNXB4IDE1cHggMTVweCAyNXB4OyBmb250LWZhbWlseTogc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IGJvbGQ7IGNvbG9yOiAjNTI1RjdmOyBmb250LXNpemU6IDMwcHg7Ij4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBocmVmPSIiIHN0eWxlPSJtYXJnaW4tdG9wOiAzMHB4Ij48aW1nIHNyYz0iaHR0cHM6Ly9sb2NhbGhvc3QvY2lfZmFtaWxvdi91cGxvYWRzL2ltYWdlcy9mYW1pbG92LWxvZ28ucG5nIiBhbHQ9IkZhbWlsb3YuY29tIiB3aWR0aD0iMzUlIj48L2E+DQoNCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90Ym9keT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90YWJsZT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHN0eWxlPSJtYXgtd2lkdGg6NTkwcHg7Ym9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czo2cHg7Ym9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6NnB4OyIgYm9yZGVyPSIwIiB3aWR0aD0iMTAwJSIgY2VsbHNwYWNpbmc9IjAiIGNlbGxwYWRkaW5nPSIwIiBhbGlnbj0iY2VudGVyIiBiZ2NvbG9yPSIjRkZGRkZGIj4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0Ym9keT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBib3JkZXI9IjAiIHdpZHRoPSIxMDAlIiBjZWxsc3BhY2luZz0iMCIgY2VsbHBhZGRpbmc9IjAiPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0Ym9keT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIHN0eWxlPSJwYWRkaW5nOiAyMHB4OyBmb250LWZhbWlseTogc2Fucy1zZXJpZjsgbGluZS1oZWlnaHQ6IDI0cHg7IGNvbG9yOiAjNTY2OTg1OyBmb250LXNpemU6IDE1cHg7Ij48cD5IaSA8c3Ryb25nPiA8L3N0cm9uZz48c3Ryb25nPlZpeCBTbWFydDwvc3Ryb25nPiw8L3A+PHA+WW91ciBvcmRlciBoYXMgYmVlbiBzdWNjZXNzZnVsbHkgcmVjZWl2ZWQuPC9wPjxwPjxzdHJvbmc+VFJBTlNBQ1RJT04gQ09ERSA6Ri0xNTI3NDI4OTA0PC9zdHJvbmc+PC9wPjxwPjxzdHJvbmc+RnJvbSZuYnNwOzombmJzcDs8L3N0cm9uZz48c3Ryb25nPlZpeCBTbWFydDwvc3Ryb25nPjwvcD48cD48c3Ryb25nPkZvciA6PC9zdHJvbmc+IDxzdHJvbmc+YXNkPC9zdHJvbmc+PC9wPjxwPjxzdHJvbmc+UGhvbmUgOjwvc3Ryb25nPiA8c3Ryb25nPisxIDIzMjMyMzwvc3Ryb25nPjwvcD48cD48c3Ryb25nPlBheW1lbnQgTWV0aG9kIDogU3RyaXBlPC9zdHJvbmc+PC9wPjxwPjxzdHJvbmc+RGVsaXZlcnkgb3B0aW9uJm5ic3A7OiBQaWNrIHVwIGZyb20gdGhlIHNob3AgPC9zdHJvbmc+PC9wPjxwPjxzdHJvbmc+U2hvcCA6IFJldHJhaXQgZmFtaWxvdiAgQmFmb3Vzc2FtIC0gTWFyY2jDqSBBPC9zdHJvbmc+PC9wPjxwPjxzdHJvbmc+RGV0YWlscyBkZSBsYSBjb21tYW5kZSA6PC9zdHJvbmc+PC9wPjxwPjxkaXYgaWQ9InByaW50YWJsZUFyZWEiPg0KPGRpdiBpZD0iaW52b2ljZSI+DQogICAgICAgICAgICAgICAgPHRhYmxlIGNsYXNzPSJ0YWJsZSB0YWJsZS1ob3ZlciI+DQogICAgICAgICAgICAgICAgICAgICAgICA8dGhlYWQ+DQogICAgICAgICAgICAgICAgICAgICAgICA8dHI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoIGNsYXNzPSJ0ZXh0LWNlbnRlciI+IzwvdGg+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoPjxzdHJvbmc+UGljPC9zdHJvbmc+PC90aD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGg+PHN0cm9uZz5OYW1lPC9zdHJvbmc+PC90aD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGggY2xhc3M9InRleHQtcmlnaHQiPjxzdHJvbmc+cHJpY2U8L3N0cm9uZz48L3RoPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aD48c3Ryb25nPjwvc3Ryb25nPjwvdGg+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoIGNsYXNzPSJ0ZXh0LWNlbnRlciI+PHN0cm9uZz5RdHk8L3N0cm9uZz4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RoPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0aCBjbGFzcz0idGV4dC1yaWdodCI+PHN0cm9uZz5Ub3RhbDwvc3Ryb25nPjwvdGg+DQogICAgICAgICAgICAgICAgICAgICAgICA8L3RyPg0KICAgICAgICAgICAgICAgICAgICAgICAgPC90aGVhZD4NCiAgICAgICAgICAgICAgICAgICAgICAgIDx0Ym9keT4NCiAgICAgICAgICAgIDx0cj4NCiAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LWNlbnRlciI+MTwvdGQ+DQogICAgICAgICAgICAgICAgICAgIDx0ZD48ZGl2DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT0iYmFja2dyb3VuZC1pbWFnZTp1cmwoaHR0cHM6Ly9sb2NhbGhvc3QvY2lfZmFtaWxvdi91cGxvYWRzL3Byb2R1Y3RzLzg5MDE1MTA2MTQwMDAucG5nKTsgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgaGVpZ2h0OjEwMHB4OyB3aWR0aDoxMDBweDsiPjwvZGl2Pg0KPC90ZD4NCiAgICAgICAgICAgICAgICAgICAgPHRkPlNhdm9uIG11bHRpdMOiY2hlIFNBTkVUIC0gMjUwZzwvdGQ+DQogICAgICAgICAgICAgICAgICAgIDx0ZCBjbGFzcz0idGV4dC1yaWdodCI+JmV1cm87MC4zOTwvdGQ+DQogICAgICAgICAgICAgICAgICAgIDx0ZCBjbGFzcz0idGV4dC1jZW50ZXIiPlg8L3RkPg0KICAgICAgICAgICAgICAgICAgICA8dGQgY2xhc3M9InRleHQtY2VudGVyIj4yPC90ZD4NCiAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LXJpZ2h0Ij4mZXVybzswLjc4PC90ZD4NCiAgICAgICAgICAgIDwvdHI+DQogICAgICAgICAgICAgICAgPHRyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZCBjb2xzcGFuPSI1Ij48c3Ryb25nPlN1YiB0b3RhbDwvc3Ryb25nPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LWNlbnRlciI+PGI+MjwvYj48L3RkPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZCBjbGFzcz0idGV4dC1yaWdodCINCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9ImNvbG9yOiMzNmNkNzUgO2ZvbnQtd2VpZ2h0OmJvbGQ7Ij4mZXVybzswLjc4PC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+PHRyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZCBjb2xzcGFuPSI1Ij48c3Ryb25nPlNlcnZpY2UgY2hhcmdlPC9zdHJvbmc+PC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQgY2xhc3M9InRleHQtY2VudGVyIj48Yj48L2I+PC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQgY2xhc3M9InRleHQtcmlnaHQiPiZldXJvOzIuNTA8L3RkPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD48L3RkPg0KICAgICAgICAgICAgICAgICAgICAgICAgPC90cj48dHI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNvbHNwYW49IjUiPjxzdHJvbmc+UGljayB1cCBmcm9tIHRoZSBzaG9wIDwvc3Ryb25nPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LWNlbnRlciI+PGI+PC9iPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LXJpZ2h0Ij4mZXVybzswLjAwPC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+PHRyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZCBjb2xzcGFuPSI1Ij48c3Ryb25nPlRvdGFsIFRUQzwvc3Ryb25nPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LWNlbnRlciI+PGI+PC9iPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkIGNsYXNzPSJ0ZXh0LXJpZ2h0Ij48c3Ryb25nIGlkPSJncmFuZHByaWNlIiBzdHlsZT0iY29sb3I6IzAwMDtmb250LXNpemU6MjBweCI+JmV1cm87My4yODwvc3Ryb25nPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPjwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICA8L3RyPjwvdGJvZHk+DQogICAgICAgICAgICAgICAgICAgIDwvdGFibGU+DQogICAgICAgICAgICAgICAgICAgPC9kaXY+DQogICAgICAgICAgICAgICAgPC9kaXY+PC9wPjxwPlJlZ2FyZHMsPC9wPjxwPlRlYW0gRkFNSUxPVjwvcD48L3RkPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRkPiZuYnNwOzwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdHI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90Ym9keT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGFibGU+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGQ+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90cj4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGJvZHk+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGFibGU+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRhYmxlIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiNGNkY5RkM7bWF4LXdpZHRoOjYwMHB4OyIgYm9yZGVyPSIwIiB3aWR0aD0iMTAwJSIgY2VsbHNwYWNpbmc9IjAiIGNlbGxwYWRkaW5nPSIwIiBhbGlnbj0ibGVmdCIgYmdjb2xvcj0iI2VhMGI0NCI+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGJvZHk+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgDQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgDQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZCBzdHlsZT0icGFkZGluZzoyMHB4O3dpZHRoOjUwJTtmb250LXNpemU6MTFweDtmb250LWZhbWlseTpzYW5zLXNlcmlmO2xpbmUtaGVpZ2h0OjE5cHg7dGV4dC1hbGlnbjpsZWZ0O2NvbG9yOiM4ODk4YWE7bWluLWhlaWdodDoxMDBweDsiPjIwMTctMjAxOCDCqSBGYW1pbG92LmNvbSA8L3RkPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQgc3R5bGU9InBhZGRpbmc6MjBweDt3aWR0aDo1MCU7Zm9udC1zaXplOjExcHg7Zm9udC1mYW1pbHk6c2Fucy1zZXJpZjtsaW5lLWhlaWdodDoxOXB4O3RleHQtYWxpZ246cmlnaHQ7Y29sb3I6Izg4OThhYTsiPk1hZGUgd2l0aCAmIzEwMDg0OyBpbSBTYWFybGFuZDwvdGQ+DQoNCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPg0KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90Ym9keT4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGFibGU+DQoNCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2NlbnRlcj4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90ZD4NCiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RyPg0KICAgICAgICAgICAgICAgICAgICAgICAgPC90Ym9keT4NCiAgICAgICAgICAgICAgICAgICAgPC90YWJsZT4NCiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz0ieWo2cW8iPiZuYnNwOzwvZGl2Pg0KICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPSJhZEwiPiZuYnNwOzwvZGl2Pg0KICAgICAgICAgICAgICAgICAgICA8L2Rpdj4NCiAgICAgICAgICAgICAgICA8L2JvZHk+DQogICAgICAgICAgICAgICAgPC9odG1sPg=='));
        /*$order_response = $this->order_model->get_order_by_id(129, 3482);
        $order_detail_array['order_info'] = $order_response;
        $order_detail_response = $this->order_model->get_order_detail_by_id(129, 3482);
        $order_detail_array['order_detail'] = $order_detail_response;
        $customer_info = $this->customer_model->get_info_by_id($this->customer_id);
        $order_detail_array['customer'] = $customer_info;
        #_pre($customer_info,0);
        #_pre($order_detail_array['order_info']->receiver_name,0);
        #_pre($order_detail_array);
        $html = $this->detail_view($order_detail_array);
            #_pre($html );
        $mail_content_data = new stdClass();
        $mail_content_data->var_user_name = 'Test';
        $mail_content_data->var_user_full_name = ' Test M';
        $mail_content_data->var_email = 'parmarvikrantr@gmail.com   ';
        $mail_content_data->var_order_code = 'F-12121212';
        $mail_content_data->var_reciever_name = 'R Name';
        $mail_content_data->var_reciever_phone_number = '+91 1111';
        $mail_content_data->var_payment_method = 'Stripe';
        $mail_content_data->var_delivery_option = 'From Shope';
        $mail_content_data->var_shop_name = 'Abc Shope';
        $mail_content_data->var_order_detail = $html;
        $mail_data = email_template('ORDER_SUCCESSFULLY_RECEIVED', $mail_content_data, MAIN_URL);
        #_pre($mail_data);

        if (is_object($mail_data)) {
            $to = MAIL_TEST_EMAIL;
            $user_name = 'vixsmart';
            $this->load->library('mail');
            $this->mail->setTo($to, $user_name);
            $this->mail->setCc(ADMIN_EMAIL, 'Familov');
            $this->mail->setSubject($mail_data->email_subject);
            $this->mail->setBody(_header_footer($mail_data->email_html));
            $status = $this->mail->send();
           # $this->mail->clear(TRUE);
            _e($status." =>".$to,0);
        }*/
       /* if (1) {
            $to = ADMIN_EMAIL;
            $user_name = 'Familov.com';
            if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                #$to = MAIL_TEST_EMAIL;
            } else {
            }
            if (is_object($mail_data)) {
                #_pre($to . " => ".$user_name,0);
                #$this->email->clear();
                $CI =& get_instance();
                $CI->load->library('mail');
                #$this->load->library('mail');
                $CI->mail->setTo($to, $user_name);
                $CI->mail->setSubject($mail_data->email_subject);
                $CI->mail->setBody(_header_footer($mail_data->email_html));
                $status = $CI->mail->send();
                _e($status." =>".$to,0);
            }
        }*/
       # _e();
    }

    public function index()
    {
        redirect(base_url('/'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function product_list($country_id = '', $city_id = '', $shop_id = '', $category = '', $search = '')
    {
        if ($country_id == '' || $city_id == '' || $shop_id == '') {
            redirect(base_url('/'));
        }
        if (!is_numeric($category)) {
            $category = '';
        }
        $this->data['country_id'] = $country_id;
        $this->data['city_id'] = $city_id;
        $this->data['shop_id'] = $shop_id;
        $this->data['category'] = $category;

        $order_info['order_info'] = new stdClass();
        $order_info['order_info']->country_id = $country_id;
        $order_info['order_info']->city_id = $city_id;
        $order_info['order_info']->shop_id = $shop_id;
        #_pre($order_info,0);
        //_pre(@$this->session->userdata('cart'));
        //var_dump(@$this->session->userdata('cart'));exit;
        if (@$this->session->userdata('order_info')->shop_id ) {
            if (@$this->session->userdata('order_info')->shop_id != $shop_id && (@$this->session->userdata('cart') != NULL && count(@$this->session->userdata('cart') > 0))) {
                redirect(base_url(MENU_CHANGE_SHOP. "/" . $country_id . "/" . $city_id . "/" . $shop_id));
                exit;
            }
        } else {
            $this->session->set_userdata($order_info);
        }
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_TITLE');
        $category_response = $this->category_model->get_category_list_shop_wise($shop_id);
        $this->data['category_list'] = $category_response;
        $shop_response = $this->shop_model->get_info_by_id($shop_id);
        $this->data['shop_info'] = $shop_response;
        #_pre($_REQUEST);
        /* Product List */
        $search_keyword = '';
        if (count($this->input->post()) > 0) {
            $search_keyword = $this->input->post('search_header');
        }
        $this->data['search_keyword'] = $search_keyword;
        $count = count($this->product_model->product_list_shop_wise($shop_id, $category, $search_keyword));
        if (1) {
            $product_response = $this->product_model->product_list_shop_wise($shop_id, $category, $search_keyword, $this->perPage, 0);
            #_pre($product_response);
            /*if (count($product_response) > 0) {
                foreach ($product_response as $k => $v) {
                    $product_response[$k]->img = array();
                }
            }*/
            $this->data['product_response'] = $product_response;
        }
        $this->data['tot'] = $count;
        $this->data['perPage'] = $this->perPage;
        /* Product List End */
        #_pre($product_response);
        #_set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_LOGOUT_SUCCESS'),FLASH_HTML);
        $this->render(PRODUCT_D . PRODUCT_LISTING_V);
    }
    public function listing()
    {
        $shop_id = $this->input->post("shop_id");
        $page = $this->input->post("page");
        $category = $this->input->post("category");
        $search_keyword = $this->input->post('search_keyword');
        if ($page != '') {
            $start = ceil($page * $this->perPage);
            $product_response = $this->product_model->product_list_shop_wise($shop_id, $category, $search_keyword, $this->perPage, $start);
            # _pre($product_response);
            /*if (count($product_response) > 0) {
                foreach ($product_response as $k => $v) {
                    $product_response[$k]->img = array();
                }
            }*/
            $data['product_response'] = $product_response;
            $result = $this->load->view(PRODUCT_D.'/_list', $data, true);
            $res_array = array('html' => $result);
            echo json_encode($res_array);
            exit;
        }
    }

    public function product_search()
    {
        $shop_id = $this->input->post("shop_id");
        $category = $this->input->post("category");
        $keyword = $this->input->post("keyword");
        if ($keyword != '') {
            $product_response = $this->product_model->product_search_keyword_wise($shop_id, $keyword, $category);
            $data['product_response'] = $product_response;
            $result = $this->load->view(PRODUCT_D . '/_search', $data, true);
            $res_array = array('html' => $result);
            echo json_encode($res_array);
            exit;
        } else {
            $res_array = array('html' => '');
            echo json_encode($res_array);
            exit;
        }
    }

    public function product_detail($product_id = '', $shop_id = '')
    {
        #$cart_array = @$this->session->userdata('cart');
        #_pre($cart_array);
        if ($product_id == '' || $shop_id == '') {
            redirect(base_url('/'));
        } else {
            $category_response = $this->category_model->get_category_list_shop_wise($shop_id);
            $this->data['category_list'] = $category_response;
            $shop_response = $this->shop_model->get_info_by_id($shop_id);
            $this->data['shop_info'] = $shop_response;
            $product_response = $this->product_model->get_product_info_by_id($product_id, 'PUBLISHED'); #1 for publish product
            $this->data['product_response'] = $product_response;
            #_pre($product_response);
        }
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_DETAIL_TITLE');
        $this->render(PRODUCT_D . PRODUCT_DETAIL_V);
    }

    public function view_cart()
    {
        $cart_array = @$this->session->userdata('cart');
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CART_TITLE');
        $this->data['cart'] = $cart_array;
        $this->render(PRODUCT_D . PRODUCT_CART_V);
    }

    public function update_cart()
    {
        if (count($this->input->post()) > 0) {
            $qty_array = $this->input->post('qty');
            if (count($qty_array) > 0) {
                $this->load->library('session');
                $cart_array = @$this->session->userdata('cart');
                if (count($cart_array) > 0) {
                    foreach ($cart_array as $k => $v) {
                        $new_qty = $qty_array[$k];
                        $cart_array[$k]['quantity'] = $qty_array[$k];
                    }
                    $cart['cart'] = $cart_array;
                    $this->session->set_userdata($cart);
                }
                _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CART_17'));
            }
            redirect(base_url(MENU_VIEW_CART));
        }
    }

    public function delete_cart_item($product_id = 0)
    {
        if ($product_id == '' || $product_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CART_20'));
            redirect(base_url(MENU_VIEW_CART));
        } else {
            $cart_array = @$this->session->userdata('cart');
            if (count($cart_array) > 0) {
                unset($cart_array[$product_id]);
                $cart['cart'] = $cart_array;
                $this->session->set_userdata($cart);
            }
            _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_PRODUCT_CART_21'));
        }
        redirect(base_url(MENU_VIEW_CART));
    }

    public function shop_change($country_id = '', $city_id = '', $shop_id = '')
    {
        $this->data['country_id'] = $country_id;
        $this->data['city_id'] = $city_id;
        $this->data['shop_id'] = $shop_id;
        //_pre(@$this->session->userdata('order_info')->shop_id);
        $this->render(PRODUCT_D . MENU_WARNING_VIEW);

    }
    public function shop_change_page()
    {
        $this->unset_cart();
        _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CART_ADD_FAILED'));
        echo true;
        exit;
        //redirect(base_url(MENU_VIEW_CART));
    }

    public function unset_cart()
    {
        $this->session->unset_userdata('order_info');
        $this->session->unset_userdata('cart');
        $this->session->unset_userdata('delivery_info');
        $this->session->unset_userdata('promo_code');
        $this->session->unset_userdata('receiver_info');
    }

    public function add_to_cart()
    {
        #_pre($this->input->post());
        #_e($this->customer_id ,0);
        $this->load->library('session');
        #$this->session->unset_userdata('991');
        #$this->session->unset_userdata('cart');
        #$product_id = 1397;
        #$product_id = 991;
        $product_id = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $session_shop_id = @$this->session->userdata('order_info')->shop_id;
        $product_response = $this->product_model->get_product_info_by_id($product_id, 'PUBLISHED');
        # _pre($product_response);
        $cart['cart'] = array();
        if (count($product_response) > 0) {
            $product_name = $product_response->product_name;
            $cart_array = @$this->session->userdata('cart');
            if (count($cart_array) > 0) {
                # _e("in if ",0);
                #foreach($cart_array as $k=>$v){
                if (array_key_exists($product_id, $cart_array)) {
                    $qty = $cart_array[$product_id]['quantity'] + $qty;
                    $cart_array[$product_id]['quantity'] = $qty;
                } else {
                    $item['customer_id'] = $this->customer_id;
                    $item['ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $item['product_id'] = $product_id;
                    $item['product_name'] = @$product_response->product_name;
                    $item['product_image'] = @$product_response->product_image;
                    if (@$product_response->special_product_prices > 0) {
                        $price = convert_price(@$product_response->special_product_prices);
                        $price_temp = @$product_response->special_product_prices;
                    } else {
                        $price = convert_price(@$product_response->product_prices);
                        $price_temp = @$product_response->product_prices;
                    }
                    $item['price'] = $price;
                    $item['price_temp'] = $price_temp;
                    $item['quantity'] = $qty;
                    $item['random_rick'] = random_string(10);
                    $item['country_id'] = @$this->session->userdata('order_info')->country_id;
                    $item['city_id'] = @$this->session->userdata('order_info')->city_id;
                    $item['shop_id'] = $session_shop_id;
                    $cart_array[$product_id] = $item;
                }
                #}
                $cart['cart'] = $cart_array;
            } else {
                $item['customer_id'] = $this->customer_id;
                $item['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $item['product_id'] = $product_id;
                $item['product_name'] = @$product_response->product_name;
                $item['product_image'] = @$product_response->product_image;
                if (@$product_response->special_product_prices > 0) {
                    $price = convert_price(@$product_response->special_product_prices);
                    $price_temp = @$product_response->special_product_prices;
                } else {
                    $price = convert_price(@$product_response->product_prices);
                    $price_temp = @$product_response->product_prices;
                }
                $item['price'] = $price;
                $item['price_temp'] = $price_temp;
                $item['quantity'] = $qty;
                $item['random_rick'] = random_string(10);
                $item['country_id'] = @$this->session->userdata('order_info')->country_id;
                $item['city_id'] = @$this->session->userdata('order_info')->city_id;
                $item['shop_id'] = $session_shop_id;
                $cart['cart'][$product_id] = $item;
            }
            $msg = str_replace("#ITEM#", @$product_name, $this->lang->line('MSG_PRODUCT_CART_ADD_SUCCESS'));
            $this->session->set_userdata($cart);
            $cart_total_item = count(@$this->session->userdata('cart'));
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $msg, 'cart_total_item' => $cart_total_item));
        } else {
            $cart_total_item = count(@$this->session->userdata('cart'));
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_PRODUCT_CART_ADD_FAILED'), 'cart_total_item' => $cart_total_item));
        }
        exit;
    }

    public function checkout()
    {
        #$this->session->unset_userdata('page');
        #var_dump($this->session->userdata('page'));
        if ($this->session->userdata('page') != NULL) {
        }
        $cart_array = @$this->session->userdata('cart');
        $this->data['cart'] = $cart_array;
        if (count($cart_array) == 0) {
            _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CHECKOUT_02'));
            redirect(base_url('/'));
            exit;
        } else {
            $shop_id = $this->session->userdata('order_info')->shop_id;
            $shop_response = $this->shop_model->get_info_by_id($shop_id);
            $this->data['shop_info'] = $shop_response;
            $delivery_info['delivery_info'] = new stdClass();
            $promo_code_array['promo_code'] = new stdClass();
            if (!$this->session->userdata('delivery_info')) {
                if (count($shop_response) > 0) {
                    $delivery_info['delivery_info']->delivery_mode = 'free_delivery';
                    $delivery_info['delivery_info']->delivery_price = $shop_response->pickup_from_shop_price;
                    $delivery_info['delivery_info']->delivery_name = $this->lang->line('MSG_PRODUCT_CHECKOUT_05');
                    $this->session->set_userdata($delivery_info);
                }
            }
            if (!$this->session->userdata('promo_code')) {
                $promo_code_array['promo_code']->promo_code = '';
                $promo_code_array['promo_code']->promo_id = '';
                $promo_code_array['promo_code']->promo_code_amount = 0;
                $this->session->set_userdata($promo_code_array);
            }
            #$this->session->unset_userdata('delivery_info');
            $this->data['delivery_info'] = $this->session->userdata('delivery_info');
            $this->data['promo_code'] = $this->session->userdata('promo_code');
            $promo_amount = $this->data['promo_code']->promo_code_amount;
            #_e($price ,0);_e($quantity,0);_e(convert_price($price)*$quantity,0);
            #_pre($cart_array,0);
            if (!@$this->is_login) {
                $last_url['page'] = base_url(MENU_CHECKOUT);
                $this->session->set_userdata($last_url);
            } else {
                $this->session->unset_userdata('page');
                #$last_url['page'] = '';
                #$this->session->set_userdata($last_url);
            }
            #_pre($this->session->userdata('page'));
            #_pre($cart_array['1284']['price_temp']);
            $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CHECKOUT_TITLE');
            $this->render(PRODUCT_D . PRODUCT_CHECKOUT_V);
        }
    }

    public function order_delivery()
    {
        $this->load->library('session');
        $delivery_info['delivery_info'] = new stdClass();
        $shop_id = $this->session->userdata('order_info')->shop_id;
        $shop_response = $this->shop_model->get_info_by_id($shop_id);
        if (count($this->input->post()) > 0) {
            #TODO Delivery Mode
            $delivery_option = $this->input->post('delivery_option');
            if ($delivery_option != '') {
                if ($delivery_option == 'home_delivery' && $shop_response->home_delivery_status == 'Activated') {
                    $delivery_info['delivery_info']->delivery_mode = 'home_delivery';
                    $delivery_info['delivery_info']->delivery_price = $shop_response->home_delivery_price;
                    $delivery_info['delivery_info']->delivery_name = $this->lang->line('MSG_PRODUCT_CHECKOUT_06');
                } else {
                    $delivery_info['delivery_info']->delivery_mode = 'free_delivery';//$delivery_option;
                    $delivery_info['delivery_info']->delivery_price = $shop_response->pickup_from_shop_price;
                    $delivery_info['delivery_info']->delivery_name = $this->lang->line('MSG_PRODUCT_CHECKOUT_05');
                }
                $this->session->set_userdata($delivery_info);
                _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CHECKOUT_09') . "<br> (" . $delivery_info['delivery_info']->delivery_name . ")");
            }
        }
        redirect(base_url(MENU_CHECKOUT));
    }

    public function order_promo_code()
    {
        $this->load->library('session');
        $promo_code_array['promo_code'] = new stdClass();
        if (count($this->input->post()) > 0) {
            $promo_code = $this->input->post('promo_code');
            $promo_response = $this->promotion_model->get_info_by_code($promo_code);
            #_pre($promo_response);
            if (count($promo_response) == 0) {
                $promo_code_array['promo_code']->promo_code = $promo_code;
                $promo_code_array['promo_code']->promo_id = '';
                $promo_code_array['promo_code']->promo_code_amount = 0;
                _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_11'), FLASH_HTML);
            } else {
                $amount = $promo_response->iAmount;
                $amount = convert_price($amount);
                $cart_array = @$this->session->userdata('cart');
                $cart = $cart_array;
                $cart_amount_total = 0.00;
                foreach (@$cart as $k => $v) {
                    $cart_amount_total += ($v['price_temp'] * $v['quantity']);
                }
                $delivery_info = $this->session->userdata('delivery_info');
                $cart_amount_total = $cart_amount_total + SERVICE_FEES + @$delivery_info->delivery_price; #TODO add all charges
                $cart_amount_total = convert_price($cart_amount_total);
                if ($cart_amount_total < $amount) {
                    $promo_code_array['promo_code']->promo_code = $promo_code;
                    $promo_code_array['promo_code']->promo_id = '';
                    $promo_code_array['promo_code']->promo_code_amount = 0;
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_13'), FLASH_HTML);
                } else {
                    $promo_code_array['promo_code']->promo_code = $promo_response->vCode;
                    $promo_code_array['promo_code']->promo_id = $promo_response->promo_id;
                    $promo_code_array['promo_code']->promo_code_amount = $promo_response->iAmount;
                    _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_PRODUCT_CHECKOUT_12'));
                }
            }
            $this->session->set_userdata($promo_code_array);
            #TODO Promo code
        }
        redirect(base_url(MENU_CHECKOUT));
    }

    public function checkout_proceed() #TODO unset order session
    {
        #_pre($_REQUEST);
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        if (count($this->input->post()) > 0) {
            $payment_type = $this->input->post('payment_type');
            #$payment_type = PAYMENT_SEPA_DD;
            #_e($payment_type);
            #_pre($_REQUEST,0);
            $this->form_validation->set_rules('recp_name', 'Recipient Number', 'trim|required');
            $this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
            $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
            if ($payment_type == PAYMENT_SEPA_DD) {
                $this->form_validation->set_rules('iban_no', 'IBAN Number', 'trim|required');
            }
            $customer_info = $this->customer_model->get_info_by_id($this->customer_id);
            if ($this->form_validation->run() == FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors);
                redirect(base_url(MENU_CHECKOUT));
            } else {
                #_e("===>".$payment_type);
                $cart = @$this->session->userdata('cart');
                $delivery_info = @$this->session->userdata('delivery_info');
                $promo_code = @$this->session->userdata('promo_code');
                $cart_total_item_qty = 0;
                $cart_amount_total = 0.00;
                $service_fees = convert_price(SERVICE_FEES);
                foreach (@$cart as $k => $v) {
                    $cart_total_item_qty += $v['quantity'];
                    $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                    $cart_amount_total = $cart_amount_total + $product_price;
                }
                $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - (convert_price(@$promo_code->promo_code_amount));
                #TODO order table
                if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
                    $ip_address = '127.0.0.1';
                    #$ip_address = $_SERVER['REMOTE_ADDR'];
                } else {
                    $ip_address = $_SERVER['REMOTE_ADDR'];
                }
                $phone_no = $this->input->post('phone_code') . " " . $this->input->post('phone_number');
                $order_array = array(
                    'customer_id' => $this->customer_id,
                    'ip_address' => $ip_address, #TODO IP Address
                    'random_rick' => 'expire',
                    'generate_code' => ORDER_TEXT . time(),
                    #'generate_code' => ORDER_TEXT . random_string('numeric', 10),
                    'order_status' => ORDER_STATUS_WAIT,
                    'order_view' => 1,
                    'country_id' => @$this->session->userdata('order_info')->country_id,
                    'city_id' => @$this->session->userdata('order_info')->city_id,
                    'shop_id' => @$this->session->userdata('order_info')->shop_id,
                    'contact_id' => 0,
                    'receiver_name' => addslashes($this->input->post('recp_name')),
                    'greeting_msg' => addslashes($this->input->post('greeting_msg')),
                    'receiver_phone' => $phone_no,
                    'payment_method' => $payment_type,
                    'date_payment' => date('Y-m-d'),
                    'discount' => 0,
                    'welcome_discount' => 0,
                    'promo_id' => @$promo_code->promo_id,
                    'service_tax' => $service_fees,
                    'delivery_option' => @$delivery_info->delivery_name,
                    'delivery_price' => convert_price(@$delivery_info->delivery_price),
                    'payment_curr' => $this->currency_code,
                    'payment_curr_code' => $this->currency_symbol,
                    'total_order' => $cart_amount_total,
                    'sub_total' => ($cart_amount_total),
                    'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                    'promo_discount_amount' => convert_price(@$promo_code->promo_code_amount),
                    'grand_total' => ($grand_total),
                    'promo_code' => @$promo_code->promo_code,
                );
                #_pre($order_array);
                $order_confirmation['order_confirmation'] = new stdClass();
                $order_confirmation['order_confirmation']->payment_type = $payment_type;
                $receiver_info['receiver_info'] = new stdClass();
                $receiver_info['receiver_info']->receiver_name = addslashes($this->input->post('recp_name'));
                $receiver_info['receiver_info']->receiver_phone = $phone_no;
                $receiver_info['receiver_info']->greeting_msg = addslashes($this->input->post('greeting_msg'));
                $this->session->set_userdata($receiver_info);
                #_pre($payment_type);
                if ($payment_type == PAYMENT_BANK_TRANSFER) {
                    $insert_response = $this->dbcommon->insert('order', $order_array);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        foreach (@$cart as $k => $v) {
                            $product_unit_price = convert_price($v['price_temp']);
                            $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                            $cart_item = array(
                                'order_id' => $insert_id,
                                'product_id' => $k,
                                'product_prize' => $product_unit_price,
                                'total_prize' => $product_all_unit_price,
                                'quantity' => $v['quantity'],
                            );
                            $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                        }
                        $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                        #$this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        redirect(base_url(MENU_ORDER_CONFIRMATION));
                    } else {
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        redirect(base_url(MENU_ORDER_CONFIRMATION));
                    }
                } else if ($payment_type == PAYMENT_STRIPE) {
                    #_e(APPPATH.'libraries/stripe_config.php');
                    $base_url = base_url();
                    #_pre($base_url);
                    if (!empty($_POST['stripeToken'])) { //  Si payment stripe
                        $stripeerror = '';
                        try {
                            #require_once(APPPATH.'libraries/stripe_config.php');
                            require_once APPPATH . "third_party/stripe/init.php";
                            $this->ci = &get_instance();
                            $this->config = $this->ci->config->config['stripe'];
                            $mode = $this->config['mode'];
                            $sk_key = $this->config['sk_' . $mode];
                            #_e($sk_key);
                            \Stripe\Stripe::setApiKey($sk_key);

                            $stripeToken = $_POST['stripeToken'];
                            $stripeEmail = strip_tags(trim($_POST['stripeEmail']));
                            #if ($customer_info->stripe_id == '') {
                            $customer = \Stripe\Customer::create(array(
                                    'source' => $stripeToken,
                                    'email' => $stripeEmail,
                                    "description" => $this->user_name,
                                )
                            );
                            $customerId = $customer->id;
                            #TODO customer id update.
                            $crud_data_customer = array('stripe_id' => $customerId);
                            $where = array('customer_id' => $this->customer_id);
                            $update_response = $this->dbcommon->update('customers', $where, $crud_data_customer);
                            /*} else {
                                $customerId = $customer_info->stripe_id;
                            }*/
                            // Charge the Customer instead of the card
                            $charge = \Stripe\Charge::create(array(
                                "amount" => ($grand_total) * 100, // amount in cents, again
                                "currency" => $this->currency_code,
                                "customer" => $customerId
                            ));
                            #_pre($charge);
                            if ($charge->id) {
                                $order_array['stripe_token'] = $charge->id;
                                $insert_response = $this->dbcommon->insert('order', $order_array);
                                $insert_id = $this->db->insert_id();
                                if ($insert_response) {
                                    foreach (@$cart as $k => $v) {
                                        $product_unit_price = convert_price($v['price_temp']);
                                        $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                                        $cart_item = array(
                                            'order_id' => $insert_id,
                                            'product_id' => $k,
                                            'product_prize' => $product_unit_price,
                                            'total_prize' => $product_all_unit_price,
                                            'quantity' => $v['quantity'],
                                        );
                                        $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                                    }
                                    $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                                    #$this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                                    $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                                    $order_confirmation['order_confirmation']->order_info = $order_array;
                                    $this->session->set_userdata($order_confirmation);
                                    redirect($base_url . MENU_ORDER_CONFIRMATION);
                                    exit;
                                } else {
                                    $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                                    $order_confirmation['order_confirmation']->order_info = $order_array;
                                    $this->session->set_userdata($order_confirmation);
                                    redirect(base_url(MENU_ORDER_CONFIRMATION));exit;
                                }
                            } else {
                                _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_37'), FLASH_HTML);
                                redirect($base_url . MENU_CHECKOUT);
                                exit;
                                #$data['stripstatus'] = 0;
                                #$data['stripeerror'] = "Something went wrong ! Please try again.";
                            }
                        } catch (Stripe_CardError $e) {
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_InvalidRequestError $e) {
                            // Invalid parameters were supplied to Stripe's API
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_AuthenticationError $e) {
                            // Authentication with Stripe's API failed
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_ApiConnectionError $e) {
                            // Network communication with Stripe failed
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_Error $e) {
                            // Display a very generic error to the user, and maybe send
                            // yourself an email
                            $stripeerror = $e->getMessage();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe
                            $stripeerror = $e->getMessage();
                        }
                        if ($stripeerror != '') {
                            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_39') . $stripeerror, FLASH_HTML);
                            redirect($base_url . MENU_CHECKOUT);
                            exit;
                        }
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
                        redirect($base_url . MENU_CHECKOUT);
                        exit;
                    }
                } else if ($payment_type == PAYMENT_PAYPAL) {
                    $base_url = base_url();
                    $this->ci = &get_instance();
                    $this->config = $this->ci->config->config['paypal'];
                    $mode = $this->config['mode'];
                    $business_id = $this->config['business_id'];
                    $url = $this->config['url_' . $mode];
                    /*<input type="text" name="shipping_1" value="' . convert_price(@$delivery_info->delivery_price) . '">*/
                    $discount = convert_price(@$promo_code->promo_code_amount);
                    echo ' <div style="display:none;"> <form action="' . $url . '" method="post" id="paypal_form">';
                    echo '
                        <input type="text" name="business" value="' . $business_id . '">
                        <input type="text" name="cpp_logo_image" value="https://familov.com/images/familov-logo.png">
                        <input type="text" name="image_url" value="https://familov.com/images/familov-logo.png">
                        <input type="text" name="cmd" value="_cart">
                        <input type="text" name="upload" value="1">';
                    $ctr_cart = 1;
                    foreach (@$cart as $k => $v) {
                        $qty = $v['quantity'];
                        $name = $v['product_name'];
                        $product_unit_price = convert_price($v['price_temp']);
                        $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                        echo '
                                <input type="text" name="item_number_' . $ctr_cart . '" value="' . $k . '">
                                <input type="text" name="item_name_' . $ctr_cart . '" value="' . $name . '">
                                <input type="text" name="amount_' . $ctr_cart . '" value="' . $product_unit_price . '">
                                <input type="text" name="quantity_' . $ctr_cart . '" value="' . $qty . '">';
                        $ctr_cart++;
                    }
                    echo '
                    <input type="text" name="item_name_' . $ctr_cart . '" value="Shipping(' . trim(@$delivery_info->delivery_name) . ')">
                                <input type="text" name="amount_' . $ctr_cart . '" value="' . convert_price(@$delivery_info->delivery_price) . '">
                                <input type="text" name="quantity_' . $ctr_cart . '" value="1">';
                    $ctr_cart++;
                    echo '
                    <input type="text" name="item_name_' . $ctr_cart . '" value="Service fees">
                    <input type="text" name="amount_' . $ctr_cart . '" value="' . @$service_fees . '">
                    <input type="text" name="quantity_' . $ctr_cart . '" value="1">

                        <input type="text" name="shipping2" value="0.00">
                        <input type="text" name="handling_cart" value="0.00">
                        <input type="text" name="discount_amount_cart" value="' . $discount . '">
                        <input type="text" name="currency_code" value="' . $this->currency_code . '">
                        <input type="image" name="submit"
                            src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_addtocart_120x26.png"
                        alt="Checkout">
                        <img alt="" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

                         <input type="hidden" id="paypal_return" style="width:100%;" name="return"
                                   value="' . $base_url . MENU_PAYPAL_RETURN . '/S">
                            <input type="hidden" name="cancel_return" id="cancel_return"
                                   value="' . $base_url . MENU_PAYPAL_RETURN . '/F">
                        ';
                    echo '</form> </div>
                        <script>
                        setTimeout(function(){document.getElementById("paypal_form").submit(); }, 1000);
                    </script>
                    ';
                } else if ($payment_type == PAYMENT_SEPA_DD) {
                    $base_url = base_url();
                    $stripeerror = '';
                    try {
                        require_once APPPATH . "third_party/stripe/init.php";
                        //require_once APPPATH . "third_party/stripe-php/vendor/autoload.php";
                        $this->ci = &get_instance();
                        $this->config = $this->ci->config->config['stripe'];
                        $mode = $this->config['mode'];
                        $sk_key = $this->config['sk_' . $mode];
                        #_e($sk_key);
                        \Stripe\Stripe::setApiKey($sk_key);
                        #$customerId = 'cus_BRmqzixkzCPokF';//$customer_info->stripe_id;
                        #_pre($customerId,0);
                        $iban_no = $this->input->post('iban_no');//$_POST['iban_no'];
                        #_pre($iban_no,0);
                        $source_1 = \Stripe\Source::create(array(
                            "type" => "sepa_debit",
                            "sepa_debit" => array("iban" => $iban_no),
                            "currency" => $this->currency_code,
                            "owner" => array(
                                "name" => $this->user_name,
                                "email" => $this->email,
                            )));

                        //var_dump($source_1);
                        //echo "<pre> SOURCE1 => "; print_r($source_1);
                        $customer = \Stripe\Customer::create(array(
                                'source' => $source_1->id,
                                'email' => $this->email,
                                "description" => $this->user_name,
                            )
                        );
                        $customerId = $customer->id;
                        #TODO customer id update.
                        $crud_data_customer = array('stripe_id' => $customerId);
                        $where = array('customer_id' => $this->customer_id);
                        $update_response = $this->dbcommon->update('customers', $where, $crud_data_customer);
			//var_dump($update_response);
                        $charge = Stripe\Charge::create(array(
                            "amount" => ($grand_total) * 100,
                            "currency" => $this->currency_code,
                            "customer" => $customerId,
                            "source" => $source_1->id,//"src_1CTQZEDPpyW76gxwyLeonxbw",
                        ));
                        //echo "<pre> Charge=> "; print_r($charge);exit;
                        if ($charge->id) {
                            $order_array['stripe_token'] = $charge->id;
                            $insert_response = $this->dbcommon->insert('order', $order_array);
                            $insert_id = $this->db->insert_id();

                            if ($insert_response) {
                                foreach (@$cart as $k => $v) {
                                    $product_unit_price = convert_price($v['price_temp']);
                                    $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                                    $cart_item = array(
                                        'order_id' => $insert_id,
                                        'product_id' => $k,
                                        'product_prize' => $product_unit_price,
                                        'total_prize' => $product_all_unit_price,
                                        'quantity' => $v['quantity'],
                                    );
                                    $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                                }
                                $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                              #  $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                                $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                                $order_confirmation['order_confirmation']->order_info = $order_array;
                                $this->session->set_userdata($order_confirmation);
                                $base_url = MAIN_URL;
                                #_pre($base_url);
                                redirect(MAIN_URL . MENU_ORDER_CONFIRMATION);
                                exit;
                            } else {
                                $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                                $order_confirmation['order_confirmation']->order_info = $order_array;
                                $this->session->set_userdata($order_confirmation);
                                redirect(base_url(MENU_ORDER_CONFIRMATION)); exit;
                            }
                        } else {
                            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_37'), FLASH_HTML);
                            redirect($base_url . MENU_CHECKOUT);
                            exit;
                        }
                    } catch (Stripe_CardError $e) {
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_InvalidRequestError $e) {
                        // Invalid parameters were supplied to Stripe's API
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_AuthenticationError $e) {
                        // Authentication with Stripe's API failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_ApiConnectionError $e) {
                        // Network communication with Stripe failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_Error $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $stripeerror = $e->getMessage();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe
                        $stripeerror = $e->getMessage();
                    }
                    if ($stripeerror != '') {
                        _set_flashdata(FLASH_STATUS_ERROR, str_replace("sepa_debit",PAYMENT_SEPA_DD,$stripeerror), FLASH_HTML);
                        redirect($base_url . MENU_CHECKOUT);
                        exit;
                    }
                } else if ($payment_type == PAYMENT_SOFORT) {
                    $base_url = base_url();
                    $stripeerror = '';
                    try {
                        require_once APPPATH . "third_party/stripe/init.php";
                        $this->ci = &get_instance();
                        $this->config = $this->ci->config->config['stripe'];
                        $mode = $this->config['mode'];
                        $sk_key = $this->config['sk_' . $mode];//'sk_test_PjXblK5gvDtXKbN0LAm9c5kS';
                        #_e($sk_key);
                        \Stripe\Stripe::setApiKey($sk_key);

                        $country = $this->input->post('bank_country');
                        #_pre($iban_no,0);
                        $source_1 = \Stripe\Source::create(array(
                            "type" => "sofort",
                            "sofort" => array('country' => $country),
                            "currency" => $this->currency_code,
                            "redirect" => array(
                                "return_url" => $base_url.MENU_SOFORT_RETURN),
                            "amount" => ($grand_total) * 100,
                            "owner" => array(
                                "name" => $this->user_name,
                                "email" => $this->email,
                            )));
			#echo "return_url : ".$base_url.MENU_SOFORT_RETURN;
                        #echo "<pre> SOURCE1 => "; print_r($source_1);
			#echo "txn url :: ".$source_1->redirect->url;die;
                        redirect($source_1->redirect->url);                        
                    } catch (Stripe_CardError $e) {
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_InvalidRequestError $e) {
                        // Invalid parameters were supplied to Stripe's API
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_AuthenticationError $e) {
                        // Authentication with Stripe's API failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_ApiConnectionError $e) {
                        // Network communication with Stripe failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_Error $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $stripeerror = $e->getMessage();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe
                        $stripeerror = $e->getMessage();
                    }
                    if ($stripeerror != '') {
                        _set_flashdata(FLASH_STATUS_ERROR, str_replace("sofort",PAYMENT_SOFORT,$stripeerror), FLASH_HTML);
                        redirect($base_url . MENU_CHECKOUT);
                        exit;
                    }
                }  else if ($payment_type == PAYMENT_BANCONTACT) {
                    $base_url = base_url();
                    $stripeerror = '';
                    try {
                        require_once APPPATH . "third_party/stripe/init.php";
                        $this->ci = &get_instance();
                        $this->config = $this->ci->config->config['stripe'];
                        $mode = $this->config['mode'];
                        $sk_key = 'sk_test_PjXblK5gvDtXKbN0LAm9c5kS';//$this->config['sk_' . $mode];
                        #_e($sk_key);
                        \Stripe\Stripe::setApiKey($sk_key);
                        #$customerId = 'cus_BRmqzixkzCPokF';//$customer_info->stripe_id;
                        #_pre($customerId,0);
                        $country = $this->input->post('country');
                        #_pre($iban_no,0);
                        $source_1 = \Stripe\Source::create(array(
                            "type" => "bancontact",
                            "currency" => $this->currency_code,
                            "redirect" => array("return_url" => $base_url.MENU_BANCONTACT_RETURN),
                            "amount" => ($grand_total) * 100,
                            "owner" => array(
                                "name" => $this->user_name,
                                "email" => $this->email,
                            )));
			#echo "return_url : ".$base_url.MENU_SOFORT_RETURN;
                        #echo "<pre> SOURCE1 => "; print_r($source_1);
			#echo "txn url :: ".$source_1->redirect->url;die;
                        redirect($source_1->redirect->url);                        
                    } catch (Stripe_CardError $e) {
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_InvalidRequestError $e) {
                        // Invalid parameters were supplied to Stripe's API
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_AuthenticationError $e) {
                        // Authentication with Stripe's API failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_ApiConnectionError $e) {
                        // Network communication with Stripe failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_Error $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $stripeerror = $e->getMessage();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe
                        $stripeerror = $e->getMessage();
                    }
                    if ($stripeerror != '') {
                        _set_flashdata(FLASH_STATUS_ERROR, str_replace("bancontact",PAYMENT_BANCONTACT,$stripeerror), FLASH_HTML);
                        redirect($base_url . MENU_CHECKOUT);
                        exit;
                    }
                }
		else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
                    redirect(base_url(MENU_CHECKOUT));
                }
            }
        } else {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
            redirect(base_url(MENU_CHECKOUT));
        }
        echo "<center> <h3><span>" . $this->lang->line('MSG_PRODUCT_CHECKOUT_41') . " </span> </h3></center>";
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CHECKOUT_TITLE');
    }

    protected function order_mail($customer_id, $order_id, $admin_mail = 0)
    {
        $this->load->helper('url');
        $order_response = $this->order_model->get_order_by_id(@$customer_id, $order_id);
        $order_detail_array['order_info'] = $order_response;
        $order_detail_response = $this->order_model->get_order_detail_by_id(@$customer_id, $order_id);
        $order_detail_array['order_detail'] = $order_detail_response;
        $customer_info = $this->customer_model->get_info_by_id($this->customer_id);
        $order_detail_array['customer'] = $customer_info;
        #_pre($customer_info,0);
        #_pre($order_detail_array['order_info']->receiver_name,0);
        #_pre($order_detail_array);
        $html ='';
        $html .= $this->detail_view($order_detail_array);
        if($order_detail_array['order_info']->payment_method == PAYMENT_BANK_TRANSFER) {
            $html .= '<br> <br>  <div class="col-md-5 col-md-offset-4 " >
                            <div class="left_beneficiary">
                                <div class="alert-group">
                                    <div class="alert alert-success alert-dismissable section_gray">
                                        <h2 class="text-center"><img src="'.ASSETS_URL_FRONT . 'img/images/bank-48.png" class="payment-method-logo lft-40"> Bank Details </h2>
                                        <span style="color:#ee3682; font-weight: bold;">Please transfert ['.@$order_detail_array['order_info']->payment_curr . ' ' ._number_format( @$order_detail_array['order_info']->grand_total).'] in below bank account.</span>
                                        <br>
                                        <br>
                                        <strong>
                                            <table>
                                                <tbody><tr>
                                                    <td> Holder Name</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> FAMILOV LIMT</td>
                                                </tr>
                                                <tr>
                                                    <td> Bank Name</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> Sparkasse Saarbruecken</td>
                                                </tr>
                                                <tr>
                                                    <td> IBAN</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> DE12590501010067111195</td>
                                                </tr>
                                                <tr>
                                                    <td> BIC</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> SAKSDE55XXX</td>
                                                </tr>
                                                <tr>
                                                    <td> Ref/VW</td>
                                                    <td> &nbsp; : &nbsp;</td>
                                                    <td> Your Name / Your Code </td>
                                                </tr>
                                            </tbody></table>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        ';
        }
            #_pre($html);
        $to = $this->email;
        if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
            $to = MAIL_TEST_EMAIL;
        } else {
        }
        $user_name = $this->user_name;
        $mail_content_data = new stdClass();
        $mail_content_data->var_user_name = $user_name;
        $mail_content_data->var_user_full_name = $user_name;
        $mail_content_data->var_email = $to;
        $mail_content_data->var_order_code = $order_response->generate_code;
        $mail_content_data->var_reciever_name = $order_detail_array['order_info']->receiver_name;
        $mail_content_data->var_reciever_phone_number = $order_detail_array['order_info']->receiver_phone;
        $mail_content_data->var_payment_method = $order_detail_array['order_info']->payment_method;
        $mail_content_data->var_delivery_option = $order_detail_array['order_info']->delivery_option;
        $mail_content_data->var_shop_name = $order_detail_array['order_info']->shop_name;
        $mail_content_data->var_order_detail = $html;
        $mail_data = email_template('ORDER_SUCCESSFULLY_RECEIVED', $mail_content_data, MAIN_URL);
        
        /*if (is_object($mail_data)) {
           $this->CI =& get_instance();
           $this->CI->config->load('mail');
           $this->load->library('mail');
           $this->mail->setTo($to, $user_name);
           $this->mail->setCc(ADMIN_EMAIL, MAIL_SITE_NAME);
           $this->mail->setSubject($mail_data->email_subject);
           $this->mail->setBody(_header_footer($mail_data->email_html));
           $status = $this->mail->send();
           #_e($status);
       }*/

        if($order_detail_array['order_info']->payment_method == PAYMENT_BANK_TRANSFER) {
            if (is_object($mail_data)) {
                $this->load->library('mail');
                $this->mail->setTo($to, $user_name);
                $this->mail->setCc(ADMIN_EMAIL, MAIL_SITE_NAME);
                $this->mail->setSubject($mail_data->email_subject);
                $this->mail->setBody(_header_footer($mail_data->email_html));
                $status = $this->mail->send();
                #_e($status);
            }
            #if ($admin_mail != 0) {
           /* if (1) {
                $to = ADMIN_EMAIL;
                if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                    $to = MAIL_TEST_EMAIL;
                } else {
                }
                if (is_object($mail_data)) {
                    _pre($to . " => ".$user_name);
                    $this->load->library('mail');
                    $this->mail->setTo($to, $user_name);
                    $this->mail->setSubject($mail_data->email_subject);
                    $this->mail->setBody(_header_footer($mail_data->email_html));
                    $status = $this->mail->send();
                    #_e($status);
                }
            }*/
        }else{
            #_e($order_detail_array['order_info']->payment_method);
            if (is_object($mail_data)) {
                /* $this->load->library('mail');
                 $this->mail->setTo($to, $user_name);
                 $this->mail->setSubject($mail_data->email_subject);
                 $this->mail->setBody(_header_footer($mail_data->email_html));
                 $status = $this->mail->send();*/
                $mail_array1 = array(
                    'to_id' => $to,
                    'name' => $user_name,
                    'subject' => $mail_data->email_subject,
                    'body' => base64_encode(_header_footer($mail_data->email_html)),
                    'send_status' => 0,
                );
                $mail_array['customer_mail'] = $mail_array1;
                $this->session->set_userdata($mail_array);
                #$insert_response = $this->dbcommon->insert('mail_list', $mail_array1);
                #$insert_id = $this->db->insert_id();
                #_pre($mail_array1);
            }
            if (1) {
                $to = ADMIN_EMAIL;
                if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                    #$to = MAIL_TEST_EMAIL;
                } else {
                }
                if (is_object($mail_data)) {
                    /*$mail_array2 = array(
                        'to_id' => $to,
                        'name' => SITE_NAME_COM,
                        'subject' => $mail_data->email_subject,
                        'body' => '',//base64_encode(_header_footer($mail_data->email_html)),
                        'send_status' => 0,
                    );*/
                    #$mail_array['customer_mail'] = $mail_array1;
                    #$this->session->set_userdata($mail_array);
                #    _pre($mail_array2,0);
                   # $insert_response = $this->dbcommon->insert('mail_list', $mail_array2);
                #    $insert_id = $this->db->insert_id();
                }
            }
           # _e();
        }
    }

    public function detail_view($order_detail_array)
    {
        $this->load->helper('url');
        #_e(MAIN_URL.MENU_ORDER_CONFIRMATION);
        $order_info = $order_detail_array['order_info'];
        $customer = $order_detail_array['customer'];
        $order_detail = $order_detail_array['order_detail'];
        $html = '';
        $html .= '<div id="printableArea">
<div id="invoice">
                ';
        $html .= '<table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <!--<th><strong>' . $this->lang->line('MSG_PRODUCT_CART_05') . '</strong></th>-->
                            <th><strong>' . $this->lang->line('MSG_PRODUCT_CART_06') . '</strong></th>
                            <th class="text-right"><strong>' . $this->lang->line('MSG_PRODUCT_CART_07') . '</strong></th>
                            <th><strong></strong></th>
                            <th class="text-center"><strong>' . $this->lang->line('MSG_PRODUCT_CART_08') . '</strong>
                            </th>
                            <th class="text-right"><strong>' . $this->lang->line('MSG_PRODUCT_CART_09') . '</strong></th>
                        </tr>
                        </thead>
                        <tbody>';

        $ctr = 0;
        $cart_total_item_qty = 0;
        $cart_amount_total = 0.0;
        $currency_code = @$order_info->payment_curr_code;
        foreach (@$order_detail as $k => $v) {
            $ctr++;
            $cart_total_item_qty += @$v->quantity;
            $cart_amount_total = $cart_amount_total + @$v->total_prize;
            $html .= '
            <tr>
                    <td class="text-center">' . $ctr . '</td>
                    <!--<td><div
                                    style="background-image:url(' . (DIR_PRODUCT_URL . $v->product_image) . '); background-size: cover; height:100px; width:100px;"></div></td>-->
                    <td>' . @$v->product_name . '</td>
                    <td class="text-right">' . @$currency_code . @$v->product_prize . '</td>
                    <td class="text-center">X</td>
                    <td class="text-center">' . @$v->quantity . '</td>
                    <td class="text-right">' . @$currency_code . @$v->total_prize . '</td>
            </tr>
                ';
        }
        #sub total
        $html .= '<tr>
                            <td colspan="4"><strong>' . $this->lang->line('MSG_PRODUCT_CART_10') . '</strong></td>
                            <td class="text-center"><b>' . @$cart_total_item_qty . '</b></td>
                            <td class="text-right"
                                style="color:#36cd75 ;font-weight:bold;">' . @$currency_code . _number_format($cart_amount_total) . '</td>
                            <td></td>
                        </tr>';
        #Service Fees
        $html .= '<tr>
                            <td colspan="4"><strong>' . $this->lang->line('MSG_PRODUCT_CART_11') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . _number_format(@$order_info->service_tax) . '</td>
                            <td></td>
                        </tr>';
        #Delivery Type section
        $html .= '<tr>
                            <td colspan="4"><strong>' . @$order_info->delivery_option . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . _number_format(@$order_info->delivery_charges) . '</td>
                            <td></td>
                        </tr>';
        #Discount section
        if (@$order_info->promo_code != '') {
            $html .= '<tr>
                            <td colspan="4"><strong>' . $this->lang->line('MSG_PRODUCT_CHECKOUT_10') . "(" . @$order_info->promo_code . ")" . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . _number_format(@$order_info->promo_discount_amount) . '</td>
                            <td></td>
                        </tr>';
        }
        #Grand Total section
        $html .= '<tr>
                            <td colspan="4"><strong>' . $this->lang->line('MSG_PRODUCT_CART_16') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right"><strong id="grandprice" style="color:#000;font-size:20px">' . @$currency_code . _number_format(@$order_info->grand_total) . '</strong></td>
                            <td></td>
                        </tr>';

        $html .= '</tbody>
                    </table>
                   </div>
                </div>';

        return $html;
    }

    public function paypal_return($type)
    {
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        if ($type == '') {
            redirect(base_url('/'));
        } else {
            $this->load->library('session');
            $cart = @$this->session->userdata('cart');
            $delivery_info = @$this->session->userdata('delivery_info');
            $promo_code = @$this->session->userdata('promo_code');
            $receiver_info = @$this->session->userdata('receiver_info');
            $cart_total_item_qty = 0;
            $cart_amount_total = 0.00;
            $service_fees = convert_price(SERVICE_FEES);
            foreach (@$cart as $k => $v) {
                $cart_total_item_qty += $v['quantity'];
                $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                $cart_amount_total = $cart_amount_total + $product_price;
            }
            $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - (convert_price(@$promo_code->promo_code_amount));
            #TODO order table
            if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
                $ip_address = '127.0.0.1';
                #$ip_address = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip_address = $_SERVER['REMOTE_ADDR'];
            }
            $phone_no = $receiver_info->receiver_phone;
            $recp_name = $receiver_info->receiver_name;
            $greeting_msg = $receiver_info->greeting_msg;
            $order_array = array(
                'customer_id' => $this->customer_id,
                'ip_address' => $ip_address, #TODO IP Address
                'random_rick' => 'expire',
                'generate_code' => ORDER_TEXT . time(),
                #'generate_code' => ORDER_TEXT . random_string('numeric', 10),
                'order_status' => ORDER_STATUS_WAIT,
                'order_view' => 1,
                'country_id' => @$this->session->userdata('order_info')->country_id,
                'city_id' => @$this->session->userdata('order_info')->city_id,
                'shop_id' => @$this->session->userdata('order_info')->shop_id,
                'contact_id' => 0,
                'receiver_name' => $recp_name,
                'greeting_msg' => $greeting_msg,
                'receiver_phone' => $phone_no,
                'payment_method' => PAYMENT_PAYPAL,
                'date_payment' => date('Y-m-d'),
                'discount' => 0,
                'welcome_discount' => 0,
                'promo_id' => @$promo_code->promo_id,
                'service_tax' => $service_fees,
                'delivery_option' => @$delivery_info->delivery_name,
                'delivery_price' => convert_price(@$delivery_info->delivery_price),
                'payment_curr' => $this->currency_code,
                'payment_curr_code' => $this->currency_symbol,
                'total_order' => $cart_amount_total,
                'sub_total' => ($cart_amount_total),
                'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                'promo_discount_amount' => convert_price(@$promo_code->promo_code_amount),
                'grand_total' => ($grand_total),
                'promo_code' => @$promo_code->promo_code,
            );
            #_pre($order_array);
            $order_confirmation['order_confirmation'] = new stdClass();
            $order_confirmation['order_confirmation']->payment_type = PAYMENT_PAYPAL;
            $insert_response = $this->dbcommon->insert('order', $order_array);
            $insert_id = $this->db->insert_id();
            if ($insert_response) {
                foreach (@$cart as $k => $v) {
                    $product_unit_price = convert_price($v['price_temp']);
                    $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                    $cart_item = array(
                        'order_id' => $insert_id,
                        'product_id' => $k,
                        'product_prize' => $product_unit_price,
                        'total_prize' => $product_all_unit_price,
                        'quantity' => $v['quantity'],
                    );
                    $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                }
            }
            if ($type == 'S') {
                if ($insert_response) {
                    $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                   # $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                    $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                    $order_confirmation['order_confirmation']->order_info = $order_array;
                    $this->session->set_userdata($order_confirmation);
                    #$this->unset_cart(); #TODO
                    redirect(base_url(MENU_ORDER_CONFIRMATION));
                    exit;
                } else {
                }
            } else if ($type == 'F') {
                $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                $order_confirmation['order_confirmation']->order_info = $order_array;
                $this->session->set_userdata($order_confirmation);
                #$this->unset_cart(); #TODO
                redirect(base_url(MENU_ORDER_CONFIRMATION));
                exit;
            }
        }
        redirect(base_url('/'));
        exit;
    }

    public function order_confirmation()
    {
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        $this->unset_cart(); #TODO
        $cart_total_item = count($this->session->userdata('cart'));
        $this->cart_total_item = $cart_total_item;
        $order_confirmation = @$this->session->userdata('order_confirmation');
        if (count($order_confirmation) == 0) {
            redirect(base_url(MENU_MY_ORDER));
            exit;
        }
        $order_mails = @$this->session->userdata('customer_mail');
        if(count($order_mails)>0){
                $this->load->library('mail');
                $this->mail->setTo($order_mails['to_id'], $order_mails['name']);
                $this->mail->setCc(ADMIN_EMAIL, MAIL_SITE_NAME);
                $this->mail->setSubject($order_mails['subject']);
                $this->mail->setBody(base64_decode($order_mails['body']));
                $status = $this->mail->send();
               # _e($status,0);
                $this->session->unset_userdata('customer_mail');
        
        }
        #_pre($order_mails);    
        
        $this->data['page_title'] = $this->lang->line('MSG_OC_01');
        $this->data['order_confirmation'] = $order_confirmation;
        $this->render(PRODUCT_D . ORDER_SUCCESS_V);
    }

    public function order_list()
    {
        /*$this->order_mail($this->customer_id,718);#TODO MAIL Client Mail
        $this->order_mail($this->customer_id,718,1);#TODO MAIL Admin Mail
        _e();*/
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        $order_list = $this->order_model->get_order_list($this->customer_id);
        #_pre($order_list);
        $this->data['page_title'] = $this->lang->line('MSG_VO_TITLE');
        $this->data['order_list'] = $order_list;
        $this->render(PRODUCT_D . ORDER_LIST_V);
    }

    public function order_detail_pop_up()
    {
        if (@$this->customer_id == '' || @$this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        $order_id = $this->input->post('order_id');
        $order_response = $this->order_model->get_order_by_id(@$this->customer_id, $order_id);
        $order_detail_array = array();
        if (count($order_response) > 0) {
            $order_detail_array['order_info'] = $order_response;
            $order_detail_response = $this->order_model->get_order_detail_by_id(@$this->customer_id, $order_id);
            $select_response = $this->customer_model->get_customer_name($order_response->customer_id);
            $order_detail_array['customer'] = $select_response;
            // _pre($select_response);
            if (count($order_detail_response) > 0) {
                $order_detail_array['order_detail'] = $order_detail_response;
                $html = $this->detail_view($order_detail_array);
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $this->lang->line('MSG_VO_05'), 'html' => $html));
                exit;
            } else {
                $html = '';
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06')));
                exit;
            }

        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06')));
            exit;
        }
    }
	
    public function sofort_return(){
        //echo "source id ".$source_id.", ".$client_secret;die;
	//var_dump($this->input->get('client_secret'));
	//var_dump($client_secret,$is_live, $source_id);die;
	$source_id = $this->input->get('source');
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        if($source_id !== ""){
            $base_url = base_url();
            $stripeerror = '';
            $this->load->library('session');
            $cart = @$this->session->userdata('cart');
            $delivery_info = @$this->session->userdata('delivery_info');
            $promo_code = @$this->session->userdata('promo_code');
            $receiver_info = @$this->session->userdata('receiver_info');
            $cart_total_item_qty = 0;
            $cart_amount_total = 0.00;
            $service_fees = convert_price(SERVICE_FEES);
            foreach (@$cart as $k => $v) {
                $cart_total_item_qty += $v['quantity'];
                $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                $cart_amount_total = $cart_amount_total + $product_price;
            }
            $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - (convert_price(@$promo_code->promo_code_amount));

	    if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
                $ip_address = '127.0.0.1';
                #$ip_address = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip_address = $_SERVER['REMOTE_ADDR'];
            }
            #TODO order table
            $order_array = array(
                'customer_id' => $this->customer_id,
                'ip_address' => $ip_address, #TODO IP Address
                'random_rick' => 'expire',
                'generate_code' => ORDER_TEXT . time(),
                #'generate_code' => ORDER_TEXT . random_string('numeric', 10),
                'order_status' => ORDER_STATUS_WAIT,
                'order_view' => 1,
                'country_id' => @$this->session->userdata('order_info')->country_id,
                'city_id' => @$this->session->userdata('order_info')->city_id,
                'shop_id' => @$this->session->userdata('order_info')->shop_id,
                'contact_id' => 0,
                'receiver_name' => $receiver_info->receiver_name,
                'greeting_msg' => $receiver_info->greeting_msg,
                'receiver_phone' => $receiver_info->receiver_phone,
                'payment_method' => PAYMENT_SOFORT,
                'date_payment' => date('Y-m-d'),
                'discount' => 0,
                'welcome_discount' => 0,
                'promo_id' => @$promo_code->promo_id,
                'service_tax' => $service_fees,
                'delivery_option' => @$delivery_info->delivery_name,
                'delivery_price' => convert_price(@$delivery_info->delivery_price),
                'payment_curr' => $this->currency_code,
                'payment_curr_code' => $this->currency_symbol,
                'total_order' => $cart_amount_total,
                'sub_total' => ($cart_amount_total),
                'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                'promo_discount_amount' => convert_price(@$promo_code->promo_code_amount),
                'grand_total' => ($grand_total),
                'promo_code' => @$promo_code->promo_code,
            );

            try {
                require_once APPPATH . "third_party/stripe/init.php";
                $this->ci = &get_instance();
                $this->config = $this->ci->config->config['stripe'];
                $mode = $this->config['mode'];
                $sk_key = $this->config['sk_' . $mode];//'sk_test_PjXblK5gvDtXKbN0LAm9c5kS';
                #_e($sk_key);
                \Stripe\Stripe::setApiKey($sk_key);
		$customer = \Stripe\Customer::create(array(
                        'source' => $source_id,
                        'email' => $this->email,
                        "description" => $this->user_name,
                    )
                );
                $customerId = $customer->id;
                #TODO customer id update.
                $crud_data_customer = array('stripe_id' => $customerId);
                $where = array('customer_id' => $this->customer_id);
                $update_response = $this->dbcommon->update('customers', $where, $crud_data_customer);
                $charge = Stripe\Charge::create(array(
                    "amount" => ($grand_total) * 100,
                    "currency" => $this->currency_code,
                    "source" => $source_id,
		    "customer" => $customer->id
                ));
                #echo "<pre> Charge=> "; print_r($charge);exit;
                if ($charge->id) {
                    $order_array['stripe_token'] = $charge->id;
                    $insert_response = $this->dbcommon->insert('order', $order_array);
                    $insert_id = $this->db->insert_id();
                    $order_confirmation['order_confirmation'] = new stdClass();
                    $order_confirmation['order_confirmation']->payment_type = PAYMENT_SOFORT;
                    if ($insert_response) {
                        foreach (@$cart as $k => $v) {
                            $product_unit_price = convert_price($v['price_temp']);
                            $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                            $cart_item = array(
                                'order_id' => $insert_id,
                                'product_id' => $k,
                                'product_prize' => $product_unit_price,
                                'total_prize' => $product_all_unit_price,
                                'quantity' => $v['quantity'],
                            );
                            $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                        }
                        $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                        $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        $base_url = MAIN_URL;
                        #_pre($base_url);
                        redirect(MAIN_URL . MENU_ORDER_CONFIRMATION);
                        exit;
                    } else {
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        redirect(base_url(MENU_ORDER_CONFIRMATION)); exit;
                    }
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_37'), FLASH_HTML);
                    redirect($base_url . MENU_CHECKOUT);
                    exit;
                }
            } catch (Stripe_CardError $e) {
                $stripeerror = $e->getMessage();
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API
                $stripeerror = $e->getMessage();
            } catch (Stripe_AuthenticationError $e) {
                // Authentication with Stripe's API failed
                $stripeerror = $e->getMessage();
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed
                $stripeerror = $e->getMessage();
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $stripeerror = $e->getMessage();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $stripeerror = $e->getMessage();
            }
            if ($stripeerror != '') {
                _set_flashdata(FLASH_STATUS_ERROR, str_replace("sofort",PAYMENT_SOFORT,$stripeerror), FLASH_HTML);
                redirect($base_url . MENU_CHECKOUT);
                exit;
            }
        } else {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
            redirect(base_url(MENU_CHECKOUT));
        }
        echo "<center> <h3><span>" . $this->lang->line('MSG_PRODUCT_CHECKOUT_41') . " </span> </h3></center>";
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CHECKOUT_TITLE');
    }

    public function bancontact_return(){
        //echo "source id ".$source_id.", ".$client_secret;die;
	//var_dump($this->input->get('client_secret'));
	//var_dump($client_secret,$is_live, $source_id);die;
	$source_id = $this->input->get('source');
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        if($source_id !== ""){
            $base_url = base_url();
            $stripeerror = '';
            $this->load->library('session');
            $cart = @$this->session->userdata('cart');
            $delivery_info = @$this->session->userdata('delivery_info');
            $promo_code = @$this->session->userdata('promo_code');
            $receiver_info = @$this->session->userdata('receiver_info');
            $cart_total_item_qty = 0;
            $cart_amount_total = 0.00;
            $service_fees = convert_price(SERVICE_FEES);
            foreach (@$cart as $k => $v) {
                $cart_total_item_qty += $v['quantity'];
                $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                $cart_amount_total = $cart_amount_total + $product_price;
            }
            $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - (convert_price(@$promo_code->promo_code_amount));
            #TODO order table
            if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
                $ip_address = '127.0.0.1';
                #$ip_address = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip_address = $_SERVER['REMOTE_ADDR'];
            }
            $order_array = array(
                'customer_id' => $this->customer_id,
                'ip_address' => $ip_address, #TODO IP Address
                'random_rick' => 'expire',
                'generate_code' => ORDER_TEXT . time(),
                #'generate_code' => ORDER_TEXT . random_string('numeric', 10),
                'order_status' => ORDER_STATUS_WAIT,
                'order_view' => 1,
                'country_id' => @$this->session->userdata('order_info')->country_id,
                'city_id' => @$this->session->userdata('order_info')->city_id,
                'shop_id' => @$this->session->userdata('order_info')->shop_id,
                'contact_id' => 0,
                'receiver_name' => $receiver_info->receiver_name,
                'greeting_msg' => $receiver_info->greeting_msg,
                'receiver_phone' => $receiver_info->receiver_phone,
                'payment_method' => PAYMENT_BANCONTACT,
                'date_payment' => date('Y-m-d'),
                'discount' => 0,
                'welcome_discount' => 0,
                'promo_id' => @$promo_code->promo_id,
                'service_tax' => $service_fees,
                'delivery_option' => @$delivery_info->delivery_name,
                'delivery_price' => convert_price(@$delivery_info->delivery_price),
                'payment_curr' => $this->currency_code,
                'payment_curr_code' => $this->currency_symbol,
                'total_order' => $cart_amount_total,
                'sub_total' => ($cart_amount_total),
                'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                'promo_discount_amount' => convert_price(@$promo_code->promo_code_amount),
                'grand_total' => ($grand_total),
                'promo_code' => @$promo_code->promo_code,
            );
            try {
                require_once APPPATH . "third_party/stripe/init.php";
                $this->ci = &get_instance();
                $this->config = $this->ci->config->config['stripe'];
                $mode = $this->config['mode'];
                $sk_key = 'sk_test_PjXblK5gvDtXKbN0LAm9c5kS';//$this->config['sk_' . $mode];
                #_e($sk_key);
                \Stripe\Stripe::setApiKey($sk_key);
		$customer = \Stripe\Customer::create(array(
                        'source' => $source_id,
                        'email' => $this->email,
                        "description" => $this->user_name,
                    )
                );
                $customerId = $customer->id;
                #TODO customer id update.
                $crud_data_customer = array('stripe_id' => $customerId);
                $where = array('customer_id' => $this->customer_id);
                $update_response = $this->dbcommon->update('customers', $where, $crud_data_customer);
                $charge = Stripe\Charge::create(array(
                    "amount" => ($grand_total) * 100,
                    "currency" => $this->currency_code,
                    "source" => $source_id,
		    "customer" => $customer->id
                ));
                #echo "<pre> Charge=> "; print_r($charge);exit;
                if ($charge->id) {
                    $order_array['stripe_token'] = $charge->id;
                    $insert_response = $this->dbcommon->insert('order', $order_array);
                    $insert_id = $this->db->insert_id();
                    $order_confirmation['order_confirmation'] = new stdClass();
                    $order_confirmation['order_confirmation']->payment_type = PAYMENT_BANCONTACT;
                    if ($insert_response) {
                        foreach (@$cart as $k => $v) {
                            $product_unit_price = convert_price($v['price_temp']);
                            $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                            $cart_item = array(
                                'order_id' => $insert_id,
                                'product_id' => $k,
                                'product_prize' => $product_unit_price,
                                'total_prize' => $product_all_unit_price,
                                'quantity' => $v['quantity'],
                            );
                            $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                        }
                        $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                        $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        $base_url = MAIN_URL;
                        #_pre($base_url);
                        redirect(MAIN_URL . MENU_ORDER_CONFIRMATION);
                        exit;
                    } else {
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        redirect(base_url(MENU_ORDER_CONFIRMATION)); exit;
                    }
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_37'), FLASH_HTML);
                    redirect($base_url . MENU_CHECKOUT);
                    exit;
                }
            } catch (Stripe_CardError $e) {
                $stripeerror = $e->getMessage();
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API
                $stripeerror = $e->getMessage();
            } catch (Stripe_AuthenticationError $e) {
                // Authentication with Stripe's API failed
                $stripeerror = $e->getMessage();
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed
                $stripeerror = $e->getMessage();
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $stripeerror = $e->getMessage();
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $stripeerror = $e->getMessage();
            }
            if ($stripeerror != '') {
                _set_flashdata(FLASH_STATUS_ERROR, str_replace("bancontact",PAYMENT_BANCONTACT,$stripeerror), FLASH_HTML);
                redirect($base_url . MENU_CHECKOUT);
                exit;
            }
        } else {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
            redirect(base_url(MENU_CHECKOUT));
        }
        echo "<center> <h3><span>" . $this->lang->line('MSG_PRODUCT_CHECKOUT_41') . " </span> </h3></center>";
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CHECKOUT_TITLE');
    }

}
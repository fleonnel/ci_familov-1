<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends Front_Controller
{
    var $table = 'products';
    var $primary_key = 'product_id';
    var $dir = '';
    private $perPage = 48;
    private $perPageimage = 1;


    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("shop_model");
        $this->load->model("product_model");
        $this->load->model("shop_model");
        $this->load->model("category_model");
        $this->load->model("promotion_model");
        $this->load->model("customer_model");
        $this->load->model("order_model");
        $this->load->helper('string');
    }

    public function index()
    {
        redirect(base_url('/'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function product_list($country_id = '', $city_id = '', $shop_id = '', $category = '', $search = '')
    {
        if ($country_id == '' || $city_id == '' || $shop_id == '') {
            redirect(base_url('/'));
        }
        if (!is_numeric($category)) {
            $category = '';
        }
        $this->data['country_id'] = $country_id;
        $this->data['city_id'] = $city_id;
        $this->data['shop_id'] = $shop_id;
        $this->data['category'] = $category;

        $order_info['order_info'] = new stdClass();
        $order_info['order_info']->country_id = $country_id;
        $order_info['order_info']->city_id = $city_id;
        $order_info['order_info']->shop_id = $shop_id;
        #_pre($order_info,0);
        //_pre(@$this->session->userdata('cart'));
        //var_dump(@$this->session->userdata('cart'));exit;
        if (@$this->session->userdata('order_info')->shop_id) {
            if (@$this->session->userdata('order_info')->shop_id != $shop_id && (@$this->session->userdata('cart') != NULL && count(@$this->session->userdata('cart') > 0))) {
                redirect(base_url(MENU_CHANGE_SHOP . "/" . $country_id . "/" . $city_id . "/" . $shop_id));
                exit;
            }
        } else {
            $this->session->set_userdata($order_info);
        }
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_TITLE');
        $category_response = $this->category_model->get_category_list_shop_wise($shop_id);
        $this->data['category_list'] = $category_response;
        $shop_response = $this->shop_model->get_info_by_id($shop_id);
        $this->data['shop_info'] = $shop_response;
        #_pre($_REQUEST);
        /* Product List */
        $search_keyword = '';
        if (count($this->input->post()) > 0) {
            $search_keyword = $this->input->post('search_header');
        }
        $this->data['search_keyword'] = $search_keyword;
        $count = count($this->product_model->product_list_shop_wise($shop_id, $category, $search_keyword));
        if (1) {
            $product_response = $this->product_model->product_list_shop_wise($shop_id, $category, $search_keyword, $this->perPage, 0);
            #_pre($product_response);
            /*if (count($product_response) > 0) {
                foreach ($product_response as $k => $v) {
                    $product_response[$k]->img = array();
                }
            }*/
            $this->data['product_response'] = $product_response;
        }
        $this->data['tot'] = $count;
        $this->data['perPage'] = $this->perPage;
        /* Product List End */
        #_pre($product_response);
        #_set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_LOGOUT_SUCCESS'),FLASH_HTML);
        $this->render(PRODUCT_D . PRODUCT_LISTING_V);
    }

    public function listing()
    {
        $shop_id = $this->input->post("shop_id");
        $page = $this->input->post("page");
        $category = $this->input->post("category");
        $search_keyword = $this->input->post('search_keyword');
        if ($page != '') {
            $start = ceil($page * $this->perPage);
            $product_response = $this->product_model->product_list_shop_wise($shop_id, $category, $search_keyword, $this->perPage, $start);
            # _pre($product_response);
            /*if (count($product_response) > 0) {
                foreach ($product_response as $k => $v) {
                    $product_response[$k]->img = array();
                }
            }*/
            $data['product_response'] = $product_response;
            $result = $this->load->view(PRODUCT_D . '/_list', $data, true);
            $res_array = array('html' => $result);
            echo json_encode($res_array);
            exit;
        }
    }

    public function product_search()
    {
        $shop_id = $this->input->post("shop_id");
        $category = $this->input->post("category");
        $keyword = $this->input->post("keyword");
        if ($keyword != '') {
            $product_response = $this->product_model->product_search_keyword_wise($shop_id, $keyword, $category);
            $data['product_response'] = $product_response;
            $result = $this->load->view(PRODUCT_D . '/_search', $data, true);
            $res_array = array('html' => $result);
            echo json_encode($res_array);
            exit;
        } else {
            $res_array = array('html' => '');
            echo json_encode($res_array);
            exit;
        }
    }

    public function product_detail($product_id = '', $shop_id = '')
    {
        #$cart_array = @$this->session->userdata('cart');
        #_pre($cart_array);
        if ($product_id == '' || $shop_id == '') {
            redirect(base_url('/'));
        } else {
            $category_response = $this->category_model->get_category_list_shop_wise($shop_id);
            $this->data['category_list'] = $category_response;
            $shop_response = $this->shop_model->get_info_by_id($shop_id);
            $this->data['shop_info'] = $shop_response;
            $product_response = $this->product_model->get_product_info_by_id($product_id, 'PUBLISHED'); #1 for publish product
            $this->data['product_response'] = $product_response;
            #_pre($product_response);
        }
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_DETAIL_TITLE');
        $this->render(PRODUCT_D . PRODUCT_DETAIL_V);
    }

    public function view_cart()
    {
        $cart_array = @$this->session->userdata('cart');
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CART_TITLE');
        $this->data['cart'] = $cart_array;
        $this->render(PRODUCT_D . PRODUCT_CART_V);
    }

    public function update_cart()
    {
        if (count($this->input->post()) > 0) {
            $qty_array = $this->input->post('qty');
            if (count($qty_array) > 0) {
                $this->load->library('session');
                $cart_array = @$this->session->userdata('cart');
                if (count($cart_array) > 0) {
                    foreach ($cart_array as $k => $v) {
                        $new_qty = $qty_array[$k];
                        $cart_array[$k]['quantity'] = $qty_array[$k];
                    }
                    $cart['cart'] = $cart_array;
                    $this->session->set_userdata($cart);
                }
                _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CART_17'));
            }
            redirect(base_url(MENU_VIEW_CART));
        }
    }

    public function delete_cart_item($product_id = 0)
    {
        if ($product_id == '' || $product_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CART_20'));
            redirect(base_url(MENU_VIEW_CART));
        } else {
            $cart_array = @$this->session->userdata('cart');
            if (count($cart_array) > 0) {
                unset($cart_array[$product_id]);
                $cart['cart'] = $cart_array;
                $this->session->set_userdata($cart);
            }
            _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_PRODUCT_CART_21'));
        }
        redirect(base_url(MENU_VIEW_CART));
    }

    public function shop_change($country_id = '', $city_id = '', $shop_id = '')
    {
        $this->data['country_id'] = $country_id;
        $this->data['city_id'] = $city_id;
        $this->data['shop_id'] = $shop_id;
        //_pre(@$this->session->userdata('order_info')->shop_id);
        $this->render(PRODUCT_D . MENU_WARNING_VIEW);

    }

    public function shop_change_page()
    {
        $this->unset_cart();
        _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CART_ADD_FAILED'));
        echo true;
        exit;
        //redirect(base_url(MENU_VIEW_CART));
    }

    public function unset_cart()
    {
        $this->session->unset_userdata('order_info');
        $this->session->unset_userdata('cart');
        $this->session->unset_userdata('delivery_info');
        $this->session->unset_userdata('promo_code');
        $this->session->unset_userdata('receiver_info');
    }

    public function add_to_cart()
    {
        #_pre($this->input->post());
        #_e($this->customer_id ,0);
        $this->load->library('session');
        #$this->session->unset_userdata('991');
        #$this->session->unset_userdata('cart');
        #$product_id = 1397;
        #$product_id = 991;
        $product_id = $this->input->post('product_id');
        $qty = $this->input->post('qty');
        $session_shop_id = @$this->session->userdata('order_info')->shop_id;
        $product_response = $this->product_model->get_product_info_by_id($product_id, 'PUBLISHED');
        # _pre($product_response);
        $cart['cart'] = array();
        if (count($product_response) > 0) {
            $product_name = $product_response->product_name;
            $cart_array = @$this->session->userdata('cart');
            if (count($cart_array) > 0) {
                # _e("in if ",0);
                #foreach($cart_array as $k=>$v){
                if (array_key_exists($product_id, $cart_array)) {
                    $qty = $cart_array[$product_id]['quantity'] + $qty;
                    $cart_array[$product_id]['quantity'] = $qty;
                } else {
                    $item['customer_id'] = $this->customer_id;
                    $item['ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $item['product_id'] = $product_id;
                    $item['product_name'] = @$product_response->product_name;
                    $item['product_image'] = @$product_response->product_image;
                    if (@$product_response->special_product_prices > 0) {
                        $price = convert_price(@$product_response->special_product_prices);
                        $price_temp = @$product_response->special_product_prices;
                    } else {
                        $price = convert_price(@$product_response->product_prices);
                        $price_temp = @$product_response->product_prices;
                    }
                    $item['price'] = $price;
                    $item['price_temp'] = $price_temp;
                    $item['quantity'] = $qty;
                    $item['random_rick'] = random_string(10);
                    $item['country_id'] = @$this->session->userdata('order_info')->country_id;
                    $item['city_id'] = @$this->session->userdata('order_info')->city_id;
                    $item['shop_id'] = $session_shop_id;
                    $cart_array[$product_id] = $item;
                }
                #}
                $cart['cart'] = $cart_array;
            } else {
                $item['customer_id'] = $this->customer_id;
                $item['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $item['product_id'] = $product_id;
                $item['product_name'] = @$product_response->product_name;
                $item['product_image'] = @$product_response->product_image;
                if (@$product_response->special_product_prices > 0) {
                    $price = convert_price(@$product_response->special_product_prices);
                    $price_temp = @$product_response->special_product_prices;
                } else {
                    $price = convert_price(@$product_response->product_prices);
                    $price_temp = @$product_response->product_prices;
                }
                $item['price'] = $price;
                $item['price_temp'] = $price_temp;
                $item['quantity'] = $qty;
                $item['random_rick'] = random_string(10);
                $item['country_id'] = @$this->session->userdata('order_info')->country_id;
                $item['city_id'] = @$this->session->userdata('order_info')->city_id;
                $item['shop_id'] = $session_shop_id;
                $cart['cart'][$product_id] = $item;
            }
            $msg = str_replace("#ITEM#", @$product_name, $this->lang->line('MSG_PRODUCT_CART_ADD_SUCCESS'));
            $this->session->set_userdata($cart);
            $cart_total_item = count(@$this->session->userdata('cart'));
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $msg, 'cart_total_item' => $cart_total_item));
        } else {
            $cart_total_item = count(@$this->session->userdata('cart'));
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_PRODUCT_CART_ADD_FAILED'), 'cart_total_item' => $cart_total_item));
        }
        exit;
    }

    public function checkout()
    {
        #$this->session->unset_userdata('page');
        #var_dump($this->session->userdata('page'));
        if ($this->session->userdata('page') != NULL) {
        }
        $cart_array = @$this->session->userdata('cart');
        $this->data['cart'] = $cart_array;
        if (count($cart_array) == 0) {
            _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CHECKOUT_02'));
            redirect(base_url('/'));
            exit;
        } else {
            $shop_id = $this->session->userdata('order_info')->shop_id;
            $shop_response = $this->shop_model->get_info_by_id($shop_id);
            $this->data['shop_info'] = $shop_response;
            $delivery_info['delivery_info'] = new stdClass();
            $promo_code_array['promo_code'] = new stdClass();
            if (!$this->session->userdata('delivery_info')) {
                if (count($shop_response) > 0) {
                    $delivery_info['delivery_info']->delivery_mode = 'free_delivery';
                    $delivery_info['delivery_info']->delivery_price = $shop_response->pickup_from_shop_price;
                    $delivery_info['delivery_info']->delivery_name = $this->lang->line('MSG_PRODUCT_CHECKOUT_05');
                    $this->session->set_userdata($delivery_info);
                }
            }
            if (!$this->session->userdata('promo_code')) {
                $promo_code_array['promo_code']->promo_code = '';
                $promo_code_array['promo_code']->promo_id = '';
                $promo_code_array['promo_code']->promo_code_amount = 0;
                $this->session->set_userdata($promo_code_array);
            }
            #$this->session->unset_userdata('delivery_info');
            $this->data['delivery_info'] = $this->session->userdata('delivery_info');
            $this->data['promo_code'] = $this->session->userdata('promo_code');
            $promo_amount = $this->data['promo_code']->promo_code_amount;
            #_e($price ,0);_e($quantity,0);_e(convert_price($price)*$quantity,0);
            #_pre($cart_array,0);
            if (!@$this->is_login) {
                $last_url['page'] = base_url(MENU_CHECKOUT);
                $this->session->set_userdata($last_url);
            } else {
                $this->session->unset_userdata('page');
                #$last_url['page'] = '';
                #$this->session->set_userdata($last_url);
            }
            #_pre($this->session->userdata('page'));
            #_pre($cart_array['1284']['price_temp']);
            $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CHECKOUT_TITLE');
            $this->render(PRODUCT_D . PRODUCT_CHECKOUT_V);
        }
    }

    public function order_delivery()
    {
        $this->load->library('session');
        $delivery_info['delivery_info'] = new stdClass();
        $shop_id = $this->session->userdata('order_info')->shop_id;
        $shop_response = $this->shop_model->get_info_by_id($shop_id);
        if (count($this->input->post()) > 0) {
            #TODO Delivery Mode
            $delivery_option = $this->input->post('delivery_option');
            if ($delivery_option != '') {
                if ($delivery_option == 'home_delivery' && $shop_response->home_delivery_status == 'Activated') {
                    $delivery_info['delivery_info']->delivery_mode = 'home_delivery';
                    $delivery_info['delivery_info']->delivery_price = $shop_response->home_delivery_price;
                    $delivery_info['delivery_info']->delivery_name = $this->lang->line('MSG_PRODUCT_CHECKOUT_06');
                } else {
                    $delivery_info['delivery_info']->delivery_mode = 'free_delivery';//$delivery_option;
                    $delivery_info['delivery_info']->delivery_price = $shop_response->pickup_from_shop_price;
                    $delivery_info['delivery_info']->delivery_name = $this->lang->line('MSG_PRODUCT_CHECKOUT_05');
                }
                $this->session->set_userdata($delivery_info);
                _set_flashdata(FLASH_STATUS_INFO, $this->lang->line('MSG_PRODUCT_CHECKOUT_09') . "<br> (" . $delivery_info['delivery_info']->delivery_name . ")");
            }
        }
        redirect(base_url(MENU_CHECKOUT));
    }

    public function order_promo_code()
    {
        $this->load->library('session');
        $promo_code_array['promo_code'] = new stdClass();
        if (count($this->input->post()) > 0) {
            $promo_code = $this->input->post('promo_code');
            $promo_response = $this->promotion_model->get_info_by_code($promo_code);
            #_pre($promo_response);
            if (count($promo_response) == 0) {
                $promo_code_array['promo_code']->promo_code = $promo_code;
                $promo_code_array['promo_code']->promo_id = '';
                $promo_code_array['promo_code']->promo_code_amount = 0;
                _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_11'), FLASH_HTML);
            } else {
                $amount = $promo_response->iAmount;
                $amount = convert_price($amount);
                $cart_array = @$this->session->userdata('cart');
                $cart = $cart_array;
                $cart_amount_total = 0.00;
                foreach (@$cart as $k => $v) {
                    $cart_amount_total += ($v['price_temp'] * $v['quantity']);
                }
                $delivery_info = $this->session->userdata('delivery_info');
                $cart_amount_total = $cart_amount_total + SERVICE_FEES + @$delivery_info->delivery_price; #TODO add all charges
                $cart_amount_total = convert_price($cart_amount_total);
                if ($cart_amount_total < $amount) {
                    $promo_code_array['promo_code']->promo_code = $promo_code;
                    $promo_code_array['promo_code']->promo_id = '';
                    $promo_code_array['promo_code']->promo_code_amount = 0;
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_13'), FLASH_HTML);
                } else {
                    $promo_code_array['promo_code']->promo_code = $promo_response->vCode;
                    $promo_code_array['promo_code']->promo_id = $promo_response->promo_id;
                    $promo_code_array['promo_code']->promo_code_amount = $promo_response->iAmount;
                    _set_flashdata(FLASH_STATUS_SUCCESS, $this->lang->line('MSG_PRODUCT_CHECKOUT_12'));
                }
            }
            $this->session->set_userdata($promo_code_array);
            #TODO Promo code
        }
        redirect(base_url(MENU_CHECKOUT));
    }

    public function checkout_proceed() #TODO unset order session
    {
        #_pre($_REQUEST);
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        if (count($this->input->post()) > 0) {
            #$payment_type = $this->input->post('payment_type');
            $payment_type = PAYMENT_SEPA_DD;
            # _e($payment_type);
            $this->form_validation->set_rules('recp_name', 'Recipient Number', 'trim|required');
            $this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
            $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
            $customer_info = $this->customer_model->get_info_by_id($this->customer_id);
            if ($this->form_validation->run() != FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors);
                redirect(base_url(MENU_CHECKOUT));
            } else {
                #_e($payment_type);
                $cart = @$this->session->userdata('cart');
                $delivery_info = @$this->session->userdata('delivery_info');
                $promo_code = @$this->session->userdata('promo_code');
                $cart_total_item_qty = 0;
                $cart_amount_total = 0.00;
                $service_fees = convert_price(SERVICE_FEES);
                foreach (@$cart as $k => $v) {
                    $cart_total_item_qty += $v['quantity'];
                    $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                    $cart_amount_total = $cart_amount_total + $product_price;
                }
                $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - (convert_price(@$promo_code->promo_code_amount));
                #TODO order table
                if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
                    $ip_address = '127.0.0.1';
                    #$ip_address = $_SERVER['REMOTE_ADDR'];
                } else {
                    $ip_address = $_SERVER['REMOTE_ADDR'];
                }
                $phone_no = $this->input->post('phone_code') . " " . $this->input->post('phone_number');
                $order_array = array(
                    'customer_id' => $this->customer_id,
                    'ip_address' => $ip_address, #TODO IP Address
                    'random_rick' => 'expire',
                    'generate_code' => ORDER_TEXT . time(),
                    #'generate_code' => ORDER_TEXT . random_string('numeric', 10),
                    'order_status' => ORDER_STATUS_WAIT,
                    'order_view' => 1,
                    'country_id' => @$this->session->userdata('order_info')->country_id,
                    'city_id' => @$this->session->userdata('order_info')->city_id,
                    'shop_id' => @$this->session->userdata('order_info')->shop_id,
                    'contact_id' => 0,
                    'receiver_name' => addslashes($this->input->post('recp_name')),
                    'greeting_msg' => addslashes($this->input->post('greeting_msg')),
                    'receiver_phone' => $phone_no,
                    'payment_method' => $payment_type,
                    'date_payment' => date('Y-m-d'),
                    'discount' => 0,
                    'welcome_discount' => 0,
                    'promo_id' => @$promo_code->promo_id,
                    'service_tax' => $service_fees,
                    'delivery_option' => @$delivery_info->delivery_name,
                    'delivery_price' => convert_price(@$delivery_info->delivery_price),
                    'payment_curr' => $this->currency_code,
                    'payment_curr_code' => $this->currency_symbol,
                    'total_order' => $cart_amount_total,
                    'sub_total' => ($cart_amount_total),
                    'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                    'promo_discount_amount' => convert_price(@$promo_code->promo_code_amount),
                    'grand_total' => ($grand_total),
                    'promo_code' => @$promo_code->promo_code,
                );
                #_pre($order_array);
                $order_confirmation['order_confirmation'] = new stdClass();
                $order_confirmation['order_confirmation']->payment_type = $payment_type;
                $receiver_info['receiver_info'] = new stdClass();
                $receiver_info['receiver_info']->receiver_name = addslashes($this->input->post('recp_name'));
                $receiver_info['receiver_info']->receiver_phone = $phone_no;
                $receiver_info['receiver_info']->greeting_msg = addslashes($this->input->post('greeting_msg'));
                $this->session->set_userdata($receiver_info);
                if ($payment_type == PAYMENT_BANK_TRANSFER) {
                    $insert_response = $this->dbcommon->insert('order', $order_array);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        foreach (@$cart as $k => $v) {
                            $product_unit_price = convert_price($v['price_temp']);
                            $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                            $cart_item = array(
                                'order_id' => $insert_id,
                                'product_id' => $k,
                                'product_prize' => $product_unit_price,
                                'total_prize' => $product_all_unit_price,
                                'quantity' => $v['quantity'],
                            );
                            $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                        }
                        $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                        $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        redirect(base_url(MENU_ORDER_CONFIRMATION));
                    } else {
                        $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                        $order_confirmation['order_confirmation']->order_info = $order_array;
                        $this->session->set_userdata($order_confirmation);
                        redirect(base_url(MENU_ORDER_CONFIRMATION));
                    }
                } else if ($payment_type == PAYMENT_STRIPE) {
                    #_e(APPPATH.'libraries/stripe_config.php');
                    $base_url = base_url();
                    #_pre($base_url);
                    if (!empty($_POST['stripeToken'])) { //  Si payment stripe
                        $stripeerror = '';
                        try {
                            #require_once(APPPATH.'libraries/stripe_config.php');
                            require_once APPPATH . "third_party/stripe/init.php";
                            $this->ci = &get_instance();
                            $this->config = $this->ci->config->config['stripe'];
                            $mode = $this->config['mode'];
                            $sk_key = $this->config['sk_' . $mode];
                            #_e($sk_key);
                            \Stripe\Stripe::setApiKey($sk_key);

                            $stripeToken = $_POST['stripeToken'];
                            $stripeEmail = strip_tags(trim($_POST['stripeEmail']));
                            if ($customer_info->stripe_id == '') {
                                $customer = \Stripe\Customer::create(array(
                                        'source' => $stripeToken,
                                        'email' => $stripeEmail,
                                        "description" => $this->user_name,
                                    )
                                );
                                $customerId = $customer->id;
                                #TODO customer id update.
                                $crud_data_customer = array('stripe_id' => $customerId);
                                $where = array('customer_id' => $this->customer_id);
                                $update_response = $this->dbcommon->update('customers', $where, $crud_data_customer);
                            } else {
                                $customerId = $customer_info->stripe_id;
                            }
                            // Charge the Customer instead of the card
                            $charge = \Stripe\Charge::create(array(
                                "amount" => convert_price($grand_total) * 100, // amount in cents, again
                                "currency" => $this->currency_code,
                                "customer" => $customerId
                            ));
                            #_pre($charge);
                            if ($charge->id) {
                                $order_array['stripe_token'] = $charge->id;
                                $insert_response = $this->dbcommon->insert('order', $order_array);
                                $insert_id = $this->db->insert_id();
                                if ($insert_response) {
                                    foreach (@$cart as $k => $v) {
                                        $product_unit_price = convert_price($v['price_temp']);
                                        $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                                        $cart_item = array(
                                            'order_id' => $insert_id,
                                            'product_id' => $k,
                                            'product_prize' => $product_unit_price,
                                            'total_prize' => $product_all_unit_price,
                                            'quantity' => $v['quantity'],
                                        );
                                        $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                                    }
                                    $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                                    $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                                    $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                                    $order_confirmation['order_confirmation']->order_info = $order_array;
                                    $this->session->set_userdata($order_confirmation);
                                    redirect($base_url . MENU_ORDER_CONFIRMATION);
                                    exit;
                                } /*else {
                                    $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                                    $order_confirmation['order_confirmation']->order_info = $order_array;
                                    $this->session->set_userdata($order_confirmation);
                                    redirect(base_url(MENU_ORDER_CONFIRMATION));
                                }*/
                            } else {
                                _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_37'), FLASH_HTML);
                                redirect($base_url . MENU_CHECKOUT);
                                exit;
                                #$data['stripstatus'] = 0;
                                #$data['stripeerror'] = "Something went wrong ! Please try again.";
                            }
                        } catch (Stripe_CardError $e) {
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_InvalidRequestError $e) {
                            // Invalid parameters were supplied to Stripe's API
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_AuthenticationError $e) {
                            // Authentication with Stripe's API failed
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_ApiConnectionError $e) {
                            // Network communication with Stripe failed
                            $stripeerror = $e->getMessage();
                        } catch (Stripe_Error $e) {
                            // Display a very generic error to the user, and maybe send
                            // yourself an email
                            $stripeerror = $e->getMessage();
                        } catch (Exception $e) {
                            // Something else happened, completely unrelated to Stripe
                            $stripeerror = $e->getMessage();
                        }
                        if ($stripeerror != '') {
                            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_39') . $stripeerror, FLASH_HTML);
                            redirect($base_url . MENU_CHECKOUT);
                            exit;
                        }
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
                        redirect($base_url . MENU_CHECKOUT);
                        exit;
                    }
                } else if ($payment_type == PAYMENT_PAYPAL) {
                    $base_url = base_url();
                    $this->ci = &get_instance();
                    $this->config = $this->ci->config->config['paypal'];
                    $mode = $this->config['mode'];
                    $business_id = $this->config['business_id'];
                    $url = $this->config['url_' . $mode];
                    /*<input type="text" name="shipping_1" value="' . convert_price(@$delivery_info->delivery_price) . '">*/
                    $discount = convert_price(@$promo_code->promo_code_amount);
                    echo ' <div style="display:none;"> <form action="' . $url . '" method="post" id="paypal_form">';
                    echo '
                        <input type="text" name="business" value="' . $business_id . '">
                        <input type="text" name="cpp_logo_image" value="https://familov.com/images/familov-logo.png">
                        <input type="text" name="image_url" value="https://familov.com/images/familov-logo.png">
                        <input type="text" name="cmd" value="_cart">
                        <input type="text" name="upload" value="1">';
                    $ctr_cart = 1;
                    foreach (@$cart as $k => $v) {
                        $qty = $v['quantity'];
                        $name = $v['product_name'];
                        $product_unit_price = convert_price($v['price_temp']);
                        $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                        echo '
                                <input type="text" name="item_number_' . $ctr_cart . '" value="' . $k . '">
                                <input type="text" name="item_name_' . $ctr_cart . '" value="' . $name . '">
                                <input type="text" name="amount_' . $ctr_cart . '" value="' . $product_unit_price . '">
                                <input type="text" name="quantity_' . $ctr_cart . '" value="' . $qty . '">';
                        $ctr_cart++;
                    }
                    echo '
                    <input type="text" name="item_name_' . $ctr_cart . '" value="Shipping(' . trim(@$delivery_info->delivery_name) . ')">
                                <input type="text" name="amount_' . $ctr_cart . '" value="' . convert_price(@$delivery_info->delivery_price) . '">
                                <input type="text" name="quantity_' . $ctr_cart . '" value="1">';
                    $ctr_cart++;
                    echo '
                    <input type="text" name="item_name_' . $ctr_cart . '" value="Service fees">
                    <input type="text" name="amount_' . $ctr_cart . '" value="' . @$service_fees . '">
                    <input type="text" name="quantity_' . $ctr_cart . '" value="1">

                        <input type="text" name="shipping2" value="0.00">
                        <input type="text" name="handling_cart" value="0.00">
                        <input type="text" name="discount_amount_cart" value="' . $discount . '">
                        <input type="text" name="currency_code" value="' . $this->currency_code . '">
                        <input type="image" name="submit"
                            src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_addtocart_120x26.png"
                        alt="Checkout">
                        <img alt="" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

                         <input type="hidden" id="paypal_return" style="width:100%;" name="return"
                                   value="' . $base_url . MENU_PAYPAL_RETURN . '/S">
                            <input type="hidden" name="cancel_return" id="cancel_return"
                                   value="' . $base_url . MENU_PAYPAL_RETURN . '/F">
                        ';
                    echo '</form> </div>
                        <script>
                        setTimeout(function(){document.getElementById("paypal_form").submit(); }, 1000);
                    </script>
                    ';
                } else if ($payment_type == PAYMENT_SEPA_DD) {
                    _pre($_REQUEST, 0);
                    _pre($order_array, 0);

                    $stripeerror = '';
                    try {
                        #require_once(APPPATH.'libraries/stripe_config.php');
                        //require_once APPPATH . "third_party/stripe/init.php";
                        require_once APPPATH . "third_party/stripe-php/vendor/autoload.php";
                        $this->ci = &get_instance();
                        $this->config = $this->ci->config->config['stripe'];
                        $mode = $this->config['mode'];
                        $sk_key = $this->config['sk_' . $mode];
                        #_e($sk_key);
                        \Stripe\Stripe::setApiKey($sk_key);

                        #$customerId = 'cus_BRmqzixkzCPokF';//$customer_info->stripe_id;
                        #_pre($customerId,0);
                        $iban_no = $_POST['iban_no'];
                        _pre($iban_no, 0);
                        $source_1 = \Stripe\Source::create(array(
                            "type" => "sepa_debit",
                            "sepa_debit" => array("iban" => $iban_no),
                            "currency" => "eur",
                            "owner" => array(
                                "name" => "Jenny Rosen",
                            )));

                        var_dump($source_1);
                        echo "<pre> SOURCE1 => ";
                        print_r($source_1);

                        #if ($customer_info->stripe_id == '') {
                        $customer = \Stripe\Customer::create(array(
                                'source' => $source_1->id,
                                'email' => 'vm@vm.vm',
                                "description" => 'VMV'//$this->user_name,
                            )
                        );
                        $customerId = $customer->id;
                        #TODO customer id update.
                        #$crud_data_customer = array('stripe_id' => $customerId);
                        #$where = array('customer_id' => $this->customer_id);
                        #$update_response = $this->dbcommon->update('customers', $where, $crud_data_customer);
                        /*} else {
                            $customerId = $customer_info->stripe_id;
                        }*/
                        $charge = Stripe\Charge::create(array(
                            "amount" => 300,
                            "currency" => "eur",
                            "customer" => $customerId,
                            "source" => $source_1->id,//"src_1CTQZEDPpyW76gxwyLeonxbw",
                        ));
                        echo "<pre> Charge=> ";
                        print_r($charge);
                        exit;


                    } catch (Stripe_CardError $e) {
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_InvalidRequestError $e) {
                        // Invalid parameters were supplied to Stripe's API
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_AuthenticationError $e) {
                        // Authentication with Stripe's API failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_ApiConnectionError $e) {
                        // Network communication with Stripe failed
                        $stripeerror = $e->getMessage();
                    } catch (Stripe_Error $e) {
                        // Display a very generic error to the user, and maybe send
                        // yourself an email
                        $stripeerror = $e->getMessage();
                    } catch (Exception $e) {
                        // Something else happened, completely unrelated to Stripe
                        $stripeerror = $e->getMessage();
                    }
                    _pre($stripeerror);
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
                    redirect(base_url(MENU_CHECKOUT));
                }
            }
        } else {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_36'), FLASH_HTML);
            redirect(base_url(MENU_CHECKOUT));
        }
        echo "<center> <h3><span>" . $this->lang->line('MSG_PRODUCT_CHECKOUT_41') . " </span> </h3></center>";
        $this->data['page_title'] = $this->lang->line('MSG_PRODUCT_CHECKOUT_TITLE');
    }

    protected function order_mail($customer_id, $order_id, $admin_mail = 0)
    {
        $order_response = $this->order_model->get_order_by_id(@$customer_id, $order_id);
        $order_detail_array['order_info'] = $order_response;
        $order_detail_response = $this->order_model->get_order_detail_by_id(@$customer_id, $order_id);
        $order_detail_array['order_detail'] = $order_detail_response;
        $customer_info = $this->customer_model->get_info_by_id($this->customer_id);
        $order_detail_array['customer'] = $customer_info;
        #       _pre($customer_info);
        #      _pre($order_detail_array);
        $html = $this->detail_view($order_detail_array);
        $to = $this->email;
        if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
            $to = MAIL_TEST_EMAIL;
        } else {
        }
        $user_name = $this->user_name;
        $mail_content_data = new stdClass();
        $mail_content_data->var_user_name = $user_name;
        $mail_content_data->var_user_full_name = $user_name;
        $mail_content_data->var_email = $to;
        $mail_content_data->var_order_code = $order_response->generate_code;
        $mail_content_data->var_reciever_name = '';
        $mail_content_data->var_reciever_phone_number = '';
        $mail_content_data->var_order_detail = $html;
        $mail_data = email_template('ORDER_SUCCESSFULLY_RECEIVED', $mail_content_data);
        if (is_object($mail_data)) {
            $this->load->library('mail');
            $this->mail->setTo($to, $user_name);
            $this->mail->setSubject($mail_data->email_subject);
            $this->mail->setBody(_header_footer($mail_data->email_html));
            $status = $this->mail->send();
            #_e($status);
        }
        if ($admin_mail != 0) {
            $to = ADMIN_EMAIL;
            if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                $to = MAIL_TEST_EMAIL;
            } else {
            }
            if (is_object($mail_data)) {
                $this->load->library('mail');
                $this->mail->setTo($to, $user_name);
                $this->mail->setSubject($mail_data->email_subject);
                $this->mail->setBody(_header_footer($mail_data->email_html));
                $status = $this->mail->send();
                #_e($status);
            }
        }
    }

    public function detail_view($order_detail_array)
    {
        #   _e(MAIN_URL.MENU_ORDER_CONFIRMATION);
        $order_info = $order_detail_array['order_info'];
        $customer = $order_detail_array['customer'];
        $order_detail = $order_detail_array['order_detail'];
        $html = '';
        $html .= '<div id="printableArea"><div id="invoice"><div class="row"><div class="col-md-4"><div id="logo"><img src="' . (MAIN_URL . 'uploads/shop' . $order_info->shop_logo) . '" class=""
                    alt="shop_logo" style="margin-top: 10px; margin-bottom:30px"
                             width="150px" onerror="this.src=\'' . MAIN_URL . 'assets/front/img/images/familov-logo.png\'"></div></div>
                    <div class="col-md-4"><div id="logo"><img src="' . ASSETS_URL_FRONT . 'img/images/familov-logo.png" class=""
                    alt="shop_logo" style="margin-top: 10px; margin-bottom:30px"
                             width="150px"></div></div>
                    <div class="col-md-4">
                    <p id="details">
                            <strong>Invoice N°:</strong> ' . @$order_info->generate_code . ' <br>
                            <strong>Issued:</strong> ' . date_time(@$order_info->date_time, 7) . ' <br>
                            <strong>Payment Method:</strong> ' . @$order_info->payment_method . ' <br>
                            <strong>' . $this->lang->line('MSG_VO_13') . ':</strong> ' . @$order_info->order_status . '<br>
                            <strong>Delivery option:</strong> : ' . @$order_info->delivery_option . '
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3>' . @$order_info->shop_name . '   </h3>
                        <h2>CODE : ' . @$order_info->generate_code . '   </h2>
                    </div>
                    <div class="col-md-4">
                        <strong class="margin-bottom-5">From :</strong>
                        <p>
                            Familov,inc.<br>
                            Innovation Campus A4<br>
                            66115 Saarbrücken/ Germany <br>
                            P: (+49)15259-948834  <br>
                            E: sales@familov.com  <br>
                        </p>
                    </div>

                    <div class="col-md-4">
                        <strong class="margin-bottom-5">To :</strong>
                        <p>
                            ' . @$customer->username . " " . @$customer->lastname . '<br>
                            ' . @$customer->home_address . '<br>
                           ' . @$customer->city_id . ',' . @$customer->country_id . ',' . @$customer->postal_code . '<br>
                            P: ' . @$customer->phone_code . " " . @$customer->sender_phone . ' <br>
                            E: ' . @$customer->email_address . ' <br>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <strong class="margin-bottom-5">For :</strong>
                        <p>
                            ' . _set_dash(@$order_info->receiver_name) . '<br>
                            Bafoussamstreet 451<br>
                            Yaoundé / Cameroun<br>
                            Phone: ' . _set_dash(@$order_info->receiver_phone) . ' <br>
                            ' . $this->lang->line('MSG_VO_10') . ': ' . _set_dash(@$order_info->greeting_msg) . '  <br>

                        </p>
                    </div>';
        $html .= '<table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th><strong>' . $this->lang->line('MSG_PRODUCT_CART_05') . '</strong></th>
                            <th><strong>' . $this->lang->line('MSG_PRODUCT_CART_06') . '</strong></th>
                            <th class="text-right"><strong>' . $this->lang->line('MSG_PRODUCT_CART_07') . '</strong></th>
                            <th><strong></strong></th>
                            <th class="text-center"><strong>' . $this->lang->line('MSG_PRODUCT_CART_08') . '</strong>
                            </th>
                            <th class="text-right"><strong>' . $this->lang->line('MSG_PRODUCT_CART_09') . '</strong></th>
                        </tr>
                        </thead>
                        <tbody>';

        $ctr = 0;
        $cart_total_item_qty = 0;
        $cart_amount_total = 0.0;
        $currency_code = @$order_info->payment_curr_code;
        foreach (@$order_detail as $k => $v) {
            $ctr++;
            $cart_total_item_qty += @$v->quantity;
            $cart_amount_total = $cart_amount_total + @$v->total_prize;
            $html .= '
            <tr>
                    <td class="text-center">' . $ctr . '</td>
                    <td><div
                                    style="background-image:url(' . (DIR_PRODUCT_URL . $v->product_image) . '); background-size: cover; height:100px; width:100px;"></div>
</td>
                    <td>' . @$v->product_name . '</td>
                    <td class="text-right">' . @$currency_code . @$v->product_prize . '</td>
                    <td class="text-center">X</td>
                    <td class="text-center">' . @$v->quantity . '</td>
                    <td class="text-right">' . @$currency_code . @$v->total_prize . '</td>
            </tr>
                ';
        }
        #sub total
        $html .= '<tr>
                            <td colspan="5"><strong>' . $this->lang->line('MSG_PRODUCT_CART_10') . '</strong></td>
                            <td class="text-center"><b>' . @$cart_total_item_qty . '</b></td>
                            <td class="text-right"
                                style="color:#36cd75 ;font-weight:bold;">' . @$currency_code . $cart_amount_total . '</td>
                            <td></td>
                        </tr>';
        #Service Fees
        $html .= '<tr>
                            <td colspan="5"><strong>' . $this->lang->line('MSG_PRODUCT_CART_11') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . @$order_info->service_tax . '</td>
                            <td></td>
                        </tr>';
        #Delivery Type section
        $html .= '<tr>
                            <td colspan="5"><strong>' . @$order_info->delivery_option . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . @$order_info->delivery_charges . '</td>
                            <td></td>
                        </tr>';
        #Discount section
        if (@$order_info->promo_code != '') {
            $html .= '<tr>
                            <td colspan="5"><strong>' . $this->lang->line('MSG_PRODUCT_CHECKOUT_10') . "(" . @$order_info->promo_code . ")" . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right">' . @$currency_code . @$order_info->promo_discount_amount . '</td>
                            <td></td>
                        </tr>';
        }
        #Grand Total section
        $html .= '<tr>
                            <td colspan="5"><strong>' . $this->lang->line('MSG_PRODUCT_CART_16') . '</strong></td>
                            <td class="text-center"><b></b></td>
                            <td class="text-right"><strong id="grandprice" style="color:#000;font-size:20px">' . @$currency_code . @$order_info->grand_total . '</strong></td>
                            <td></td>
                        </tr>';

        $html .= '</tbody>
                    </table>
                   </div>
                </div>';

        return $html;
    }

    public function paypal_return($type)
    {
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        if ($type == '') {
            redirect(base_url('/'));
        } else {
            $this->load->library('session');
            $cart = @$this->session->userdata('cart');
            $delivery_info = @$this->session->userdata('delivery_info');
            $promo_code = @$this->session->userdata('promo_code');
            $receiver_info = @$this->session->userdata('receiver_info');

            $cart_total_item_qty = 0;
            $cart_amount_total = 0.00;
            $service_fees = convert_price(SERVICE_FEES);
            foreach (@$cart as $k => $v) {
                $cart_total_item_qty += $v['quantity'];
                $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                $cart_amount_total = $cart_amount_total + $product_price;
            }
            $grand_total = ($cart_amount_total + $service_fees + convert_price(@$delivery_info->delivery_price)) - (convert_price(@$promo_code->promo_code_amount));
            #TODO order table
            if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost') {
                $ip_address = '127.0.0.1';
                #$ip_address = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip_address = $_SERVER['REMOTE_ADDR'];
            }
            $phone_no = $receiver_info->receiver_phone;
            $recp_name = $receiver_info->recp_name;
            $greeting_msg = $receiver_info->greeting_msg;
            $order_array = array(
                'customer_id' => $this->customer_id,
                'ip_address' => $ip_address, #TODO IP Address
                'random_rick' => 'expire',
                'generate_code' => ORDER_TEXT . time(),
                #'generate_code' => ORDER_TEXT . random_string('numeric', 10),
                'order_status' => ORDER_STATUS_WAIT,
                'order_view' => 1,
                'country_id' => @$this->session->userdata('order_info')->country_id,
                'city_id' => @$this->session->userdata('order_info')->city_id,
                'shop_id' => @$this->session->userdata('order_info')->shop_id,
                'contact_id' => 0,
                'receiver_name' => $recp_name,
                'greeting_msg' => $greeting_msg,
                'receiver_phone' => $phone_no,
                'payment_method' => PAYMENT_PAYPAL,
                'date_payment' => date('Y-m-d'),
                'discount' => 0,
                'welcome_discount' => 0,
                'promo_id' => @$promo_code->promo_id,
                'service_tax' => $service_fees,
                'delivery_option' => @$delivery_info->delivery_name,
                'delivery_price' => convert_price(@$delivery_info->delivery_price),
                'payment_curr' => $this->currency_code,
                'payment_curr_code' => $this->currency_symbol,
                'total_order' => $cart_amount_total,
                'sub_total' => ($cart_amount_total),
                'delivery_charges' => convert_price(@$delivery_info->delivery_price),
                'promo_discount_amount' => convert_price(@$promo_code->promo_code_amount),
                'grand_total' => ($grand_total),
                'promo_code' => @$promo_code->promo_code,
            );
            #_pre($order_array);
            $order_confirmation['order_confirmation'] = new stdClass();
            $order_confirmation['order_confirmation']->payment_type = PAYMENT_PAYPAL;
            $insert_response = $this->dbcommon->insert('order', $order_array);
            $insert_id = $this->db->insert_id();
            if ($insert_response) {
                foreach (@$cart as $k => $v) {
                    $product_unit_price = convert_price($v['price_temp']);
                    $product_all_unit_price = _number_format($product_unit_price * $v['quantity']);
                    $cart_item = array(
                        'order_id' => $insert_id,
                        'product_id' => $k,
                        'product_prize' => $product_unit_price,
                        'total_prize' => $product_all_unit_price,
                        'quantity' => $v['quantity'],
                    );
                    $insert_item_response = $this->dbcommon->insert('order_items', $cart_item);
                }
            }
            if ($type == 'S') {
                if ($insert_response) {
                    $this->order_mail($this->customer_id, $insert_id);#TODO MAIL Client Mail
                    $this->order_mail($this->customer_id, $insert_id, 1);#TODO MAIL Admin Mail
                    $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_SUCCESS;
                    $order_confirmation['order_confirmation']->order_info = $order_array;
                    $this->session->set_userdata($order_confirmation);
                    #$this->unset_cart(); #TODO
                    redirect(base_url(MENU_ORDER_CONFIRMATION));
                    exit;
                } else {
                }
            } else if ($type == 'F') {
                $order_confirmation['order_confirmation']->payment_status = ORDER_PAYMENT_STATUS_FAILED;
                $order_confirmation['order_confirmation']->order_info = $order_array;
                $this->session->set_userdata($order_confirmation);
                #$this->unset_cart(); #TODO
                redirect(base_url(MENU_ORDER_CONFIRMATION));
                exit;
            }
        }
        redirect(base_url('/'));
        exit;
    }

    public function order_confirmation()
    {
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        $this->unset_cart(); #TODO
        $order_confirmation = @$this->session->userdata('order_confirmation');
        if (count($order_confirmation) == 0) {
            redirect(base_url(MENU_MY_ORDER));
            exit;
        }
        $this->data['page_title'] = $this->lang->line('MSG_OC_01');
        $this->data['order_confirmation'] = $order_confirmation;
        $this->render(PRODUCT_D . ORDER_SUCCESS_V);
    }

    public function order_list()
    {
        /*$this->order_mail($this->customer_id,718);#TODO MAIL Client Mail
        $this->order_mail($this->customer_id,718,1);#TODO MAIL Admin Mail
        _e();*/
        if ($this->customer_id == '' || $this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        $order_list = $this->order_model->get_order_list($this->customer_id);
        #_pre($order_list);
        $this->data['page_title'] = $this->lang->line('MSG_VO_TITLE');
        $this->data['order_list'] = $order_list;
        $this->render(PRODUCT_D . ORDER_LIST_V);
    }

    public function order_detail_pop_up()
    {
        if (@$this->customer_id == '' || @$this->customer_id <= 0) {
            _set_flashdata(FLASH_STATUS_ERROR, $this->lang->line('MSG_PRODUCT_CHECKOUT_38'));
            redirect(base_url(MENU_LOG_IN));
            exit;
        }
        $order_id = $this->input->post('order_id');
        $order_response = $this->order_model->get_order_by_id(@$this->customer_id, $order_id);
        $order_detail_array = array();
        if (count($order_response) > 0) {
            $order_detail_array['order_info'] = $order_response;
            $order_detail_response = $this->order_model->get_order_detail_by_id(@$this->customer_id, $order_id);
            $select_response = $this->customer_model->get_customer_name($order_response->customer_id);
            $order_detail_array['customer'] = $select_response;
            // _pre($select_response);
            if (count($order_detail_response) > 0) {
                $order_detail_array['order_detail'] = $order_detail_response;
                $html = $this->detail_view($order_detail_array);
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => $this->lang->line('MSG_VO_05'), 'html' => $html));
                exit;
            } else {
                $html = '';
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06')));
                exit;
            }

        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => $this->lang->line('MSG_VO_06')));
            exit;
        }
    }
}
<style>
    .account-main-title {
        background: #2bb673 !important;
        padding: 9px 14px !important;
        color: #fff !important;
    }

    .bg-box {
        /*background: #ececec !important;*/
    }

    .bb_panel {
        background-color: #ececec !important;
        border: 0 !important;
    }

    .panel-default > .panel-heading {
        background-image: none !important;
        color: #fff !important;
        background-color: #36cd75 !important;
        border-color: #ddd;
    }
</style>
<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_MA_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
<!-- =========================
    My Account SECTION
============================== -->
<section id="login" class=" p-y-lg1 bg-color">
    <div class="container">
        <div class="row">
            <?php _notify(); ?>
        </div>
        <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-1">&nbsp;</div>
        </div>
        <form class="form-horizontal" id="form_login" role="form" method="post"
              action="<?= base_url(MENU_MY_ACCOUNT_UPDATE) ?>">
            <div class="row bg-box ">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <h2><?= $this->lang->line('MSG_MA_02') ?></h2>

                        <p><?= $this->lang->line('MSG_MA_03') ?></p>
                    </div>
                    <div class="col-md-4 pull-right  ">
                        <a href="<?php echo base_url(MENU_CHANGE_PASSWORD); ?>"
                           class="pull-right  btn btn-blue"><?= $this->lang->line('MSG_CP_01') ?></a>
                    </div>
                </div>
                <div class="col-md-12 m-b-0 " style="margin-top: 15px;">
                    <div class="panel panel-default bb_panel">
                        <div class="panel-heading"><h4><strong><?= $this->lang->line('MSG_MA_04') ?></strong></h4></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <!--<div class="form-group">
                                    <label class="col-sm-4 control-label">I am*</label>
                                    <div class="col-sm-8">
                                        <select name="home_gender"  id="home_gender" class="form-control" required>
                                            <option value=""> Select Gender </option>
                                            <option value="Male" <?php if (@$home_gender == 'Male') { ?> selected="" <?php } ?>> Male </option>
                                            <option value="Female" <?php if (@$home_gender == 'Female') { ?> selected="" <?php } ?>> Female</option>
                                        </select>
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_04') ?>*</label>

                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="username" name="username"
                                               value="<?php echo @$customer_info->username; ?>" required>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="disabledInput" class="col-sm-4 control-label">Date of birth*</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text"  name="dob"  id="dob" value="<?php if (@$dob != '' && @$dob != '0000-00-00') {
                                    echo @$dob;
                                } ?>"  placeholder="YYYY-MM-DD" data-date-format="YYYY-MM-DD" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Enter a date in this formart YYYY-MM-DD" required readonly>
                                        <!--<input type="text" name="input" placeholder="YYYY-MM-DD" required
                                               pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"
                                               title="Enter a date in this format YYYY-MM-DD"/>-
                                    </div>
                                </div>-->

                            </div>
                            <div class="col-md-6">
                                <!--<div class="form-group">
                                    <label for="disabledInput" class="col-sm-2 control-label"> &nbsp;</label>
                                    <div class="col-sm-4">
                                        &nbsp;
                                    </div>
                                </div>-->
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_06') ?>*</label>

                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="lastname" name="lastname"
                                               value="<?php echo @$customer_info->lastname; ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
                                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                <script>
                                    $(function () {
                                        $("#dob").datepicker({
                                            dateFormat: 'yy-mm-dd', changeMonth: true,
                                            changeYear: true,
                                            yearRange: '-40:+0'
                                        });
                                    });
                                </script>
                                <span class="text-right pull-right"><?= $this->lang->line('MSG_MA_07') ?>&nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 m-b-0 " style="margin-top: 15px;">
                    <div class="panel panel-default bb_panel">
                        <div class="panel-heading"><h4><strong><?= $this->lang->line('MSG_MA_08') ?></strong></h4></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_09') ?>
                                        *</label>

                                    <div class="col-sm-8">
                                        <input class="form-control" type="email"
                                               value="<?php echo @$customer_info->email_address; ?>"
                                               placeholder="<?= $this->lang->line('MSG_MA_10') ?>" required readonly
                                               disabled>
                                    </div>
                                    <!--<label for="disabledInput" class="col-sm-2 control-label">Phone Number*</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="tel" id="phone_code" name="phone_code"  placeholder="" value="<?php /*echo @$phone_code; */ ?>" readonly>
                                    </div>
                                    <div class="col-sm-3" style="    margin-left: 19px;
    width: 200px;">
                                        <input class="form-control" type="text"  id="sender_number" name="sender_number" value="<?php /*echo $sender_number; */ ?>" placeholder="+918000255245" required>
                                    </div>-->
                                    <div class="col-sm-4">
                                        <!--<input type="tel" class="form-control" id="mobile_number" name="mobile_number" value="<?php /*echo $sender_number; */ ?>" placeholder="+49176xxxxxxxx ">-->
                                        <!--<input class="form-control" type="hidden" id="phone_code" name="phone_code" value="<?php /*echo @$phone_code; */ ?>" readonly>
                                    <input class="form-control" type="text"  id="sender_number" name="sender_number" value="<?php /*echo $sender_number; */ ?>" placeholder="+918000255245" required>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_11') ?>*</label>

                                    <div class="col-sm-2">
                                        <input class="form-control" type="tel" id="phone_code" name="phone_code"
                                               placeholder="" value="<?php echo @$customer_info->phone_code; ?>"
                                               readonly>
                                    </div>
                                    <div class="col-sm-6" style=" margin-left: 19px; width: 200px;">
                                        <input class="form-control" type="text" id="sender_number" name="sender_number"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               value="<?php echo @$customer_info->sender_phone; ?>"
                                               placeholder="15259948834" required>
                                    </div>
                                    <!--<div class="col-sm-4">-->
                                    <!--<input type="tel" class="form-control" id="mobile_number" name="mobile_number" value="<?php /*echo $sender_number; */ ?>" placeholder="+49176xxxxxxxx ">-->
                                    <!--<input class="form-control" type="hidden" id="phone_code" name="phone_code" value="<?php /*echo @$phone_code; */ ?>" readonly>
                                    <input class="form-control" type="text"  id="sender_number" name="sender_number" value="<?php /*echo $sender_number; */ ?>" placeholder="+918000255245" required>-->
                                    <!--</div>-->
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="text-right pull-right"><?= $this->lang->line('MSG_MA_07') ?> &nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 m-b-0 " style="margin-top: 15px;">
                    <div class="panel panel-default bb_panel">
                        <div class="panel-heading"><h4><strong><?= $this->lang->line('MSG_MA_12') ?></strong></h4></div>
                        <div class="panel-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_13') ?>*</label>

                                    <div class="col-sm-8">
                                        <input type="hidden" name="country_id" id="country_id"
                                               value="<?php echo @$customer_info->country_id; ?>">
                                        <select name="country_code" id="country_code" class="form-control">
                                            <option value='ad' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ad"
                                                    data-title="Andorra"  <?php if (@$customer_info->country_code == 'ad') { ?> selected="" <?php } ?>>
                                                Andorra
                                            </option>
                                            <option value='ae' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ae"
                                                    data-title="United Arab Emirates"  <?php if (@$customer_info->country_code == 'ae') { ?> selected="" <?php } ?>>
                                                United Arab Emirates
                                            </option>
                                            <option value='af' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag af"
                                                    data-title="Afghanistan"  <?php if (@$customer_info->country_code == 'af') { ?> selected="" <?php } ?>>
                                                Afghanistan
                                            </option>
                                            <option value='ag' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ag"
                                                    data-title="Antigua and Barbuda"  <?php if (@$customer_info->country_code == 'ag') { ?> selected="" <?php } ?>>
                                                Antigua and Barbuda
                                            </option>
                                            <option value='ai' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ai"
                                                    data-title="Anguilla"  <?php if (@$customer_info->country_code == 'ai') { ?> selected="" <?php } ?>>
                                                Anguilla
                                            </option>
                                            <option value='al' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag al"
                                                    data-title="Albania"  <?php if (@$customer_info->country_code == 'al') { ?> selected="" <?php } ?>>
                                                Albania
                                            </option>
                                            <option value='am' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag am"
                                                    data-title="Armenia"  <?php if (@$customer_info->country_code == 'am') { ?> selected="" <?php } ?>>
                                                Armenia
                                            </option>
                                            <option value='an' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag an"
                                                    data-title="Netherlands Antilles" <?php if (@$customer_info->country_code == 'an') { ?> selected="" <?php } ?>>
                                                Netherlands Antilles
                                            </option>
                                            <option value='ao' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ao"
                                                    data-title="Angola" <?php if (@$customer_info->country_code == 'ao') { ?> selected="" <?php } ?>>
                                                Angola
                                            </option>
                                            <option value='aq' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag aq"
                                                    data-title="Antarctica" <?php if (@$customer_info->country_code == 'aq') { ?> selected="" <?php } ?>>
                                                Antarctica
                                            </option>
                                            <option value='ar' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ar"
                                                    data-title="Argentina" <?php if (@$customer_info->country_code == 'ar') { ?> selected="" <?php } ?>>
                                                Argentina
                                            </option>
                                            <option value='as' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag as"
                                                    data-title="American Samoa" <?php if (@$customer_info->country_code == 'as') { ?> selected="" <?php } ?>>
                                                American Samoa
                                            </option>
                                            <option value='at' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag at"
                                                    data-title="Austria"  <?php if (@$customer_info->country_code == 'at') { ?> selected="" <?php } ?>>
                                                Austria
                                            </option>
                                            <option value='au' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag au"
                                                    data-title="Australia"  <?php if (@$customer_info->country_code == 'au') { ?> selected="" <?php } ?>>
                                                Australia
                                            </option>
                                            <option value='aw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag aw"
                                                    data-title="Aruba" <?php if (@$customer_info->country_code == 'aw') { ?> selected="" <?php } ?>>
                                                Aruba
                                            </option>
                                            <option value='ax' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ax"
                                                    data-title="Aland Islands" <?php if (@$customer_info->country_code == 'ax') { ?> selected="" <?php } ?>>
                                                Aland Islands
                                            </option>
                                            <option value='az' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag az"
                                                    data-title="Azerbaijan" <?php if (@$customer_info->country_code == 'az') { ?> selected="" <?php } ?>>
                                                Azerbaijan
                                            </option>
                                            <option value='ba' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ba"
                                                    data-title="Bosnia and Herzegovina"  <?php if (@$customer_info->country_code == 'ba') { ?> selected="" <?php } ?>>
                                                Bosnia and Herzegovina
                                            </option>
                                            <option value='bb' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bb"
                                                    data-title="Barbados" <?php if (@$customer_info->country_code == 'bb') { ?> selected="" <?php } ?>>
                                                Barbados
                                            </option>
                                            <option value='bd' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bd"
                                                    data-title="Bangladesh" <?php if (@$customer_info->country_code == 'bd') { ?> selected="" <?php } ?>>
                                                Bangladesh
                                            </option>
                                            <option value='be' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag be"
                                                    data-title="Belgium" <?php if (@$customer_info->country_code == 'be') { ?> selected="" <?php } ?>>
                                                Belgium
                                            </option>
                                            <option value='bf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bf"
                                                    data-title="Burkina Faso" <?php if (@$customer_info->country_code == 'bf') { ?> selected="" <?php } ?>>
                                                Burkina Faso
                                            </option>
                                            <option value='bg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bg"
                                                    data-title="Bulgaria" <?php if (@$customer_info->country_code == 'bg') { ?> selected="" <?php } ?>>
                                                Bulgaria
                                            </option>
                                            <option value='bh' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bh"
                                                    data-title="Bahrain" <?php if (@$customer_info->country_code == 'bh') { ?> selected="" <?php } ?>>
                                                Bahrain
                                            </option>
                                            <option value='bi' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bi"
                                                    data-title="Burundi" <?php if (@$customer_info->country_code == 'bi') { ?> selected="" <?php } ?>>
                                                Burundi
                                            </option>
                                            <option value='bj' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bj"
                                                    data-title="Benin" <?php if (@$customer_info->country_code == 'bj') { ?> selected="" <?php } ?>>
                                                Benin
                                            </option>
                                            <option value='bm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bm"
                                                    data-title="Bermuda" <?php if (@$customer_info->country_code == 'bm') { ?> selected="" <?php } ?>>
                                                Bermuda
                                            </option>
                                            <option value='bn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bn"
                                                    data-title="Brunei Darussalam" <?php if (@$customer_info->country_code == 'bn') { ?> selected="" <?php } ?>>
                                                Brunei Darussalam
                                            </option>
                                            <option value='bo' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bo"
                                                    data-title="Bolivia" <?php if (@$customer_info->country_code == 'bo') { ?> selected="" <?php } ?>>
                                                Bolivia
                                            </option>
                                            <option value='br' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag br" data-title="Brazil">Brazil
                                            </option>
                                            <option value='bs' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bs" data-title="Bahamas">Bahamas
                                            </option>
                                            <option value='bt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bt" data-title="Bhutan">Bhutan
                                            </option>
                                            <option value='bv' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bv" data-title="Bouvet Island">Bouvet Island
                                            </option>
                                            <option value='bw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bw" data-title="Botswana">Botswana
                                            </option>
                                            <option value='by' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag by" data-title="Belarus">Belarus
                                            </option>
                                            <option value='bz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag bz" data-title="Belize">Belize
                                            </option>
                                            <option value='ca' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ca" data-title="Canada">Canada
                                            </option>
                                            <option value='cc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cc" data-title="Cocos (Keeling) Islands">Cocos
                                                (Keeling) Islands
                                            </option>
                                            <option value='cd' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cd"
                                                    data-title="Democratic Republic of the Congo">Democratic Republic of
                                                the Congo
                                            </option>
                                            <option value='cf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cf" data-title="Central African Republic">
                                                Central African Republic
                                            </option>
                                            <option value='cg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cg" data-title="Congo">Congo
                                            </option>
                                            <option value='ch' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ch" data-title="Switzerland">Switzerland
                                            </option>
                                            <option value='ci' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ci" data-title="Cote D'Ivoire (Ivory Coast)">
                                                Cote D'Ivoire (Ivory Coast)
                                            </option>
                                            <option value='ck' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ck" data-title="Cook Islands">Cook Islands
                                            </option>
                                            <option value='cl' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cl" data-title="Chile">Chile
                                            </option>
                                            <option value='cm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cm" data-title="Cameroon">Cameroon
                                            </option>
                                            <option value='cn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cn" data-title="China">China
                                            </option>
                                            <option value='co' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag co" data-title="Colombia">Colombia
                                            </option>
                                            <option value='cr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cr" data-title="Costa Rica">Costa Rica
                                            </option>
                                            <option value='cs' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cs" data-title="Serbia and Montenegro">Serbia
                                                and Montenegro
                                            </option>
                                            <option value='cu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cu" data-title="Cuba">Cuba
                                            </option>
                                            <option value='cv' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cv" data-title="Cape Verde">Cape Verde
                                            </option>
                                            <option value='cx' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cx" data-title="Christmas Island">Christmas
                                                Island
                                            </option>
                                            <option value='cy' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cy" data-title="Cyprus">Cyprus
                                            </option>
                                            <option value='cz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag cz" data-title="Czech Republic">Czech Republic
                                            </option>
                                            <option value='de' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag de" data-title="Germany" selected="selected">
                                                Germany
                                            </option>
                                            <option value='dj' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag dj" data-title="Djibouti">Djibouti
                                            </option>
                                            <option value='dk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag dk" data-title="Denmark">Denmark
                                            </option>
                                            <option value='dm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag dm" data-title="Dominica">Dominica
                                            </option>
                                            <option value='do' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag do" data-title="Dominican Republic">Dominican
                                                Republic
                                            </option>
                                            <option value='dz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag dz" data-title="Algeria">Algeria
                                            </option>
                                            <option value='ec' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ec" data-title="Ecuador">Ecuador
                                            </option>
                                            <option value='ee' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ee" data-title="Estonia">Estonia
                                            </option>
                                            <option value='eg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag eg" data-title="Egypt">Egypt
                                            </option>
                                            <option value='eh' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag eh" data-title="Western Sahara">Western Sahara
                                            </option>
                                            <option value='er' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag er" data-title="Eritrea">Eritrea
                                            </option>
                                            <option value='es' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag es" data-title="Spain">Spain
                                            </option>
                                            <option value='et' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag et" data-title="Ethiopia">Ethiopia
                                            </option>
                                            <option value='fi' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fi" data-title="Finland">Finland
                                            </option>
                                            <option value='fj' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fj" data-title="Fiji">Fiji
                                            </option>
                                            <option value='fk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fk" data-title="Falkland Islands (Malvinas)">
                                                Falkland Islands (Malvinas)
                                            </option>
                                            <option value='fm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fm" data-title="Federated States of Micronesia">
                                                Federated States of Micronesia
                                            </option>
                                            <option value='fo' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fo" data-title="Faroe Islands">Faroe Islands
                                            </option>


                                            <option value='fr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fr" data-title="France">France
                                            </option>
                                            <option value='fx' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag fx" data-title="France, Metropolitan">France,
                                                Metropolitan
                                            </option>
                                            <option value='ga' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ga" data-title="Gabon">Gabon
                                            </option>
                                            <option value='gb' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gb" data-title="Great Britain (UK)">Great
                                                Britain (UK)
                                            </option>
                                            <option value='gd' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gd" data-title="Grenada">Grenada
                                            </option>
                                            <option value='ge' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ge" data-title="Georgia">Georgia
                                            </option>
                                            <option value='gf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gf" data-title="French Guiana">French Guiana
                                            </option>
                                            <option value='gh' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gh" data-title="Ghana">Ghana
                                            </option>
                                            <option value='gi' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gi" data-title="Gibraltar">Gibraltar
                                            </option>
                                            <option value='gl' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gl" data-title="Greenland">Greenland
                                            </option>
                                            <option value='gm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gm" data-title="Gambia">Gambia
                                            </option>
                                            <option value='gn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gn" data-title="Guinea">Guinea
                                            </option>
                                            <option value='gp' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gp" data-title="Guadeloupe">Guadeloupe
                                            </option>
                                            <option value='gq' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gq" data-title="Equatorial Guinea">Equatorial
                                                Guinea
                                            </option>
                                            <option value='gr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gr" data-title="Greece">Greece
                                            </option>
                                            <option value='gs' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gs"
                                                    data-title="S. Georgia and S. Sandwich Islands">S. Georgia and S.
                                                Sandwich Islands
                                            </option>
                                            <option value='gt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gt" data-title="Guatemala">Guatemala
                                            </option>
                                            <option value='gu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gu" data-title="Guam">Guam
                                            </option>
                                            <option value='gw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gw" data-title="Guinea-Bissau">Guinea-Bissau
                                            </option>
                                            <option value='gy' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag gy" data-title="Guyana">Guyana
                                            </option>
                                            <option value='hk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag hk" data-title="Hong Kong">Hong Kong
                                            </option>
                                            <option value='hm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag hm"
                                                    data-title="Heard Island and McDonald Islands">Heard Island and
                                                McDonald Islands
                                            </option>
                                            <option value='hn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag hn" data-title="Honduras">Honduras
                                            </option>
                                            <option value='hr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag hr" data-title="Croatia (Hrvatska)">Croatia
                                                (Hrvatska)
                                            </option>
                                            <option value='ht' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ht" data-title="Haiti">Haiti
                                            </option>
                                            <option value='hu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag hu" data-title="Hungary">Hungary
                                            </option>
                                            <option value='id' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag id" data-title="Indonesia">Indonesia
                                            </option>
                                            <option value='ie' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ie" data-title="Ireland">Ireland
                                            </option>
                                            <option value='il' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag il" data-title="Israel">Israel
                                            </option>
                                            <option value='in' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag in" data-title="India">India
                                            </option>
                                            <option value='io' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag io" data-title="British Indian Ocean Territory">
                                                British Indian Ocean Territory
                                            </option>
                                            <option value='iq' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag iq" data-title="Iraq">Iraq
                                            </option>
                                            <option value='ir' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ir" data-title="Iran">Iran
                                            </option>
                                            <option value='is' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag is" data-title="Iceland">Iceland
                                            </option>
                                            <option value='it' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag it" data-title="Italy">Italy
                                            </option>
                                            <option value='jm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag jm" data-title="Jamaica">Jamaica
                                            </option>
                                            <option value='jo' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag jo" data-title="Jordan">Jordan
                                            </option>
                                            <option value='jp' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag jp" data-title="Japan">Japan
                                            </option>
                                            <option value='ke' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ke" data-title="Kenya">Kenya
                                            </option>
                                            <option value='kg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kg" data-title="Kyrgyzstan">Kyrgyzstan
                                            </option>
                                            <option value='kh' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kh" data-title="Cambodia">Cambodia
                                            </option>
                                            <option value='ki' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ki" data-title="Kiribati">Kiribati
                                            </option>
                                            <option value='km' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag km" data-title="Comoros">Comoros
                                            </option>
                                            <option value='kn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kn" data-title="Saint Kitts and Nevis">Saint
                                                Kitts and Nevis
                                            </option>
                                            <option value='kp' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kp" data-title="Korea (North)">Korea (North)
                                            </option>
                                            <option value='kr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kr" data-title="Korea (South)">Korea (South)
                                            </option>
                                            <option value='kw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kw" data-title="Kuwait">Kuwait
                                            </option>
                                            <option value='ky' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ky" data-title="Cayman Islands">Cayman Islands
                                            </option>
                                            <option value='kz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag kz" data-title="Kazakhstan">Kazakhstan
                                            </option>
                                            <option value='la' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag la" data-title="Laos">Laos
                                            </option>
                                            <option value='lb' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lb" data-title="Lebanon">Lebanon
                                            </option>
                                            <option value='lc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lc" data-title="Saint Lucia">Saint Lucia
                                            </option>
                                            <option value='li' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag li" data-title="Liechtenstein">Liechtenstein
                                            </option>
                                            <option value='lk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lk" data-title="Sri Lanka">Sri Lanka
                                            </option>
                                            <option value='lr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lr" data-title="Liberia">Liberia
                                            </option>
                                            <option value='ls' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ls" data-title="Lesotho">Lesotho
                                            </option>
                                            <option value='lt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lt" data-title="Lithuania">Lithuania
                                            </option>
                                            <option value='lu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lu" data-title="Luxembourg">Luxembourg
                                            </option>
                                            <option value='lv' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag lv" data-title="Latvia">Latvia
                                            </option>
                                            <option value='ly' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ly" data-title="Libya">Libya
                                            </option>
                                            <option value='ma' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ma" data-title="Morocco">Morocco
                                            </option>
                                            <option value='mc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mc" data-title="Monaco">Monaco
                                            </option>
                                            <option value='md' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag md" data-title="Moldova">Moldova
                                            </option>
                                            <option value='mg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mg" data-title="Madagascar">Madagascar
                                            </option>
                                            <option value='mh' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mh" data-title="Marshall Islands">Marshall
                                                Islands
                                            </option>
                                            <option value='mk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mk" data-title="Macedonia">Macedonia
                                            </option>
                                            <option value='ml' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ml" data-title="Mali">Mali
                                            </option>
                                            <option value='mm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mm" data-title="Myanmar">Myanmar
                                            </option>
                                            <option value='mn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mn" data-title="Mongolia">Mongolia
                                            </option>
                                            <option value='mo' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mo" data-title="Macao">Macao
                                            </option>
                                            <option value='mp' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mp" data-title="Northern Mariana Islands">
                                                Northern Mariana Islands
                                            </option>
                                            <option value='mq' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mq" data-title="Martinique">Martinique
                                            </option>
                                            <option value='mr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mr" data-title="Mauritania">Mauritania
                                            </option>
                                            <option value='ms' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ms" data-title="Montserrat">Montserrat
                                            </option>
                                            <option value='mt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mt" data-title="Malta">Malta
                                            </option>
                                            <option value='mu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mu" data-title="Mauritius">Mauritius
                                            </option>
                                            <option value='mv' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mv" data-title="Maldives">Maldives
                                            </option>
                                            <option value='mw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mw" data-title="Malawi">Malawi
                                            </option>
                                            <option value='mx' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mx" data-title="Mexico">Mexico
                                            </option>
                                            <option value='my' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag my" data-title="Malaysia">Malaysia
                                            </option>
                                            <option value='mz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag mz" data-title="Mozambique">Mozambique
                                            </option>
                                            <option value='na' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag na" data-title="Namibia">Namibia
                                            </option>
                                            <option value='nc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag nc" data-title="New Caledonia">New Caledonia
                                            </option>
                                            <option value='ne' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ne" data-title="Niger">Niger
                                            </option>
                                            <option value='nf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag nf" data-title="Norfolk Island">Norfolk Island
                                            </option>
                                            <option value='ng' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ng" data-title="Nigeria">Nigeria
                                            </option>
                                            <option value='ni' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ni" data-title="Nicaragua">Nicaragua
                                            </option>
                                            <option value='nl' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag nl" data-title="Netherlands">Netherlands
                                            </option>
                                            <option value='no' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag no" data-title="Norway">Norway
                                            </option>
                                            <option value='np' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag np" data-title="Nepal">Nepal
                                            </option>
                                            <option value='nr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag nr" data-title="Nauru">Nauru
                                            </option>
                                            <option value='nu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag nu" data-title="Niue">Niue
                                            </option>
                                            <option value='nz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag nz" data-title="New Zealand (Aotearoa)">New
                                                Zealand (Aotearoa)
                                            </option>
                                            <option value='om' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag om" data-title="Oman">Oman
                                            </option>
                                            <option value='pa' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pa" data-title="Panama">Panama
                                            </option>
                                            <option value='pe' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pe" data-title="Peru">Peru
                                            </option>
                                            <option value='pf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pf" data-title="French Polynesia">French
                                                Polynesia
                                            </option>
                                            <option value='pg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pg" data-title="Papua New Guinea">Papua New
                                                Guinea
                                            </option>
                                            <option value='ph' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ph" data-title="Philippines">Philippines
                                            </option>
                                            <option value='pk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pk" data-title="Pakistan">Pakistan
                                            </option>
                                            <option value='pl' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pl" data-title="Poland">Poland
                                            </option>
                                            <option value='pm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pm" data-title="Saint Pierre and Miquelon">Saint
                                                Pierre and Miquelon
                                            </option>
                                            <option value='pn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pn" data-title="Pitcairn">Pitcairn
                                            </option>
                                            <option value='pr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pr" data-title="Puerto Rico">Puerto Rico
                                            </option>
                                            <option value='ps' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ps" data-title="Palestinian Territory">
                                                Palestinian Territory
                                            </option>
                                            <option value='pt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pt" data-title="Portugal">Portugal
                                            </option>
                                            <option value='pw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag pw" data-title="Palau">Palau
                                            </option>
                                            <option value='py' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag py" data-title="Paraguay">Paraguay
                                            </option>
                                            <option value='qa' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag qa" data-title="Qatar">Qatar
                                            </option>
                                            <option value='re' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag re" data-title="Reunion">Reunion
                                            </option>
                                            <option value='ro' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ro" data-title="Romania">Romania
                                            </option>
                                            <option value='ru' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ru" data-title="Russian Federation">Russian
                                                Federation
                                            </option>
                                            <option value='rw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag rw" data-title="Rwanda">Rwanda
                                            </option>
                                            <option value='sa' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sa" data-title="Saudi Arabia">Saudi Arabia
                                            </option>
                                            <option value='sb' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sb" data-title="Solomon Islands">Solomon Islands
                                            </option>
                                            <option value='sc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sc" data-title="Seychelles">Seychelles
                                            </option>
                                            <option value='sd' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sd" data-title="Sudan">Sudan
                                            </option>
                                            <option value='se' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag se" data-title="Sweden">Sweden
                                            </option>
                                            <option value='sg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sg" data-title="Singapore">Singapore
                                            </option>
                                            <option value='sh' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sh" data-title="Saint Helena">Saint Helena
                                            </option>
                                            <option value='si' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag si" data-title="Slovenia">Slovenia
                                            </option>
                                            <option value='sj' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sj" data-title="Svalbard and Jan Mayen">Svalbard
                                                and Jan Mayen
                                            </option>
                                            <option value='sk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sk" data-title="Slovakia">Slovakia
                                            </option>
                                            <option value='sl' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sl" data-title="Sierra Leone">Sierra Leone
                                            </option>
                                            <option value='sm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sm" data-title="San Marino">San Marino
                                            </option>
                                            <option value='sn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sn" data-title="Senegal">Senegal
                                            </option>
                                            <option value='so' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag so" data-title="Somalia">Somalia
                                            </option>
                                            <option value='sr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sr" data-title="Suriname">Suriname
                                            </option>
                                            <option value='st' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag st" data-title="Sao Tome and Principe">Sao Tome
                                                and Principe
                                            </option>
                                            <option value='su' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag su" data-title="USSR (former)">USSR (former)
                                            </option>
                                            <option value='sv' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sv" data-title="El Salvador">El Salvador
                                            </option>
                                            <option value='sy' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sy" data-title="Syria">Syria
                                            </option>
                                            <option value='sz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag sz" data-title="Swaziland">Swaziland
                                            </option>
                                            <option value='tc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tc" data-title="Turks and Caicos Islands">Turks
                                                and Caicos Islands
                                            </option>
                                            <option value='td' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag td" data-title="Chad">Chad
                                            </option>
                                            <option value='tf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tf" data-title="French Southern Territories">
                                                French Southern Territories
                                            </option>
                                            <option value='tg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tg" data-title="Togo">Togo
                                            </option>
                                            <option value='th' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag th" data-title="Thailand">Thailand
                                            </option>
                                            <option value='tj' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tj" data-title="Tajikistan">Tajikistan
                                            </option>
                                            <option value='tk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tk" data-title="Tokelau">Tokelau
                                            </option>
                                            <option value='tl' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tl" data-title="Timor-Leste">Timor-Leste
                                            </option>
                                            <option value='tm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tm" data-title="Turkmenistan">Turkmenistan
                                            </option>
                                            <option value='tn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tn" data-title="Tunisia">Tunisia
                                            </option>
                                            <option value='to' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag to" data-title="Tonga">Tonga
                                            </option>
                                            <option value='tp' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tp" data-title="East Timor">East Timor
                                            </option>
                                            <option value='tr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tr" data-title="Turkey">Turkey
                                            </option>
                                            <option value='tt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tt" data-title="Trinidad and Tobago">Trinidad
                                                and Tobago
                                            </option>
                                            <option value='tv' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tv" data-title="Tuvalu">Tuvalu
                                            </option>
                                            <option value='tw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tw" data-title="Taiwan">Taiwan
                                            </option>
                                            <option value='tz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag tz" data-title="Tanzania">Tanzania
                                            </option>
                                            <option value='ua' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ua" data-title="Ukraine">Ukraine
                                            </option>
                                            <option value='ug' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ug" data-title="Uganda">Uganda
                                            </option>
                                            <option value='uk' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag uk" data-title="United Kingdom">United Kingdom
                                            </option>
                                            <option value='um' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag um"
                                                    data-title="United States Minor Outlying Islands">United States
                                                Minor Outlying Islands
                                            </option>
                                            <option value='us' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag us" data-title="United States">United States
                                            </option>
                                            <option value='uy' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag uy" data-title="Uruguay">Uruguay
                                            </option>
                                            <option value='uz' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag uz" data-title="Uzbekistan">Uzbekistan
                                            </option>
                                            <option value='va' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag va" data-title="Vatican City State (Holy See)">
                                                Vatican City State (Holy See)
                                            </option>
                                            <option value='vc' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag vc"
                                                    data-title="Saint Vincent and the Grenadines">Saint Vincent and the
                                                Grenadines
                                            </option>
                                            <option value='ve' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ve" data-title="Venezuela">Venezuela
                                            </option>
                                            <option value='vg' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag vg" data-title="Virgin Islands (British)">Virgin
                                                Islands (British)
                                            </option>
                                            <option value='vi' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag vi" data-title="Virgin Islands (U.S.)">Virgin
                                                Islands (U.S.)
                                            </option>
                                            <option value='vn' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag vn" data-title="Viet Nam">Viet Nam
                                            </option>
                                            <option value='vu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag vu" data-title="Vanuatu">Vanuatu
                                            </option>
                                            <option value='wf' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag wf" data-title="Wallis and Futuna">Wallis and
                                                Futuna
                                            </option>
                                            <option value='ws' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ws" data-title="Samoa">Samoa
                                            </option>
                                            <option value='ye' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag ye" data-title="Yemen">Yemen
                                            </option>
                                            <option value='yt' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag yt" data-title="Mayotte">Mayotte
                                            </option>
                                            <option value='yu' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag yu" data-title="Yugoslavia (former)">Yugoslavia
                                                (former)
                                            </option>
                                            <option value='za' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag za" data-title="South Africa">South Africa
                                            </option>
                                            <option value='zm' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag zm" data-title="Zambia">Zambia
                                            </option>
                                            <option value='zr' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag zr" data-title="Zaire (former)">Zaire (former)
                                            </option>
                                            <option value='zw' data-image="images/msdropdown/icons/blank.gif"
                                                    data-imagecss="flag zw" data-title="Zimbabwe">Zimbabwe
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_14') ?>*</label>

                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="city_id" name="city_id"
                                               value="<?php echo @$customer_info->city_id; ?>" placeholder="Enter City"
                                               required>
                                    </div>
                                    <!--<label for="disabledInput" class="col-sm-2 control-label">Postal Code*</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text"  id="postal_code" name="postal_code" value="<?php /*echo $postal_code; */ ?>" placeholder="Enter Postal Code" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                </div>-->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_15') ?>*</label>

                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" id="home_address" name="home_address"
                                               value="<?php echo @$customer_info->home_address; ?>" placeholder=""
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="disabledInput"
                                           class="col-sm-4 control-label"><?= $this->lang->line('MSG_MA_16') ?>*</label>

                                    <div class="col-sm-8">
                                        <!--<input class="form-control" type="text"  id="postal_code" name="postal_code" value="<?php echo @$customer_info->postal_code; ?>" placeholder="Enter Postal Code" required onkeypress='return event.charCode >= 48 && event.charCode <= 57'>-->
                                        <input class="form-control" type="text" id="postal_code" name="postal_code"
                                               value="<?php echo @$customer_info->postal_code; ?>"
                                               placeholder="Enter Postal Code">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="text-right pull-right"><?= $this->lang->line('MSG_MA_07') ?> &nbsp;&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 m-b-0 " style="margin-top: 15px;">
                    <div class="panel panel-default bb_panel">
                        <div class="panel-heading"><h4><strong><?= $this->lang->line('MSG_MA_17') ?></strong></h4></div>
                        <span class="text-left pull-left">&nbsp;&nbsp;&nbsp;<?= $this->lang->line('MSG_MA_18') ?>.&nbsp;&nbsp;&nbsp;</span><br>

                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12 form-control">
                                    <label class="checkbox-inline"><input type="checkbox" name="notification_email"
                                                                          id="notification_email"
                                                                          value="1" <?php if (@$customer_info->notification_email == 1) { ?> checked="checked" <?php } ?>><?= $this->lang->line('MSG_MA_19') ?>
                                    </label>
                                </div>
                                <div class="col-sm-12 form-control">
                                    <label class="checkbox-inline"><input type="checkbox" name="notification_text"
                                                                          id="notification_text"
                                                                          value="1" <?php if (@$customer_info->notification_text == 1) { ?> checked="checked" <?php } ?>><?= $this->lang->line('MSG_MA_20') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 pull-right  ">
                    <button type="submit"
                            class="pull-right  btn btn-blue"><?= $this->lang->line('MSG_MA_21') ?></button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 pull-right  ">
                </div>
            </div>
        </form>
    </div>

</section>
<!-- /End  Section -->

<script>
    $(document).ready(function () {
        //swal("Login Error !", "asda", "error");
        //toastr.error('Vikrant', '<?=ERROR?>',{positionClass: 'toast-top-full-width'});
        $("#form_login").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                /* home_gender: {
                 required: true
                 },*/
                username: {
                    required: true
                },
                lastname: {
                    required: true
                },
                /* dob: {
                 required: true
                 },*/
                sender_number: {
                    required: true
                },
                countries: {
                    required: true
                },
                city_id: {
                    required: true
                },
                postal_code: {
                    required: true
                },
                home_address: {
                    required: true
                }
            },
            messages: {
                /* home_gender: {
                 required: 'Please Select gender first.'
                 },*/
                username: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_01');  ?>'
                },
                lastname: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_02');  ?>'
                },
                /*email: {
                 required: 'Please Enter Email Id.',
                 email: 'Please Enter Valid Email Id.'
                 },*/
                /* dob: {
                 required: 'Please Select date of birth.'
                 },*/
                countries: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_03');  ?>'
                },
                city_id: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_04');  ?>'
                },
                sender_number: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_05');  ?>'
                },
                home_address: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_06');  ?>'
                },
                postal_code: {
                    required: '<?php echo $this->lang->line('LBL_MA_VALIDATION_07');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            /*submitHandler: function (frmadd) {
             successHandler1.show();
             errorHandler1.hide();
             }*/
            submitHandler: function (frmadd) {
                //successHandler1.show();
                //errorHandler1.hide();
                //console.log(frmadd);
                //console.log($('#country_code').val());
                var s_id = $('#country_code').find(':selected').data('title');
                var c_code = $('ul.country-list > li.active > .dial-code').html();
                $('#country_id').val(s_id);
                // $('#phone_code').val(c_code);
                // alert(c_code);return false;
                //successHandler1.show();
                //errorHandler1.hide();
                toastr.info('<?= $this->lang->line('MSG_WAIT') ?>', '', {positionClass: 'toast-top-full-width'});
                window.setTimeout(function () {
                    $.ajax({
                        url: '<?=base_url(MENU_MY_ACCOUNT_UPDATE)?>',//frmadd.action,
                        type: frmadd.method,
                        data: $(frmadd).serialize(),
                        success: function (response) {
                            toastr.clear();
                            var data = JSON.parse(response);
                            if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                                //toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>', {positionClass: 'toast-top-full-width'});
                                location.reload();
                            }
                            else { //(data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                            }

                            /* if(data.status==1){
                             swal("Mise `à jour reussie !","
                            <?php echo @LBL_SWEET_ALERT_2; ?>", "success");
                             window.setTimeout(function(){
                             //location.reload()
                             window.location.href="
                            <?=@$http_url ?>";
                             },2000);
                             }else{
                             swal("Error !","
                            <?php echo @LBL_SOMETHING_ERROR; ?>", "error");
                             window.setTimeout(function(){
                             //    location.reload()
                             window.location.href="
                            <?=@$http_url ?>";
                             },2000);
                             }*/
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            toastr.clear();
                            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                        }
                    });

                }, 1000);

                return false;
            }
        });
        return false;
    });
</script>
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url(ASSETS . 'ex_plugins/flag_dd/css/msdropdown/dd.css'); ?>"/>
<script src="<?php echo base_url(ASSETS . 'ex_plugins/flag_dd/js/msdropdown/jquery.dd.min.js'); ?>"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url(ASSETS . 'ex_plugins/flag_dd/css/msdropdown/flags.css'); ?>"/>
<script>
    $(document).ready(function () {
        <?php if(@$customer_info->country_code !='') { ?>
        $("#country_code option[value='<?=@$customer_info->country_code?>']").attr("selected", "selected");
        <?php }?>
        $("#country_code").msDropdown();
        //$("#country_code > [value='bg']").attr("selected", "true");
    })
</script>
<link rel="stylesheet" href="<?php echo base_url(ASSETS . 'ex_plugins/dd/intlTelInput.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url(ASSETS . 'ex_plugins/dd/demo.css'); ?>">
<script src="<?php echo base_url(ASSETS . 'ex_plugins/dd/intlTelInput.js'); ?>"></script>
<script>
    /*$("#sender_number").intlTelInput({
     utilsScript: "js/utils.js"
     });*/
    $("#phone_code").intlTelInput({
        utilsScript: "js/utils.js"
    });
    $("#phone_code").intlTelInput();
    // $("#phone_code").intlTelInput();
    window.setTimeout(function () {
        <?php if(@$customer_info->phone_code !='' ){ @$code = str_replace("+","",@$customer_info->phone_code); ?>
        //$('li[data-dial-code = <?=@$code?>]').addClass('highlight active');
        //alert('<?=@$code ?>');
        /*var c_code = $('.country[data-dial-code =
        <?=@$code?>]').data('country-code');
         $(".selected-flag>.flag").removeClass().addClass('flag '+c_code);*/
        /*window.setTimeout(function(){
         //$('.iti-flag, .selected-flag>.flag ').addClass(c_code);
         $(".flag").removeClass().addClass('flag '+c_code);
         alert(c_code);
         },2000);*/
        $('.country').removeClass('highlight active');
        $('.country[data-dial-code = "<?=@$code?>"]').addClass('highlight active');
        <?php }?>

    }, 1000);

    <?php /*if($_REQUEST['st'] !='' && $_REQUEST['st']==0){ */?>/*
     swal("Information !","Pour davantage sécuriser vos transferts vous devez mettre à jour votre profil.", "info");
     */<?php /*}*/?>
</script>

<style>
    /*.intl-tel-input input, .intl-tel-input input[type=text], .intl-tel-input input[type=tel]{
         position: relative;
         z-index: 0;
         margin-top: -9px !important;
         margin-bottom: 0 !important;
         padding-right: 0 !important;
         margin-right: 0;
        height: auto !important;
    }
    .intl-tel-input input{
        width: 94px !important;
    }*/

    #phone_code {
        width: 100px !important;
        padding-right: 10px !important;
    }

    .selected-flag {
        padding: 18px 16px 6px 6px !important;
    }

    .selected-flag > .flag {
        margin-top: -10px !important;
    }
</style>


<!-- =========================
    Forgot Password SECTION
============================== -->
<section id="login" class="login p-y-xlg bg-color">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <?php _notify(); ?>
            </div>
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"
                    style="color:#ec1e73"><?php echo $this->lang->line('MSG_FP_01'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="well1">
                            <form class="form-horizontal_1" id="form_login" role="form" method="post"
                                  action="<?= base_url(MENU_FORGOT_PASSWORD_SEND) ?>">
                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_LOG_IN_10'); ?><span
                                            class="text-red">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="e.g. name@example.com "
                                           value="<?php echo edit_display('email', @$info); ?>" required>

                                    <p class="text-info"><?php echo $this->lang->line('LBL_LOG_IN_12'); ?></p>
                                </div>
                                <div class="form-group">
                                    <button type="submit" style="" id="login" name="btn" value="forgot-password"
                                            class="btn btn-blue"><?php echo $this->lang->line('MSG_FP_02'); ?></button>
                                </div>
                            </form>
                            <p class=" text-center" style="font-size: 15px"><a href="<?= MENU_LOG_IN ?>"
                                                                               style="color:#36cd75"><?php echo $this->lang->line('LBL_LOG_IN_09'); ?></a>
                            </p><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End Login Section -->
<script>
    $(document).ready(function () {
        //swal("Login Error !", "asda", "error");
        //toastr.error('Vikrant', '<?=ERROR?>',{positionClass: 'toast-top-full-width'});
        $("#form_login").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_03');  ?>',
                    email: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_04');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
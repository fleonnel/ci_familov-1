<!-- =========================
           SIGNUP SECTION
============================== -->
<section id="login" class="login p-y-xlg bg-color">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <?php _notify(); ?>
            </div>
            <div class="modal-header">
                <h4 class="m-t-md m-b-0 text-center center-md  m-b-md " style="font-weight:400; color:#ec1e73 ">
                    <?php echo $this->lang->line('LBL_SIGN_UP_01'); ?> <i class="icon-smile"></i></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md1-12 col-xs1-12">
                        <div class="well1">
                            <form class="form-horizontal_1" id="signupForm" role="form" method="post">
                                <div class="col-md-6 col-xs-6">
                                <div class="form-group ">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_SIGN_UP_02'); ?>
                                        <span class="text-red">*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                           placeholder="e.g. Anne "
                                           value="<?php echo edit_display('first_name', @$info); ?>"
                                           required>
                                </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                <div class="form-group ">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_SIGN_UP_03'); ?>
                                        <span class="text-red">*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                           placeholder="e.g. Njenga "
                                           value="<?php echo edit_display('first_name', @$info); ?>"
                                           required>
                                </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                <div class="form-group ">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_SIGN_UP_04'); ?>
                                        <span class="text-red">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="e.g. name@example.com "
                                           value="<?php echo edit_display('email', @$info); ?>" required>
                                </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                <div class="form-group ">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_SIGN_UP_05'); ?>
                                        <span class="text-red">*</span></label>
                                    <br>
                                    <input class="form-control  bene_cls" type="tel" id="phone_code" name="phone_code"
                                           placeholder="" value="" readonly>
                                    <input class="form-control bene_cls phone_number" type="text" id="phone_number"
                                           name="phone_number"
                                           value="<?php echo edit_display('phone_number', @$info); ?>"
                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                           placeholder=" Ex : 15259948834" required>
                                </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <div class="form-group ">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_SIGN_UP_06'); ?>
                                        <span class="text-red">*</span></label>
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="Enter Password"
                                           value="<?php echo edit_display('password', @$info); ?>"
                                           required>
                                </div>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <div class="form-group ">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_SIGN_UP_07'); ?>
                                        <span class="text-red">*</span></label>
                                    <input type="password" class="form-control" id="repeat_password"
                                           name="repeat_password"
                                           placeholder="Enter Repeat Password"
                                           value="<?php echo edit_display('repeat_password', @$info); ?>"
                                           required>
                                </div>
                                </div>

                                <div class="form-group" style="clear:both">
                                    <button type="submit" style="height:50px" id="sign_up" name="sign_up"
                                            class="btn btn-blue"><?php echo $this->lang->line('LBL_SIGN_UP_08');; ?></button>
                                </div>
                            </form>
                            <p class="terms text-center"> <?php echo $this->lang->line('LBL_SIGN_UP_09'); ?><br/>
                                <br/><?php echo $this->lang->line('LBL_SIGN_UP_10'); ?></p><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End Signup Section -->

<style>
    #phone_code {
        width: 102px !important;
        padding-right: 0px !important;
    }

    .intl-tel-input {
        width: 110px !important;
        float: left !important;
    }

    #sender_number {
        width: 339px !important;
    }

    .phone_number {
        width: 77% !important;
        /*width: 280px !important;*/
    }

</style>

<link rel="stylesheet" href="<?php echo base_url(ASSETS . 'ex_plugins/dd/intlTelInput.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url(ASSETS . 'ex_plugins/dd/demo.css'); ?>">
<script src="<?php echo base_url(ASSETS . 'ex_plugins/dd/intlTelInput.js'); ?>"></script>
<script>
    $("#phone_code").intlTelInput();
</script>
<script>
    $(document).ready(function () {
        $("#signupForm").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required: true,
                    digits: true
                },
                password: {
                    required: true,
                    minlength: 8
                },
                repeat_password: {
                    required: true,
                    equalTo: '#password'
                }
            },
            messages: {
                first_name: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_01');  ?>'
                },
                last_name: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_02');  ?>'
                },
                email: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_03');  ?>',
                    email: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_04');  ?>'
                },
                phone_number: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_05');  ?>',
                    digits: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_10');  ?>'
                },
                password: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_06');  ?>',
                    minlength: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_09');  ?>'
                },
                repeat_password: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_07');  ?>',
                    equalTo: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_08');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
<!-- =========================
            LOGIN SECTION
============================== -->
<section id="login" class="login p-y-xlg bg-color">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <?php _notify(); ?>
            </div>
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"
                    style="color:#ec1e73"><?php echo $this->lang->line('LBL_LOG_IN_01'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="well">
                            <form class="form-horizontal_1" id="form_login" role="form" method="post">
                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_LOG_IN_10'); ?><span
                                            class="text-red">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           placeholder="e.g. name@example.com "
                                           value="<?php echo edit_display('email', @$info); ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('LBL_LOG_IN_11'); ?><span
                                            class="text-red">*</span></label>
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="Enter Password"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" style="" id="login" name="login" value="login"
                                            class="btn btn-blue"><?php echo $this->lang->line('LBL_LOG_IN_09'); ?></button>
                                </div>
                            </form>
                            <p class=" text-center" style="font-size: 15px"><a href="<?= MENU_FORGOT_PASSWORD ?>"
                                                                               style="color:#36cd75"><?php echo $this->lang->line('LBL_LOG_IN_08'); ?></a>
                            </p><br/>
                            <!--<div class="bar-devider"><span>OR</span></div>
                          <a href="<?= htmlspecialchars($loginURL) ?>">

                          <button class="loginBtn loginBtn--facebook">
                              Login with Facebook
                          </button>
                          </a>
                          <br>
                          <a href="<?= filter_var($authUrl, FILTER_SANITIZE_URL) ?>">

                          <button class="loginBtn loginBtn--google">
                              Login with Google
                          </button>
                          </a>-->
                        </div>
                    </div>
                    <div class="col-md-6 hidden-xs ">
                        <p class="" style="font-size: 14px">
                            <?php echo $this->lang->line('LBL_LOG_IN_02'); ?>
                        </p>
                        <ul class="list-unstyled" style="line-height: 2">
                            <li><span
                                    class="fa fa-check text-success"></span> <?php echo $this->lang->line('LBL_LOG_IN_03'); ?>
                            </li>
                            <li><span
                                    class="fa fa-check text-success"></span> <?php echo $this->lang->line('LBL_LOG_IN_04'); ?>
                            </li>
                            <li><span
                                    class="fa fa-check text-success"></span> <?php echo $this->lang->line('LBL_LOG_IN_05'); ?>
                            </li>
                            <li><span
                                    class="fa fa-check text-success"></span> <?php echo $this->lang->line('LBL_LOG_IN_06'); ?>
                            </li>
                            <li><span
                                    class="fa fa-check text-success"></span> <?php echo $this->lang->line('LBL_LOG_IN_07'); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End Login Section -->
<script>
    $(document).ready(function () {
        //swal("Login Error !", "asda", "error");
        //toastr.error('Vikrant', '<?=ERROR?>',{positionClass: 'toast-top-full-width'});
        $("#form_login").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 8
                }
            },
            messages: {
                email: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_03');  ?>',
                    email: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_04');  ?>'
                },
                password: {
                    required: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_06');  ?>',
                    minlength: '<?php echo $this->lang->line('LBL_SIGN_UP_VALIDATION_09');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
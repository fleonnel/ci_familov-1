<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_CP_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
<!-- =========================
            Change Password SECTION
============================== -->
<section id="login" class="login p-y-xlg1 bg-color">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <?php _notify(); ?>
            </div>
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="well1">
                            <form class="form-horizontal_1" id="form_login" role="form" method="post"
                                  action="<?= base_url('change-password-update') ?>">
                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('MSG_CP_02'); ?><span
                                            class="text-red">*</span></label>
                                    <input type="password" class="form-control" id="current_password"
                                           name="current_password"
                                           placeholder="<?php echo $this->lang->line('MSG_CP_03'); ?>"
                                           value="" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('MSG_CP_04'); ?><span
                                            class="text-red">*</span></label>
                                    <input type="password" class="form-control" id="new_password" name="new_password"
                                           placeholder="<?php echo $this->lang->line('MSG_CP_05'); ?>"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="control-label"><?php echo $this->lang->line('MSG_CP_06'); ?><span
                                            class="text-red">*</span></label>
                                    <input type="password" class="form-control" id="repeat_password"
                                           name="repeat_password"
                                           placeholder="<?php echo $this->lang->line('MSG_CP_07'); ?>"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" style="" id="login" name="login" value="login"
                                            class="btn btn-blue"><?php echo $this->lang->line('MSG_CP_08'); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /End  Section -->
<script>
    $(document).ready(function () {
        //swal("Login Error !", "asda", "error");
        //toastr.error('Vikrant', '<?=ERROR?>',{positionClass: 'toast-top-full-width'});
        $("#form_login").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                current_password: {
                    required: true
                },
                new_password: {
                    required: true,
                    minlength: 8
                },
                repeat_password: {
                    required: true,
                    equalTo: '#new_password'
                }
            },
            messages: {
                current_password: {
                    required: '<?php echo $this->lang->line('LBL_CP_VALIDATION_01');  ?>'
                },
                new_password: {
                    required: '<?php echo $this->lang->line('LBL_CP_VALIDATION_02');  ?>',
                    minlength: '<?php echo $this->lang->line('LBL_CP_VALIDATION_05');  ?>'
                },
                repeat_password: {
                    required: '<?php echo $this->lang->line('LBL_CP_VALIDATION_03');  ?>',
                    equalTo: '<?php echo $this->lang->line('LBL_CP_VALIDATION_04');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
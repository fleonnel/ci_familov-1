<!-- =========================
    Reset Password SECTION
============================== -->
<style>
    .p-y-xlg {
        padding-bottom: 100px !important;
    }
</style>
<section id="login" class="login p-y-xlg bg-color">
    <div id="login-overlay" class="modal-dialog">
        <div class="modal-content">
            <?php if (@$FLASH_STATUS == '') { ?>
                <div class="row">
                    <?php _notify(); ?>
                </div>
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"
                        style="color:#ec1e73"><?php echo $this->lang->line('MSG_RP_01'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="well1">
                                <form class="form-horizontal_1" id="form_reset_password" role="form" method="post"
                                      action="<?= base_url(@MENU_RESET_PASSWORD_UPDATE) ?>">
                                    <div class="form-group">
                                        <label for="inputlbl"
                                               class="control-label"><?php echo $this->lang->line('MSG_CP_04'); ?><span
                                                class="text-red">*</span></label>
                                        <input type="password" class="form-control" id="new_password"
                                               name="new_password"
                                               placeholder="<?php echo $this->lang->line('MSG_CP_05'); ?>"
                                               value=""
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputlbl"
                                               class="control-label"><?php echo $this->lang->line('MSG_CP_06'); ?><span
                                                class="text-red">*</span></label>
                                        <input type="password" class="form-control" id="repeat_password"
                                               name="repeat_password"
                                               placeholder="<?php echo $this->lang->line('MSG_CP_07'); ?>"
                                               value=""
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control hidden" id="token"
                                               name="token"
                                               placeholder=""
                                               value="<?php echo @$token; ?>">
                                        <input type="text" class="form-control hidden" id="e_token"
                                               name="e_token"
                                               placeholder=""
                                               value="<?php echo @$e_token; ?>">
                                        <button type="submit" style="" id="login" name="btn" value="forgot-password"
                                                class="btn btn-blue"><?php echo $this->lang->line('MSG_RP_02'); ?></button>
                                    </div>
                                </form>
                            </div>
                            <div class="well1">
                                <p class=" text-center" style="font-size: 15px"><a href="<?= MENU_LOG_IN ?>"
                                                                                   style="color:#36cd75"><?php echo $this->lang->line('LBL_LOG_IN_09'); ?></a>
                                </p><br/>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="alert alert-danger alert-dismissible">
                        <?= @$FLASH_MESSAGE ?>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="well1">
                                <p class=" text-center" style="font-size: 15px">
                                    <a href="<?= MENU_LOG_IN ?>" class="btn btn-blue"
                                       style=""><?php echo $this->lang->line('LBL_LOG_IN_09'); ?></a>
                                </p><br/>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!-- /End Login Section -->
<script>
    $(document).ready(function () {
        //swal("Login Error !", "asda", "error");
        //toastr.error('Vikrant', '<?=ERROR?>',{positionClass: 'toast-top-full-width'});
        $("#form_reset_password").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                new_password: {
                    required: true,
                    minlength: 8
                },
                repeat_password: {
                    required: true,
                    equalTo: '#new_password'
                }
            },
            messages: {
                new_password: {
                    required: '<?php echo $this->lang->line('LBL_CP_VALIDATION_02');  ?>',
                    minlength: '<?php echo $this->lang->line('LBL_CP_VALIDATION_05');  ?>'
                },
                repeat_password: {
                    required: '<?php echo $this->lang->line('LBL_CP_VALIDATION_03');  ?>',
                    equalTo: '<?php echo $this->lang->line('LBL_CP_VALIDATION_04');  ?>'
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
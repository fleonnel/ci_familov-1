<script>
    //toastr.error('<?=MESSAGE_DELETE_ERROR?>', '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
    $('.add_to_cart_btn').click(function (e) {
        var product_id = $('.quantity').data('product_id');
        var qty = $('.quantity').val();
        //toastr.info('<?= $this->lang->line('MSG_WAIT') ?>', '', {positionClass: 'toast-top-full-width'});
        $.ajax({
            url: '<?=base_url(MENU_ADD_TO_CART)?>',
            type: "POST",
            data: {product_id: product_id, qty: qty, action: 'ADD'},
            dataType: "JSON",
            success: function (data) {
                toastr.clear();
                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>', {positionClass: 'toast-top-full-width'});
                }
                else { //(data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                }
                $('.cart_total_item').html(data.cart_total_item);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.clear();
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    });
    $('.add_to_cart_btns').click(function (e) {
        var product_id = $(this).data('product_id');
        var qty = $(this).data('qty');
        // toastr.info('<?= $this->lang->line('MSG_WAIT') ?>', '', {positionClass: 'toast-top-full-width'});
        $.ajax({
            url: '<?=base_url(MENU_ADD_TO_CART)?>',
            type: "POST",
            data: {product_id: product_id, qty: qty, action: 'ADD'},
            dataType: "JSON",
            success: function (data) {
                toastr.clear();
                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>', {positionClass: 'toast-top-full-width'});
                }
                else { //(data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                }
                $('.cart_total_item').html(data.cart_total_item);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.clear();
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    });

    function add_to_cart(product_id, qty) {
        //alert(product_id);
        if (product_id != '' && qty != '') {
            //toastr.info('<?= $this->lang->line('MSG_WAIT') ?>', '', {positionClass: 'toast-top-full-width'});
            $.ajax({
                url: '<?=base_url(MENU_ADD_TO_CART)?>',
                type: "POST",
                data: {product_id: product_id, qty: qty, action: 'ADD'},
                dataType: "JSON",
                success: function (data) {
                    toastr.clear();
                    if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                        toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>', {positionClass: 'toast-top-full-width'});
                    }
                    else { //(data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                        toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                    }
                    $('.cart_total_item').html(data.cart_total_item);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.clear();
                    toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                }
            });
        } else {
            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
        }
    }
    $('.minus').click(function (e) {
        var product_id = $(this).data('product_id');
        var qty = parseInt($('.view_qty_' + product_id).val());

        if (qty == 1) {
            qty = 1;
        } else {
            qty = qty - 1;
        }
        $('.view_qty_' + product_id).val(qty);
    });
    $('.plus').click(function (e) {
        var product_id = $(this).data('product_id');
        var qty = parseInt($('.view_qty_' + product_id).val());
        qty = qty + 1;
        $('.view_qty_' + product_id).val(qty);
    });

    function delete_item(id, name) {
        swal({
            title: '<?= $this->lang->line('MSG_PRODUCT_CART_18') ?>',
            html: "<?= $this->lang->line('MSG_PRODUCT_CART_19') ?><br>(" + name + ")",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            window.location.href = '<?=base_url(MENU_DELETE_CART_ITEM)?>/' + id;
        })
    }
    $('.view_detail').click(function () {
        var o_id = $(this).data('id');
        $('.od_view').html('');
        if (o_id != '' && o_id > '') {
            toastr.info('<?= $this->lang->line('MSG_WAIT') ?>', '', {positionClass: 'toast-top-full-width'});
            $.ajax({
                url: '<?=base_url(MENU_ORDER_DETAIL_MODAL)?>',
                type: "POST",
                data: {order_id: o_id},
                dataType: "JSON",
                success: function (data) {
                    toastr.clear();
                    if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                        $('.od_view').html(data.html);
                        $('.order_detail_popup').modal('show');
                    }
                    else { //(data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                        toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.clear();
                    toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                }
            });
        } else {
            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
        }

    })
</script>
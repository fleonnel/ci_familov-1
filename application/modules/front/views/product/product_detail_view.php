<?php if (count(@$product_response) == 0 && count(@$shop_info) == 0) { ?>
    <section id="hero12" class="hero hero-countdown bg-img">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h1 class="text-white"><?= $this->lang->line('MSG_PRODUCT_DETAIL_01') ?></h1>
                </div>
                <div class="col-md-6 col-md-offset-3 text-white text-center">
                </div>
            </div>
        </div>
    </section>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissible">
                        <?= $this->lang->line('MSG_PRODUCT_DETAIL_02') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>
<?php if (@$shop_info->shop_banner != "") { ?>
<section class="page-head bg-img p-y-md "
         style="background-image:url('<?php echo DIR_SHOP_URL . @$shop_info->shop_banner; ?>')">
    <?php } else { ?>
    <section class="page-head bg-img p-y-md "
             style="background-image:url('<?php echo(ASSETS_URL_FRONT . 'img/images/default-banner.png'); ?>')">
        <?php } ?>
        <div class="container">
            <div class="row c2 h-bg">
                <div class="col-md-4 col-sm-12">
                    <?php if (@$shop_info->shop_logo != "") { ?>
                        <img src="<?php echo DIR_SHOP_URL . @$shop_info->shop_logo; ?>" style="width:20%;"/>
                    <?php } else { ?>
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/default-logo.png'); ?>" style="width:20%;"/>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                    <center><h1 class="h3 f-w-900 m-b-0"
                                style="text-transform:uppercase; text-decoration: underline;"><?php echo @$shop_info->shop_name; ?></h1>
                        <br/>

                        <p class="small"
                           style="font-weight: bold; width:50%;"><?php echo @$shop_info->shop_address; ?></p></center>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
          Product SECTION
    ============================== -->
    <section class="p-y-md">
        <div class="container" style="background:#fff">
            <div class="row">
                <div class="col-md-3"> <!--  hidden-xs hidden-sm -->
                    <div class="widget">
                        <div class="w-title">
                            <h5><?= $this->lang->line('MSG_PRODUCT_08'); ?></h5>
                        </div>
                        <ul class="w-comments">
                            <li>
                                <a href="<?= base_url(MENU_PRODUCT_LIST . "/" . @$this->session->userdata('order_info')->country_id . "/" . @$this->session->userdata('order_info')->city_id . "/" . @$this->session->userdata('order_info')->shop_id) ?>">
                                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/groceries.png'); ?>"
                                         style="width: 30px;padding-right: 5px;"><span
                                        style="font-size:14px;font-weight:500;margin-left:7px;"><?= $this->lang->line('MSG_PRODUCT_01'); ?></span></a>
                            </li>
                            <?php
                            if (count($category_list) > 0) {
                                foreach ($category_list as $k => $v) {
                                    ?>
                                    <a href="<?= base_url(MENU_PRODUCT_LIST . "/" . @$this->session->userdata('order_info')->country_id . "/" . @$this->session->userdata('order_info')->city_id . "/" . @$this->session->userdata('order_info')->shop_id . "/" . $v->category_id) ?>">
                                        <li><span class="filter_category" style="cursor:pointer;"
                                                  data-id="<?= $v->category_id ?>"><img
                                                    src="<?php echo(DIR_CATEGORY_URL . @$v->category_image); ?>"
                                                    style="width: 30px;padding-right: 5px;"><span
                                                    style="font-size:14px;font-weight:normal;margin-left:10px;"><?= $v->category_name ?></span>
                                        </li>
                                    </a>
                                <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <!--<br/><br/><br/>
                    <img src="<?php /*echo(ASSETS_URL_FRONT . 'img/images/familov-express.png'); */ ?>" width="150px"
                         alt="familov-express"
                         class=""> -->
                </div>
                <div class="col-md-9">
                    <div class="row ">
                        <?php if (count(@$product_response) > 0) { ?>
                            <div class="col-md-12 ">
                                <h5>
                                    <a href="<?= base_url(MENU_PRODUCT_LIST . "/" . @$this->session->userdata('order_info')->country_id . "/" . @$this->session->userdata('order_info')->city_id . "/" . @$this->session->userdata('order_info')->shop_id) ?>">
                                        <span
                                            style="color:#ee3682;text-decoration: underline;"><?= $this->lang->line('MSG_PRODUCT_DETAIL_04') ?></span></a>
                                    <?php /*echo @$product_response->category_name; */ ?><!-- - --><?php echo @$product_response->product_name; ?>
                                </h5>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <div class="widget">
                                        <div
                                            style="background-image:url('<?= DIR_PRODUCT_URL ?><?= @$product_response->product_image ?>'); background-size: cover; height:350px;"></div>
                                    </div>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="col-md-12 col-xs-12 text-center">
                                        <br>
                                        <br>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <!--<div class="main-price">
                                            <span
                                                class="main-price-right"><? /*= $this->currency_symbol . convert_price(@$v->special_product_prices); */ ?></span>
                                            <span
                                                class="main-price-left main_price"><? /*= $this->currency_symbol . convert_price(@$v->product_prices); */ ?></span>

                                        </div>-->
                                        <p class="main-price"
                                           style="font-weight:bold; text-align: left"><?= $this->lang->line('MSG_PRODUCT_DETAIL_05') ?>
                                            :
                                            <?php if (@$product_response->special_product_prices > 0) { ?>
                                                <span
                                                    class="main-price-right"><?= $this->currency_symbol . convert_price(@$product_response->special_product_prices); ?></span>
                                                <span
                                                    class="main-price-left main_price"><?= $this->currency_symbol . convert_price(@$product_response->product_prices); ?></span>
                                            <?php } else { ?>
                                                <span><?= $this->currency_symbol . convert_price(@$product_response->product_prices); ?></span>
                                            <?php } ?>
                                        </p>

                                        <p><?= strip_tags(@$product_response->product_short_desc) ?></p>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <span class="minus"
                                              data-product_id="<?php echo @$product_response->product_id; ?>"
                                              style="cursor:pointer;"><i class="fa fa-minus"></i></span>
                                        <input type="text" id="quantity" name="quantity" readonly
                                               class="form-control  quantity view_qty_<?php echo @$product_response->product_id; ?>"
                                               value="1" data-product_id="<?php echo @$product_response->product_id; ?>"
                                               style="width: 15%; text-align:center; display:inline-block;"/>
                                        <span class="plus"
                                              data-product_id="<?php echo @$product_response->product_id; ?>"
                                              style="cursor:pointer;"><i class="fa fa-plus"></i></span>
                                    </div>
                                    <?php if(@$product_response->product_availability == PRODUCT_STATUS_AVAILABLE){ ?>
                                    <div class="col-md-12 ">
                                        <span class="btn btn-orange m-t col-md-6 col-xs-12 add_to_cart_btn"
                                          id="add_cart_<?php echo @$product_response->product_id; ?>"
                                          data-product_id="<?php echo @$product_response->product_id; ?>"><?= $this->lang->line('MSG_PRODUCT_02') ?></span>
                                    </div>
                                    <?php } else{?>
                            <div class="btn btn-red-disabled m-t wd_auto " id="add_cart123_2069"
                            data-product_id="<?= @$v->product_id ?>" data-qty="1"
                                 disabled><i
                                    class="icon-cart"></i> <?= $this->lang->line('MSG_PRODUCT_02_NOT_AVAILABLE') ?></div>
                      <?php   }?>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <hr>
                                <p><?= (@$product_response->product_desc) ?></p>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-12 text-center">
                                <div class="alert alert-info alert-dismissible">
                                    <?= $this->lang->line('MSG_PRODUCT_DETAIL_03') ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- /End Section -->
    <?php } ?>
    <style>
        .inner_product_section {
            background: #fff;
            border: 1px solid #f5f5f5;
        }

        .wd_auto {
            width: 100%;
        }

        .price-box {
            background: #EE2982;
            font-weight: bold;
            font-size: 15px;
            color: #fff;
            text-align: center;
            padding-left: 0px;
        }

        .price-box1 {
            /*background: rgba(255, 254, 3, 0.90);*/
            width: 60px;
            height: 60px;
            border-radius: 50px;
            padding-top: 7px;
            padding-left: 12px;
            margin-top: -150px;
            margin-left: 10px;
            font-size: 15px;
            font-weight: 500;
            /* border: 5px solid rgba(181, 181, 133, 0.29); */
        }

        .main_price {
            text-decoration: line-through;
            font-size: 12px;
        }

        .main-price {
            text-align: center;
            font-weight: bold;
            font-size: larger;
            color: #36cd75;
        }

        .main-price-left {
            color: #9c9191;
            /*font-size: medium;*/
            text-decoration: line-through;
        }
    </style>
<?php
@$this->load->view('_cart');
?>
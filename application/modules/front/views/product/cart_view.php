<section id="hero12" class="hero hero-countdown bg-img hidden-xs">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_PRODUCT_CART_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
<?php if (count(@$cart) == 0) { ?>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissible">
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/groceries.png'); ?>">   <?= $this->lang->line('MSG_PRODUCT_CART_02') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>

    <style>
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            vertical-align: inherit !important;
        }

        @media (max-width: 960px) {
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                padding: 0px;
                padding-top: 10px;
                vertical-align: inherit !important;
            }

            .qty_product {
                width: 50px !important;
                margin-left: 10px !important;
                margin-right: 10px !important;
            }

            .delete_item_a {
                margin-left: 6px !important;
            }

            .table {
                font-size: 11px !important;
            }
            
            .navbar {
                height: 40px;
            }
            
            .form-control {
               
                padding: 5px;
                font-size:11px;
       
            }

            .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
                background-color: transparent !important;
            }

            .table-hover > tbody > tr:hover > td, .table-hover > tbody > tr:hover > th {
                background-color: transparent !important;
            }
            
            .page-header {
               
                margin: 60px 0 0px;
       
            }
            
        }

    </style>

    <!-- =========================
              Product Cart SECTION
    ============================== -->
    <section id="login" class="p-y-lg1 bg-color" style="">
        <div class="container">
           <!-- <div class="row ">
                <div class="alert-group">
                    <div class="alert alert-default alert-dismissable">
                        <?= $this->lang->line('MSG_PRODUCT_CART_03') ?>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <?= _notify(); ?>
            </div>
            <div class="row" style="padding:0px 15px 0px 15px;">
                <div class="page-header ">
                    <p><?= $this->lang->line('MSG_PRODUCT_CART_04') ?></p>
                </div>
                <form method="post" action="<?= base_url(MENU_UPDATE_CART) ?>">
                    <div class="table-responsive1">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center ">#</th>
                                <th width="10%" class="text-center ">
                                    <strong><?= $this->lang->line('MSG_PRODUCT_CART_05') ?></strong></th>
                                <th><strong><?= $this->lang->line('MSG_PRODUCT_CART_06') ?></strong></th>
                                <th class="text-right"><strong><?= $this->lang->line('MSG_PRODUCT_CART_07') ?></strong>
                                </th>
                                <th><strong></strong></th>
                                <th class="text-center"><strong><?= $this->lang->line('MSG_PRODUCT_CART_08') ?></strong>
                                </th>
                                <th class="text-right"><strong><?= $this->lang->line('MSG_PRODUCT_CART_09') ?></strong>
                                </th>
                                <th><strong>&nbsp;&nbsp;</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $ctr = 1;
                            $cart_total_item_qty = 0;
                            $cart_amount_total = 0.00;
                            $service_fees = convert_price(SERVICE_FEES);
                            foreach (@$cart as $k => $v) {
                                $cart_total_item_qty += $v['quantity'];
                                $product_price = _number_format(convert_price($v['price_temp']) * $v['quantity']);
                                $cart_amount_total = $cart_amount_total + $product_price;
                                ?>
                                <tr>
                                    <td class="text-center "><?= $ctr++ ?></td>
                                    <td>
                                        <div
                                            style="background-image:url('<?php echo(DIR_PRODUCT_URL . $v['product_image']); ?>'); background-size: cover; height:50px; width:50px;"></div>
                                    </td>
                                    <td><?= stripslashes($v['product_name']) ?></td>
                                    <td class="text-right"><?= $this->currency_symbol . convert_price($v['price_temp']) ?></td>
                                    <td class="text-center">&nbsp;&nbsp;</td>
                                    <td class="text-right">
                                        <select name="qty[<?= $k ?>]" id="qty_<?= $k ?>"
                                                class="form-control qty_product" required style=""
                                                onchange="this.form.submit()">
                                            <?php for ($ctr_qty = 1; $ctr_qty <= 50; $ctr_qty++) { ?>
                                                <option value="<?= $ctr_qty ?>" <?php if ($ctr_qty == $v['quantity']) {
                                                    echo "selected";
                                                } ?>><?= $ctr_qty ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td class="text-right"><?= $this->currency_symbol . _number_format(convert_price($v['price_temp']) * $v['quantity']) ?></td>
                                    <td class="text-right"><a href="javascript:void(0)"
                                                              class="text-red pull-right delete_item_a"
                                                              onclick="delete_item(<?= $k ?>,'<?= $v['product_name'] ?>')"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <!--<td class="text-center"></td>
                                <td></td>
                                <td></td>-->
                                <td colspan="5"><strong><?= $this->lang->line('MSG_PRODUCT_CART_10') ?></strong></td>
                                <td class="text-center"><b><?= @$cart_total_item_qty ?></b></td>
                                <td class="text-right"
                                    style="color:#36cd75 ;font-weight:bold;"><?= $this->currency_symbol . ($cart_amount_total) ?></td>
                                <td></td>
                            </tr>
                            <tr>
                                <!--<td class="text-center"></td>
                                <td></td>
                                <td></td>-->
                                <td colspan="5">
                                    <strong><?= $this->lang->line('MSG_PRODUCT_CART_11') /*. " " . str_replace("#PERCENTAGE#", 15, $this->lang->line('MSG_PRODUCT_CART_15')) */ ?></strong>
                                </td>
                                <td class="text-center"></td>
                                <td class="text-right"><?= $this->currency_symbol . $service_fees ?></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="6"><strong><?= $this->lang->line('MSG_PRODUCT_CART_16') ?></strong></td>
                                <!--<td class="text-center"><b>10</b></td>-->
                                <td class="text-right" style="color:#36cd75 ;font-weight:bold;">
                                    <strong id="grandprice" style="color:#302e64;font-size:15px">
                                        <?php $grand_total = _number_format($cart_amount_total + $service_fees);
                                        echo $this->currency_symbol . ($grand_total); #todo currency icon set in front controller.
                                        ?>
                                    </strong>
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-xs-6 text-left">
                    <a href="<?= base_url(MENU_PRODUCT_LIST . "/" . @$this->session->userdata('order_info')->country_id . "/" . @$this->session->userdata('order_info')->city_id . "/" . @$this->session->userdata('order_info')->shop_id) ?>"
                       class="btn btn-greenX"><?= $this->lang->line('MSG_PRODUCT_CART_13') ?></a>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?= base_url(MENU_CHECKOUT) ?>"
                       class="btn btn-green"><?= $this->lang->line('MSG_PRODUCT_CART_14') ?> <i
                            class="icon-check"></i></a>
                </div>
            </div>
        </div>
    </section>
    <!-- /End Section -->
<?php } ?>
<?php
@$this->load->view('_cart');
?>
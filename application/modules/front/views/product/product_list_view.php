<style>
    .inner_product_section {
        background: #fff;
        border: 1px solid #f5f5f5;
    }
    .wd_auto {
        width: 100%;
    }
    .main-price {
        text-align: center;
        font-weight: bold;
        font-size: 15px !important;
        color: #36cd75;
        margin-top: -20px;
    }
    .btn.btn-orange {
        border: 3px solid #36cd75;
        font-size: 14px;
        padding: 5px 5px;
        letter-spacing: 1px;
        font-weight: 400;
        width: 88%;
        margin-left: 10px;
        font-weight:bold;
    }
    .price-box {
        background: #36cd75;
        font-weight: bold;
        font-size: 14px;
        color: #fff;
        text-align: center;
        padding-left: 0px;
        width: 45px;
        height: 45px;
        margin-top: -135px;
        padding-top:13px;
    }
    .price-box1 {
        /*background: rgba(255, 254, 3, 0.90);*/
        width: 60px;
        height: 60px;
        border-radius: 50px;
        padding-top: 7px;
        padding-left: 12px;
        margin-top: -150px;
        margin-left: 10px;
        font-size: 15px;
        font-weight: 500;
        /* border: 5px solid rgba(181, 181, 133, 0.29); */
    }

    .main_price {
        text-decoration: line-through;
        font-size: 12px;
    }

    .main-price {
        text-align: center;
        font-weight: bold;
        font-size: larger;
        color: #36cd75;
    }

    .main-price-left {
        color: #9c9191;
        /*font-size: medium;*/
        text-decoration: line-through;
    }

    #product-search-list {
        list-style: none;
        max-height: 200px;
        position: inherit;
        overflow-y: auto;
        background: #f5f5f5;
    }

    .product_info_list {
        /*margin-top: 0 !important;*/
    }
    .add_to_cart_btns1{
        vertical-align: middle;
        text-align: center;
        width: 100%;
        margin-top: 15px;
        /* margin-left: 30px; */
        font-size: 15px;
        padding: 9px 3px 9px 3px;
        background-color: #fff;
        color: #2ab673;
        border: 1px dashed #566985 !important;
        border-radius:6px;
    }
    
    
</style>

<style type="text/css">
    .suggesstion-box {
        width: 96% !important;
    }

    /*Clearing Floats*/
    .cf:before, .cf:after {
        content: "";
        display: table;
    }

    .cf:after {
        clear: both;
    }

    .cf {
        zoom: 1;
    }

    /* Form wrapper styling */
    .form-wrapper {
        /*width: 450px;*/
        padding: 15px;
        /*margin: 150px auto 50px auto;
        background: #444;
        background: rgba(0,0,0,.2);
        border-radius: 10px;
        box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 1px 0 rgba(255,255,255,.2);*/
    }

    /* Form text input */

    .form-wrapper input {
        padding: 12px 5px;
        float: left;
        font: bold 15px 'lucida sans', 'trebuchet MS', 'Tahoma';
        border: 2px solid #eee;
        background: #fff;
        border-radius: 3px 0 0 3px;
    }

    .form-wrapper input:focus {
        outline: 0;
        /*background: #fff;
        box-shadow: 0 0 2px rgba(0,0,0,.8) inset;*/
    }

    .form-wrapper input::-webkit-input-placeholder {
        color: #999;
        font-weight: normal;
        font-style: italic;
    }

    .form-wrapper input:-moz-placeholder {
        color: #999;
        font-weight: normal;
        font-style: italic;
    }

    .form-wrapper input:-ms-input-placeholder {
        color: #999;
        font-weight: normal;
        font-style: italic;
    }

    /* Form submit button */
    .form-wrapper button {
        overflow: visible;
        position: relative;

        border: 0;
        padding: 0;
        cursor: pointer;
        height: 47px;
        width: 110px;
        /*font: bold 15px/40px 'lucida sans', 'trebuchet MS', 'Tahoma';*/
        font: "GothamRoundedMedium", "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif;
        color: #fff;
        text-transform: uppercase;
        background: #36cd75;
        border-radius: 0 3px 3px 0;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .3);
    }

    .form-wrapper button:hover {
        background: #36cd75;
    }

    .form-wrapper button:active,
    .form-wrapper button:focus {
        background: #36cd75;
        outline: 0;
    }

    .form-wrapper button:before {
        /* left arrow */
        content: '';
        position: absolute;
        border-width: 8px 8px 8px 0;
        border-style: solid solid solid none;
        border-color: transparent #36cd75 transparent;
        top: 15px;
        left: -6px;
    }

    .form-wrapper button:hover:before {
        border-right-color: #36cd75;
    }

    .form-wrapper button:focus:before,
    .form-wrapper button:active:before {
        border-right-color: #36cd75;
    }

    .form-wrapper button::-moz-focus-inner {
        /* remove extra button spacing for Mozilla Firefox */
        border: 0;
        padding: 0;
    }
</style>
<!-- <form class="form-wrapper cf1">
        <input type="text" placeholder="Search here..." required><button type="submit"> <i id="filtersubmit" class="fa fa-search"></i></button>
    </form> -->
<?php if (@$tot==0 && count(@$shop_info)==0) { ?>
    <section id="hero12" class="hero hero-countdown bg-img">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h1 class="text-white"><?= $this->lang->line('MSG_PRODUCT_03') ?></h1>
                </div>
                <div class="col-md-6 col-md-offset-3 text-white text-center">
                </div>
            </div>
        </div>
    </section>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissible">
                        <?= $this->lang->line('MSG_PRODUCT_04') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }else{ ?>
<?php if (@$shop_info->shop_banner != "") { ?>
<section class="page-head bg-img p-y-md phone"
         style="background-image:url('<?php echo DIR_SHOP_URL . @$shop_info->shop_banner; ?>')"
         data-id="<?php echo DIR_SHOP_URL . @$shop_info->shop_banner; ?>">
    <!--background-image:url('<?php /*echo DIR_SHOP_URL.@$shop_info->shop_banner; */ ?>')-->
    <?php } else { ?>
    <section class="page-head bg-img p-y-md phone " style="background-image:url('<?php echo(ASSETS_URL_FRONT . 'img/images/default-banner.png'); ?>')">
        <?php } ?>
        <div class="container">
            <div class="row c2 h-bg">
                <div class="col-md-4 col-sm-12 shop-logo">
                    <?php if (@$shop_info->shop_logo != "") { ?>
                        <img src="<?php echo DIR_SHOP_URL.@$shop_info->shop_logo; ?>" style="width:20%;"/>
                    <?php } else { ?>
                        <img  src="<?php echo(ASSETS_URL_FRONT . 'img/images/default-logo.png'); ?>" style="width:20%;"/>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                    <center><h1 class="h3 f-w-900 m-b-0"
                                style="text-transform:uppercase; text-decoration: underline;"><?php echo @$shop_info->shop_name; ?></h1>
                       
                        <p class="small" style="font-weight: bold; width:50%;"><?php echo @$shop_info->shop_address; ?></p></center>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
          Product SECTION
    ============================== -->
    <section class="p-y-md" style="padding-top:30px !important">
        <div class="container" style="background:#fff">
            <!--<div class="col-md-12">
                <div class="well well-sm pull-right">
                    <div class="btn-group">
                        <a href="#" id="list" class="btn btn-default btn-sm"><span
                                class="glyphicon glyphicon-th-list"></span>List</a>
                        <a href="#" id="grid" class="btn btn-default btn-sm"><span
                                class="glyphicon glyphicon-th"></span>Grid</a>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <!--<div class="col-md-12">
                <div class="panel panel-default pull-right" style="border:none !important;float:right;">
                    <form method="GET" id="header_search_form" action="">
                        <input type="text" id="search_header" name="search_header" class="search-input"
                               autocomplete="on" style="padding: 9px;" value="<?php /*echo @$search; */?>"
                               placeholder="Type Here"/>
                        <input type="submit" name="submit" value="Search"
                               class="btn-nav btn-green smooth-scroll ui-autocomplete-input"/>
                    </form>
                    <div class="pull-left">
                        <div class="btn-group">
                            <a href="#" id="list" class="btn btn-default btn-sm"><span
                                    class="glyphicon glyphicon-th-list"></span>List</a>
                            <a href="#" id="grid" class="btn btn-default btn-sm"><span
                                    class="glyphicon glyphicon-th"></span>Grid</a>
                        </div>
                    </div>
                </div>
            </div>-->
                <style>
                    nav.cat_menu select {
                        display: none;
                    }

                    @media (max-width: 960px) {
                        nav.cat_menu ul {
                            display: none;
                        }
                        
                        .shop-logo{
                            margin-bottom:5px !important;
                        }
                        
                        .shop-logo img{
                            width:35px !important;
                            margin-top:10px;
                        }
                         .phone  {
                            padding-bottom:15px !important;
                        }
                        
                        .page-head h1 {
                           
                            font-size: 15px;
                        }

                        nav.cat_menu select {
                            display: inline-block;
                        }

                        .search-box {
                            width: 70%;
                        }
                        
                        .navbar {
                            height: 40px;
                        }
                        
                        .form-wrapper button{
                            width:105px;
                        }
                    }

                </style>
                <div class="col-md-3 text-center"> <!--  hidden-xs hidden-sm -->
                    <nav class="cat_menu">
                        <div class="widget text-left hidden-xs hidden-sm ">
                        <div class="w-title">
                            <h5><?= $this->lang->line('MSG_PRODUCT_08'); ?></h5>
                        </div>
                        <ul class="w-comments">
                            <?php
                            if (count($category_list) > 0) { ?>
                            <li>
                                <a href="<?=base_url(MENU_PRODUCT_LIST."/".@$country_id."/".@$city_id."/".@$shop_id)?>">
                                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/stars.svg'); ?>"
                                         style="width: 35px;padding-right: 3px;"><span
                                        style="font-size:14px;font-weight:500;margin-left:7px;"
                                        class="<?php if (@$category == '') {
                                            echo "text-success";
                                        } ?>"><?= $this->lang->line('MSG_PRODUCT_01'); ?></span></a>
                            </li>
                            <?php
                                foreach ($category_list as $k => $v) {
                                    ?>
                                    <a href="<?= base_url(MENU_PRODUCT_LIST . "/" . @$country_id . "/" . @$city_id . "/" . @$shop_id . "/" . $v->category_id) ?>">
                                        <li><span class="filter_category <?php if (@$category == $v->category_id) {
                                                echo "text-success";
                                            } ?>" style="cursor:pointer;"
                                                  data-id="<?= $v->category_id ?>"><img
                                                    src="<?php echo(DIR_CATEGORY_URL . @$v->category_image); ?>"
                                                    style="width: 30px;padding-right: 5px;"><span
                                                    style="font-size:14px;font-weight:normal;margin-left:10px;"><?= stripslashes($v->category_name) ?></span>
                                        </li>
                                    </a>
                                <?php
                                }
                            }
                            ?>
                        </ul>
                            <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-express.png'); ?>" width="150px"
                                 alt="familov-express"
                                 class="text-center  hidden-sm hidden-xs "> <!-- hidden-sm hidden-xs -->
                    </div>
                        <div class="col-md-6">
                            <select class="form-control cat_list_dd " onchange="this.form.submit();">
                                <option
                                    value="<?= base_url(MENU_PRODUCT_LIST . "/" . @$country_id . "/" . @$city_id . "/" . @$shop_id) ?>"
                                    selected="selected"><?= $this->lang->line('MSG_PRODUCT_01'); ?></option>
                                <?php
                                foreach ($category_list as $k => $v) { ?>
                                    <option
                                        value="<?= base_url(MENU_PRODUCT_LIST . "/" . @$country_id . "/" . @$city_id . "/" . @$shop_id . "/" . $v->category_id) ?>" <?php if (@$category == $v->category_id) {
                                        echo 'selected="selected"';
                                    } ?> ><?= stripslashes($v->category_name) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </nav>
                </div>

                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class=" " style="border:none !important;">
                                <!--<div class="pull-left">
                                    <div class="btn-group">
                                        <a href="#" id="list" class="btn btn-default btn-sm"><span
                                                class="glyphicon glyphicon-th-list"></span>List</a>
                                        <a href="#" id="grid" class="btn btn-default btn-sm"><span
                                                class="glyphicon glyphicon-th"></span>Grid</a>
                                    </div>
                                </div>-->
                               

                                <div class="pull-right1">
                                    <form method="post" id="header_search_form" action="" class="form-wrapper cf">
                                        <!-- <div class="form-group">
                                            <div class="col-md-10">
                                                <input type="text" id="search_header" name="search_header"
                                                       class="col-md-10 search-input form-control search-box"
                                                       autocomplete="on" style="" value="<?= @$search_keyword ?>"
                                                       placeholder="Search For Products"/>

                                                <div class="suggesstion-box"></div>
                                            </div>

                                            <input type="submit" name="submit" value="Search"
                                                   class="btn-nav btn-green smooth-scroll ui-autocomplete-input col-sm-2 control-label"/>
                                        </div> -->
                                        <!-- <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Search here..." required><button type="submit"> <i id="filtersubmit" class="fa fa-search"></i></button>
                                        </div>
                                       </div> -->
                                        <!-- </form>
                                     <form class="form-wrapper cf row"> -->
                                     
                                        <input type="text" id="search_header" name="search_header" placeholder="Recherchez un produit" value="<?= @$search_keyword ?>" required="" class="form-contro1l col-md-10 search-box"><button type="submit" class="col-md-2   "> <i id="filtersubmit" class="fa fa-search"></i></button>
                                        <div class="suggesstion-box"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!--<div class="row ">
                        <div class="col-md-12 text-center">
                            <div class="alert alert-default alert-dismissible">
                                <?= str_replace("#COUNT#",@$tot,$this->lang->line('MSG_PRODUCT_05')) ?>
                            </div>
                        </div>
                    </div>-->
                    <div class="row">
                        <div class="col-md-12">
                            <div id="products" class="list-group product_list">
                                <?php
                                $this->load->view('_list',@$product_response);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="ajax-load text-center" style="display:none">
                                <p><img src="<?php echo(ASSETS_URL_FRONT . 'img/loader.gif'); ?>"><?=$this->lang->line('MSG_PRODUCT_07')?></p>
                            </div>
                            <?php if (@$tot > @$perPage) { ?>
                                <button type="button" class="load_more btn btn-green " style="border-radius: 20px;text-transform: UPPERCASE; font-weight: bold;"><?= str_replace("#COUNT#",@$tot,$this->lang->line('MSG_PRODUCT_09')) ?></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- /End Section -->
    <?php }?>
    <script type="text/javascript">
        var page = 0;
        var category_id = '<?=@$category?>';
        var search_keyword = '<?= @$search_keyword ?>';
        var total = '<?=@$tot?>';
        // $(window).scroll(function() {
        // if($(window).scrollTop() + $(window).height() == $(document).height()) {
        //    page++;
        //     loadMoreData(page, category_id, search_keyword);
        //      }
        // });
        $('.load_more').click(function () {
            page++;
            loadMoreData(page, category_id, search_keyword);
        });
        function loadMoreData(page, category, search_keyword) {
            $.ajax(
                {
                    //url: '<?=base_url('listing')?>',
                    url: '<?=base_url(MENU_PRODUCT_LISTING)?>',
                    type: "post",
                    data: {page: page, category: category, shop_id: '<?=@$shop_id?>', search_keyword: search_keyword},
                    beforeSend: function () {
                        $('.ajax-load').show();
                    }
                })
                .done(function (data) {
                    var res = JSON.parse(data);
                    if (data.html == "") {
                        /*$('.ajax-load').html("No more records found");
                         return;*/
                        // $('.load_more').hide();
                    }
                    $('.ajax-load').hide();
                    $(".product_list").append(res.html);
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    $('.ajax-load').hide();
                    toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                });
        }
        /* $('.filter_category').click(function () {
         page = 0;
         page++;
         category_id = $(this).data('id');
         loadMoreDataCategory(page,category_id);
         });*/
        /*Product Search List */
        $(document).ready(function () {
            $(".search-box").keyup(function () {
                var keyword = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "<?=base_url(MENU_PRODUCT_SEARCH)?>",
                    //data:'keyword='+$(this).val(),
                    data: {category: category_id, shop_id: '<?=@$shop_id?>', 'keyword': keyword},
                    beforeSend: function () {
                        $("#search-box").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function (data) {
                        var data = JSON.parse(data);
                        $(".suggesstion-box").show();
                        $(".suggesstion-box").html(data.html);
                        $(".search-box").css("background", "#FFF");
                    }
                });
            });
        });
        //To select country name
        function selectProduct(val) {
            $(".search-box").val(val);
            $(".suggesstion-box").hide();
        }
        $(document).ready(function () {
            $('.cat_list_dd').change(function () {
                location.href = $(this).val();
            });
        });
    </script>
    <script type="text/javascript">
        /*$(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });*/
    </script>


    <?php
    @$this->load->view('_cart');
    ?>

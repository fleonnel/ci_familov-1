<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('MSG_PRODUCT_CART_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
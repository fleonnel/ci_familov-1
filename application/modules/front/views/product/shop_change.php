<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('SHOP_CHANGE_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>
 <!-- <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissible">
                           <?= $this->lang->line('SHOP_CHANGE_02') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
                <div class="col-xs-5 text-center">
                    <a href="<?= base_url(MENU_PRODUCT_LIST . "/" . @$this->session->userdata('order_info')->country_id . "/" . @$this->session->userdata('order_info')->city_id . "/" . @$this->session->userdata('order_info')->shop_id) ?>"
                       class="btn btn-greenX"><?= $this->lang->line('SHOP_CHANGE_03') ?></a>
                </div>
                <div class="col-xs-6 text-center">
                    <a href="<?= base_url() ?>"
                       class="btn btn-green"><?= $this->lang->line('SHOP_CHANGE_04') ?> </a>
                </div>
            </div> -->
            <?php //_pre($country_id)?>
 <div class="row">
    <div class="col-md-12 col-md-offset-4" >
<div class="text-center" tabindex="-1" style="width:90%; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: auto;">
         <ul  style="display: none;"></ul>
        <div class="swal2-icon swal2-error" style="display: none;">
            <span class="x-mark"><span class="line left"></span>
            <span class="line right"></span>
        </div>
    <div class="swal2-icon swal2-warning pulse-warning" style="display: block;">!</div>
    <div class="swal2-icon swal2-info" style="display: none;">i</div>
    <div class="swal2-icon swal2-success" style="display: none;">
    <span class="line tip"></span> <span class="line long"></span>
    <div class="placeholder"></div> <div class="fix"></div>
</div>
    <h2 class="swal2-title" id="modalTitleId"><?= $this->lang->line('SHOP_CHANGE_06') ?></h2>
<div id="modalContentId" class="swal2-content" style="display: block;"><?= $this->lang->line('SHOP_CHANGE_02') ?><br>(<?= $this->lang->line('SHOP_CHANGE_05') ?>)</div>
    <input style="display: none;" class="swal2-input">
    <input type="file" style="display: none;" class="swal2-file">
<div class="swal2-range" style="display: none;"><output></output>
    <input type="range"></div><select style="display: none;" class="swal2-select"></select>
    <div class="swal2-radio" style="display: none;"></div><label for="swal2-checkbox" class="swal2-checkbox" style="display: none;">
    <input type="checkbox"></label>
    <textarea style="display: none;" class="swal2-textarea"></textarea>
    <div class="swal2-validationerror" style="display: none;"></div>
    <hr class="swal2-spacer" style="display: block;">
    <button type="button" role="button" tabindex="0" class="btn btn-green change_btn" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);"><?= $this->lang->line('SHOP_CHANGE_03') ?></button>
    <a href="<?= base_url(MENU_VIEW_CART) ?>" class="btn btn-green" style="display: inline-block; background-color: rgb(221, 51, 51);"><?= $this->lang->line('SHOP_CHANGE_04') ?> </a>
</div>
</div>
</div>
<?php //_pre($country_id)?>
<script>

    $('.change_btn').click(function (e) {
        $.ajax({
            url: '<?=base_url(CHANGE_SHOP)?>',
            type: "POST",
            data: {},
            dataType: "JSON",
            success: function (data) {
                var country = <?= $country_id ?>;
                var city = <?= $city_id ?>;
                var shop = <?= $shop_id ?>;

                window.location = '<?=base_url(MENU_PRODUCT_LIST)?>/'+country+'/'+city+'/'+shop;
                /*if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>', {positionClass: 'toast-top-full-width'});
                }
                else { //(data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>', {positionClass: 'toast-top-full-width'});
                }*/
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.clear();
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    });
</script>
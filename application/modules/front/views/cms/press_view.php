<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('LBL_PRESS_01') ?></h1>
                <!--<a href="#pricing6-1" class="btn btn-shadow btn-green btn-lg smooth-scroll m-b-md">RESERVE YOUR SEAT</a>-->
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>
<!-- =========================
   Press
============================== -->
<section id="blog2-1" class="p-y-lg blog bg-edit">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="section-header text-center wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <!--<h2><?php /*echo "LBL_PRESS_TITLE"; */ ?></h2>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 content-block c3">
                <div class="col-sm-4">
                    <a href="http://www.saarbruecker-zeitung.de/wirtschaft/Saarbruecken-Allgemeine-nicht-fachgebundene-Universitaeten-Arbeitgeber-Einwanderer-Experten-Migranten-Migrationshintergrund-Studenten-Unternehmer-Wirtschaftlicher-Markt;art2819,5911202">
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/press-1.png'); ?>" alt=""
                             class="img-responsive img-rounded"></a>

                    <p class="p-opacity"><?= $this->lang->line('LBL_PRESS_03') ?> </p>
                    <a href="http://www.saarbruecker-zeitung.de/wirtschaft/Saarbruecken-Allgemeine-nicht-fachgebundene-Universitaeten-Arbeitgeber-Einwanderer-Experten-Migranten-Migrationshintergrund-Studenten-Unternehmer-Wirtschaftlicher-Markt;art2819,5911202"
                       class="more-link f-w-700 edit"><?= $this->lang->line('LBL_PRESS_02') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('LBL_PRIVACY_POLICY_01') ?></h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>

<section class="p-y-lg faqs schedule">
    <div class="container">
        <div class="row">
            <?php if ($this->current_language == 'english') { ?>
                <div class="col-md-12 justify">
                    <h4>What is this Privacy Policy for?</h4>

                    <p>This privacy policy is for this website Familov.com and governs the privacy of its users who
                        choose to use it. <br> The policy sets out the different areas where user privacy is concerned
                        and outlines the obligations &amp; requirements of the users, the website and website owners.
                        Furthermore the way this website processes, stores and protects user data and information will
                        also be detailed within this policy.</p>

                    <p></p>
                    <h4>The Website</h4>

                    <p>This website and its owners take a proactive approach to user privacy and ensure the necessary
                        steps are taken to protect the privacy of its users throughout their visiting experience. This
                        website complies to all UK national laws and requirements for user privacy.</p>
                    <h4>Use of Cookies</h4>

                    <p>This website uses cookies to better the users experience while visiting the website. Where
                        applicable this website uses a cookie control system allowing the user on their first visit to
                        the website to allow or disallow the use of cookies on their computer / device. This complies
                        with recent legislation requirements for websites to obtain explicit consent from users before
                        leaving behind or reading files such as cookies on a user's computer / device.</p>

                    <p>Cookies are small files saved to the user's computers hard drive that track, save and store
                        information about the user's interactions and usage of the website. This allows the website,
                        through its server to provide the users with a tailored experience within this website.<br>
                        Users are advised that if they wish to deny the use and saving of cookies from this website on
                        to their computers hard drive they should take necessary steps within their web browsers
                        security settings to block all cookies from this website and its external serving vendors.</p>

                    <p>This website uses tracking software to monitor its visitors to better understand how they use it.
                        This software is provided by Google Analytics which uses cookies to track visitor usage. The
                        software will save a cookie to your computers hard drive in order to track and monitor your
                        engagement and usage of the website, but will not store, save or collect personal information.
                        You can read Google's privacy policy here for further information [ <a
                            href="http://www.google.com/privacy.html"
                            target="_blank">http://www.google.com/privacy.html </a>].<br>
                    </p>

                    <p>Other cookies may be stored to your computers hard drive by external vendors when this website
                        uses referral programs, sponsored links or adverts. Such cookies are used for conversion and
                        referral tracking and typically expire after 30 days, though some may take longer. No personal
                        information is stored, saved or collected.</p>
                    <h4>Contact &amp; Communication</h4>

                    <p>Users contacting this website and/or its owners do so at their own discretion and provide any
                        such personal details requested at their own risk. Your personal information is kept private and
                        stored securely until a time it is no longer required or has no use, as detailed in the Data
                        Protection Act 1998. Every effort has been made to ensure a safe and secure form to email
                        submission process but advise users using such form to email processes that they do so at their
                        own risk.<br>
                    </p>

                    <p>This website and its owners use any information submitted to provide you with further information
                        about the products / services they offer or to assist you in answering any questions or queries
                        you may have submitted. This includes using your details to subscribe you to any email
                        newsletter program the website operates but only if this was made clear to you and your express
                        permission was granted when submitting any form to email process. Or whereby you the consumer
                        have previously purchased from or enquired about purchasing from the company a product or
                        service that the email newsletter relates to. This is by no means an entire list of your user
                        rights in regard to receiving email marketing material.
                        Your details are not passed on to any third parties. </p>
                    <h4>Email Newsletter</h4>

                    <p>This website operates an email newsletter program, used to inform subscribers about products and
                        services supplied by this website. Users can subscribe through an online automated process
                        should they wish to do so but do so at their own discretion. Some subscriptions may be manually
                        processed through prior written agreement with the user. </p>

                    <p>Subscriptions are taken in compliance with UK Spam Laws detailed in the Privacy and Electronic
                        Communications Regulations 2003. All personal details relating to subscriptions are held
                        securely and in accordance with the Data Protection Act 1998. No personal details are passed on
                        to third parties nor shared with companies / people outside of the company that operates this
                        website. Under the Data Protection Act 1998 you may request a copy of personal information held
                        about you by this website's email newsletter program. A small fee will be payable. If you would
                        like a copy of the information held on you please write to the business address at the bottom of
                        this policy.</p>

                    <p>Email marketing campaigns published by this website or its owners may contain tracking facilities
                        within the actual email. Subscriber activity is tracked and stored in a database for future
                        analysis and evaluation. Such tracked activity may include; the opening of emails, forwarding of
                        emails, the clicking of links within the email content, times, dates and frequency of activity
                        [this is by no far a comprehensive list].<br>
                        This information is used to refine future email campaigns and supply the user with more relevant
                        content based around their activity.</p>

                    <p>In compliance with UK Spam Laws and the Privacy and Electronic Communications Regulations 2003
                        subscribers are given the opportunity to un-subscribe at any time through an automated system.
                        This process is detailed at the footer of each email campaign. If an automated un-subscription
                        system is unavailable clear instructions on how to un-subscribe will by detailed instead.</p>
                    <h4>External Links</h4>

                    <p>Although this website only looks to include quality, safe and relevant external links, users are
                        advised adopt a policy of caution before clicking any external web links mentioned throughout
                        this website. (External links are clickable text / banner / image links to other websites,
                        similar to; <a href="http://www.craftykingsboutique.co.uk" title="Folded Book Art"
                                       target="_blank">Folded Book Art</a> or <a
                            href="http://www.kingstrains.co.uk/articles/Used-Model-Trains-For-Sale"
                            title="Used model trains for sale" target="_blank">Used Model Trains For Sale</a>.)</p>

                    <p>The owners of this website cannot guarantee or verify the contents of any externally linked
                        website despite their best efforts. Users should therefore note they click on external links at
                        their own risk and this website and its owners cannot be held liable for any damages or
                        implications caused by visiting any external links mentioned.</p>
                    <h4>Adverts and Sponsored Links</h4>

                    <p>This website may contain sponsored links and adverts. These will typically be served through our
                        advertising partners, to whom may have detailed privacy policies relating directly to the
                        adverts they serve. </p>

                    <p>Clicking on any such adverts will send you to the advertisers website through a referral program
                        which may use cookies and will track the number of referrals sent from this website. This may
                        include the use of cookies which may in turn be saved on your computers hard drive. Users should
                        therefore note they click on sponsored external links at their own risk and this website and its
                        owners cannot be held liable for any damages or implications caused by visiting any external
                        links mentioned.</p>
                    <h4>Social Media Platforms</h4>

                    <p>Communication, engagement and actions taken through external social media platforms that this
                        website and its owners participate on are custom to the terms and conditions as well as the
                        privacy policies held with each social media platform respectively. </p>

                    <p>Users are advised to use social media platforms wisely and communicate / engage upon them with
                        due care and caution in regard to their own privacy and personal details. This website nor its
                        owners will ever ask for personal or sensitive information through social media platforms and
                        encourage users wishing to discuss sensitive details to contact them through primary
                        communication channels such as by telephone or email.</p>

                    <p>This website may use social sharing buttons which help share web content directly from web pages
                        to the social media platform in question. Users are advised before using such social sharing
                        buttons that they do so at their own discretion and note that the social media platform may
                        track and save your request to share a web page respectively through your social media platform
                        account.</p>
                    <h4>Shortened Links in Social Media</h4>

                    <p>This website and its owners through their social media platform accounts may share web links to
                        relevant web pages. By default some social media platforms shorten lengthy urls [web addresses]
                        (this is an example: http://bit.ly/zyVUBo). <br>
                    </p>

                    <p>Users are advised to take caution and good judgement before clicking any shortened urls published
                        on social media platforms by this website and its owners. Despite the best efforts to ensure
                        only genuine urls are published many social media platforms are prone to spam and hacking and
                        therefore this website and its owners cannot be held liable for any damages or implications
                        caused by visiting any shortened links.</p>
                    <h4>Resources &amp; Further Information</h4>
                    <ul>
                        <li>
                            <a href="http://www.ico.gov.uk/for_organisations/privacy_and_electronic_communications/the_guide.aspx"
                               target="_blank">Privacy and Electronic Communications Regulations 2003 - The Guide</a>
                        </li>
                        <li><a href="http://twitter.com/privacy" target="_blank">Twitter Privacy Policy</a></li>
                        <li><a href="http://www.facebook.com/about/privacy/" target="_blank">Facebook Privacy Policy</a>
                        </li>
                        <li><a href="http://www.google.com/privacy.html" target="_blank">Google Privacy Policy</a></li>
                        <li><a href="http://www.linkedin.com/static?key=privacy_policy" target="_blank">Linkedin Privacy
                                Policy</a></li>
                        <li><a href="http://mailchimp.com/legal/privacy/" target="_self">Mailchimp Privacy Policy</a>
                        </li>
                    </ul>
                    <p>v.2.0 July 2016 </p>
                </div>
            <?php } else { ?>
                <div class="col-md-12 justify">
                    <h4>À quoi sert cette politique de confidentialité?</h4>

                    <p>Sa politique de confidentialité est pour ce site Familov.com et régit la vie privée de ses
                        utilisateurs qui choisissent de l'utiliser.</p>

                    <p>La politique définit les différents domaines où la vie privée des utilisateurs est concerné et
                        décrit les obligations & amp; Exigences des utilisateurs, du site Web et des propriétaires de
                        sites Web. En outre, la façon dont ce site traite, stocke et protège les données et informations
                        utilisateur sera également détaillé dans cette politique.</p>
                    <h4>Le site Web</h4>

                    <p>Ce site Web et ses propriétaires adoptent une approche proactive de la confidentialité des
                        utilisateurs et veillent à ce que les mesures nécessaires soient prises pour protéger la vie
                        privée de leurs utilisateurs tout au long de leur séjour. Ce site Web est conforme à toutes les
                        lois et exigences nationales en matière de confidentialité.</p>
                    <h4>Utilisation des cookies</h4>

                    <p>Ce site Web utilise des cookies pour améliorer l'expérience des utilisateurs tout en visitant le
                        site Web. Le cas échéant, ce site Web utilise un système de contrôle des cookies qui permet à
                        l'utilisateur lors de sa première visite sur le site Web d'autoriser ou de refuser l'utilisation
                        de cookies sur leur ordinateur / appareil. Cela est conforme aux exigences légales récentes pour
                        les sites Web d'obtenir le consentement explicite des utilisateurs avant de quitter le site ou
                        de lire des fichiers tels que des cookies sur l'ordinateur / l'appareil d'un utilisateur.</p>

                    <p>Les cookies sont de petits fichiers enregistrés sur le disque dur de l'ordinateur de
                        l'utilisateur qui permettent de suivre, d'enregistrer et de stocker des informations sur les
                        interactions de l'utilisateur et l'utilisation du site Web. Cela permet au site, via son
                        serveur, de fournir aux utilisateurs une expérience sur mesure dans ce site.Les utilisateurs
                        sont informés que s'ils souhaitent refuser l'utilisation et l'enregistrement des cookies de ce
                        site sur leur disque dur, ils doivent prendre les mesures nécessaires dans leurs paramètres de
                        sécurité pour bloquer tous les cookies de ce site et de ses fournisseurs externes.</p>

                    <p>Ce site Web utilise un logiciel de suivi pour surveiller ses visiteurs afin de mieux comprendre
                        comment ils l'utilisent. Ce logiciel est fourni par Google Analytics qui utilise des cookies
                        pour suivre l'utilisation des visiteurs. Le logiciel enregistre un cookie sur le disque dur de
                        votre ordinateur afin de suivre et de surveiller votre engagement et l'utilisation du site Web,
                        mais ne stockera pas, n'épargner ou recueillir des informations personnelles. Vous pouvez lire
                        la politique de confidentialité de Google ici pour plus d'informations [<a
                            href="http://www.google.com/privacy.html" target="_blank">
                            http://www.google.com/privacy.html < / A>]. <br></p>

                    <p>D'autres cookies peuvent être stockés sur le disque dur de votre ordinateur par des fournisseurs
                        externes lorsque ce site Web utilise des programmes de parrainage, des liens sponsorisés ou des
                        annonces. Ces cookies sont utilisés pour la conversion et le suivi des références et expirent
                        habituellement après 30 jours, bien que certains peuvent prendre plus de temps. Aucune
                        information personnelle n'est stockée, enregistrée ou collectée.</p>
                    <h4>Contact & amp; la communication</h4>

                    <p>Les utilisateurs qui entrent en contact avec ce site Web et / ou leurs propriétaires le font à
                        leur propre discrétion et fournissent tous les détails personnels demandés à leurs propres
                        risques. Vos informations personnelles sont gardées privées et stockées en toute sécurité
                        jusqu'à un moment où il n'est plus nécessaire ou n'a pas d'utilité, comme détaillé dans la loi
                        de 1998 sur la protection des données. Tous les efforts ont été faits pour assurer un formulaire
                        sûr et sécurisé pour le processus de soumission, En utilisant ce formulaire pour envoyer des
                        processus par courrier électronique qu'ils le font à leurs risques et périls.
                    </p>

                    <p>Ce site Web et ses propriétaires utilisent toutes les informations soumises pour vous fournir des
                        informations supplémentaires sur les produits / services qu'ils offrent ou pour vous aider à
                        répondre aux questions ou questions que vous avez soumises. Cela inclut l'utilisation de vos
                        détails pour vous abonner à tout programme de bulletin d'e-mail le site Web fonctionne, mais
                        seulement si cela a été clairement indiqué à vous et votre autorisation expresse a été accordée
                        lors de la soumission de tout formulaire pour le processus de courrier électronique. Ou par
                        lequel vous le consommateur ont précédemment acheté ou demandé à l'achat de la société un
                        produit ou un service que le bulletin d'information électronique se rapporte à. Ce n'est en
                        aucun cas une liste complète de vos droits d'utilisateur en ce qui concerne la réception de
                        matériel de marketing par courriel.Vos coordonnées ne sont transmises à aucun tiers.</p>
                    <h4>Courriel</h4>

                    <p>Ce site Web exploite un programme de bulletin électronique, utilisé pour informer les abonnés sur
                        les produits et services fournis par ce site Web. Les utilisateurs peuvent s'abonner à un
                        processus automatisé en ligne s'ils le souhaitent, mais ils le font à leur propre discrétion.
                        Certaines souscriptions peuvent être traitées manuellement par accord écrit préalable avec
                        l'utilisateur.</p>

                    <p>Les abonnements sont pris en conformité avec les lois britanniques sur les spams détaillées dans
                        le Règlement sur la confidentialité et les communications électroniques de 2003. Tous les
                        détails personnels relatifs aux souscriptions sont conservés en toute sécurité et conformément à
                        la loi de 1998 sur la protection des données. Entreprises / personnes extérieures à l'entreprise
                        qui exploite ce site Web. En vertu de la Loi de 1998 sur la protection des données, vous pouvez
                        demander une copie des renseignements personnels détenus à votre sujet par le programme de
                        bulletin électronique de ce site Web. Une petite somme vous sera due. Si vous désirez obtenir
                        une copie des informations détenues sur vous, veuillez écrire à l'adresse professionnelle au bas
                        de cette politique.</p>

                    <p>Les campagnes de marketing par courriel publiées par ce site Web ou ses propriétaires peuvent
                        contenir des installations de suivi dans le courrier électronique actuel. L'activité de l'abonné
                        est suivie et stockée dans une base de données pour analyse et évaluation futures. Une telle
                        activité suivie peut inclure: L'ouverture de courriels, l'envoi de courriels, le clic des liens
                        dans le contenu du courrier électronique, les horaires, les dates et la fréquence des activités
                        [ceci est de loin une liste exhaustive].Ces informations sont utilisées pour affiner les futures
                        campagnes de courrier électronique et fournir à l'utilisateur un contenu plus pertinent basé sur
                        leur activité.</p>

                    <p>Conformément aux lois britanniques sur les spams et au Règlement sur la confidentialité et les
                        communications électroniques de 2003, les abonnés ont la possibilité de se désabonner à tout
                        moment par le biais d'un système automatisé. Ce processus est détaillé au bas de chaque campagne
                        d'email. Si un système de désabonnement automatisé n'est pas disponible, des instructions
                        claires sur la façon de se désabonner seront détaillées à la place.</p>
                    <h4>Liens externes</h4>

                    <p>Bien que ce site ne vise qu'à inclure des liens externes de qualité, sûrs et pertinents, il est
                        conseillé aux utilisateurs d'adopter une politique de prudence avant de cliquer sur les liens
                        externes mentionnés dans ce site Web. (Les liens externes sont des liens cliquables de texte /
                        bannière / image à d'autres sites Web, semblables à; <a
                            href="http://www.craftykingsboutique.co.uk" title="Art de livre plié" target="_blank"> Livre
                            plié Art </a> ou <a href="http://www.kingstrains.co.uk/articles/Used-Model-Trains-For-Sale"
                                                title="Les trains de modèle usagés à vendre" target="_blank"> Trains
                            d'occasion à vendre </a>.)</p>

                    <p>Les propriétaires de ce site Web ne peuvent garantir ou vérifier le contenu d'un site Web externe
                        en dépit de leurs meilleurs efforts. Les utilisateurs doivent donc noter qu'ils cliquent sur des
                        liens externes à leurs propres risques et ce site et ses propriétaires ne peuvent être tenus
                        responsables de tout dommage ou des conséquences causées par la visite de liens externes
                        mentionnés.</p>
                    <h4>Annonces et liens sponsorisés</h4>

                    <p>Ce site peut contenir des liens sponsorisés et des annonces. Ceux-ci seront généralement servis
                        par nos partenaires publicitaires, qui peuvent avoir des politiques de confidentialité
                        détaillées se rapportant directement aux annonces qu'ils servent.</p>

                    <p>En cliquant sur ces annonces, vous serez dirigé vers le site Web des annonceurs par le biais d'un
                        programme de référence qui peut utiliser des cookies et suivra le nombre de renvois envoyés à
                        partir de ce site Web. Cela peut inclure l'utilisation de cookies qui peuvent à leur tour être
                        enregistrés sur le disque dur de votre ordinateur. Les utilisateurs doivent donc noter qu'ils
                        cliquent sur des liens externes parrainé à leurs propres risques et ce site et ses propriétaires
                        ne peuvent être tenus responsables de tout dommage ou des conséquences causées par la visite de
                        liens externes mentionnés.</p>
                    <h4>Plateformes de médias sociaux</h4>

                    <p>La communication, l'engagement et les actions prises par des plateformes externes de médias
                        sociaux auxquelles ce site Web et ses propriétaires participent sont personnalisés aux termes et
                        conditions ainsi qu'aux politiques de confidentialité tenues respectivement avec chaque
                        plate-forme de médias sociaux.</p>

                    <p>Users are advised to use social media platforms wisely and communicate / engage upon them with
                        due care and caution in regard to their own privacy and personal details. This website nor its
                        owners will ever ask for personal or sensitive information through social media platforms and
                        encourage users wishing to discuss sensitive details to contact them through primary
                        communication channels such as by telephone or email.</p>

                    <p>Il est conseillé aux utilisateurs d'utiliser judicieusement les plateformes de médias sociaux et
                        de communiquer avec eux en toute prudence et prudence en ce qui concerne leur propre vie privée
                        et leurs données personnelles. Ce site Web et ses propriétaires ne demanderont jamais des
                        informations personnelles ou sensibles à travers les plateformes de médias sociaux et encourager
                        les utilisateurs souhaitant discuter de détails sensibles de les contacter par les canaux de
                        communication primaire tels que par téléphone ou</p>
                    <h4>Liens raccourcis dans les médias sociaux</h4>

                    <p>Ce site Web et ses propriétaires par le biais de leurs comptes de plateforme de médias sociaux
                        peuvent partager des liens Web vers des pages Web pertinentes. Par défaut, certaines plateformes
                        de médias sociaux raccourcissent de longues URL [adresses Web] (c'est un exemple:
                        http://bit.ly/zyVUBo). Les</p>

                    <p>Les utilisateurs sont invités à prendre des précautions et un bon jugement avant de cliquer sur
                        les URL raccourcies publiées sur les plateformes de médias sociaux par ce site Web et ses
                        propriétaires. Malgré les meilleurs efforts pour s'assurer que seuls les urls authentiques sont
                        publiés de nombreuses plates-formes de médias sociaux sont sujettes au spam et le piratage et
                        donc ce site et ses propriétaires ne peuvent être tenus pour responsables de tous les dommages
                        ou implications causés par la visite de liens raccourcis.</p>
                    <h4>Ressources & amp; Informations complémentaires</h4>
                    <ul>
                        <li>
                            <a href="http://www.ico.gov.uk/for_organisations/privacy_and_electronic_communications/the_guide.aspx"
                               target="_blank">Règlement sur la protection des renseignements personnels et les
                                communications électroniques 2003 - Guide</a></li>
                        <li><a href="http://twitter.com/privacy" target="_blank">Politique de confidentialité
                                Twitter</a></li>
                        <li><a href="http://www.facebook.com/about/privacy/" target="_blank">Politique de
                                confidentialité Facebook</a></li>
                        <li><a href="http://www.google.com/privacy.html" target="_blank">Politique de confidentialité
                                Google</a></li>
                        <li><a href="http://www.linkedin.com/static?key=privacy_policy" target="_blank">Politique de
                                confidentialité Linkedin</a></li>
                        <li><a href="http://mailchimp.com/legal/privacy/" target="_self">Politique de confidentialité de
                                Mailchimp</a></li>
                    </ul>
                    <p>V.2.0 July 2016</p>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

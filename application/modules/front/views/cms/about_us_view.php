<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('LBL_ABOUT_US_01') ?></h1>
                <!--<a href="#pricing6-1" class="btn btn-shadow btn-green btn-lg smooth-scroll m-b-md">RESERVE YOUR SEAT</a>-->
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>
<!-- =========================
   About Us
============================== -->
<!--<section id="hero12" class="hero hero-countdown bg-img" style="background-image:url('images/27.jpg');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white">About us</h1>
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
            </div>
        </div>
    </div>
</section>-->
<?php if ($this->current_language == 'english') { ?>
    <section id="content7-1" class="p-t-lg bg-edit">
        <div class="container ">
            <div class="row">
                <div class="col-md-12 justify">
                    <div class="section-header  wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <p class="lead m-b-md  zoomIn text-center">Familov: the future of money transfer</p>

                        <p class="lead m-b-md  zoomIn ">If you are working and living in a foreign country with an aim
                            of helping your family back at home, then you are familiar with the heavy costs that come
                            with money transfer services. Statistics shows that , in some area 90% of the money we send
                            to our families is for food and basic needs but it is also impractical to transfer small
                            amounts of money. again, most of the money sent to family members ends up being misused and
                            also contributed to family conflicts.</p>

                        <p class="lead m-b-md  zoomIn">Familov is a web-based platform that offers you the opportunity
                            to buy basic needs from local stores, which can be collected or delivered to and promptly
                            delivery to friends and family back at home at the same time guarantees that the money is
                            used for the right purpose.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container p-a-0">
            <div class="row content-grid">
                <div class="col-md-6">
                    <div class="img-block"
                         style="background-image:url('<?php echo(ASSETS_URL_FRONT . 'img/images/buro-2.jpg'); ?>')"></div>
                </div>
                <div class="col-md-6">
                    <div class="lead-block bg-edit bg-green">
                        <h2 class="text-white m-b-md">Our mission</h2>

                        <p class="lead text-white m-b-0 wow zoomIn"
                           style="visibility: visible; animation-name: zoomIn;">Maximizing the financial , social und
                            human impact of the transfer of values </p>
                    </div>
                    <div class="lead-block bg-edit bg-green">
                        <h2 class="text-white m-b-md">Award</h2>

                        <p class=" text-white m-b-0 wow zoomIn" style="visibility: visible; animation-name: zoomIn;">
                            Social Innovation price at Google start-up Week-End Saar 2015</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="our_values" style="padding-top:60px; padding-bottom:120px !important;" class=" section-our_founders">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="section-header text-center wow fadeIn">
                        <h2>Our Value</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <i class="fa fa-thumbs-up"></i>
                    <h5>Embrace Transparency</h5>

                    <p>
                        We truly value and respect our customers and partners. If it were not for them, we would not be
                        us.</p>
                </div>
                <div class="col-md-4 text-center">
                    <i class="fa fa-life-ring"></i>
                    <h5>Positive Impact</h5>

                    <p>
                        We care for each other and believe in relationships in which people value and support one
                        another.</p>
                </div>
                <div class="col-md-4 text-center">
                    <i class="fa fa-heart"></i>
                    <h5>Passion</h5>

                    <p>
                        We love helping our clients achieve their goals, take care of their loved ones or just grow
                        their savings.</p>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <section id="content7-1" class="p-t-lg bg-edit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center wow fadeIn"
                         style="visibility: visible; animation-name: fadeIn;">
                        <p class="lead m-b-md  zoomIn">Familov est une web plate-forme qui offre la possibilité aux
                            personnes vivants à l´etranger d'acheter produits et services auprès des magasins locaux et
                            les faire livrer à leur famille restée au pays</p>
                        <br/><br/>
                        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/teamwork.png'); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-header text-center wow fadeIn"
                         style="visibility: visible; animation-name: fadeIn;">

                        <p class="  m-b-md">

                            " Nous sommes une aventure entreprenariale sociale et collaborative,
                            <br/> avec un véritable mission...
                        </p>

                        <h2 class="m-b-md">Pourquoi nous ?</h2>

                        <p class="lead m-b-md  zoomIn">Loin devant les grandes institutions internationales, la diaspora
                            africaine demeure le premier donateur du continent. En 2015, l'équivalent de 35 milliards de
                            dollars a été transféré par des communautés africaines expatriées à leurs proches restés
                            dans les pays d'origine. Un gain inestimable, mais dont une partie de la valeur reste
                            confisquée en amont en raison de commissions élevées, qui peuvent facilement dépasser le
                            seuil de 10%. Parfois, 15%. Un manque à gagner pour les familles qui reçoivent les
                            fonds.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container p-a-0">
            <div class="row content-grid">

                <div class="col-md-6">
                    <div class="img-block"
                         style="background-image:url('<?php echo(ASSETS_URL_FRONT . 'img/images/buro-2.jpg'); ?>')"></div>
                </div>

                <div class="col-md-6">
                    <div class="lead-block bg-edit bg-green">
                        <h2 class="text-white m-b-md">Notre mission</h2>

                        <p class="lead text-white m-b-0 wow zoomIn"
                           style="visibility: visible; animation-name: zoomIn;">Maximiser l'impact financier, social et
                            humain du transfert d´argent </p>
                    </div>
                    <div class="lead-block bg-edit bg-green">
                        <h2 class="text-white m-b-md">Prix</h2>

                        <p class="lead text-white m-b-0 wow zoomIn"
                           style="visibility: visible; animation-name: zoomIn;">Prix ​​de l'innovation sociale au
                            démarrage de Google Week-End Saar 2015</p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <div id="our_values" style="padding-top:60px; padding-bottom:120px !important;" class=" section-our_founders">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="section-header text-center wow fadeIn">
                        <h2>Notre valeur</h2>
                    </div>
                </div>
            </div>
            <!-- /End Section Header -->
            <div class="row">
                <div class="col-md-4 text-center">
                    <i class="fa fa-thumbs-up"></i>
                    <h5>Embrasser la transparence</h5>

                    <p>
                        Nous apprécions et respectons nos clients et nos partenaires. Si ce n'était pas pour eux, nous
                        ne serions pas nous.</p>
                </div>
                <div class="col-md-4 text-center">
                    <i class="fa fa-life-ring"></i>
                    <h5>Impact positif</h5>

                    <p>
                        Nous nous soucions les uns des autres et croyons aux relations dans lesquelles les gens se
                        valorisent et se soutiennent mutuellement.</p>
                </div>
                <div class="col-md-4 text-center">
                    <i class="fa fa-heart"></i>
                    <h5>La passion</h5>

                    <p>
                        Nous aimons aider nos clients à atteindre leurs objectifs, prendre soin de leurs proches ou
                        simplement accroître leurs économies.</p>
                </div>
            </div>

        </div>
    </div>
<?php } ?>
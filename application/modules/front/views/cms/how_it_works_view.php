<!-- =========================
    PAGE HEADER
============================== -->
<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?php echo $this->lang->line('LBL_HIW_01'); ?></h1>
                <!--<a href="#pricing6-1" class="btn btn-shadow btn-green btn-lg smooth-scroll m-b-md">RESERVE YOUR SEAT</a>-->
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>
<!-- =========================
   How it Works SECTION
============================== -->
<section id="features2-1" class="p-y-md" style="margin-top: 5%;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-header text-center wow fadeIn">
                    <!-- <h2><?php echo LBL_HOW_IT_WORK; ?>?</h2>-->
                    <p class="lead"><?php echo $this->lang->line('LBL_HIW_02'); ?></p>
                </div>
            </div>
        </div>
        <div class="row text-center features-block c3">
            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <i class="icon-cart"></i>

                    <h2><span class="number">1</span></h2>
                    <h5><?php echo $this->lang->line('LBL_HIW_03'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_04'); ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <i class="icon-wallet2"></i>

                    <h2><span class="number">2</span></h2>
                    <h5><?php echo $this->lang->line('LBL_HIW_05'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_06'); ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <i class="icon-envelope"></i>

                    <h2><span class="number">3</span></h2>
                    <h5><?php echo $this->lang->line('LBL_HIW_07'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_08'); ?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="gt_main_services bg_3">
                    <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-deliverman.png'); ?>" width="145px"
                         alt="" style="margin-bottom: 13px;">

                    <h2><span class="number">4</span></h2>
                    <h5><?php echo $this->lang->line('LBL_HIW_09'); ?></h5>

                    <p><?php echo $this->lang->line('LBL_HIW_10'); ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-md-offset-1 text-center p-t-md wow fadeIn"
             style="visibility: visible; animation-name: fadeIn;background:lightyellow;">
            <h2><?php echo $this->lang->line('LBL_HIW_11'); ?></h2>

            <p class="lead"><?php echo $this->lang->line('LBL_HIW_12'); ?></p>

            <p class="lead"><?php echo $this->lang->line('LBL_HIW_13'); ?></p>

            <p class="lead"><?php echo $this->lang->line('LBL_HIW_14'); ?></p>
            <a href="<?php echo base_url(MENU_SIGN_UP); ?>"
               class="btn  btn-green "><?php echo $this->lang->line('LBL_HIW_15'); ?></b></a>
            <br/>
        </div>
</section>

<section id="features2-1" class="p-y-lg" style="background:#f5f5f5">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-header text-center wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                    <h2><?php echo $this->lang->line('LBL_HIW_16'); ?></h2>
                    <!--   <p class="lead">Harum voluptate sunt alias eveniet necessitatibus natus, odio quisquam cumque, laudantium quasi ut ipsam. Omnis quaerat veniam nesciunt earum.</p>    -->
                </div>
            </div>
        </div>
        <div class="row text-center features-block c3">
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin6.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HIW_17'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HIW_18'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin1.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HIW_19'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HIW_20'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin3.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HIW_21'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HIW_22'); ?></p>
            </div>
        </div>
        <div class="row text-center features-block c3 p-y-md">
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin2.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HIW_23'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HIW_24'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin4.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HIW_25'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HIW_26'); ?></p>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/right/coin5.png'); ?>" alt="">
                <h4><?php echo $this->lang->line('LBL_HIW_27'); ?></h4>

                <p><?php echo $this->lang->line('LBL_HIW_28'); ?></p>
            </div>
        </div>
    </div>
    <!--<div class="col-md-8 col-md-offset-2 text-center p-t-md wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
        <a href="signup.php" class="btn  btn-green "><?php /*echo LBL_SECTION_BUTTON; */ ?></a>
    </div>-->
</section>
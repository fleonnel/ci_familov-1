<section id="hero12" class="hero hero-countdown bg-img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center">
                <h1 class="text-white"><?= $this->lang->line('LBL_FAQ_01') ?></h1>
                <!--<a href="#pricing6-1" class="btn btn-shadow btn-green btn-lg smooth-scroll m-b-md">RESERVE YOUR SEAT</a>-->
            </div>
            <div class="col-md-6 col-md-offset-3 text-white text-center">
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae alias perspiciatis nisi expedita reprehenderi. Sariatur asperiores adipisci et, sint incidunt.</p>-->
            </div>
        </div>
    </div>
</section>
<!-- =========================
   FAQ
============================== -->
<?php if ($this->current_language == 'english') { ?>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <div class="row p-t-md c2">
                <div class="col-md-6">
                    <h4 class="text-center m-b-md"><?= $this->lang->line('LBL_FAQ_02') ?></h4>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">What´s
                                        Familov.com?</a>
                                </p>
                            </div>
                            <div id="collapse10" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <p>It’s all in the name. At familov.com, you can order delivery online from local
                                        store for your family. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">How do I
                                        create an account?</a>
                                </p>
                            </div>
                            <div id="collapse11" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <p>On the Familov Startpage, click on Sign up (or go directly to the Create an
                                        account page). Fill out the information and your password and confirm it a
                                        second time. Your email address will directly be linked to the account. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">I have
                                        forgotten my password?</a>
                                </p>
                            </div>
                            <div id="collapse12" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <p>You have already created an account but you can't remember your password? Click
                                        on 'Login' at the top of the page. Then click on 'Forgot Password?'. Fill out
                                        your email address and a new password will be sent to you by email.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">How long does
                                        a delivery take?</a>
                                </p>
                            </div>
                            <div id="collapse13" class="panel-collapse collapse inX ">
                                <div class="panel-body">
                                    <p>On average, it takes under 36 hours from the moment when the order is placed
                                        until the moment it's delivered. The delivery time can vary from one Shop to
                                        another and depends on the number of orders the Shop has to prepare.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">How much is
                                        delivery?</a>
                                </p>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse  ">
                                <div class="panel-body">
                                    <p>We charge a flat rate of 2.95 € for delivery.<br/><br/>
                                        Familov.com is only an intermediate, between you as consumer and the Shop. Seen
                                        the low income we gain from each order, we must charge for the use of an online
                                        payment method. The costs cover the technical maintenance of the online
                                        payments, the transaction costs that banks charge and our administrative costs
                                        for processing payments.All websites charge these costs but they often directly
                                        add them to the sending and/or administrative costs. We choose to separate these
                                        costs so that only the customers willing to use an online payment method have to
                                        pay them. We thank you for your understanding.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">What happens
                                        after I have placed my order?</a>
                                </p>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>The order arrives in our system and after we recieve payement, it is directly
                                        sent to the Shop (Shopper) you have ordered from . After only a few seconds the
                                        Shops receives the order by the means of our own System Shop-Connect .
                                        If the Shop has no working connection, your order will immediately be passed by
                                        phone by a member of our Team. This way you can always be sure that your order
                                        will be correctly sent to the chosen Shop.<br/>Upon removal, we confirm its
                                        identity with an ID-Card and SMS code<br/>

                                        <br/>1-the Shop received all the details of your order <br/> 2-The shopper
                                        prepare immediately your order <br/>3-We inform the reciever and send him a CODE
                                        per SMS <br/>4-You receive a confirmation peer SMS or email that your order is
                                        delivered.</p>

                                    <p>NB: We have hired shop-partners and personal shoppers to deliver your products.
                                        Don’t worry, they have been individually trained and background checked. Our
                                        shoppers are trained to give best quality and will carefully and accurately make
                                        your purchase, pack your items and deliver them.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Where can I see
                                        my receipts?</a>
                                </p>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>You can view your receipts one of two ways:

                                        Email: When your order is placed, you will receive an email with a receipt.
                                        From your account: on the website hover over your name in the top right hand
                                        corner and a menu will appear. Select My Orders. Click on an order number to see
                                        a detailed receipt for that order.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">How is my order
                                        confirmed?</a>
                                </p>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Once you have placed your order you will receive a confirmation email sent to the
                                        email address you entered when you placed your order. Keep this email and your
                                        order number in case you have any questions or remarks regarding your order.
                                        When you have received the confirmation email, Familov does the rest. Just sit
                                        back, relax and enjoy !</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">I have not
                                        received a confirmation email</a>
                                </p>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If an order number is displayed on your screen, that is an indication that your
                                        order has been submitted. The confirmation email may be in your 'spam' rather
                                        than in your inbox.
                                        If you don't receive an order number please contact our customer service to find
                                        the status of your order. You must provide the e-mail address you entered when
                                        ordering and the name of the selected Shop so we can find your order.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="text-center m-b-md"><?= $this->lang->line('LBL_FAQ_03') ?></h4>

                    <div class="panel-group" id="accordion-tech">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse31">What are
                                        the types of payment I can use?</a>
                                </p>
                            </div>
                            <div id="collapse31" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Credit card (VISA/American Express/Master Card/ )<br/>Paypal <br/> Bank
                                        Transfert</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse32">How
                                        secure are online payments?</a>
                                </p>
                            </div>
                            <div id="collapse32" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>When processing online payments on Familov.com we use secure SSL pages. This
                                        means that these pages ensure the protection of your personal and payment
                                        details. This way you can be sure that this information is only visible to you
                                        and to us, for the processing of your order.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse33">Why do I
                                        have to pay transaction costs?</a>
                                </p>
                            </div>
                            <div id="collapse33" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Familov.com is only an intermediate, between you as consumer and the Shop. Seen
                                        the low income we gain from each order, we must charge for the use of an online
                                        payment method. The costs cover the technical maintenance of the online
                                        payments, the transaction costs that banks charge and our administrative costs
                                        for processing payments.All websites charge these costs but they often directly
                                        add them to the sending and/or administrative costs. We choose to separate these
                                        costs so that only the customers willing to use an online payment method have to
                                        pay them. We thank you for your understanding.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse34">My online
                                        payment failed</a>
                                </p>
                            </div>
                            <div id="collapse34" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If you think the online payment has failed, always check if you have received a
                                        confirmation email. This email will enable you to see if your order has been
                                        submitted and if the order is already paid for or not.
                                        The confirmation email is directly sent once the payment is complete. This may
                                        take a little longer if the payment is being processed by the bank, the credit
                                        card company or any other payment company. This can take up to 15 minutes.
                                        If after 15 minutes you still have no confirmation email in your inbox, contact
                                        our customer service. They will tell you if the order and the payment have
                                        succeeded.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse35">I have
                                        many more questions for you?</a>
                                </p>
                            </div>
                            <div id="collapse35" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Contact us! We will be there at hello@familov.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="section-header text-center wow fadeIn">
                        <!--<h2 class="m-b-md">Dear Familover</h2>-->
                        <p class="lead m-b-md">How can we help you today?</p>
                        <a href="mailto:<?= MAIL_CONTACT ?>" class="btn btn-green">Email Your Question</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section id="faq3-1" class="p-y-lg faqs schedule">
        <div class="container">
            <!-- Faq Panel -->
            <div class="row p-t-md c2">
                <div class="col-md-6">
                    <h4 class="text-center m-b-md"><?= $this->lang->line('LBL_FAQ_02') ?></h4>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">Qu'est-ce que
                                        Familov.com?</a>
                                </p>
                            </div>
                            <div id="collapse10" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <p>Tout est dans le nom. Avec familov.com, commandez depuis l´étranger et nous
                                        livrons votre famille au pays en moins de 36 heures. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">Comment
                                        puis-je créer un compte?</a>
                                </p>
                            </div>
                            <div id="collapse11" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <p>Sur la page de démarrage de Familov, cliquez sur Inscrivez-vous (ou allez
                                        directement à la page Créer un compte). Remplissez les informations et votre mot
                                        de passe et confirmez-les une deuxième fois. Votre adresse de courriel sera
                                        directement liée au compte. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">J'ai oublié
                                        mon mot de passe?</a>
                                </p>
                            </div>
                            <div id="collapse12" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <p>Vous avez déjà créé un compte mais vous ne vous souvenez plus de votre mot de
                                        passe? Cliquez sur «Login» en haut de la page. Puis cliquez sur 'Mot de passe
                                        oublié?'. Remplissez votre adresse e-mail et un nouveau mot de passe vous sera
                                        envoyé par e-mail.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">Combien de
                                        temps prend une livraison?</a>
                                </p>
                            </div>
                            <div id="collapse13" class="panel-collapse collapse inX ">
                                <div class="panel-body">
                                    <p>En moyenne, nous avons besoin de moins de 36 heures à partir du moment où nous
                                        avons recu le payement jusqu'au moment de la livraison à votre proche. Le délai
                                        de livraison peut varier d'une boutique à l'autre et dépend du nombre de
                                        commandes que la boutique doit préparer et la disponibilité du bénéficiaire.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Combien coûte
                                        la livraison?</a>
                                </p>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse  ">
                                <div class="panel-body">
                                    <p>Nous facturons un forfait de 2,95 € (2,50€ pendant le phase béta) pour le service
                                        et 0,00€ pour les livraison en magasin.En fonction de lieu de residence le coût
                                        des livraison à domicile varie<br/><br/>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Que se
                                        passe-t-il après avoir passé ma commande?</a>
                                </p>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <p>L'ordre arrive dans notre système et après que nous recevons le paiement, il est
                                        envoyé directement à la boutique (Shopper) que vous avez commandé. Après
                                        seulement quelques secondes, les magasins reçoivent l'ordre par le biais de
                                        notre propre système Shop-Connect.<br/>


                                        <br/>1- Le magasin recoit tous les détails de votre commande <br/> 2- Votre
                                        proche recoit le code de retrait par SMS<br/> 3-Le livreur prépare le paquet et
                                        recontacte le bénéficiaire dans les 36 heures pour la livraison <br/>4-Vous
                                        recevez une confirmation par SMS ou e-mail que votre commande est bien livrée.
                                    </p>

                                    <p>NB: Nous travaillons avec partenaires n et des livreurs proffessionnels pour
                                        gerer vos commanades. Ne vous inquiétez pas, ils ont été formés individuellement
                                        et les antécédents ont été vérifiés. Nos équipes sont formés pour donner la
                                        meilleure qualité , soigner vos articles et les livrer.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Où puis-je voir
                                        mes reçus?</a>
                                </p>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Vous pouvez afficher vos reçus de deux façons:

                                        Email: Lorsque votre commande est passée, vous recevrez un courriel avec un
                                        reçu. <br/> <br/>
                                        Sur le site Web cliquez sur votre nom dans le coin supérieur droit et un menu
                                        apparaîtra. Sélectionnez Mes commandes. Cliquez sur un numéro de commande pour
                                        afficher un reçu détaillé de cet ordre.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Comment ma
                                        commande est-elle confirmée?</a>
                                </p>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Une fois votre commande passée, vous recevrez un e-mail de confirmation envoyé à
                                        l'adresse e-mail que vous avez saisie lors de votre commande. Conservez ce
                                        courriel et votre numéro de commande pour toute question ou remarque concernant
                                        votre commande. Lorsque vous recevez l'e-mail de confirmation, Familov fait le
                                        reste. Il suffit de s'asseoir, de se détendre et de profiter!</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Je n'ai pas
                                        reçu d'email de confirmation</a>
                                </p>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Si un numéro de commande s'affiche à l'écran, cela signifie que votre commande a
                                        été envoyée. L'e-mail de confirmation peut être dans votre «spam» plutôt que
                                        dans votre boîte de réception.
                                        Si vous ne recevez pas de numéro de commande, veuillez contacter notre service à
                                        la clientèle pour connaître l'état de votre commande. Vous devez fournir
                                        l'adresse e-mail que vous avez saisie lors de la commande et le nom de la
                                        boutique sélectionnée afin que nous puissions trouver votre commande.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /End Panel Group -->
                </div>
                <!-- /End Col -->
                <div class="col-md-6">
                    <h4 class="text-center m-b-md"><?= $this->lang->line('LBL_FAQ_03') ?></h4>

                    <div class="panel-group" id="accordion-tech">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse31">Quels
                                        sont les types de paiement que je peux utiliser?</a>
                                </p>
                            </div>
                            <div id="collapse31" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Carte de crédit (VISA / American Express / Master Card /)<br/>Pay Pal <br/>
                                        Virement</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse32">Dans
                                        quelle mesure les paiements en ligne sont-ils sécurisés?</a>
                                </p>
                            </div>
                            <div id="collapse32" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Lors du traitement des paiements en ligne sur Familov.com, nous utilisons des
                                        pages SSL sécurisées. Cela signifie que ces pages assurent la protection de vos
                                        données personnelles et de paiement. De cette façon, vous pouvez être sûr que
                                        cette information est seulement visible pour vous et pour nous, pour le
                                        traitement de votre commande.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse33">Pourquoi
                                        dois-je payer les frais de transaction?</a>
                                </p>
                            </div>
                            <div id="collapse33" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Familov.com est seulement un intermédiaire, entre vous en tant que consommateur
                                        et la Boutique. Vu le faible revenu que nous gagnons sur chaque commande, nous
                                        devons facturer pour l'utilisation d'une méthode de paiement en ligne. Les coûts
                                        couvrent l'entretien technique des paiements en ligne, les coûts de transaction
                                        que les banques facturent et nos frais administratifs pour le traitement des
                                        paiements. Tous les sites facturent ces coûts, mais ils les ajoutent souvent
                                        directement aux frais d'envoi et / ou d'administration.. Nous vous remercions
                                        pour votre compréhension.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse34">Mon
                                        paiement en ligne a échoué</a>
                                </p>
                            </div>
                            <div id="collapse34" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Si vous pensez que le paiement en ligne a échoué, vérifiez toujours si vous avez
                                        reçu un e-mail de confirmation. Cet e-mail vous permettra de voir si votre
                                        commande a été soumise et si la commande est déjà payée ou non. <Br/><br/>
                                        L'e-mail de confirmation est envoyé directement une fois le paiement terminé.
                                        Cela peut prendre un peu plus longtemps si le paiement est traité par la banque,
                                        la société de carte de crédit ou toute autre société de paiement. Cela peut
                                        prendre jusqu'à 15 minutes.
                                        Si après 15 minutes vous n'avez toujours pas de courriel de confirmation dans
                                        votre boîte de réception, contactez notre service à la clientèle. Ils vous
                                        diront si l'ordre et le paiement ont réussi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-tech" href="#collapse35">J'ai
                                        encore beaucoup de questions à vous poser?</a>
                                </p>
                            </div>
                            <div id="collapse35" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Contactez nous! Nous serons là à :<Br/><br/> Email:hello@familov.com <Br/><br/>
                                        Whatsapp : (+49 1525 9948834)<Br/><br/> Chat : www.familov.com</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /End Panel Group -->
                </div>
                <!-- /End Col -->
            </div>
            <!-- /End Row -->
            <!-- Section Header -->
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="section-header text-center wow fadeIn">
                        <!--<h2 class="m-b-md">Dear Familover</h2>-->
                        <p class="lead m-b-md">Que peut-on faire pour vous aider aujourd'hui?</p>
                        <a href="mailto:<?= MAIL_CONTACT ?>" class="btn btn-green">Envoyer votre question par
                            courriel</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /End Container -->
    </section>
<?php } ?>
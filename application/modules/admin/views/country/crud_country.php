<!-- Select2 -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN . 'dist/js/bootstrap-formhelpers-countries.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN . 'dist/js/bootstrap-formhelpers-currencies.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN . 'dist/js/bootstrap-formhelpers-selectbox.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_ADMIN . 'dist/js/bootstrap-formhelpers-countries.en_US.js'); ?>"></script>
 <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/bootstrap-formhelpers-countries.flags.css');?>">
 <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/bootstrap-formhelpers-currencies.flags.css');?>">
 <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/bootstrap-formhelpers.css');?>">
<?php
$action_url = '';
$list_url = admin_url(COUNTRY_C . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    
                    <form class="form-horizontal" name="crud_form" id="crud_form" method="post">
                    <div class="box-body">
                       <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Country Name<span class="text-red">*</span></label>
                                    <!-- <div class="col-sm-6">
                                        <input type="text" class="form-control bfh-selectbox-filter" id="country_name"  name="country_name" placeholder="Enter Country Name" value="<?php echo edit_display('country_name', @$info); ?>">
                                    </div> -->
                                    <div class="col-sm-4">
                                       <!--  <input type="text" value="" id="country_name"  name="country_name"> -->
                                        <select class=" input-medium bfh-countries form-control " name="country_code" id="country_code" data-country="US" data-flags="true"></select>
                                        
                                        <div class="bfh-selectbox bfh-countries" data-country="US" data-flags="true" name="country_code" id="country_code" onchange="showDiv()">
                                          <input type="hidden" value="" id="country_name"  name="country_name">
                                          <a class="bfh-selectbox-toggle"  role="button" data-toggle="bfh-selectbox" id="bfh-selectbox" href="#">
                                            <span class="bfh-selectbox-option input-medium" id="bfh-selectbox-option" data-option=""></span>
                                            <b class="caret"></b>
                                          </a>
                                          <div class="bfh-selectbox-options" id="bfh-selectbox-options">
                                            <input type="text" class="bfh-selectbox-filter">
                                            <div role="listbox">
                                            <ul role="option">
                                            </ul>
                                            </div>
                                          </div>
                                        </div>
                                    </div>s
                        </div>

                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>

                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success ">Save</button>
                                <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </a>
                            </div>
                            
                        </div>
                    </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script>
function showDiv() {
    
//$('#country_code').change(function(){
    //var c_name =  $(this).find(':selected').text();
    //var c_name = document.getElementById("bfh-selectbox-toggle");
    var e = document.getElementById("bfh-selectbox-option");
    var c_name = e.options[e.selectedIndex].value;
    alert(c_name);
    if(c_name !=''){
        $('#country_name').val(c_name);
    }else{
        $('#country_name').val('');
    }
};
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                country_code1: {
                    required: true
                }
                
            },
            messages: {
                country_code1: {
                    required: 'Please Enter Country Name.'
                }
                
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
               errorHandler1.hide();
            }
        });
    });
</script>
<!-- Select2 -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<?php
$action_url = '';
$list_url = admin_url(CATEGORY . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    <?php echo form_open('', 'class="form-horizontal" id="crud_form"  name="crud_form" enctype="multipart/form-data" '); ?>
                    <div class="box-body">
                       <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Category Name<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="country_name"  name="category_name" placeholder="Enter Category Name" value="<?php echo edit_display('category_name', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Category Image</label>
                                <?php  
                                //if (@$info->category_image == "") { ?>
                                <div class="col-sm-6">
                                     <input type="file" id="category_image" name="category_image" class="form-control form-control-flat">
                                </div>
                                <?php //} ?>
                        </div>
                        <div class="form-group">
                                <?php  
                               # _pre($files);
                                         if (@$info->category_image != "") { ?>
                                        <p style="margin-left:20%;" id="idrow_<?= @$info->category_id ?>">
                                            <img src="<?= DIR_CATEGORY_URL ?><?= @$info->category_image; ?>" width="150" height="150" />
                                            <button type="button" onclick="deletimage(<?= @$info->category_id; ?>)"
                                               data-toggle="tooltip" title="" value="<?= @$info->category_id; ?>" class="MultiFile-remove"
                                               data-original-title="remove"> <i class="fa fa-trash fa-o"></i>
                                            </button>
                                        </p>
                                        <?php }  ?>
                        </div>
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>

                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success ">Save</button>
                                <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                category_name: {
                    required: true
                },
                
            },
            messages: {
                category_name: {
                    required: 'Please Enter Category Name.'
                },
                
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
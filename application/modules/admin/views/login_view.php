<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=ADMIN_PAGE_TITLE?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/AdminLTE.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'plugins/iCheck/square/blue.css');?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- NProgress -->
    <link rel="stylesheet" href="<?php echo base_url(ASSETS.'vendors/nprogress/nprogress.css');?>">
    <!-- bootstrap-wysiwyg -->
    <link rel="stylesheet" href="<?php echo base_url(ASSETS.'vendors/google-code-prettify/bin/prettify.min.css');?>">

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(ASSETS_ADMIN.'plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url(ASSETS_ADMIN.'bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(ASSETS_ADMIN.'plugins/iCheck/icheck.min.js');?>"></script>

    <script src="<?php echo base_url(ASSETS_BUILD_JS.'bootstrapValidator.js');?>"></script>
    <script src="<?php echo base_url(ASSETS.'js/validate.js');?>"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    <link rel="shortcut icon" href="<?php echo base_url(ASSETS.'common/favicon.png');?>" type="image/png">
    <link rel="icon" href="<?php echo base_url(ASSETS.'common/favicon.png');?>" type="image/png">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN . 'custom/custom.css'); ?>">
</head>
<body class="hold-transition login-page bg-primary admin-login">
<div class="login-box bg-overlay">
    <div class="login-logo ">
        <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/admin.png'); ?>" class="img-responsive"
             alt="<?= _get_config('admin_login_header_one') ?> <?= _get_config('admin_login_subheader') ?>" width="80%">
        <!--<a href="<? /*= A_TAG_DISABLE */ ?>"><b><? /*= _get_config('admin_login_header_one') */ ?></b> --><? /*= _get_config('admin_login_subheader') */ ?>
        <!--</a>-->
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body ">
        <p class="login-box-msg "><?=LOGIN_PAGE_SUB_TITLE?></p>
        <form class="form-signin panel" method="post" role='form' name="login" action="">
            <?php
            if (@$this->session->userdata(SESSION_FLASH)[FLASH_STATUS] !==''){
                if (@$this->session->userdata(SESSION_FLASH)[FLASH_STATUS] == FLASH_STATUS_SUCCESS ) { ?>
                <div class="alert alert-success">
                    <?php echo $this->session->userdata(SESSION_FLASH)[FLASH_MESSAGE]; ?>
                </div>
            <?php } else if (@$this->session->userdata(SESSION_FLASH)[FLASH_STATUS] == FLASH_STATUS_ERROR ) { ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->userdata(SESSION_FLASH)[FLASH_MESSAGE]; ?>
                </div>
            <?php } else if (@$this->session->userdata(SESSION_FLASH)[FLASH_STATUS] == FLASH_STATUS_WARNING) { ?>
                <div class="alert alert-warning">
                    <?php echo $this->session->userdata(SESSION_FLASH)[FLASH_MESSAGE]; ?>
                </div>
            <?php } else if (@$this->session->userdata(SESSION_FLASH)[FLASH_STATUS] == FLASH_STATUS_INFO ) { ?>
                <div class="alert alert-info">
                    <?php echo $this->session->userdata(SESSION_FLASH)[FLASH_MESSAGE]; ?>
                </div>
            <?php }
                @$this->session->unset_userdata('logout');
            }
            ?>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Email/Username" name="username" id="username" value="<?=@$user_name?>" autofocus required="" >
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="pass" class="form-control" placeholder="Password" name="password" value="<?=@$password?>"" required="" >
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" name="btnLogin" value="login" class="btn btn-success btn-block btn-flat">Connexion</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
       <!-- <a href="<?php echo base_url(ADMIN_C.'forgot-password');?>">I forgot my password <i class="fa fa-fw fa-arrow-right"></i></a><br>-->
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
</html>

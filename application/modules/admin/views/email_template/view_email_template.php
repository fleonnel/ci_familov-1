<?php
$list_url = admin_url(EMAIL_TEMPLATE_C);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$info->email_title ?></strong></h3>

                    <div class="rom-md-12">
                        <a href="<?php echo $list_url; ?>">
                            <button type="button" class="btn btn-default pull-right">Back</button>
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <div class="form-group">
                        <label for="inputlbl" class="col-sm-2 control-label">Email (English)</label>
                    </div>
                    <?php echo _header_footer(@$info->tEmailFormatDesc_EN); ?>
                </div>
                <div class="box-body">
                    <?php _notify(); ?>
                    <div class="form-group">
                        <label for="inputlbl" class="col-sm-2 control-label">Email (French)</label>
                    </div>
                    <?php echo _header_footer(@$info->tEmailFormatDesc_FR); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
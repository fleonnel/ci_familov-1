<?php
$list_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "data_list");
$add_update_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "add_update");
$update_status_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "update_status") . '/';
$btn_add_text = 'Add Email Template';
$export_prefix = 'Email_Template_';
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><strong><?=@$level_two?></strong></h3>
                    <!--<div class="rom-md-12">
                        <a href="<?php /*echo $add_update_url; */ ?>">
                            <button type="button" class="btn btn-primary pull-right"><? /*= $btn_add_text */ ?></button>
                        </a>
                    </div>-->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <table id="list_table_one" class="table table-striped table-bordered table-hover dataTables-example newTab">
                        <thead>
                        <tr>
                            <th>No#</th>
                            <th>Email Key</th>
                            <th>Email Title</th>
                            <th>Email Subject (English)</th>
                            <th>Email Subject (French)</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<script type="text/javascript">
    var list_table_one;
    var list_url = "<?php echo $list_url; ?>";
    $(document).ready(function () {
        var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
        var export_coulmn = "0,1,2,3";
        <?php //TODO Change Name for export file here ?>
        list_table_one = $('#list_table_one').DataTable({
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": list_url,
                "type": "POST"
            },
            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    <?php //TODO Set Column number here for disable sorting ?>
                    "targets": [0, 1,2,3,4,5], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],
            pageLength: 10,
            responsive: true,
            "autoWidth": false

        });
    });
    function reload_table() {
        list_table_one.ajax.reload(null, false); //reload datatable ajax
    }
</script>
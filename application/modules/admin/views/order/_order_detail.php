<link rel="stylesheet" href="<?php echo base_url(ASSETS_ADMIN.'dist/css/invoice.css');?>">

<div id="printableArea">
<div id="invoice" style="padding:10px;font-weight:400 !important;">

    <!-- Header -->
    <div class="row">
        <!--<div class="col-md-4">
            <div id="logo"><img src="<?php echo base_url('uploads/shop'.$order_info->shop_logo);?>" class=""
                             alt="<?= SITE_NAME_COM ?>" ></div>
        </div>-->
        <div class="col-xs-4">
            <div id="logo">
                <img src="<?php echo(ASSETS_URL_FRONT . 'img/images/familov-logo.png'); ?>" class=""
                             alt="<?= SITE_NAME_COM ?>" style="width:90%;max-height:60px!important;height:auto;">
            </div>
       
        </div>

        <div class="col-xs-8" >  

            <p id="details">
                <strong>CODE: <?= @$order_info->generate_code;?></strong> <br>
                <strong>Livraison:</strong> : <?= @$order_info->delivery_option;?> <br>
                <strong><?= @$order_info->shop_name;?><br></strong>
               
                
            </p>
        </div>
    </div>


    <!-- Client & Supplier -->
    <div class="row" style="background:#f7f7f7;border:1px solid #aaaaaa;padding-top:10px;margin-bottom:10px;">
        


        <div class="col-md-7" >  
            
            <p style="border-right:1px dashed #dddddd">
               <strong >De :</strong> <?= @$customer->username . " " . @$customer->lastname ?> <br>
               <strong >Pour :</strong> <?= @$order_info->receiver_name;?> <br>
               <strong >Contact :</strong> <?= @$order_info->receiver_phone;?> <br>
                 
                  
               
            </p>
        </div>
        
       
        
        <div class="col-md-5"> 
      
            <strong class="margin-bottom-5">Message :</strong>   <?= @$order_info->greeting_msg;?> <br>
            
            <p>
            
        </div>
        
       
        
        
    </div>


    <!-- Invoice -->
    <div class="row">
        
       <?= @$html ?>
    </div>
    <!-- Footer -->
    <div class="row">
        <div class="col-xs-6">
            Signature du béneficiaire
        </div>
         <div class="col-xs-6">
            Signature du Chargé de commande
        </div>
        
        <div class="col-md-12">
            <a href="#" class="print-button" onclick="printDiv()">Imprimer / Print</a>
        </div>
    </div>
    <!-- Print Button -->
</div>
</div>
<?php include_once("_script_crud.php"); ?>
<?php
#TODO : Change Controller Name, Add Button Text, Export Prefix name, Column Order in Datatable js.
$list_url = admin_url(PRODUCT. "data_list");
$add_update_url = admin_url(PRODUCT . "add");
$btn_add_text = 'Add Product';
$export_prefix = 'product_';
?>
<style type="text/css">li{list-style: none;}</style>
        <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <?php
                            if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                            {
                        ?>
                                <div class="col-md-12">
                                    <a href="<?php echo $add_update_url; ?>">
                                        <button type="button" class="btn btn-success pull-right"><?= $btn_add_text ?></button>
                                    </a>
                                </div>
                        <?php
                            }
                            else
                            {
                        ?>
                                
                        <?php
                            }
                        ?>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php _notify(); ?>
                        <div style="height:100%;overflow:auto; width:100%">
                        <table id="list_table_one"
                               class="table table-striped table-bordered table-hover dataTables-example newTab">
                            <thead>
                            <tr>
                                <th>No#</th>
                                <th>image</th>
                                <th>Name</th>
                                <th>Category</th>
                                <?php
                                    if ($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                                    {
                                ?>
                                        <th>Price</th>
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                <?php
                                    }
                                ?>
                                <th>Shop</th>
                                <?php
                                    if ($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                                    {
                                ?>
                                        <th>Status</th>
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                <?php
                                    }
                                ?>
                                <th>Availability</th>
                                <?php
                                    if ($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                                    {
                                ?>
                                        <th>Action</th>
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                <?php
                                    }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
    <!-- /.content -->

    <script type="text/javascript">
        var list_table_one;
        var list_url = "<?php echo $list_url; ?>";
        $(document).ready(function () {
            var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
            var export_coulmn = "0,1";
            <?php //TODO Change Name for export file here ?>
            list_table_one = $('#list_table_one').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": list_url,
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        <?php //TODO Set Column number here for disable sorting ?>
                        <?php
                            if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
                            {
                        ?>
                                "targets": [0, 1, 6, 7, 8], //first column / numbering column
                        <?php
                            }
                            else
                            {
                        ?>
                                "targets": [0, 1, 5], //first column / numbering column
                        <?php
                            }
                        ?>
                        
                        "orderable": false, //set not orderable
                    },
                ],
                pageLength: 10,
                responsive: true,
                "autoWidth": false

            });
        });
        function reload_table() {
            list_table_one.ajax.reload(null, false); //reload datatable ajax
        }
    </script>
<?php #include_once("_script_crud.php"); ?>
<script>
function action_delete(id, status) {
            var url = "<?php echo base_url(ROOT_DIR_ADMIN.PRODUCT."action_delete") . '/' ;?>";
            swal({
                    title: "<?=CONFIRMATION_MESSAGE_FOR_DELETE_TITLE?>",
                    text: "<?=CONFIRMATION_MESSAGE_FOR_DELETE_TEXT?>",
                    type: "<?=SWEET_ALERT_INFO?>",
                    showCancelButton: true,
                    confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                    confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                    cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {id: id, status: status},
                            dataType: "JSON",
                            success: function (data) {
                                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                                {
                                    reload_table();
                                    var STATUS_TYPE = '';
                                    STATUS_TYPE = '<?=MESSAGE_DELETE_SUCCESS?>';
                                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                                }
                                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                                } else {
                                    toastr.error('<?=MESSAGE_DELETE_ERROR?>', '<?=ERROR?>');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                            }
                        });
                    } else {
                        toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                    }
                });
        }
</script>
<script>
function action_duplicate(id, status) {
            var url = "<?php echo base_url(ROOT_DIR_ADMIN.PRODUCT."action_duplicate") . '/' ;?>";
            swal({
                   title: "<?=CONFIRMATION_MESSAGE_FOR_DUPLICATE_TITLE?>",
                    text: "<?=CONFIRMATION_MESSAGE_FOR_DELETE_TEXT?>",
                   type: "<?=SWEET_ALERT_INFO?>",
                    showCancelButton: true,
                    confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                    confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                    cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {id: id, status: status},
                            dataType: "JSON",
                            success: function (data) {
                                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                                {
                                    reload_table();
                                    var STATUS_TYPE = '';
                                    STATUS_TYPE = '<?=MESSAGE_ADD_SUCCESS?>';
                                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                                }
                                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                                } else {
                                    toastr.error('<?=MESSAGE_DELETE_ERROR?>', '<?=ERROR?>');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                            }
                        });
                    } else {
                        toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                    }
                });
        }
</script>
<script>
    function status_update(id,label) {
        var url = "<?php echo base_url(ROOT_DIR_ADMIN.PRODUCT."change_status") . '/' ;?>";
         if(label == 'UNPUBLISHED'){
            var label_new='PUBLISHED';
        }
        else{var label_new='UNPUBLISHED';}
        var userdata = "id=" + id + "&label=" + label_new;
        var popup_title = "<?=CONFIRMATION_MESSAGE_FOR_PRODUCT_STATUS_UPDATE_TITLE?>";
        popup_title = popup_title.replace("#OLD_STATUS#",label);
        popup_title = popup_title.replace("#NEW_STATUS#",label_new);
        swal({
                title: popup_title,
                text: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TEXT?>",
                type: "<?=SWEET_ALERT_INFO?>",
                showCancelButton: true,
                confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: userdata,
                        dataType: "JSON",
                        cache: false,
                        success: function (data) {
                            if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                            {
                                reload_table();
                                var STATUS_TYPE = '';
                                STATUS_TYPE = '<?=MESSAGE_CHANGE_STATUS_SUCCESS?>';
                                toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                            }
                            else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                            } else {
                                toastr.error('<?=MESSAGE_CHANGE_STATUS_ERROR?>', '<?=ERROR?>');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                        }
                    });
                } else {
                    toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                }
            });
        //});
    }
</script>
<script>
    function availability_update(id,label) {
        var url = "<?php echo base_url(ROOT_DIR_ADMIN.PRODUCT."change_availability") . '/' ;?>";
        if(label == 'EN RUPTURE'){
            var label_new='DISPONIBLE';
        }
        else{var label_new='EN RUPTURE';}
        var p_name = $("#p_" + id).data('name');
        var userdata = "id=" + id + "&label=" + label_new;
        var popup_title = "<?=CONFIRMATION_MESSAGE_FOR_AVAILABLE_STATUS_UPDATE_TITLE?>";
        popup_title = popup_title.replace("#OLD_AVAILABLE#",label);
        popup_title = popup_title.replace("#NEW_AVAILABLE#",label_new);
        popup_title = popup_title.replace("#PRODUCT_NAME#", p_name);
        //popup_title = popup_title.replace("#NEW_STATUS#",value);

        swal({
                title: popup_title,
                text: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TEXT?>",
                type: "<?=SWEET_ALERT_INFO?>",
                showCancelButton: true,
                confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                //cancelButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: userdata,
                        dataType: "JSON",
                        cache: false,
                        success: function (data) {
                            if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                            {
                                reload_table();
                                var STATUS_TYPE = '';
                                STATUS_TYPE = '<?=MESSAGE_CHANGE_STATUS_SUCCESS?>';
                                toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                            }
                            else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                            } else {
                                toastr.error('<?=MESSAGE_CHANGE_STATUS_ERROR?>', '<?=ERROR?>');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                        }
                    });
                } else {
                    toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                }
            });
        //});
    }
</script>
<style>
    .sa-button-container > .cancel {
        background-color: #dc3545 !important;
    }

    .sa-button-container > .confirm {
        background-color: #2cfb93 !important;
        color: #222222 !important;
    }
</style>
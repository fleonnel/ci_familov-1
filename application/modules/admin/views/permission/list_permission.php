<?php
#TODO : Change Controller Name, Add Button Text, Export Prefix name, Column Order in Datatable js.
$list_url = admin_url(SERVICE_C . "data_list");
$add_update_url = admin_url(SERVICE_C . "add_update");
$update_status_url = admin_url(SERVICE_C . "update_status") . '/';
$btn_add_text = 'Add Service';
$export_prefix = 'Service_';
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>

                        <div class="rom-md-12">
                            <a href="<?php echo $add_update_url; ?>">
                                <button type="button" class="btn btn-success pull-right"><?= $btn_add_text ?></button>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php _notify(); ?>
                        <table id="list_table_one"
                               class="table table-striped table-bordered table-hover dataTables-example newTab">
                            <thead>
                            <tr>
                                <th>No#</th>
                                <th>Service Name</th>
                                <th>Department Name</th>
                                <th>Periodicity Name</th>
                                <th>Description</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include_once("_script_crud.php"); ?>
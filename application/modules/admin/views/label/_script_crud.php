<?php
if ($method == 'lists') { ?>
    <script type="text/javascript">
        var list_table_one;
        var list_url = "<?php echo $list_url; ?>";
        $(document).ready(function () {
            var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
            var export_coulmn = "0,1";
            <?php //TODO Change Name for export file here ?>
            list_table_one = $('#list_table_one').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [[1, 'asc']], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": list_url,
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        <?php //TODO Set Column number here for disable sorting ?>
                        "targets": [0, 1], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],
                pageLength: 10,
                responsive: true,

            });
        });
        function reload_table() {
            list_table_one.ajax.reload(null, false); //reload datatable ajax
        }
    </script>
<?php } else if ($method == 'add' || $method == 'update') { ?>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $(".select2").select2();
        });
    </script>

<?php } ?>
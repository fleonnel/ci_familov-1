<?php
#TODO : Change Controller Name, Add Button Text, Export Prefix name, Column Order in Datatable js.
$list_url = admin_url(LABEL. "data_list");
$add_update_url = admin_url(LABEL . "add");
$btn_add_text = 'Add Label';
$export_prefix = 'label_';
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>

                        <div class="col-md-12">
                            <a href="<?php echo $add_update_url; ?>">
                                <button type="button" class="btn btn-success pull-right"><?= $btn_add_text ?></button>
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php _notify(); ?>
                        <table id="list_table_one"
                               class="table table-striped table-bordered table-hover dataTables-example newTab">
                            <thead>
                            <tr>
                                <th>No#</th>
                                <th>Label Name</th>
                                <th>Label French</th>
                                <th>Label English</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
    <!-- /.content -->

    <script type="text/javascript">
        var list_table_one;
        var list_url = "<?php echo $list_url; ?>";
        $(document).ready(function () {
            var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
            var export_coulmn = "0,1";
            <?php //TODO Change Name for export file here ?>
            list_table_one = $('#list_table_one').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [[1, 'asc']], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": list_url,
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        <?php //TODO Set Column number here for disable sorting ?>
                        "targets": [0, 4 ], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],
                pageLength: 10,
                responsive: true,
                "autoWidth": false

            });
        });
        function reload_table() {
            list_table_one.ajax.reload(null, false); //reload datatable ajax
        }
    </script>
<?php #include_once("_script_crud.php"); ?>
<?php
$list_url = admin_url(CUSTOMER_C);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$info->username ?>  <?= @$info->lastname ?></strong></h3>

                    <div class="rom-md-12">
                        <a href="<?php echo $list_url; ?>">
                            <button type="button" class="btn btn-default pull-right">Back</button>
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <div id="detail_section">

                <table width="600" class="table">
                        <tbody>
                        <tr class="print_hide">
                            <td><b>Name </b></td>
                            <td><?= @$info->username ?>  <?= @$info->lastname ?></td>
                        </tr>

                        <tr class="print_hide">
                            <td><b>Gender</b></td>
                            <td>-</td>
                        </tr>
                        <tr class="print_hide">
                            <td><b>Data of Birth </b></td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td><b>Email </b></td>
                            <td><?= @$info->email_address ?></td>
                        </tr>
                        <tr>
                            <td><b>Phone Number </b></td>
                            <td><?= @$info->phone_number ?></td>
                        </tr>
                        <tr>
                            <td><b>Address</b></td>
                            <td><?= @$info->home_address ?></td>
                        </tr>
                        <tr>
                            <td><b>Postal Code</b></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><b>City</b></td>
                            <td><?= @$city_name->city_name ?></td>
                        </tr>
                        <tr>
                            <td><b>Country</b></td>
                            <td><?= @$country_name->country_name ?></td>
                        </tr>
                        <tr>
                            <td><b>Newsletter</b></td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td><b>Offer</b></td>
                            <td>-</td>
                        </tr>
                        </tbody>
                    </table>
    </div>
                    <?php //echo _header_footer(@$info->email_html); ?>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
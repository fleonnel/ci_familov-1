<!-- Select2 -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<?php
$action_url = '';
$list_url = admin_url(CUSTOMER_C . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    <?php echo form_open('', 'class="form-horizontal" id="crud_form"  name="crud_form" enctype="multipart/form-data"'); ?>
                    <div class="box-body">
                       <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Username<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="username"  name="username" placeholder="Enter Username" value="<?php echo edit_display('username', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Lastname<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="lastname"  name="lastname" placeholder="Enter Lastname" value="<?php echo edit_display('lastname', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">DOB<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="date" class="form-control" id="dob"  name="dob" placeholder="Enter DOB" value="<?php echo edit_display('dob', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Phone Number<span class="text-red">*</span></label>
                                    <div class="col-sm-2">
                                        <input type="number" class="form-control" id="phone_code"  name="phone_code" placeholder="Enter Phone Code" value="<?php echo edit_display('phone_code', @$info); ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" id="sender_phone"  name="sender_phone" placeholder="Enter Phone Number" value="<?php echo edit_display('sender_phone', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Home Address<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="home_address"  name="home_address" placeholder="Enter Your Home Address" value=""><?php echo edit_display('home_address', @$info); ?></textarea>
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Country Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="country_id"
                                                id="country_id">
                                            <option value="">Select Country</option>
                                            <?php echo select_display(@$list_country, 'country_name', 'country_name', edit_display('country_id', @$info, 'country_id')); ?>
                                        </select>
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">City Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="city_id" id="city_id">
                                            <option value="">Select City</option>
                                            <?php echo select_display(@$list_city, 'city_name', 'city_name', edit_display('city_id', @$info, 'city_id')); ?>
                                        </select>
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Postal Code<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="number" class="form-control" id="postal_code"  name="postal_code" placeholder="Enter Postal Code" value="<?php echo edit_display('postal_code', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Status<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="status" id="status" data-placeholder="Choose status" style="display: none;">
                                        <option value="Inactive" <?php if(@$info->status =="Inactive") { echo "selected='selected'"; } ?>>
                                            Inactive
                                        </option>
                                        <option value="Active" <?php if(@$info->status =="Active") { echo "selected='selected'"; } ?>>
                                            Active
                                        </option>
                                    </select>
                                </div>
                        </div>
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <label class="checkbox-inline"><input type="checkbox" name="notification_email" id="notification_email" value="1" <?php echo (@$info->notification_email== 1) ?  "checked='checked'" : "" ;  ?>>Let me know about news and offers by email </label>
                                </div>
                        </div>
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <label class="checkbox-inline"><input type="checkbox" name="notification_text" id="notification_text" value="1" <?php echo (@$info->notification_text== 1) ?  "checked='checked'" : "" ;  ?>>Let me know about news and offers by text   </label>                                
                                </div>
                        </div>
                       
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>

                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success ">Save</button>
                                <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </a>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                username: {
                    required: true
                },
                lastname: {
                    required: true
                },
                home_address: {
                    required: true
                },
                country_id: {
                    required: true
                },
                city_id: {
                    required: true
                },
                dob: {
                    required: true,
                },
                sender_phone: {
                    required: true,
                    number:true
                },
                phone_code: {
                    required: true,
                    number:true
                }, 
                postal_code: {
                    required: true,
                    number:true
                }, 
                status: {
                    required: true
                },
                
            },
            messages: {
                username: {
                    required: 'Please Enter Username.'
                },
                lastname: {
                    required: 'Please Enter Lastname.'
                },
                home_address: {
                    required: 'Please Enter Home Address.'
                },
                dob: {
                    required: 'Please Enter DOB.'
                },
                country_id: {
                    required: 'Please Select a Country.'
                },
                city_id: {
                    required: 'Please Select a City.'
                },
                sender_phone: {
                    required: 'Please Enter Sender Phone Number.',
                    number:'Please Enter a Number Only.'
                },
                phone_code: {
                    required: 'Please Enter Phone Code.',
                    number:'Please Enter a Number Only.'
                },
                postal_code: {
                    required: 'Please Enter Postal Code.',
                    number:'Please Enter a Number Only.'
                },
                status: {
                    required: 'Please Select Status.'
                },
                
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
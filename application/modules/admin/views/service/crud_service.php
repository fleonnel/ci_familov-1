<?php
$action_url = '';
$list_url = admin_url(SERVICE_C . 'lists');
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                    </div>
                    <!-- /.box-header -->
                    <?php _notify(); ?>
                    <?php ##_pre(@$list_department);?>
                    <div class="box-body">
                        <form class="form-horizontal" name="crud_form" id="crud_form" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Department Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="department_id"
                                                id="department_id">
                                            <option value="">Select Department</option>
                                            <?php echo select_display(@$list_department, 'dep_service_id', 'dep_service_name', edit_display('department_id', @$info, 'department_id')); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Service Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="service_name" name="service_name"
                                               placeholder="Enter Service Name"
                                               value="<?php echo edit_display('service_name', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Periodicity<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;"
                                                name="service_periodicity"
                                                id="service_periodicity">
                                            <option value="">Select Periodicity</option>
                                            <?php echo select_display(@$list_periodicity, '', '', edit_display('service_periodicity', @$info, 'service_periodicity')); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Service Description</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="service_description"
                                               name="service_description" placeholder="Enter Service Description"
                                               value="<?php echo edit_display('service_description', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Close Period (In Days)<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="close_period" name="close_period"
                                               placeholder="Enter No Of Days"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               value="<?php echo edit_display('close_period', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Due Period (In Days)<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="due_period" name="due_period"
                                               placeholder="Enter No Of Days"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               value="<?php echo edit_display('due_period', @$info); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label"></label>

                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-success ">Save</button>
                                        <a href="<?php echo $list_url; ?>">
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?=@$level_two?></strong></h3>
                    <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <form class="form-horizontal" name="crud_form" id="crud_form" method="post" >
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Select Admin/Manager<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" style="width: 100%;" name="admin_id"
                                            id="admin_id">
                                        <option value="">Choose a Admin/Manager</option>
                                        <?php echo select_display(@$list_admin_manager, 'admin_id', 'full_name', edit_display('admin_id', @$info, 'admin_id')); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">New Password<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control"  id="new_password"  name="new_password" placeholder="Enter New Password"  value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label">Confirm New Password<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control"  id="new_confirm_password"  name="new_confirm_password" placeholder="Repeat Password" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label"></label>
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success ">Change Password</button>
                                    <a href="<?php echo base_url(ROOT_DIR_ADMIN.DASHBOARD_C); ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                admin_id: {
                    required: true
                },
                new_password: {
                    required: true,
                    minlength: 6
                },
                new_confirm_password: {
                    required: true,
                    equalTo: new_password
                }
            },
            messages: {
                admin_id: {
                    required: 'Please Select Admin/Manager.',
                },
                new_password: {
                    required: 'Please Enter New Password.',
                    minlength: 'Password must be at least 6 characters!'                    
                },
                new_confirm_password: {
                    required: 'Please Enter Confirm Password.',
                    equalTo: "Password Do Not Match."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>
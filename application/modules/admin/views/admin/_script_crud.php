<?php
if ($method == 'lists') { ?>
    <script type="text/javascript">
        var list_table_one;
        var list_url = "<?php echo $list_url; ?>";
        $(document).ready(function () {
            var name_of_file = "<?php echo string_prefix($export_prefix,_str_repalace("-","_",get_current_date()));?>";
            var export_coulmn = "0,1,2,3";
            <?php //TODO Change Name for export file here ?>
            list_table_one = $('#list_table_one').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": list_url,
                    "type": "POST"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        <?php //TODO Set Column number here for disable sorting ?>
                        "targets": [0, 3,4, 5], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],
                pageLength: 10,
                responsive: true,
                "autoWidth": false
                /*dom: '<"html5buttons"B>lTfgitp',
                 buttons: [
                 *//*{ extend: 'copy'},*//*
                 {extend: 'csv', title: name_of_file, exportOptions: {columns: [export_coulmn]}},
                 {extend: 'excel', title: name_of_file, exportOptions: {columns: [export_coulmn]}},
                 {extend: 'pdf', title: name_of_file, exportOptions: {columns: [export_coulmn]}},
                 {
                 extend: 'print',
                 exportOptions: {columns: [export_coulmn]},
                 customize: function (win) {
                 $(win.document.body).addClass('white-bg');
                 $(win.document.body).css('font-size', '10px');
                 $(win.document.body).find('table')
                 .addClass('compact')
                 .css('font-size', 'inherit');
                 }
                 }
                 ]*/
            });
        });
        function reload_table() {
            list_table_one.ajax.reload(null, false); //reload datatable ajax
        }
        function status_update(id, status) {
            var url = "<?php echo $update_status_url ;?>";
            swal({
                    title: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TITLE?>",
                    text: "<?=CONFIRMATION_MESSAGE_FOR_STATUS_TEXT?>",
                    type: "<?=SWEET_ALERT_INFO?>",
                    showCancelButton: true,
                    confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                    confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                    cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {id: id, status: status},
                            dataType: "JSON",
                            success: function (data) {
                                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                                {
                                    reload_table();
                                    var STATUS_TYPE = '';
                                    STATUS_TYPE = '<?=MESSAGE_DELETE_SUCCESS?>';
                                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                                }
                                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                                } else {
                                    toastr.error('<?=MESSAGE_DELETE_ERROR?>', '<?=ERROR?>');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                            }
                        });
                    } else {
                        toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                    }
                });
        }
        function action_delete(id, status) {
            var url = "<?php echo $delete_url ;?>";
            swal({
                    title: "<?=CONFIRMATION_MESSAGE_FOR_DELETE_TITLE?>",
                    text: "<?=CONFIRMATION_MESSAGE_FOR_DELETE_TEXT?>",
                    type: "<?=SWEET_ALERT_INFO?>",
                    showCancelButton: true,
                    confirmButtonColor: "<?=SWEET_ALERT_CONFIRMATION_COLOR?>",
                    confirmButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_YES_TEXT?>",
                    cancelButtonText: "<?=SWEET_ALERT_CONFIRMATION_BUTTON_CANCEL_TEXT?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {id: id, status: status},
                            dataType: "JSON",
                            success: function (data) {
                                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') //if success close modal and reload ajax table
                                {
                                    reload_table();
                                    var STATUS_TYPE = '';
                                    STATUS_TYPE = '<?=MESSAGE_DELETE_SUCCESS?>';
                                    toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                                }
                                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_ERROR?>') {
                                    toastr.error(data.FLASH_MESSAGE, '<?=ERROR?>');
                                } else {
                                    toastr.error('<?=MESSAGE_DELETE_ERROR?>', '<?=ERROR?>');
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
                            }
                        });
                    } else {
                        toastr.info('<?=MESSAGE_LAST_OPERATION_DISCARD?>', '<?=INFO?>');
                    }
                });
        }
    </script>

<?php } else if ($method == 'add' || $method == 'update') { ?>
    <script>
        $(document).ready(function () {
            $("#crud_form").validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    admin_type_id: {
                        required: true
                    },
                    admin_status: {
                        required: true
                    }
                },
                messages: {
                    first_name: {
                        required: 'Please Enter First Name.'
                    },
                    last_name: {
                        required: 'Please Enter Last Name.'
                    },
                    email: {
                        required: 'Please Enter Email Id.',
                        email: 'Please Enter Valid Email Id.'
                    },
                    admin_type_id: {
                        required: 'Please Select Admin Type.'
                    },
                    admin_status: {
                        required: 'Please Select Admin Status.'
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }
            });
        });
    </script>
<?php } ?>
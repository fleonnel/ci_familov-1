<!-- Main content -->
<section class="content">

    <!-- Main row -->
    <div class="row">
        <section class="col-lg-12 connectedSortable">
            <div class="box box-danger">
                <div class="box-header">
                    <i class="fa fa-lock text-red"></i>

                    <h3 class="box-title">Not Access</h3>

                </div>
                <div class="box-body">
                    <?php /*_notify(); */ ?>
                    <div class="alert alert-danger callout callout-danger">
                        <h4><i class="icon fa fa-ban"></i> <?= WARNING ?>!</h4>
                        <?= MESSAGE_ACCESS_DENIED_ERROR_MSG ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<!-- /.content -->

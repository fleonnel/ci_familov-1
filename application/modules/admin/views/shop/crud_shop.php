<!-- Select2 -->
<script src="<?php echo base_url(ASSETS_ADMIN . 'plugins/select2/select2.full.min.js'); ?>"></script>
<?php
$action_url = '';
$list_url = admin_url(SHOP . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                        <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                                class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <?php _notify(); ?>
                <div class="box-body">
                    <?php echo form_open('', 'class="form-horizontal" id="crud_form"  name="crud_form" enctype="multipart/form-data"'); ?>
                    <div class="box-body">
                       <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Shop Name<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="shop_name"  name="shop_name" placeholder="Enter Shop Name" value="<?php echo edit_display('shop_name', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Shop Address<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="shop_address"  name="shop_address" placeholder="Enter Your Shop Address" value=""><?php echo edit_display('shop_address', @$info); ?></textarea>
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Country Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="form-control select2" style="width: 100%;" name="country_id"
                                                id="country_id">
                                            <option value="">Select Country</option>
                                            <?php echo select_display(@$list_country, 'country_id', 'country_name', edit_display('country_id', @$info, 'country_id')); ?>
                                        </select>
                                    </div>
                        </div>
                        <?php //_pre($list_city);?>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">City Name<span
                                            class="text-red">*</span></label>

                                    <div class="col-sm-6">
                                        
                                        <select name="city_id" id="city_id" class="form-control wd_2501 city" required>
                                            <option value="">Select City</option>
                                            <?php echo select_display(@$list_city, 'city_id', 'city_name', edit_display('city_id', @$info, 'city_id')); ?>
                                        </select>
                                    </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Shop Logo</label>
                                <?php  
                                //if (@$info->shop_logo == "") { ?>
                                <div class="col-sm-6">
                                    <input type="file" id="shop_logo" name="shop_logo" class="form-control form-control-flat">
                                </div>
                                <?php //} ?>
                        </div>
                        <div class="form-group">
                                <?php  
                               # _pre($files);
                                         if (@$info->shop_logo != "") { ?>
                                        <p style="margin-left:20%;" id="idrow_<?= @$info->shop_id ?>">
                                            <img src="<?= DIR_SHOP_URL ?><?= @$info->shop_logo; ?>" width="160" height="130" />
                                            <button type="button" onclick="deletlogo(<?= @$info->shop_id; ?>)"
                                               data-toggle="tooltip" title="" value="<?= @$info->shop_id; ?>" class="MultiFile-remove"
                                               data-original-title="remove"> <i class="fa fa-trash fa-o"></i>
                                            </button>
                                        </p>
                                        <?php }  ?>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Shop Banner</label>
                                <?php  
                                //if (@$info->shop_banner == "") { ?>
                                <div class="col-sm-6">
                                     <input type="file" id="shop_banner" name="shop_banner" class="form-control form-control-flat">
                                </div>
                                <?php //} ?>
                        </div>
                        <div class="form-group">
                                <?php  
                               # _pre($files);
                                         if (@$info->shop_banner != "") { ?>
                                        <p style="margin-left:20%;" id="idrow_<?= @$info->shop_id ?>">
                                            <img src="<?= DIR_SHOP_URL ?><?= @$info->shop_banner; ?>" width="500" height="130" />
                                            <button type="button" onclick="deletbanner(<?= @$info->shop_id; ?>)"
                                               data-toggle="tooltip" title="" value="<?= @$info->shop_id; ?>" class="MultiFile-remove"
                                               data-original-title="remove"> <i class="fa fa-trash fa-o"></i>
                                            </button>
                                        </p>
                                        <?php }  ?>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Pick Up From The Shop<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="pickup_from_shop_price"  name="pickup_from_shop_price" placeholder="Enter price without currency sign" value="<?php echo edit_display('pickup_from_shop_price', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                    <label for="inputlbl" class="col-sm-2 control-label">Home Delivery Price<span class="text-red">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="home_delivery_price"  name="home_delivery_price" placeholder="Enter price without currency sign" value="<?php echo edit_display('home_delivery_price', @$info); ?>">
                                    </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Home Delivery Status<span class="text-red">*</span></label>
                                <div class="col-sm-6">
                                    <select class="form-control select2" name="home_delivery_status" id="home_delivery_status" data-placeholder="Choose home delivery status" style="display: none;">
                                        <option value="Inactivated" <?php if(@$info->home_delivery_status=="Inactivated") { echo "selected='selected'"; } ?>>
                                            Inactivated
                                        </option>
                                        <option value="Activated" <?php if(@$info->home_delivery_status=="Activated") { echo "selected='selected'"; } ?>>
                                            Activated
                                        </option>
                                    </select>
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="inputlbl" class="col-sm-2 control-label"></label>

                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success ">Save</button>
                                <a href="<?php echo $list_url; ?>">
                                    <button type="button" class="btn btn-default">Cancel</button>
                                </a>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php include_once("_script_crud.php"); ?>
<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                shop_name: {
                    required: true
                },
                shop_address: {
                    required: true
                },
                country_id: {
                    required: true
                },
                city_id: {
                    required: true
                },
                // shop_logo: {
                //     required: true
                // },
                // shop_banner: {
                //     required: true
                // },
                pickup_from_shop_price: {
                    required: true,
                    number:true
                },
                home_delivery_price: {
                    required: true,
                    number:true
                },
                home_delivery_status: {
                    required: true
                },
                
            },
            messages: {
                shop_name: {
                    required: 'Please Enter Shop Name.'
                },
                shop_address: {
                    required: 'Please Enter Shop Address.'
                },
                country_id: {
                    required: 'Please Select a Country.'
                },
                city_id: {
                    required: 'Please Select a City.'
                },
                // shop_logo: {
                //     required: 'Please Choose a Shop Logo.'
                // },
                // shop_banner: {
                //     required: 'Please Choose a Shop Banner.'
                // },
                pickup_from_shop_price: {
                    required: 'Please Enter Pickup From Shop Price.',
                    number:'Please Enter a Number Only.'
                },
                home_delivery_price: {
                    required: 'Please Enter Home Delivery Price.',
                    number:'Please Enter a Number Only.'
                },
                home_delivery_status: {
                    required: 'Please Select Home Delivery Status.'
                },
                
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
<script type="text/javascript">
$("#country_id").change(function (e) {
        var country_id = $(this).val();
        $.ajax({
            
            url: '<?php echo base_url(ROOT_DIR_ADMIN.SHOP."get_city_list") . '/' ;?>',
            type: "POST",
            data: {country_id: country_id},
            dataType: "JSON",
            success: function (data) {
                $('#city_id').html(data.option_html);
                if (data.FLASH_STATUS == '<?=FLASH_STATUS_SUCCESS?>') {
                    //toastr.success(data.FLASH_MESSAGE, '<?=SUCCESS?>');
                }
                else if (data.FLASH_STATUS == '<?=FLASH_STATUS_INFO?>') {
                    toastr.info(data.FLASH_MESSAGE, '<?=INFO?>');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error('<?=DATA_TABLE_ERROR?>', '<?=ERROR?>');
            }
        });
    });
</script>
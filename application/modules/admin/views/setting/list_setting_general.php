<?php
$action_url = '';
$list_url = admin_url(SETTING_C . 'lists');
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><strong><?= @$level_two ?></strong></h3>
                    <span class="pull-right text-muted">Mandatory information is marked with an asterisk <span
                            class="text-red">*</span></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php _notify(); ?>
                    <form class="form-horizontal" name="crud_form" id="crud_form" method="post">
                        <div class="box-body">
                            <?php foreach (@$result as $setting) {?>

                                <div class="form-group">
                                    <label for="inputlbl"
                                           class="col-sm-2 control-label"><?php echo edit_display('key_title', $setting); ?>
                                        <span
                                        class="text-red">*</span></label>

                                <div class="col-sm-6">
                                    <input type="text" class="form-control"
                                           id="<?php echo edit_display('keys', $setting); ?>"
                                           name="setting_array[<?php echo edit_display('keys', $setting); ?>]"
                                           placeholder="Enter <?php echo edit_display('value', $setting); ?>"
                                           value="<?php echo edit_display('value', $setting); ?>">
                                </div>
                                </div>
                            <?php } ?>
                            <!--<div class="box-header">
                                <h3 class="box-title"><strong><? /*= @$level_three */ ?></strong></h3>
                            </div>-->
                            <?php foreach (@$sms as $key => $setting) {
                                if ($setting->keys == 'sms_sender_id') {
                                    ?>
                                    <div class="form-group">
                                        <label for="inputlbl"
                                               class="col-sm-2 control-label"><?php echo edit_display('key_title', $setting); ?>
                                            <span
                                                class="text-red">*</span></label>
                                        <span class="text-muted">Notes : Please add a comma(,) separator value </span>

                                        <div class="col-sm-6">
                                            <input type="text" class="form-control"
                                                   id="<?php echo edit_display('keys', $setting); ?>"
                                                   name="setting_array[<?php echo edit_display('keys', $setting); ?>]"
                                                   placeholder="Enter <?php echo edit_display('value', $setting); ?>"
                                                   value="<?php echo edit_display('value', $setting); ?>">

                                        </div>
                                    </div>
                                <?php }
                                if ($setting->keys == 'client_email_notification') {
                                    ?>
                                    <div class="form-group">
                                        <label for="inputlbl" class="col-sm-2 control-label">Client Email
                                            Notification<span class="text-red">*</span></label>

                                        <div class="col-sm-6">
                                            <select class="form-control select2" style="width: 100%;"
                                                    name="setting_array[<?php echo edit_display('keys', $setting); ?>]"
                                                    id="<?php echo edit_display('keys', $setting); ?>">
                                                <option
                                                    value="Yes"  <?php echo (edit_display('value', @$setting) == 'Yes') ? "selected" : '' ?>>
                                                    Yes
                                                </option>
                                                <option
                                                    value="No"  <?php echo (edit_display('value', @$setting) == 'No') ? "selected" : '' ?>>
                                                    No
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputlbl" class="col-sm-2 control-label"></label>

                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-success ">Save</button>
                                    <a href="<?php echo $list_url; ?>">
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $("#crud_form").validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
            },
            ignore: "",
            rules: {
                admin_title: {
                    required: true
                },

            },
            messages: {
                admin_title: {
                    required: 'Please Enter URL status Name.'
                },
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (frmadd) {
                successHandler1.show();
                errorHandler1.hide();
            }
        });
    });
</script>
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Currency extends Admin_Controller
{
    var $table = 'currency';
    var $primary_key = 'iCurrencyId';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = CURRENCY; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("currency_model");
    }

    public function index()
    {
        redirect(admin_url(CURRENCY . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Currency List';
        $this->data['level_one'] = 'Currency Management';
        $this->data['level_two'] = 'Currency List';
        $this->breadcrumbs->push('Currency List', 'admin/currency/lists');
        $this->render($this->dir . LIST_CURRENCY); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->currency_model->get_list();
        #_pre($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->iCurrencyId;
            $update_url = admin_url(CURRENCY . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->vName));
            $row[] = ucfirst(trim($list_value->vCode));
            $row[] = ucfirst(trim($list_value->vLangCode));
            $row[] = ucfirst(trim($list_value->vLanguage));
            $row[] = ucfirst(trim($list_value->vSymbol));
            $row[] = ucfirst(trim($list_value->rate));
            //$row[] = (count($service_name_list)) ? $service_name_list->service_name_list : '-';
            $row[] = edit_button($update_url);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->currency_model->count_all(),
            "recordsFiltered" => $this->currency_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(CURRENCY . 'lists'); #TODO : Change Controller Name
        $level_one = 'Currency Management';
        if ($id == '') {
            $title = 'Currency Add Section';
            $error_url = admin_url(CURRENCY . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Currency Update Section';
            $error_url = admin_url(CURRENCY . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('rate', 'Rate', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                   
                } else {
                    $crud_data = array(
                        'rate' => $this->db->escape_str(trim($this->input->post('rate'))),
                    );
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
             if ($id != "" && is_numeric($id)) {
                 $this->data['info'] = $this->currency_model->get_info_by_id($id);
             }
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(CURRENCY . 'lists'));
        }
        $this->breadcrumbs->push('Currency Management', 'admin/currency/lists');
        $this->breadcrumbs->push('Update', 'admin/currency/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_CURRENCY);
    }
}



<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends Admin_Controller
{
    var $table = 'products';
    var $primary_key = 'product_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = PRODUCT; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("shop_model");
        $this->load->model("product_model");
        $this->load->model("category_model");
        $this->load->library('upload');
    }

    public function index()
    {
        redirect(admin_url(PRODUCT . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Product List';
        $this->data['level_one'] = 'Product Management';
        $this->data['level_two'] = 'Product List';
        $this->breadcrumbs->push('Product List', 'admin/product/lists');
        $this->render($this->dir . LIST_PRODUCT); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
            $list = $this->product_model->get_list();
        }
        else
        {
            $shop_id = $this->admin_model->get_info_admin_by_id($this->admin_id)->shop_id;
            $list = $this->product_model->get_list($shop_id);
        }
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->product_id;
            $update_url = admin_url(PRODUCT . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $product_status = '';
            $product_availability = '';
            $row[] = ($list_value->product_image != '') ? '<img src="'.DIR_PRODUCT_URL.$list_value->product_image.'" width="110" height="111" />':'<img src="'.base_url().'/uploads/images/100_no_img.jpg" width="110" height="111" />';
            $row[] = stripslashes(ucfirst(trim($list_value->product_name)));
            $row[] = stripslashes(ucfirst(trim($list_value->category_name)));
            if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
            {
                $row[] = ucfirst(trim($list_value->product_prices));
            }
            $row[] = stripslashes(ucfirst(trim($list_value->shop_name)));

            if ($list_value->product_status == PRODUCT_STATUS_PUBLISHED) {
                $product_status .= action_status($p_key,PRODUCT_STATUS_PUBLISHED,COLOR_S);
            } else {
                $product_status .= action_status($p_key,PRODUCT_STATUS_UNPUBLISHED,COLOR_E);}
            if ($list_value->product_availability == PRODUCT_STATUS_AVAILABLE) {
                $product_availability .= action_available($p_key,PRODUCT_STATUS_AVAILABLE,COLOR_S);
            } else {
                $product_availability .= action_available($p_key,PRODUCT_STATUS_NOT_AVAILABLE,COLOR_E);}
            if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
            {
                #$row[] = '<span id="p_'.$p_key.'" data-name="'.stripslashes(ucfirst(trim($list_value->shop_name))).'">'.$product_status.'</span>';
                $row[] = $product_status;
            }
            $row[] = '<span id="p_' . $p_key . '" data-name="' . stripslashes(ucfirst(trim($list_value->product_name))) . '">' . $product_availability . '</span>';
            //$row[] = (count($service_name_list)) ? $service_name_list->service_name_list : '-';
            if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
            {
                $row[] = edit_button($update_url). ' ' . delete_button($p_key, DELETE_USER). ' ' .duplicate_button($p_key, DUPLICATE_ENTRY);
            }
            $data[] = $row;
        }
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->product_model->count_all(),
                "recordsFiltered" => $this->product_model->count_filtered(),
                "data" => $data,
            );
        }
        else
        {
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->product_model->count_all($shop_id),
                "recordsFiltered" => $this->product_model->count_filtered($shop_id),
                "data" => $data,
            );
        }
        //output to json format
        echo json_encode($output);
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('Product Management', 'admin/product/lists');
        $this->breadcrumbs->push('Add', 'admin/product/add');
        $this->render($this->dir . CRUD_PRODUCT); #TODO : Change View File Name
    }
    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(PRODUCT . 'lists'); #TODO : Change Controller Name
        $level_one = 'Product Management';
        if ($id == '') {
            $title = 'Product Add Section';
            $error_url = admin_url(PRODUCT . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Product Update Section';
            $error_url = admin_url(PRODUCT . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {

            $this->form_validation->set_rules('product_name', 'Product Name', 'required|trim');
            $this->form_validation->set_rules('category_id', 'Category Name', 'required|trim');
            $this->form_validation->set_rules('product_short_desc', 'Product Short Description', 'required|trim');
            $this->form_validation->set_rules('product_desc', 'Product Description', 'required|trim');
            $this->form_validation->set_rules('product_prices', 'Product Price', 'required|trim');
            $this->form_validation->set_rules('special_product_prices', 'Special Product Price', 'required|trim');
            $this->form_validation->set_rules('shop_id', 'Shop Name', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                    if (!empty($_FILES)) {
                        $files = $_FILES['product_image'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_PRODUCT_PATH;
                        $img_response = image_upload($files, 'product_image', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                    } else {
                        $image_name_actual = NULL;
                    }
                    //$this->product_model->create_files($files_url['file_name'],$insert_id);
                    $crud_data = array(
                        'product_name' => $this->db->escape_str(trim($this->input->post('product_name'))),
                        'category_id' => $this->db->escape_str(trim($this->input->post('category_id'))),
                        'product_short_desc' => $this->db->escape_str(trim($this->input->post('product_short_desc'))),
                        'product_desc' => $this->db->escape_str(trim($this->input->post('product_desc'))),
                        'product_prices' => $this->db->escape_str(trim($this->input->post('product_prices'))),
                        'special_product_prices' => $this->db->escape_str(trim($this->input->post('special_product_prices'))),
                        'shop_id' => $this->db->escape_str(trim($this->input->post('shop_id'))),
                        'product_image' => $image_name_actual,
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $crud_data = array(
                        'product_name' => $this->db->escape_str(trim($this->input->post('product_name'))),
                        'category_id' => $this->db->escape_str(trim($this->input->post('category_id'))),
                        'product_short_desc' => $this->db->escape_str(trim($this->input->post('product_short_desc'))),
                        'product_desc' => $this->db->escape_str(trim($this->input->post('product_desc'))),
                        'product_prices' => $this->db->escape_str(trim($this->input->post('product_prices'))),
                        'special_product_prices' => $this->db->escape_str(trim($this->input->post('special_product_prices'))),
                        'shop_id' => $this->db->escape_str(trim($this->input->post('shop_id'))),
                    );
                    if ($_FILES['product_image']['name'] !='') {
                        $files = $_FILES['product_image'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_PRODUCT_PATH;
                        $img_response = image_upload($files, 'product_image', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                        $crud_data['product_image'] = $image_name_actual;
                        $data['data']=$this->product_model->getimagedata($id);
                        if($data['data']->product_id != ''){
                            if ($data['data']->product_image != '') {
                                if (file_exists(DIR_PRODUCT_PATH.$data['data']->product_image)) {
                                    unlink(DIR_PRODUCT_PATH.$data['data']->product_image);
                                }
                            }
                            //$this->product_model->update_files($files_url['file_name'],$id);
                        }
                    } else {
                        #$image_name_actual = NULL;
                    }
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
            if ($id != "" && is_numeric($id)) {
                $this->data['info'] = $this->product_model->get_info_by_id($id);
            }
        }
        $this->data['list_shop'] = $this->shop_model->get_shop_list();
        $this->data['list_category'] = $this->category_model->get_category_list();
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(PRODUCT . 'lists'));
        }
        if($this->roles_id == SUPER_ADMIN_ROLE_ID || $this->roles_id == ADMIN_ROLE_ID)
        {

        }
        else
        {
            redirect(admin_url(PRODUCT . 'lists'));
        }
        $this->breadcrumbs->push('Product Management', 'admin/product/lists');
        $this->breadcrumbs->push('Update', 'admin/product/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_PRODUCT);
    }
    public function action_delete()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['id']= $this->product_model->get_info_by_id($id);
        $order_check = $this->order_model->get_order_by_product($id);
        if(count($order_check) == 0){
            if($data['id'] != ''){
                @unlink(DIR_PRODUCT_PATH.$data['id']->product_image);
            }
            $delete_response=$this->product_model->delete($id);
            if ($delete_response) {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_DELETE_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
        }else{
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_PRODUCT));
            exit;
        }

    }
    public function remove($id)
    {
        $data['id'] = $this->product_model->getdata($id);
        if($data['id'] != ''){
            unlink(DIR_PRODUCT_PATH.$data['id']->product_image);
            return $this->product_model->update_product($id);
        }
        // redirect(admin_url(NEWS_C . 'lists'));
    }
    public function action_duplicate()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['data'] = $this->product_model->get_info_by_id($id);
        $crud_data = array(
            'product_name' => $this->db->escape_str(trim($data['data']->product_name)),
            'category_id' => $this->db->escape_str(trim($data['data']->category_id)),
            'product_short_desc' => $this->db->escape_str(trim($data['data']->product_short_desc)),
            'product_desc' => $this->db->escape_str(trim($data['data']->product_desc)),
            'product_prices' => $this->db->escape_str(trim($data['data']->product_prices)),
            'special_product_prices' => $this->db->escape_str(trim($data['data']->special_product_prices)),
            'shop_id' => $this->db->escape_str(trim($data['data']->shop_id)),
        );
        $insert_response = $this->dbcommon->insert($this->table, $crud_data);
        $insert_id = $this->db->insert_id();
        $Path=DIR_PRODUCT_PATH; //complete image directory path
        $destPath = DIR_PRODUCT_PATH;
        $imageName=$data['data']->product_image;
        if($imageName != ''){
            $Path_image=$Path.$imageName;
            $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
            $dest=$destPath.$insert_id.'_product.'.$ext;
            $image_name=$insert_id.'_product.'.$ext;
            if(copy($Path_image, $dest));
        }else{
            $image_name='';
        }
        $crud_data = array(
            'product_image' => $image_name,
        );
        $where = array($this->primary_key => $insert_id);
        $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
        if ($insert_response) {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_ADD_SUCCESS));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_ADD_ERROR));
            exit;
        }
    }
    public function change_status()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $status = $this->db->escape_str(trim($this->input->post('label')));
        $change_response=$this->product_model->change_status($id,$status);

        //$delete_response = $this->dbcommon->delete($id);
        if ($change_response) {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
            exit;
        }
    }
    public function change_availability()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $status = $this->db->escape_str(trim($this->input->post('label')));
        $change_response=$this->product_model->available_status($id,$status);

        //$delete_response = $this->dbcommon->delete($id);
        if ($change_response) {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
            exit;
        }
    }
}
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class City extends Admin_Controller
{
    var $table = 'city';
    var $primary_key = 'city_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = CITY; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("city_model");
         $this->load->model("country_model");
    }

    public function index()
    {
        redirect(admin_url(CITY . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'City List';
        $this->data['level_one'] = 'City Management';
        $this->data['level_two'] = 'City List';
        $this->breadcrumbs->push('City List', 'admin/city/lists');
        $this->render($this->dir . LIST_CITY); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->city_model->get_list();
        #_pre($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->city_id;
            $update_url = admin_url(CITY . "update") . '/' . $p_key; #TODO : Change Controller Name
            #@$country_name_list = $this->country_model->get_country_name_list($list_value->country_id);
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->city_name));
            $row[] = ($list_value->country_name != '') ? $list_value->country_name : '-';
            $row[] = edit_button($update_url). ' ' . delete_button($p_key, DELETE_USER);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->city_model->count_all(),
            "recordsFiltered" => $this->city_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('City Management', 'admin/city/lists');
        $this->breadcrumbs->push('Add', 'admin/city/add');
        $this->render($this->dir . CRUD_CITY); #TODO : Change View File Name
    }

    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(CITY . 'lists'); #TODO : Change Controller Name
        $level_one = 'City Management';
        if ($id == '') {
            $title = 'City Add Section';
            $error_url = admin_url(CITY . "add"); #TODO : Change Controller Name
        } else {
            $title = 'City Update Section';
            $error_url = admin_url(CITY . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('city_name', 'City Name', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                    $crud_data = array(
                        'city_name' => $this->db->escape_str(trim($this->input->post('city_name'))),
                        'country_id' => $this->db->escape_str(trim($this->input->post('country_id'))),
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $crud_data = array(
                        'city_name' => $this->db->escape_str(trim($this->input->post('city_name'))),
                        'country_id' => $this->db->escape_str(trim($this->input->post('country_id'))),
                    );
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
             if ($id != "" && is_numeric($id)) {
                 $this->data['info'] = $this->city_model->get_info_by_id($id);
             }
        }
        $this->data['list_country'] = $this->country_model->get_country_list();
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(CITY . 'lists'));
        }
        $this->breadcrumbs->push('City Management', 'admin/city/lists');
        $this->breadcrumbs->push('Update', 'admin/city/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_CITY);
    }
    public function action_delete()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $delete_response=$this->city_model->delete($id);
            //$delete_response = $this->dbcommon->delete($id);
            if ($delete_response) {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_DELETE_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
    }

}



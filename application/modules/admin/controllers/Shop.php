<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop extends Admin_Controller
{
    var $table = 'shops';
    var $primary_key = 'shop_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = SHOP; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("shop_model");
        $this->load->model("country_model");
        $this->load->model("city_model");
        $this->load->library('upload');
        $this->load->model("product_model");
        $this->load->model("order_model");
        $this->load->helper('string');

        /*$order_check = $this->order_model->get_order_by_shop(2);
        _pre($order_check);
        _e();*/

        /*$product_list = $this->product_model->product_list_shop_wise(43);

        $Path = DIR_PRODUCT_PATH;
        $destPath = DIR_PRODUCT_PATH;

        foreach($product_list as $k=>$v) {
            $image_name = $v->product_image;
            $final_img = '';
            if($image_name != ''){
                $Path_image = $Path.$image_name;
                $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
                $image_name_new = PREFIX_ATTACHMENT . time() .".".$ext ;
                $image_source = $destPath.$image_name_new;

                if(copy($Path_image, $image_source))
                {
                    $final_img = $image_name_new;
                }
            }
            $crud_data = array(
                'shop_id' => $v->shop_id,
                'category_id' => $v->category_id,
                'customer_id' => $v->customer_id,
                'product_name' => $v->product_name,
                'product_short_desc' => $v->product_short_desc,
                'product_desc' => $v->product_desc,
                'product_image' => $final_img,
                'product_price_currency_id' => $v->product_price_currency_id,
                'product_prices' => $v->product_prices,
                'special_product_prices' => $v->special_product_prices,
                'product_status' => $v->product_status,
                'product_availability' => $v->product_availability,
            );
            _pre($crud_data,0);
        }

        _pre($product_list);*/
    }

    public function index()
    {
        redirect(admin_url(SHOP . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Shop List';
        $this->data['level_one'] = 'Shop Management';
        $this->data['level_two'] = 'Shop List';
        $this->breadcrumbs->push('Shop List', 'admin/shop/lists');
        $this->render($this->dir . LIST_SHOP); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->shop_model->get_list();
        #_pre($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->shop_id;
            $update_url = admin_url(SHOP . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $shop_status = '';
            $row[] = '<img src="'.DIR_SHOP_URL.$list_value->shop_logo.'" width="100" height="60" style="margin-bottom: 10px;" /></br><img src="'.DIR_SHOP_URL.$list_value->shop_banner.'"" width="100" height="27" />';
            $row[] = stripslashes(ucfirst(trim($list_value->shop_name)));
            $row[] = ucfirst(trim($list_value->country_name));
            $row[] = ucfirst(trim($list_value->city_name));
            $row[] = stripslashes(ucfirst(trim($list_value->shop_address)));
            $row[] = ucfirst(trim($list_value->home_delivery_price));
            if ($list_value->home_delivery_status == 'Activated') {
                $home_delivery_status = status_tag(ucfirst(trim($list_value->home_delivery_status)), COLOR_S);
            } else {
                $home_delivery_status = status_tag(ucfirst(trim($list_value->home_delivery_status)), COLOR_E);}
            $row[] = $home_delivery_status;
            if ($list_value->shop_status == PRODUCT_STATUS_PUBLISHED) {
                $shop_status .= action_status($p_key,PRODUCT_STATUS_PUBLISHED,COLOR_S);
            } else {
                $shop_status .= action_status($p_key,PRODUCT_STATUS_UNPUBLISHED,COLOR_E);}
            $row[] = $shop_status;
            //$row[] = (count($service_name_list)) ? $service_name_list->service_name_list : '-';
            $row[] = edit_button($update_url). ' ' . delete_button($p_key, DELETE_USER).''.duplicate_button($p_key, DUPLICATE_ENTRY);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->shop_model->count_all(),
            "recordsFiltered" => $this->shop_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('Shop Management', 'admin/shop/lists');
        $this->breadcrumbs->push('Add', 'admin/shop/add');
        $this->render($this->dir . CRUD_SHOP); #TODO : Change View File Name
    }
    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(SHOP . 'lists'); #TODO : Change Controller Name
        $level_one = 'Shop Management';
        if ($id == '') {
            $title = 'Shop Add Section';
            $error_url = admin_url(SHOP . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Shop Update Section';
            $error_url = admin_url(SHOP . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {

            $this->form_validation->set_rules('shop_name', 'Shop Name', 'required|trim');
            $this->form_validation->set_rules('shop_address', 'Shop Address', 'required|trim');
            $this->form_validation->set_rules('city_id', 'City Name', 'required|trim');
            $this->form_validation->set_rules('country_id', 'Country Name', 'required|trim');
            $this->form_validation->set_rules('pickup_from_shop_price', 'Pickup From Shop Price', 'required|trim');
            $this->form_validation->set_rules('home_delivery_price', 'Home Delivery Price', 'required|trim');
            $this->form_validation->set_rules('home_delivery_status', 'Home Delivery Status', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                    if (!empty($_FILES)) {
                        $files = $_FILES['shop_logo'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_SHOP_PATH;
                        $img_response = image_upload($files, 'shop_logo', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                    } else {
                        $image_name_actual = NULL;
                    }
                    if (!empty($_FILES)) {
                        $files = $_FILES['shop_banner'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_SHOP_PATH;
                        $img_response = image_upload($files, 'shop_banner', $path, $new_file_name);
                        $image_name_actual_banner = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual_banner = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                    } else {
                        $image_name_actual_banner = NULL;
                    }
                    $crud_data = array(
                        'shop_name' => $this->db->escape_str(trim($this->input->post('shop_name'))),
                        'shop_address' => $this->db->escape_str(trim($this->input->post('shop_address'))),
                        'city_id' => $this->db->escape_str(trim($this->input->post('city_id'))),
                        'country_id' => $this->db->escape_str(trim($this->input->post('country_id'))),
                        'pickup_from_shop_price' => $this->db->escape_str(trim($this->input->post('pickup_from_shop_price'))),
                        'home_delivery_price' => $this->db->escape_str(trim($this->input->post('home_delivery_price'))),
                        'home_delivery_status' => $this->db->escape_str(trim($this->input->post('home_delivery_status'))),
                        'shop_logo' => $image_name_actual,
                        'shop_banner' => $image_name_actual_banner,
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $crud_data = array(
                        'shop_name' => $this->db->escape_str(trim($this->input->post('shop_name'))),
                        'shop_address' => $this->db->escape_str(trim($this->input->post('shop_address'))),
                        'city_id' => $this->db->escape_str(trim($this->input->post('city_id'))),
                        'country_id' => $this->db->escape_str(trim($this->input->post('country_id'))),
                        'pickup_from_shop_price' => $this->db->escape_str(trim($this->input->post('pickup_from_shop_price'))),
                        'home_delivery_price' => $this->db->escape_str(trim($this->input->post('home_delivery_price'))),
                        'home_delivery_status' => $this->db->escape_str(trim($this->input->post('home_delivery_status'))),
                    );
                    if ($_FILES['shop_logo']['name'] !='') {
                        $files = $_FILES['shop_logo'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_SHOP_PATH;
                        $img_response = image_upload($files, 'shop_logo', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                        $crud_data['shop_logo'] = $image_name_actual;
                    } else {
                        #$image_name_actual = NULL;
                    }
                    if ($_FILES['shop_banner']['name'] !='') {
                        $files = $_FILES['shop_banner'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_SHOP_PATH;
                        $img_response = image_upload($files, 'shop_banner', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                        $crud_data['shop_banner'] = $image_name_actual;
                        $data['data']=$this->shop_model->getimagedata($id);
                        if ($data['data']->shop_banner != '') {
                            if (file_exists(DIR_SHOP_PATH . $data['data']->shop_banner)) {
                                unlink(DIR_SHOP_PATH  . $data['data']->shop_banner);
                            }
                        }
                        if ($data['data']->shop_logo != null) {
                            if (file_exists(DIR_SHOP_PATH . $data['data']->shop_logo)) {
                                unlink(DIR_SHOP_PATH  . $data['data']->shop_logo);
                            }
                        }
                    } else {
                        #$image_name_actual = NULL;
                    }
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);


                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
            if ($id != "" && is_numeric($id)) {
                $this->data['info'] = $this->shop_model->get_info_by_id($id);
            }
        }
        $this->data['list_country'] = $this->country_model->get_country_list();
        if ($id != "" && is_numeric($id)) {
            $this->data['list_city'] = $this->city_model->get_city_list_by_country($this->data['info']->country_id);
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(SHOP . 'lists'));
        }
        $this->breadcrumbs->push('Shop Management', 'admin/shop/lists');
        $this->breadcrumbs->push('Update', 'admin/shop/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_SHOP);
    }
    public function action_delete()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['id']= $this->shop_model->get_info_by_id($id);
        $order_check = $this->order_model->get_order_by_shop($id);
        if(count($order_check) == 0){
            if($data['id'] != ''){
                @unlink(DIR_SHOP_PATH.$data['id']->shop_logo);
                @unlink(DIR_SHOP_PATH.$data['id']->shop_banner);
            }
            $delete_response=$this->shop_model->delete($id);
            if ($delete_response) {
                #TODO Delete All the product
                $product_list = $this->product_model->product_list_shop_wise($id);
                if(count($product_list) > 0){
                    foreach($product_list as $pk=>$pv){
                        @unlink(DIR_PRODUCT_PATH.$pv->product_image);
                        @$delete_response=$this->product_model->delete($pv->product_id);
                    }
                }
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_DELETE_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
        }else{
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_SHOP));
            exit;
        }

    }
    public function get_city_list()
    {
        $country_id = $this->db->escape_str(trim($this->input->post('country_id')));
        $query_response = null;
        if ($country_id != '') {
            $query_response = $this->city_model->get_city_list_by_country($country_id);
        }
        #_pre($query_response);
        $select_html = '';
        $select_html .= '<option value="" disabled selected>Select City</option>';

        if (count($query_response) > 0) {
            foreach ($query_response as $k => $v) {
                $select_html .= '<option value="' . $v->city_id . '">' . $v->city_name . '</option>';

            }
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, 'option_html' => $select_html));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_INFO, FLASH_MESSAGE => $this->lang->line('LBL_SEARCH_CITY_LIST')));
        }
    }
    public function removelogo($id)
    {
        $data['id'] = $this->shop_model->getdata($id);
        if($data['id'] != ''){
            unlink(DIR_SHOP_PATH.$data['id']->shop_logo);
            return $this->shop_model->update_logo($id);
        }
        // redirect(admin_url(NEWS_C . 'lists'));
    }
    public function removebanner($id)
    {
        $data['id'] = $this->shop_model->getdata($id);
        if($data['id'] != ''){
            unlink(DIR_SHOP_PATH.$data['id']->shop_banner);
            return $this->shop_model->update_banner($id);
        }
        // redirect(admin_url(NEWS_C . 'lists'));
    }
    public function change_status()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $status = $this->db->escape_str(trim($this->input->post('label')));
        $change_response=$this->shop_model->change_status($id,$status);

        //$delete_response = $this->dbcommon->delete($id);
        if ($change_response) {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
            exit;
        }
    }
    public function action_duplicate()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['data'] = $this->shop_model->get_info_by_id($id);
        $crud_data = array(
            'shop_name' => (trim($data['data']->shop_name)),
            'shop_address' => (trim($data['data']->shop_address)),
            'city_id' => (trim($data['data']->city_id)),
            'country_id' => (trim($data['data']->country_id)),
            'pickup_from_shop_price' => (trim($data['data']->pickup_from_shop_price)),
            'home_delivery_price' => (trim($data['data']->home_delivery_price)),
            'home_delivery_status' => (trim($data['data']->home_delivery_status)),
        );
        $insert_response = $this->dbcommon->insert($this->table, $crud_data);
        $shop_id_new = $insert_id = $this->db->insert_id();
        $Path=DIR_SHOP_PATH; //complete image directory path
        $destPath = DIR_SHOP_PATH;
        $imageName=$data['data']->shop_logo;
        $shop_img_name_new = '';
        if($imageName != ''){
            /*$Path_image =$Path.$imageName;
            $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
            #$dest=$destPath.$insert_id.'_shop.'.$ext;
            $dest=$destPath.$imageName;
            #$image_name=$insert_id.'_shop.'.$ext;
            $image_name = PREFIX_ATTACHMENT . time() .".".$ext ; //$insert_id.'_shop.'.$ext;
            if(copy($Path_image, $dest));*/

            $Path_image = $Path.$imageName;
            $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
            $shop_img_name_new = PREFIX_ATTACHMENT . random_string('calnum',10)  .".".$ext ;
            $image_source = $destPath.$shop_img_name_new;

            if(copy($Path_image, $image_source))
            {
                # $final_img = $shop_name_new;
            }

        }else{
            $image_name='';
        }

        $imageName_Banner = $data['data']->shop_banner;
        $shop_name_new_banner = '';
        if($imageName_Banner != ''){
            /*$Path_image=$Path.$imageName_Banner;
            $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
            #$dest=$destPath.$insert_id.'_shopbanner.'.$ext;
            $dest=$destPath.$imageName_Banner;
            #$image_name_banner=$insert_id.'_shopbanner.'.$ext;
            $image_name_banner = PREFIX_ATTACHMENT . time() .".".$ext ;
            if(copy($Path_image, $dest));*/

            $Path_image_banner = $Path.$imageName_Banner;
            $ext = pathinfo($Path_image_banner, PATHINFO_EXTENSION);
            $shop_name_new_banner = PREFIX_ATTACHMENT . random_string('calnum',10) .".".$ext ;
            $image_source_banner = $destPath.$shop_name_new_banner;

            if(@copy($Path_image_banner, $image_source_banner))
            {
               # $final_img = $shop_name_new;
            }
        }

        $crud_data_img = array(
            'shop_logo' => $shop_img_name_new,
            'shop_banner' => $shop_name_new_banner,
        );

        $where = array($this->primary_key => $insert_id);
        $update_response = $this->dbcommon->update($this->table, $where, $crud_data_img);

        if ($insert_response) {

            $product_list = $this->product_model->product_list_shop_wise($id);

            $Path = DIR_PRODUCT_PATH;
            $destPath = DIR_PRODUCT_PATH;

            foreach($product_list as $k=>$v) {
                $image_name = $v->product_image;
                $final_img = '';
                if($image_name != ''){
                    $Path_image = $Path.$image_name;
                    $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
                    $image_name_new = PREFIX_ATTACHMENT . random_string('calnum',10) .".".$ext ;
                    $image_source_product = $destPath.$image_name_new;

                    if(@copy($Path_image, $image_source_product))
                    {
                        $final_img = $image_name_new;
                    }
                }
                $crud_data = array();
                $crud_data = array(
                    'shop_id' => $shop_id_new,
                    'category_id' => $v->category_id,
                    'customer_id' => $v->customer_id,
                    'product_name' => $v->product_name,
                    'product_short_desc' => $v->product_short_desc,
                    'product_desc' => $v->product_desc,
                    'product_image' => $final_img,
                    'product_price_currency_id' => $v->product_price_currency_id,
                    'product_prices' => $v->product_prices,
                    'special_product_prices' => $v->special_product_prices,
                    'product_status' => $v->product_status,
                    'product_availability' => $v->product_availability,
                );
                #_pre($crud_data,0);
                $insert_response = $this->dbcommon->insert('products', $crud_data);
                $insert_id = $this->db->insert_id();
               # _e(" =>" .$insert_response,0);
            }

            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_ADD_SUCCESS));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_ADD_ERROR));
            exit;
        }
    }
}
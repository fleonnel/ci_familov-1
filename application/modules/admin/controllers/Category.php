<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category extends Admin_Controller
{
    var $table = 'categories';
    var $primary_key = 'category_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = CATEGORY; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("category_model");
        $this->load->library('upload');
    }

    public function index()
    {
        redirect(admin_url(CATEGORY . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Category List';
        $this->data['level_one'] = 'Category Management';
        $this->data['level_two'] = 'Category List';
        $this->breadcrumbs->push('Category List', 'admin/category/lists');
        $this->render($this->dir . LIST_CATEGORY); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->category_model->get_list();
        #_pre($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->category_id;
            $update_url = admin_url(CATEGORY . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $row[] = stripslashes(ucfirst(trim($list_value->category_name)));
            $row[] = ($list_value->category_image != '') ? '<img src="'.DIR_CATEGORY_URL.$list_value->category_image.'" width="100" height="100" />':'<img src="'.base_url().'/uploads/images/100_no_img.jpg" width="100" height="100" />';
            $row[] = edit_button($update_url). ' ' . delete_button($p_key, DELETE_USER);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->category_model->count_all(),
            "recordsFiltered" => $this->category_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('Category Management', 'admin/category/lists');
        $this->breadcrumbs->push('Add', 'admin/category/add');
        $this->render($this->dir . CRUD_CATEGORY); #TODO : Change View File Name
    }

    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(CATEGORY . 'lists'); #TODO : Change Controller Name
        $level_one = 'Category Management';
        if ($id == '') {
            $title = 'Category Add Section';
            $error_url = admin_url(CATEGORY . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Category Update Section';
            $error_url = admin_url(CATEGORY . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('category_name', 'Category Name', 'required|trim');
            if (empty($_FILES['category_image']['name'])) {
                $this->form_validation->set_rules('category_image', 'Category Image', 'required|trim');
            }

            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                $category_name = $this->db->escape_str(trim($this->input->post('category_name')));
                if ($id == '') {
                    if (!empty($_FILES)) {
                        $files = $_FILES['category_image'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_CATEGORY_PATH;
                        $img_response = image_upload($files, 'category_image', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                    } else {
                        $image_name_actual = NULL;
                    }
                    $crud_data = array(
                        'category_name' => $this->db->escape_str(trim($this->input->post('category_name'))),
                        'category_image' => $image_name_actual
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $crud_data = array(
                        'category_name' => $this->db->escape_str(trim($this->input->post('category_name'))),
                        #'category_image' => $image_name_actual
                    );
                    if ($_FILES['category_image']['name'] !='') {
                        $files = $_FILES['category_image'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_CATEGORY_PATH;
                        $img_response = image_upload($files, 'category_image', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                        $crud_data['category_image'] = $image_name_actual;
                        $data['data']=$this->category_model->getimagedata($id);
                                if($data['data']->category_id != ''){
                                    if ($data['data']->category_image != '') {
                                        if (file_exists(DIR_CATEGORY_PATH.$data['data']->category_image)) {
                                            unlink(DIR_CATEGORY_PATH.$data['data']->category_image);
                                        }
                                    }
                                }
                    } else {
                        #$image_name_actual = NULL;
                    }
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
            if ($id != "" && is_numeric($id)) {
                $this->data['info'] = $this->category_model->get_info_by_id($id);
            }
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(CATEGORY . 'lists'));
        }
        $this->breadcrumbs->push('Category Management', 'admin/category/lists');
        $this->breadcrumbs->push('Update', 'admin/category/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_CATEGORY);
    }

    public function action_delete()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['id']= $this->category_model->get_info_by_id($id);
        if($data['id'] != ''){
            unlink(DIR_CATEGORY_PATH . $data['id']->category_image);
        }
        $delete_response=$this->category_model->delete($id);
        //$delete_response = $this->dbcommon->delete($id);
        if ($delete_response) {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_DELETE_SUCCESS));
            exit;
        } else {
            echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
            exit;
        }
    }
    public function remove($id)
    {
        $data['id'] = $this->category_model->getdata($id);
        if($data['id'] != ''){
            unlink(DIR_CATEGORY_URL . $data['id']->category_image);
            return $this->category_model->update_category($id);
        }
        // redirect(admin_url(NEWS_C . 'lists'));
    }

}



<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends Admin_Controller
{
    var $table = 'setting';
    var $primary_key = 'setting_id';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("Setting_model");
    }

    public function index()
    {
        redirect(ROOT_DIR_ADMIN . SETTING_C . 'email_template');
    }

    /**
     * This function is used for redirect on Admin List Page
     * @return void
     */
    public function lists()
    {
        $setting_array=array();
        if (count($this->input->post()) > 0) {
            // echo "<pre>";
            // print_r($this->input->post());
            // echo "</pre>";exit;
            #$this->form_validation->set_rules('admin_title', 'Admin Title', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() != FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                
                foreach($this->input->post('setting_array') as $key=>$value){
                    $data=array();
                    $data['value'] = $value;
                    $update_response = $this->Setting_model->update_setting($key,$data);
                   
                }
                $success_url = admin_url(SETTING_D . 'lists'); #TODO : Change Controller Name
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($success_url);
                    }
                
            }
        } else {
           $this->data['result'] = $this->Setting_model->get_setting(SETTING_TYPE_GENERAL);
           $this->data['email'] = $this->Setting_model->get_setting(SETTING_TYPE_Email);
            $this->data['sms'] = $this->Setting_model->get_setting(SETTING_TYPE_SMS);
           $this->data['page_title'] = 'General Setting List';
           $this->data['level_one'] = 'Setting Management';
           $this->data['level_two'] = 'General Setting List';
            $this->data['level_three'] = 'SMS Setting List';
           $this->breadcrumbs->push('Setting Management',SETTING_D . LIST_SETTING_GENERAL);
           $this->render(SETTING_D . LIST_SETTING_GENERAL);
        }
        
    }
}



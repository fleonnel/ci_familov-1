<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{
    var $table = 'admin';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("customers_model");
        $this->load->model("order_model");
    }

    public function index()
    {
        redirect(ROOT_DIR_ADMIN . DASHBOARD_C . 'home');
    }

    public function home()
    {
        $employee_id = '';
        if ($this->roles_id == SUPER_ADMIN_ROLE_ID) {
            $this->data['total_order'] = $this->order_model->count_all();
            $this->data['total_order_eur'] = $this->order_model->order_total_currency(PAYMENT_EUR);
            $this->data['total_order_dollor'] = $this->order_model->order_total_currency(PAYMENT_DOLLOR);
            $this->data['total_order_delivered'] = $this->order_model->count_order(ORDER_STATUS_DELIVERED);
            $this->data['total_order_wait'] = $this->order_model->count_order(ORDER_STATUS_WAIT);
            $this->data['total_order_in_process'] = $this->order_model->count_order(ORDER_STATUS_IN_PROCESS);
            $this->data['order_total_eur'] = $this->order_model->order_total(PAYMENT_EUR);
            $this->data['order_total_dollor'] = $this->order_model->order_total(PAYMENT_DOLLOR);
            $this->data['total_customer'] = $this->customers_model->count_all();
            $this->data['order'] = $this->order_model->order_data();
            $this->data['order_month_total'] = $this->order_model->order_monthly_total();
            $this->data['cart_value_euro'] = $this->data['order_total_eur'][0]->total_prize / $this->data['total_order_eur'];
            $this->data['cart_value_dollor'] = $this->data['order_total_dollor'][0]->total_prize / $this->data['total_order_dollor'];
            $this->data['tax_dollor'] = $this->order_model->tax_billing(PAYMENT_DOLLOR);
            $this->data['tax_euro'] = $this->order_model->tax_billing(PAYMENT_EUR);
            $this->data['total_order_billing_dollor'] = $this->order_model->order_total_billing(PAYMENT_DOLLOR);
            $this->data['total_order_billing_euro'] = $this->order_model->order_total_billing(PAYMENT_EUR);
            $this->data['net_profit_euro'] = $this->data['order_total_eur'][0]->total_prize - ($this->data['total_order_billing_euro'][0]->order_billing_price + $this->data['tax_euro'][0]->service_tax);
            $this->data['net_profit_dollor'] = $this->data['order_total_dollor'][0]->total_prize - ($this->data['total_order_billing_dollor'][0]->order_billing_price + $this->data['tax_dollor'][0]->service_tax);
            //_pre($this->data['total_order_eur']);
           // $email_string = implode(",",$this->data['order_month_total']);
        } else {
            $employee_id = $this->admin_id;
            $customer_data = $this->admin_model->get_info_admin_by_id($employee_id);
            $this->data['customer_data'] = $customer_data;
            $total_order_in_process = 0;
            $total_order_buy = 0;
            $order = array();
            if(count($customer_data) > 0){
                $shop_id = $customer_data->shop_id;
                if($shop_id != "")
                {
                    $this->data['total_order'] = $this->order_model->count_all($shop_id);
                    $this->data['total_order_delivered'] = $this->order_model->count_order(ORDER_STATUS_DELIVERED,$shop_id);
                    
                    $this->data['total_order_wait'] = $this->order_model->count_order(ORDER_STATUS_WAIT,$shop_id);
                    
                    $total_order_in_process = $this->order_model->count_order(ORDER_STATUS_IN_PROCESS,$shop_id);
                    $total_order_buy = $this->order_model->count_order(ORDER_STATUS_BUY,$shop_id);
                    
                    $this->data['order_total_eur'] = $this->order_model->order_total(PAYMENT_EUR,$shop_id);
                    $this->data['order_total_dollor'] = $this->order_model->order_total(PAYMENT_DOLLOR,$shop_id);
                    $this->data['total_customer'] = $this->customers_model->count_all();
                    $order = $this->order_model->order_data($shop_id, $this->roles_id);
                    $this->data['order_month_total'] = $this->order_model->order_monthly_total($shop_id);
                    if($this->data['total_order'] != 0)
                        $this->data['cart_value_euro'] = $this->data['order_total_eur'][0]->total_prize / $this->data['total_order'];
                    if($this->data['total_order'] != 0)
                        $this->data['cart_value_dollor'] = $this->data['order_total_dollor'][0]->total_prize / $this->data['total_order'];
                }            
            }            
            $this->data['total_order_in_process'] = $total_order_in_process;
            $this->data['total_order_buy'] = $total_order_buy;
            $this->data['order'] = $order;
        }
    

        $this->data['page_title'] = 'Dashboard';
        $this->data['level_one'] = 'Dashboard';
        $this->breadcrumbs->push('Dashboard','admin/dashboard/home');
        $this->render(HOME_D . 'dashboard');
    }

    /**
     * FOr ACL Page View
     */

    public function access_denied()
    {
        $this->data['page_title'] = 'Dashboard : Access Denied';
        $this->data['level_one'] = 'Access Denied';
        $this->breadcrumbs->push('Access Denied', 'admin/dashboard/home');
        $this->render(HOME_D . ACCESS_DENIED_V);
    }
}



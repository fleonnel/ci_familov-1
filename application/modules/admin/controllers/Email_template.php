<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_template extends Admin_Controller
{
    var $table = 'email_template';
    var $primary_key = 'email_id';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("email_template_model");
    }

    public function index()
    {
        redirect(admin_url(EMAIL_TEMPLATE_C . 'lists'));
    }

    /**
     * This function is used for redirect on Admin List Page
     * @return void
     */
    public function lists()
    {
        /*$mail_content_data = new stdClass();
        $mail_content_data->var_greeting_name = 'Vix';
        $mail_content_data->var_email = 'parmarvikrantr@gmail.com';
        $mail_content_data->var_user_name = 'vixsmart';
        $mail_content_data->var_password = '12345' ;
        $mail_data = email_template('ADMIN_FORGOT_PASSWORD',$mail_content_data);
        $this->load->library('mail');
        $this->mail->setTo('parmarvikrantr@gmail.com', 'Vikrant P.');
        $this->mail->setSubject($mail_data->email_subject);
        $this->mail->setBody(_header_footer($mail_data->email_html));
        $status = $this->mail->send();
        _pre($status);*/
        $this->data['page_title'] = 'Email Template List';
        $this->data['level_one'] = 'Email Template Management';
        $this->data['level_two'] = 'Email Template List';
        $this->breadcrumbs->push('Email Template','admin/email_template/lists');
        $this->render(EMAIL_TEMPLATE_D . LIST_EMAIL_TEMPLATE);
    }

    /**
     * This function is fetch Email Template Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key, set $list_admin_status as per table
        $list = $this->email_template_model->get_list();
        $data = array();
        $view_base_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "view_profile");
        $update_base_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "add_update");
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key_name = $this->primary_key;
            $p_key = $list_value->$p_key_name;
            $view_url = $view_base_url . '/' . $p_key;
            $update_url = $update_base_url . '/' . $p_key;
            $row = array();
            $row[] = $no;
            $row[] = trim($list_value->vEmailFormatType);
            $row[] = trim($list_value->vEmailFormatTitle_EN);
            $row[] = trim($list_value->vEmailFormatSubject_EN);
            $row[] = trim($list_value->vEmailFormatSubject_FR);
            $row[] = view_button_email($view_url).' '.edit_button($update_url);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->email_template_model->count_all(),
            "recordsFiltered" => $this->email_template_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
     public function add_update($id = '')
    {
        $this->form_validation->set_rules('vEmailFormatTitle_EN', 'Email Title In English', 'required|trim');
        $this->form_validation->set_rules('vEmailFormatType', 'Email kEY', 'required|trim');
        $this->form_validation->set_rules('vEmailFormatSubject_EN', 'Email Subject In English', 'required|trim');
        $this->form_validation->set_rules('tEmailFormatDesc_EN', 'Email Description In English', 'required|trim');
        $this->form_validation->set_rules('vEmailFormatTitle_FR', 'Email Title In French', 'required|trim');
        $this->form_validation->set_rules('vEmailFormatSubject_FR', 'Email Subject In French', 'required|trim');
        $this->form_validation->set_rules('tEmailFormatDesc_FR', 'Email Subject In French', 'required|trim');
        $this->form_validation->set_error_delimiters('', '');
        if ($id) {
            $title = 'Email Template Update Section';
        } else {
            $title = 'Email Template Add Section';
        }
        $this->breadcrumbs->push('Email Template Management', 'admin/email_template/lists');
        $this->breadcrumbs->push('Email Template Add Section', 'admin/email_template/add_update');
        if ($id != '') {
            $this->breadcrumbs->push('Email Template Update Section', 'admin/email_template/add_update');
        }
        if ($this->form_validation->run() === FALSE) {
            if ($id != "" && is_numeric($id)) {
                $this->data['info'] = $this->email_template_model->get_email_template($id);
            }
            $this->data['form_errors'] = _array_to_obj($this->form_validation->error_array());
        } else {
            $content_EN = str_ireplace(array("\r","\n",'\r','\n'),'', $this->input->post('tEmailFormatDesc_EN'));
            $content_FR = str_ireplace(array("\r","\n",'\r','\n'),'', $this->input->post('tEmailFormatDesc_FR'));
            $crud_data = array(
                'vEmailFormatType' => $this->db->escape_str(trim($this->input->post('vEmailFormatType'))),
                'vEmailFormatTitle_EN' => $this->db->escape_str(trim($this->input->post('vEmailFormatTitle_EN'))),
                'vEmailFormatSubject_EN' => $this->db->escape_str(trim($this->input->post('vEmailFormatSubject_EN'))),
                'tEmailFormatDesc_EN' => $content_EN,
                'vEmailFormatTitle_FR' => $this->db->escape_str(trim($this->input->post('vEmailFormatTitle_FR'))),
                'vEmailFormatSubject_FR' => $this->db->escape_str(trim($this->input->post('vEmailFormatSubject_FR'))),
                'tEmailFormatDesc_FR' => $content_FR,
                //'vTags' => $this->db->escape_str(trim($this->input->post('vTags'))),
            );

            $success_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "lists");
            $error_url = base_url(ROOT_DIR_ADMIN . EMAIL_TEMPLATE_C . "add_update/".$id);
            if ($id == '') {
                $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                $insert_id = $this->db->insert_id();
                if ($insert_response) {
                    _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                    redirect($success_url);
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                    redirect($error_url);
                }
            } else {
                $where = array("email_id" => $id);
                $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                if ($update_response) {
                    _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                    redirect($success_url);
                } else {
                    _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                    redirect($error_url);
                }
            }
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = 'Email Template Management';
        $this->data['level_two'] = $title;
        $this->render(EMAIL_TEMPLATE_C . CRUD_EMAIL_TEMPLATE);
    }
    public function view_profile($id)
    {   
        $this->data['info'] = $this->email_template_model->get_email_template_view($id);
        $this->data['level_one'] = 'Email Template Management';
       // $this->data['level_two'] = 'Email Template View';
        $this->breadcrumbs->push('Email Template Management', 'admin/email_template/lists');
        $this->breadcrumbs->push('Email Template View', 'admin/email_template/view_profile');
        $this->render(EMAIL_TEMPLATE_C . VIEW_EMAIL_TEMPLATE);
    }
}

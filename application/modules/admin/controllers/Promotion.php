<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promotion extends Admin_Controller
{
    var $table = 'promocode';
    var $primary_key = 'promo_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = PROMOTION; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("promotion_model");
    }

    public function index()
    {
        redirect(admin_url(PROMOTION . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Promotion List';
        $this->data['level_one'] = 'Promotion Management';
        $this->data['level_two'] = 'Promotion List';
        $this->breadcrumbs->push('Promotion List', 'admin/promotion/lists');
        $this->render($this->dir . LIST_PROMOTION); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->promotion_model->get_list();
        $data = array();
        $no = $_POST['start'];
//         echo "<pre>";
//    print_r($list);
// echo "</pre>";exit;
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->promo_id;
            $update_url = admin_url(PROMOTION . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->vCode));
            $row[] = ucfirst(trim($list_value->vDescription));
            $row[] = ucfirst(trim($list_value->iAmount));
            $row[] = ucfirst(trim($list_value->eStatus));
            $row[] = ucfirst(trim($list_value->start_date));
            $row[] = ucfirst(trim($list_value->end_date));
            //$row[] = ($list_value->eStatus == 'Active')) ? stop_button($p_key,'Inactive') : start_button($p_key,'Active');
            $row[] = ($list_value->eStatus == 'Active') ? edit_button($update_url) .' '. stop_button($p_key,STOP) :  edit_button($update_url) .' '.start_button($p_key,START);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->promotion_model->count_all(),
            "recordsFiltered" => $this->promotion_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('Promotion Management', 'admin/promotion/lists');
        $this->breadcrumbs->push('Add', 'admin/promotion/add');
        $this->render($this->dir . CRUD_PROMOTION); #TODO : Change View File Name
    }
    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(PROMOTION . 'lists'); #TODO : Change Controller Name
        $level_one = 'Promotion Management';
        if ($id == '') {
            $title = 'Promotion Add Section';
            $error_url = admin_url(PROMOTION . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Promotion Update Section';
            $error_url = admin_url(PROMOTION . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {

            $this->form_validation->set_rules('vCode', 'Promo code', 'required|trim');
            $this->form_validation->set_rules('vDescription', 'Code Description', 'required|trim');
            $this->form_validation->set_rules('iAmount', 'Amount', 'required|trim');
            $this->form_validation->set_rules('eStatus', 'Status', 'required|trim');
            $this->form_validation->set_rules('start_date', 'Start Date', 'required|trim');
            $this->form_validation->set_rules('end_date', 'End Date', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                    $crud_data = array(
                        'vCode' => $this->db->escape_str(trim($this->input->post('vCode'))),
                        'vDescription' => $this->db->escape_str(trim($this->input->post('vDescription'))),
                        'iAmount' => $this->db->escape_str(trim($this->input->post('iAmount'))),
                        'eStatus' => $this->db->escape_str(trim($this->input->post('eStatus'))),
                        'start_date' => $this->db->escape_str(trim($this->input->post('start_date'))),
                        'end_date' => $this->db->escape_str(trim($this->input->post('end_date'))),
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $crud_data = array(
                        'vCode' => $this->db->escape_str(trim($this->input->post('vCode'))),
                        'vDescription' => $this->db->escape_str(trim($this->input->post('vDescription'))),
                        'iAmount' => $this->db->escape_str(trim($this->input->post('iAmount'))),
                        'eStatus' => $this->db->escape_str(trim($this->input->post('eStatus'))),
                        'start_date' => $this->db->escape_str(trim($this->input->post('start_date'))),
                        'end_date' => $this->db->escape_str(trim($this->input->post('end_date'))),
                    );
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
             if ($id != "" && is_numeric($id)) {
                 $this->data['info'] = $this->promotion_model->get_info_by_id($id);
             }
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(PROMOTION . 'lists'));
        }
        $this->breadcrumbs->push('Product Management', 'admin/product/lists');
        $this->breadcrumbs->push('Update', 'admin/product/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_PROMOTION);
    }
    public function change_status()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $status = $this->db->escape_str(trim($this->input->post('status')));
        $delete_response=$this->promotion_model->change_status($id,$status);
            //$delete_response = $this->dbcommon->delete($id);
            if ($delete_response) {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_CHANGE_STATUS_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
    }
   
}
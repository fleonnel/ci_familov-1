<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Label extends Admin_Controller
{
    var $table = 'language';
    var $primary_key = 'language_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = LABEL; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("label_model");
    }

    public function index()
    {
        redirect(admin_url(LABEL . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Label List';
        $this->data['level_one'] = 'Label Management';
        $this->data['level_two'] = 'Label List';
        $this->breadcrumbs->push('Label List', 'admin/label/lists');
        $this->render($this->dir . LIST_LABEL); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->label_model->get_list();
        #_pre($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->language_id;
            $update_url = admin_url(LABEL . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $row[] = ucfirst(trim($list_value->vname));
            $row[] = ucfirst(trim($list_value->vname_fr));
            $row[] = ucfirst(trim($list_value->vname_en));
            $row[] = edit_button($update_url);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->label_model->count_all(),
            "recordsFiltered" => $this->label_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('Label Management', 'admin/label/lists');
        $this->breadcrumbs->push('Add', 'admin/label/add');
        $this->render($this->dir . CRUD_LABEL); #TODO : Change View File Name
    }

    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(LABEL . 'lists'); #TODO : Change Controller Name
        $level_one = 'Label Management';
        if ($id == '') {
            $title = 'Label Add Section';
            $error_url = admin_url(LABEL . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Label Update Section';
            $error_url = admin_url(LABEL . "update"); #TODO : Change Controller Name
        }
        if (count($this->input->post()) > 0) {
            $this->form_validation->set_rules('vname', 'Label Name', 'required|trim');
            $this->form_validation->set_rules('vname_fr', 'French Label', 'required|trim');
            $this->form_validation->set_rules('vname_en', 'English Label', 'required|trim');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() === FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                    $crud_data = array(
                        'vname' => $this->db->escape_str(trim($this->input->post('vname'))),
                        'vname_fr' => $this->db->escape_str(trim($this->input->post('vname_fr'))),
                        'vname_en' => $this->db->escape_str(trim($this->input->post('vname_en'))),
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    $crud_data = array(
                        'vname' => $this->db->escape_str(trim($this->input->post('vname'))),
                        'vname_fr' => $this->db->escape_str(trim($this->input->post('vname_fr'))),
                        'vname_en' => $this->db->escape_str(trim($this->input->post('vname_en'))),
                    );
                    $where = array($this->primary_key => $id);
                    $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
                    if ($update_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_UPDATE_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_UPDATE_ERROR);
                        redirect($error_url);
                    }
                }
            }
        } else {
             if ($id != "" && is_numeric($id)) {
                 $this->data['info'] = $this->label_model->get_info_by_id($id);
             }
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }

    /**
     * This function is Add Data.
     * @return void
     */
    public function update($id = '')
    {
        if ($id == '') {
            redirect(admin_url(LABEL . 'lists'));
        }
        $this->breadcrumbs->push('Label Management', 'admin/label/lists');
        $this->breadcrumbs->push('Update', 'admin/label/update');
        $this->add_update($id);
        $this->render($this->dir . CRUD_LABEL);
    }
}



<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banner extends Admin_Controller
{
    var $table = 'banners';
    var $primary_key = 'banner_id';
    var $dir = '';

    public function __construct()
    {
        parent::__construct();
        $this->dir = BANNER; #TODO : Change Directory Name
        $this->load->library('form_validation');
        $this->load->model("banner_model");
         $this->load->library('upload');
    }

    public function index()
    {
        redirect(admin_url(BANNER . 'lists'));
    }

    /**
     * This function is used for redirect on List Page
     * @return void
     */
    public function lists()
    {
        $this->data['page_title'] = 'Banner List';
        $this->data['level_one'] = 'Banner Management';
        $this->data['level_two'] = 'Banner List';
        $this->breadcrumbs->push('Banner List', 'admin/banner/lists');
        $this->render($this->dir . LIST_BANNER); #TODO : Change View File Name
    }

    /**
     * This function is fetch Data and set them into datatable.
     * @return array (json)
     */
    public function data_list()
    {
        //#todo model name change at 3 places, set $p_key as per table
        $list = $this->banner_model->get_list();
        $data = array();
        $no = $_POST['start'];
//         echo "<pre>";
//    print_r($list);
// echo "</pre>";exit;
        foreach ($list as $list_key => $list_value) {
            $no++;
            $p_key = $list_value->banner_id;
            $update_url = admin_url(BANNER . "update") . '/' . $p_key; #TODO : Change Controller Name
            $row = array();
            $row[] = $no;
            $row[] = '<img src="'.DIR_BANNER_URL.$list_value->banner_image.'" width="100" height="27" />';
            $row[] = delete_button($p_key, DELETE_USER). ' ' .duplicate_button($p_key, DUPLICATE_ENTRY);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->banner_model->count_all(),
            "recordsFiltered" => $this->banner_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function add()
    {
        $this->add_update();
        $this->breadcrumbs->push('Banner Management', 'admin/banner/lists');
        $this->breadcrumbs->push('Add', 'admin/banner/add');
        $this->render($this->dir . CRUD_BANNER); #TODO : Change View File Name
    }
    /**
     * This function is Add Update Data.
     * @return void
     */
    private function add_update($id = '')
    {
        $success_url = admin_url(BANNER . 'lists'); #TODO : Change Controller Name
        $level_one = 'Banner Management';
        if ($id == '') {
            $title = 'Banner Add Section';
            $error_url = admin_url(BANNER . "add"); #TODO : Change Controller Name
        } else {
            $title = 'Banner Update Section';
            $error_url = admin_url(BANNER . "update"); #TODO : Change Controller Name
        }
        if (!empty($_FILES)) {
            if ($this->form_validation->run() != FALSE) {
                $form_errors = _array_to_obj($this->form_validation->error_array());
                _set_flashdata(FLASH_STATUS_ERROR, $form_errors, FLASH_HTML);
            } else {
                if ($id == '') {
                    if (!empty($_FILES)) {
                        $files = $_FILES['banner_image'];
                        $files = is_array($files) ? $files : array($files);
                        $new_file_name = PREFIX_ATTACHMENT . time();
                        $path = DIR_BANNER_PATH;
                        $img_response = image_upload($files, 'banner_image', $path, $new_file_name);
                        $image_name_actual = '';
                        if (!$img_response['status']) {
                            $error_msg = "Can not upload Image for " . $img_response['error'] . " ";
                            _set_flashdata(FLASH_STATUS_ERROR, $error_msg, FLASH_HTML);
                            redirect($error_url);
                        } else {
                            $image_name_actual = $img_response['upload_data']['file_name'];
                            $file_path_org = $img_response['upload_data']['file_path'];
                            $file_full_path = $img_response['upload_data']['full_path'];
                        }
                    } else {
                        $image_name_actual = NULL;
                    }
                        $crud_data = array(
                        'banner_image' => $image_name_actual,
                    );
                    $insert_response = $this->dbcommon->insert($this->table, $crud_data);
                    $insert_id = $this->db->insert_id();
                    if ($insert_response) {
                        _set_flashdata(FLASH_STATUS_SUCCESS, MESSAGE_ADD_SUCCESS);
                        redirect($success_url);
                    } else {
                        _set_flashdata(FLASH_STATUS_ERROR, MESSAGE_ADD_ERROR);
                        redirect($error_url);
                    }
                } else {
                    
                }
            }
        } else {
             if ($id != "" && is_numeric($id)) {
                 $this->data['info'] = $this->banner_model->get_info_by_id($id);
             }
        }
        $this->data['page_title'] = $title;
        $this->data['level_one'] = $level_one;
        $this->data['level_two'] = $title;
#        $this->render($this->dir . CRUD_CUSTOMER_CONTACT); #TODO : Change View File Name
    }
    /**
     * This function is Add Data.
     * @return void
     */
    public function action_delete()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['id']= $this->banner_model->get_info_by_id($id);
        if($data['id'] != ''){
                unlink(DIR_BANNER_PATH.$data['id']->banner_image);
        }
        $delete_response=$this->banner_model->delete($id);
            //$delete_response = $this->dbcommon->delete($id);
            if ($delete_response) {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_DELETE_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_DELETE_ERROR));
                exit;
            }
    }
    public function upload_file($file)
    {

        $this->load->library("upload");
        $upload_cfg['upload_path'] = DIR_BANNER_PATH;
        $upload_cfg['encrypt_name'] = TRUE;
        $upload_cfg['allowed_types'] = "gif|jpg|png|jpeg";
        $upload_cfg['width'] = 360;
        $upload_cfg['height'] = 260;

         // Load the Library
         $this->load->library('image_lib',  $upload_cfg);
         
         // resize image
         $this->image_lib->resize();
        /*        $upload_cfg['max_width'] = '1920'; /* max width of the image file */
        /*        $upload_cfg['max_height'] = '1080'; /* max height of the image file */
        /*        $upload_cfg['min_width'] = '300'; /* min width of the image file */
        /*        $upload_cfg['min_height'] = '300'; /* min height of the image file */

        $this->upload->initialize($upload_cfg);

        if ($this->upload->do_upload($file)) {
            $image = $this->upload->data();
            return $image;
        } else {
            $msg = $this->form_validation->field_data['file_to_upload']['error'] = $this->upload->display_errors('', '');
            $this->session->set_flashdata('success_msg', $msg);
            //redirect('files/overview/');
        }
    }
    public function remove($id)
    {
        $data['id'] = $this->banner_model->getdata($id);
        if($data['id'] != ''){
        unlink(DIR_BANNER_URL.$data['id']->banner_image);
        return $this->banner_model->update_banner($id);
         }
    }
    public function action_duplicate()
    {
        $id = $this->db->escape_str(trim($this->input->post('id')));
        $data['data'] = $this->banner_model->get_info_by_id($id);
        $crud_data = array(
                        'banner_image' => '',
                    );
        $insert_response = $this->dbcommon->insert($this->table,$crud_data);
        $insert_id = $this->db->insert_id();
                $Path=DIR_BANNER_PATH; //complete image directory path
                $destPath = DIR_BANNER_PATH;
                $imageName=$data['data']->banner_image;
                $Path_image=$Path.$imageName;
                $ext = pathinfo($Path_image, PATHINFO_EXTENSION);
                $dest=$destPath.$insert_id.'_banner.'.$ext;
                if(copy($Path_image, $dest));
                $crud_data = array(
                        'banner_image' => $insert_id.'_banner.'.$ext,
                    );
            $where = array($this->primary_key => $insert_id);
            $update_response = $this->dbcommon->update($this->table, $where, $crud_data);
            if ($insert_response) {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_SUCCESS, FLASH_MESSAGE => MESSAGE_ADD_SUCCESS));
                exit;
            } else {
                echo json_encode(array(FLASH_STATUS => FLASH_STATUS_ERROR, FLASH_MESSAGE => MESSAGE_ADD_ERROR));
                exit;
            }
    }
}
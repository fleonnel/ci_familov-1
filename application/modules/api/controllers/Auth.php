<?php

class Auth extends API_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->decodeJSONData();
    }

    public function login_post()
    {
        #if (count($this->input->post()) > 0) {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() === FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            $user_name = $this->db->escape_str(trim($this->input->post('email')));
            $password = $this->db->escape_str(md5(trim($this->input->post('password'))));
            $select_response = $this->customer_model->check_login(array('email' => $user_name, 'password' => $password));
            if (!empty($select_response)) {
                if ($select_response->status == STATUS_CUSTOMER_ACTIVE) {
                    unset($select_response->password);
                    $arr['data'] = $select_response;
                    $data = [
                        'status' => TRUE,
                        'message' => $this->lang->line('MSG_LOGIN_SUCCESS'),
                        'data' => $select_response,
                        #'data' => $this->encodeJSONData($arr),
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data = [
                        'status' => FALSE,
                        'message' => $this->lang->line('MSG_LOGIN_STATUS_INACTIVE')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_LOGIN_INVALID')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
        # }
    }

    public function sign_up_post()
    {
        # if (count($this->input->post()) > 0) {
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone_code', 'Phone Code', 'trim|required');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('repeat_password', 'Repeat Password', 'trim|required|matches[password]');
        if ($this->form_validation->run() === FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            $email = $this->db->escape_str(trim($this->input->post('email')));
            $select_response = $this->customer_model->check_email(array('email' => $email));
            if (!empty($select_response)) {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_SIGN_UP_EMAIL_EXIST')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                $first_name = $this->db->escape_str(trim($this->input->post('first_name')));
                $last_name = $this->db->escape_str(trim($this->input->post('last_name')));
                $password = $this->db->escape_str(md5(trim($this->input->post('password'))));
                $new_password = $this->db->escape_str(trim($this->input->post('password')));
                $phone_code = $this->db->escape_str(trim($this->input->post('phone_code')));
                $phone_number = $this->db->escape_str(trim($this->input->post('phone_number')));
                $friend_referral_id = $this->db->escape_str(trim($this->input->post('friend_referral_id')));
                $referral_id = uniqid("referral", true);
                $crud_data = array(
                    'email_address' => $email,
                    'username' => $first_name,
                    'lastname' => $last_name,
                    'password' => $password,
                    'phone_code' => $phone_code,
                    'sender_phone' => $phone_number,
                    'phone_number' => $phone_number,
                    'referral' => $referral_id,
                );
                $insert_response = $this->dbcommon->insert('customers', $crud_data);
                $insert_id = $this->db->insert_id();
                if ($insert_response) {
                    /* friend referral Start */
                    if ($friend_referral_id != null && $friend_referral_id != '') {
                        $crud_referral_data = array(
                            'my_referrel_id' => $referral_id,
                            'friend_referrel_id' => $friend_referral_id,
                            'promo_code' => 'fixed',
                            'discount' => 2,
                            'status' => 'unused'
                        );
                        $insert_referral_response = $this->dbcommon->insert('referrels', $crud_referral_data);
                    }
                    /* friend referral End*/
                    $user_name = trim($first_name);
                    $mail_content_data = new stdClass();
                    $mail_content_data->var_user_name = $user_name;
                    $mail_content_data->var_email = $email;
                    $mail_content_data->var_password = $new_password;
                    $mail_data = email_template('MAIL_SIGN_UP', $mail_content_data); #TODO MAIL
                    if (MAIL_TEST_MODE == MAIL_TEST_MODE_LIVE) {
                        $email = MAIL_TEST_EMAIL;
                    }
                    if (is_object($mail_data)) {
                        $this->load->library('mail');
                        $this->mail->setTo($email, $user_name);
                        $this->mail->setSubject($mail_data->email_subject);
                        $this->mail->setBody(_header_footer($mail_data->email_html));
                        $status = $this->mail->send();
                    }
                    $data = [
                        'status' => TRUE,
                        'message' => $this->lang->line('MSG_SIGN_UP_SUCCESS'),
                        'data' => $select_response,
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data = [
                        'status' => FALSE,
                        'message' => $this->lang->line('MSG_SIGN_UP_FAILED')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            }
        }
    }

    public function change_password_post()
    {
        $this->form_validation->set_rules('customer_id', 'User', 'trim|required');
        $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('repeat_new_password', 'Repeat Password', 'trim|required|min_length[8]|matches[new_password]');
        if ($this->form_validation->run() == FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            $current_password = $this->db->escape_str(md5(trim($this->input->post('current_password'))));
            $new_password = $this->db->escape_str(md5(trim($this->input->post('new_password'))));
            $customer_id = $this->db->escape_str((trim($this->input->post('customer_id'))));
            $select_response = $this->customer_model->check_password(array('password' => $current_password, 'customer_id' => $customer_id));
            if (count($select_response)) {
                $crud_password = array('password' => $new_password);
                $where = array('customer_id' => $customer_id);
                $update_response = $this->dbcommon->update('customers', $where, $crud_password);
                if ($update_response) {
                    $data = [
                        'status' => TRUE,
                        'message' => $this->lang->line('MSG_CP_SUCCESS')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data = [
                        'status' => FALSE,
                        'message' => $this->lang->line('MSG_CP_FAILED')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_CP_INVALID')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    }
    function logout_get()
    {
        $this->decodeJSONData();
        $data['key'] = $this->encodeJSONData();
        $data['message'] = 'Logout Successfully';
        $data['status'] = 1;
        $this->response($data);
    }
}

?>
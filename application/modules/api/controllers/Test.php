<?php

// Test Api Controller
class Test extends API_Controller
{

    function __construct()
    {
        parent::__construct();

    }

    function token_get()
    {
        $data['id'] = $this->get('id');
        $args['data']['id'] = $data['id'];
        $data['key'] = $this->encodeJSONData($args);
        $this->response($data);
    }

    function token_post()
    {
        $this->decodeJSONData();
        $data['id'] = $this->post('email');
        $data['userData'] = $this->getTokenData();
        $data['status'] = true;
        $this->response($data);
    }
}

?>
<?php

class User extends API_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->decodeJSONData();
    }

    public function change_password_post()
    {
        $this->form_validation->set_rules('customer_id', 'User', 'trim|required');
        $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('repeat_new_password', 'Repeat Password', 'trim|required|min_length[8]|matches[new_password]');
        if ($this->form_validation->run() == FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            $current_password = $this->db->escape_str(md5(trim($this->input->post('current_password'))));
            $new_password = $this->db->escape_str(md5(trim($this->input->post('new_password'))));
            $customer_id = $this->db->escape_str((trim($this->input->post('customer_id'))));
            $select_response = $this->customer_model->check_password(array('password' => $current_password, 'customer_id' => $customer_id));
            if (count($select_response)) {
                $crud_password = array('password' => $new_password);
                $where = array('customer_id' => $customer_id);
                $update_response = $this->dbcommon->update('customers', $where, $crud_password);
                if ($update_response) {
                    $data = [
                        'status' => TRUE,
                        'message' => $this->lang->line('MSG_CP_SUCCESS')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    $data = [
                        'status' => FALSE,
                        'message' => $this->lang->line('MSG_CP_FAILED')
                    ];
                    $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_CP_INVALID')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    }

    public function profile_update_post()
    {
        $this->form_validation->set_rules('customer_id', 'User', 'trim|required');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('sender_number', 'Phone Number', 'required');
        $this->form_validation->set_rules('phone_code', 'Phone Code', 'required');
        $this->form_validation->set_rules('country_id', 'Country', 'required'); #name Of Country
        $this->form_validation->set_rules('city_id', 'City', 'required');
        $this->form_validation->set_rules('home_address', 'Address', 'required');
        $this->form_validation->set_rules('postal_code', 'Postal Code', 'required');
        if ($this->form_validation->run() == FALSE) {
            $form_errors = _array_to_obj($this->form_validation->error_array());
            $this->set_response([
                'status' => FALSE,
                'message' => $form_errors,
            ], REST_Controller::HTTP_OK);
        } else {
            $customer_id = $this->db->escape_str((trim($this->input->post('customer_id'))));
            $first_name = $this->db->escape_str((trim($this->input->post('first_name'))));
            $last_name = $this->db->escape_str((trim($this->input->post('last_name'))));
            $sender_number = $this->db->escape_str((trim($this->input->post('sender_number'))));
            $home_address = $this->db->escape_str((trim($this->input->post('home_address'))));
            $country_id = $this->db->escape_str((trim($this->input->post('country_id'))));#name Of Country
            $city_id = $this->db->escape_str((trim($this->input->post('city_id'))));
            $country_code = $this->db->escape_str((trim($this->input->post('country_code'))));#code IN, UK
            $postal_code = $this->db->escape_str((trim($this->input->post('postal_code'))));
            $phone_code = $this->db->escape_str((trim($this->input->post('phone_code')))); #Phone Code
            $notification_email = ($this->input->post('notification_email') == 1) ? 1 : 0;
            $notification_text = ($this->input->post('notification_text') == 1) ? 1 : 0;
            $update_data = array(
                'username' => $first_name,
                'lastname' => $last_name,
                'sender_phone' => $sender_number,
                'home_address' => $home_address,
                'country_id' => $country_id,
                'city_id' => $city_id,
                'notification_email' => $notification_email,
                'notification_text' => $notification_text,
                'country_code' => $country_code,
                'postal_code' => $postal_code,
                'phone_code' => $phone_code,
            );
            $where = array('customer_id' => $customer_id);
            $update_response = $this->dbcommon->update('customers', $where, $update_data);
            if ($update_response) {
                $data = [
                    'status' => TRUE,
                    'message' => $this->lang->line('MSG_MA_22')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_MA_23')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }
    }

    public function profile_get()
    {
        $customer_id = $this->db->escape_str(trim($this->input->get('customer_id')));
        if ($customer_id == '' || $customer_id <= 0) {
            $this->set_response([
                'status' => FALSE,
                'message' => $this->lang->line('MSG_BAD_REQUEST'),
            ], REST_Controller::HTTP_OK);
        } else {
            $select_response = $this->customer_model->get_info_by_id($customer_id);
            if (!empty($select_response)) {
                unset($select_response->password);
                $arr['data'] = $select_response;
                $data = [
                    'status' => TRUE,
                    'data' => $select_response,
                    #'data' => $this->encodeJSONData($arr),
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                $data = [
                    'status' => FALSE,
                    'message' => $this->lang->line('MSG_DATA_NOT_FOUND')
                ];
                $this->set_response($data, REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
}

?>
<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    var $table = 'customers';
    var $table_country = 'country';
    var $table_city = 'city';
    var $primary_key = 'customer_id';
    var $column_order = array(null, 'customer_name','email_address','home_address','phone_number','dob','country_name','city_name', null); //set column field database for datatable orderable
    var $column_search = array('customer_name','email_address','home_address','phone_number','dob','country_name','city_name'); //set column field database for datatable searchable
    var $order = array('customer_id' => ASCENDING); // default order

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * For datatable process start
     * This function is used for get list
     * @return object
     */
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_country, 'customers.country_id =  country.country_id', 'left');
        $this->db->join($this->table_city, 'customers.city_id =  city.city_id', 'left');
        #$this->db->join($this->table_department . ' as DEPART ', ' dep_service_id =  department_id', 'left');
        # $this->db->order_by('customer_name', ASCENDING);
    }

    public function customer_info($id)
    {
        $this->fetch_data();
        $this->db->where('customers.' . $this->primary_key, $id);
        $response_data = $this->db->get()->row();
        #_e($this->db->last_query());
        return $response_data;
    }
    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    /** Datatable Process End **/
    public function check_email($where)
    {
        if (empty($where)) {
            return FALSE;
        }
        $this->db->select('*');
        $this->db->from($this->table);
        $username = $where['email'];
        $this->db->where('email_address', $username);
        $response_data = $this->db->get()->row();
        #_e($this->db->last_query());
        return $response_data;
    }
    /**
     * This function is used for authentication
     * @return object
     */
    public function check_login($where)
    {
        if (empty($where)) {
            return FALSE;
        }
        $this->db->select('*');
        $this->db->from($this->table);
        $username = $where['email'];
        $password = $where['password'];
        $this->db->where('email_address', $username);
        $this->db->where('password', $password);
        #$this->db->where('(email = "' . $username . '" OR user_name = "' . $username . '")');
        $response_data = $this->db->get()->row();
        #_e($this->db->last_query());
        return $response_data;
    }

    /**
     * This function is use for update status
     * @return object
     */
    public function update_column($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    /**
     * This function is use for getting information by id
     * @return object
     */

    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    /**
     * This function is use for getting information by id
     * @return object
     */

    public function get_customer_list($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if ($id != '') {
            $this->db->where($this->primary_key, $id);
        }
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    public function get_customer()
    {
        $this->fetch_data();
        $this->db->order_by('customer_name', ASCENDING);
        $query = $this->db->get();
        #_e($this->db->last_query());
        return $query->result();
    }
    public function get_customer_name($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('customer_id', $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function change_status($id,$status)
    {
        $this->db->where('customer_id', $id);
        $this->db->set('status',$status);
        $this->db->update($this->table);
        return TRUE;
    }

    public function check_password($where)
    {
        if (empty($where)) {
            return FALSE;
        }
        $this->db->select('*');
        $this->db->from($this->table);
        $customer_id = $where['customer_id'];
        $password = $where['password'];
        $this->db->where('customer_id', $customer_id);
        $this->db->where('password', $password);
        $response_data = $this->db->get()->row();
        #_e($this->db->last_query());
        return $response_data;
    }
    public function check_token($id,$user_token)
    {
        if(empty($id))
        { return false; }

        $token_value = array('pwd_token' => $user_token, 'email_address' => $id);
        $this->db->where($token_value);
        $this->db->from($this->table);
        $data = $this->db->get()->row();
        return $data;

    }
}


?>
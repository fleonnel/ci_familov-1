<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Email_template_model extends CI_Model
{
    var $table = 'email_template';
    var $primary_key = 'email_id';
    var $email_key = 'vEmailFormatType';
    var $column_order = array(null, 'vEmailFormatType','vEmailFormatTitle_EN', 'vEmailFormatSubject_EN',  'vEmailFormatTitle_FR', null); //set column field database for datatable orderable
    var $column_search = array('vEmailFormatType','vEmailFormatTitle_EN', 'vEmailFormatSubject_EN',  'vEmailFormatTitle_FR'); //set column field database for datatable searchable
    var $order = array('email_id' => ASCENDING); // default order

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * For datatable process start
     * This function is used for get list
     * @return object
     */
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        { 
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    /** Datatable Process End **/


    /**
     * This function is use for getting information by unique key
     * @return object
     */
    public function get_email_template($id, $type = 0)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        if ($type == 0) {
            $this->db->where($this->primary_key, $id);
        } else {
            $this->db->where($this->email_key, $id);
        }
        $response_data = $this->db->get()->row();

        return $response_data;
    }
     public function get_email_template_view($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    /**
     * This function is use for update status process
     * @return object
     */
    public function update_status($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }


    /**
     * This function is use for getting All data list
     * @return object
     */
    public function get_all_list($status = URL_CATEGORY_STATUS_ACTIVE)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('uc_status', $status);
        $response_data = $this->db->get()->result();
        return $response_data;
    }

}

?>
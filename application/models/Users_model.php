<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Users_Model extends CI_Model
{
    var $table = 'user';
    var $column_search = array('user_image','first_name', 'last_name', 'email', 'status'); //set column field database for datatable searchable

    public function loginVerify($conditions){
        if(empty($conditions)){
            return FALSE;
        }
        $this->db->where($conditions);
        $this->db->dbprefix('user');
        $Data = $this->db->get($this->db->dbprefix('user'));
        #echo "<pre>"; print_r($Data);echo "</pre>";
        #echo "<br>".$this->db->last_query();
        return $Data;
    }
    public function checkEmail($conditions){
        if(empty($conditions)){
            return FALSE;
        }
        $this->db->where($conditions);
        $this->db->dbprefix('user');
        $Data = $this->db->get($this->db->dbprefix('user'));
        #echo "<pre>"; print_r($Data);echo "</pre>";
        #echo "<br>".$this->db->last_query();
        return $Data;
    }

    public function signup_user($signup_data)
    {
        #$status['status'] = '0';
        $status = 0;
        if ($this->db->insert('user', $signup_data)) {
            #$status['status'] = '1';
            $status = 1;
        }
        #echo "<br>".$this->db->last_query();exit;
        #$status = json_encode($status); // json_encode($signup_data);
        return $status;
    }

    public function reset_pass($data,$user_id)
    {
        $status['status'] = '0';
        $this->db->where('user_id', $user_id);
        // $this->db->update('data' ,$data);
        if ($this->db->update('user' ,$data)) {
            $status['status'] = '1';
        }
        $status = json_encode($status); // json_encode($signup_data);
        return $status;
    }

    public function getverifyToken($user_token,$id)
    {
        if(empty($id))
        { return false; }


        $token_value = array('token' => $id,'user_id' => $user_token,'status' => 0); 
        $this->db->where($token_value);
        $this->db->dbprefix('reset_password');
        $Data = $this->db->get($this->db->dbprefix('reset_password'));
       
       return $Data;

    }
    
   public function set_passresettoken($id,$token)
    {
        $new_time = date("Y-m-d H:i:s", strtotime('+1 hours'));
        
    $OrderLines=$id;
    //$token = md5(time());
    $data  = array('user_id'=>$OrderLines,
            'token'=>$token,
            'status'=>0,
            'user_type'=>3,
            'expire_time'=>$new_time,
            'posted_date'=>date('Y-m-d H:i:s'));

        $Data = $this->db->insert('reset_password',$data);
        return $Data;

    }

    public function get_profile($user_id)
    {
        $this->db->where(array('user_id'=>$user_id));
        $this->db->dbprefix('user');
        $Data = $this->db->get($this->db->dbprefix('user'));
        // $Data = $Data->result_array()[0];

        return $Data;
    }

    function getList()
    {
        $this->listingData();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function listingData()
    {
        #$this->db->from($this->table);
        /*echo $sql="SELECT * FROM ".$this->table." where parent_agency_id='".$this->session->userdata('seller')['seller_data']['parent_agency_id']."'";
        $this->db->query($sql);*/ # For Custom Query.
        $this->db->select('user_id,user_image,first_name,last_name,email,status')
            ->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all()
    {
        #$this->db->from($this->table);
        $this->db->select('user_id,user_image,first_name,last_name,email,status')
            ->from($this->table);
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->listingData();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getUserDetailsById($UserId)
    { #For Get Agent Inforamtion
        $this->db->where('user_id=' . $UserId);
        $this->db->dbprefix('user');
        $query = $this->db->get($this->db->dbprefix('user'));
        $Data = $query->row();
        return $Data;
    }

    public function statusUpdate($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function updateProfile($user_id,$data)
    {
        $this->db->where('user_id', $user_id);
        // $this->db->update('data' ,$data);
        if ($this->db->update('user' ,$data)) {
            $Data['status'] = '1';
        }
        
        $this->db->where(array('user_id' => $user_id));
        $this->db->dbprefix('user');
        $query = $this->db->get($this->db->dbprefix('user'));
        $Data['userData'] = $query->row();        
        return $Data;
    }

    public function changePassword($user_id,$data)
    {
        $Data['status'] = '0';
        $this->db->where('user_id', $user_id);
        // $this->db->update('data' ,$data);
        if ($this->db->update('user' ,$data)) {
            $Data['status'] = '1';
        }
        return $Data;
    }

    public function changeEmail($user_id,$data)
    {
        $Data['status'] = '0';
        $this->db->where('user_id', $user_id);
        // $this->db->update('data' ,$data);
        if ($this->db->update('user' ,$data)) {
            $Data['status'] = '1';
        }
        return $Data;
    }



    public function seachPropertyData($where)
    { #For Get Agent Inforamtion
        // $this->db->where('property=' . $UserId);
        $this->db->dbprefix('property');
        $query = $this->db->get('property');
        $Data = $query->row();
        return $Data;
    }



}
?>
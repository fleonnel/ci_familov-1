<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{
    var $table = 'products';
    var $table_shop = 'shops';
    var $table_category = 'categories';
    var $order = array('product_id' => DESCENDING);
    var $primary_key = 'product_id';
    var $column_order = array(null, 'product_name','categories.category_name','product_prices','shops.shop_name', null); //set column field database for datatable orderable
    var $column_search = array('product_name','categories.category_name','product_prices','shop_name');
    public function __construct()
    {
        parent::__construct();
    }
    public function get_info_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function get_list($shop_id='')
    {
        $this->get_data($shop_id);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    private function get_data($shop_id='')
    {
        $this->fetch_data($shop_id);
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data($shop_id='')
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_shop, 'products.shop_id =  shops.shop_id', 'left');
        $this->db->join($this->table_category, 'products.category_id =  categories.category_id', 'left');
        if($shop_id!='')
        {
            $this->db->where_in('products.shop_id', explode(",", $shop_id));
        }
        #$this->db->order_by('shop_name', ASCENDING);
        // $query = $this->db->get();
        // _e($this->db->last_query());        
        // SELECT * FROM `products` LEFT JOIN `shops` ON `products`.`shop_id` = `shops`.`shop_id` LEFT JOIN `categories` ON `products`.`category_id` = `categories`.`category_id`
    }

    public function count_filtered($shop_id='')
    {
        $this->get_data($shop_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($shop_id='')
    {
        $this->fetch_data($shop_id);
        return $this->db->count_all_results();
    }

    public function update_files($file, $FileId)
    {
        $this->db->where('product_id', $FileId);
        $this->db->set('product_image',$file);
        $this->db->update('products');
        return TRUE;
    }

    public function get($id)
    {
       $this->db->select('*');
       $this->db->from($this->table);
       $this->db->where('product_id', $id);
       $query = $this->db->get();
        return $query->result();
    }

    public function getimagedata($id)
    {
       $this->db->select('*');
       $this->db->from($this->table);
       $this->db->where('product_id', $id);
       $response_data = $this->db->get()->row();
        return $response_data;
    }

    public function getdata($id)
    {
       $this->db->select('*');
       $this->db->from($this->table);
       $this->db->where('product_id', $id);
       $response_data = $this->db->get()->row();
        return $response_data;
    }

    public function update_product($id)
    {
        $this->db->where('product_id', $id);
        $this->db->set('product_image','');
        $this->db->update('products');
        return TRUE;
    }
    public function change_status($id,$status)
    {
        $this->db->where('product_id', $id);
        $this->db->set('product_status',$status);
        $this->db->update($this->table);
        return TRUE;
    }
    public function available_status($id,$status)
    {
        $this->db->where('product_id', $id);
        $this->db->set('product_availability',$status);
        $this->db->update($this->table);
        return TRUE;
    }
    public function delete($id)
    {
        $this->db->where('product_id', $id);
        $this->db->delete($this->table);

        return TRUE;
    }

    public function product_list_shop_wise($shop_id, $category = '', $search_keyword = '', $limit = 0, $start = 0)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('product_status', 'PUBLISHED');
        $this->db->where('shop_id', $shop_id);
        if ($category != '') {
            $this->db->where('category_id', $category);
        }
        if ($search_keyword != '') {
            $this->db->like('product_name', $search_keyword);
        }
        $this->db->order_by('CAST(product_prices AS DECIMAL(10,2))');
        $query = $this->db->limit($limit, $start)->get();
        #_e($this->db->last_query());
        return $query->result();
    }

    public function get_product_info_by_id($id, $status = '')
    {
        $this->db->select('products.*, categories.category_name');
        $this->db->from($this->table, ' ');
        $this->db->join($this->table_category, 'products.category_id =  categories.category_id', 'left');
        if ($status != '') {
            $this->db->where('product_status', "PUBLISHED");
        }
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    public function product_search_keyword_wise($shop_id, $keyword = '', $category = '')
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('product_status', 'PUBLISHED');
        $this->db->where('shop_id', $shop_id);
        if ($category != '') {
            $this->db->where('category_id', $category);
        }
        if ($keyword != '') {
            $this->db->like('product_name', $keyword);
        }
        $this->db->order_by('CAST(product_prices AS DECIMAL(10,2))');
        $response_data = $this->db->get()->result();
        #_e($this->db->last_query());
        return $response_data;
    }
}

?>
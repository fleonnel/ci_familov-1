<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    var $table = 'admin';
    var $table_role = 'roles';
     var $table_shop = 'shops';
    var $primary_key = 'admin_id';
    var $table_day_out = 'day_out_end';
    var $table_request = 'requests';
    var $primary_key_request = 'request_id';

    var $column_order = array(null, 'first_name', 'email', 'admin_status', null); //set column field database for datatable orderable
    var $column_order_day = array(null, 'first_name','doe_time', 'doe_key',null); //set column field database for datatable orderable
    var $column_search = array('first_name', 'last_name', 'email','roles.name', 'admin_status'); //set column field database for datatable searchable
    var $column_search_day = array('first_name','last_name', 'doe_time', 'doe_key'); //set column field database for datatable searchable
    var $order = array('admin_id' => ASCENDING); // default order
    var $order_day_out = array('doe_id' => ASCENDING);

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * This function is used for admin authentication ( Working in login process )
     * @return object
     */
    public function check_login($where)
    {
        if (empty($where)) {
            return FALSE;
        }
        $this->db->select('*');
        $this->db->from($this->table);
        $username = $where['email'];
        $password = $where['password'];
        $this->db->where('password', $password);
        $this->db->where('(email = "' . $username . '" OR user_name = "' . $username . '")');
        #$this->db->or_where('user_name)',$username);
        $response_data = $this->db->get()->row();
        return $response_data;
    }

    /**
     * For datatable process start
     * This function is used for datatbale list
     * @return object
     */
    public function get_list()
    {
        $this->get_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        // _e($this->db->last_query());
        return $query->result();
    }

    private function get_data()
    {
        $this->fetch_data();
        $i = 0;
        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_data(){
        //add custom filter here
        /*if ($this->input->post('property_status')) {
            $this->db->where($this->table . '.status', $this->db->escape_str($this->input->post('property_status')));
        }*/
        $this->db->select("*");
        #$this->db->where($this->primary_key, 1);
        $this->db->from($this->table);
        $this->db->join($this->table_role, 'admin.roles_id =  roles.roles_id', 'left');
        $this->db->where('admin_is_deleted', 'No');
        $this->db->order_by('admin_type_id', ASCENDING);
    }

    public function count_filtered()
    {
        $this->get_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->fetch_data();
        return $this->db->count_all_results();
    }
    /** Datatable Process End **/

    /** Datatable Process For Day Out **/
     public function get_day_out_list()
    {
        $this->get_day_out_data();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    private function get_day_out_data()
    {
        $this->fetch_day_out_data();
        $i = 0;
        foreach ($this->column_search_day as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search_day) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_day[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_day_out)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function fetch_day_out_data()
    {
        $this->db->select("*");
        $this->db->from($this->table_day_out . ' as AB ');
        $this->db->join($this->table . ' as CC ', ' CC.admin_id = AB.admin_id', 'left');
        //$this->db->order_by('admin_id', ASCENDING);
    }
    public function count_day_out_filtered()
    {
        $this->get_day_out_data();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_day_out_all()
    {
        $this->fetch_day_out_data();
        return $this->db->count_all_results();
    }

    /** Datatable Process End **/

    /**
     * This function is use for getting admin type list
     * @return object
     */

    public function get_admin_type_list()
    {
        $this->db->select('*');
        $this->db->from($this->table_role);
        $this->db->where_not_in('roles_id ', SUPER_ADMIN_ROLE_ID);
        $response_data = $this->db->get()->result();
        return $response_data;
    }
    /**
     * This function is use for getting information by id
     * @return object
     */

    public function get_info_admin_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    public function get_info_admin_by_role_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('roles_id', $id);
        $response_data = $this->db->get()->row();
        return $response_data;
    }
    /**
     * This function is use for update status
     * @return object
     */
    public function update_column($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
    /**
     * This function is use for update status
     * @return object
     */
    public function update_status($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
    /**
     * This function is use for check logged in admin current password
     * @return object
     */
    public function check_old_password($id, $current_password)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->primary_key, $id);
        $this->db->where('password', $current_password);
        return $this->db->get()->num_rows();

    }
    /**
     * This function is use for check username/email with table.
     * @return object
     */
    public function check_forgot_password($user_name)
    {
        $this->db->select('admin_id, email, user_name,first_name,last_name');
        $this->db->from($this->table);
        $this->db->where('email', $user_name);
        $this->db->or_where('user_name', $user_name);
        return $this->db->get()->row();

    }

    /**
     * This function is use for getting Employee list
     * @return object
     */

    public function get_employee_list()
    {
        $this->db->select('CONCAT(first_name," ",last_name) as full_name, admin_id');
        $this->db->from($this->table);
        $this->db->where('admin_status', ADMIN_STATUS_ACTIVE);
        $this->db->where('admin_is_deleted', 'No');
        $this->db->where_not_in('admin_type_id', array(SUPER_ADMIN_ROLE_ID, ADMIN_ROLE_ID));
        $this->db->order_by('first_name,last_name', ASCENDING);
        $response_data = $this->db->get()->result();
        return $response_data;
    }

    /**
     * This function is use for check dependency
     * @return object
     */

    public function check_dependency_request($id)
    {
        $this->db->select('*');
        $this->db->from($this->table_request);
        $this->db->where('employee_id', $id);
        $this->db->or_where('created_by_id', $id);
        $response_data = $this->db->get()->num_rows();
        return $response_data;
    }

    /**
     * For Report Section
     */
    public function get_employee()
    {
        $this->db->select('admin_id,admin_status,admin_is_deleted, CONCAT(first_name," ",last_name) as full_name');
        $this->db->from($this->table);
        #$this->db->where('admin_is_deleted', 'No');
        #$this->db->where('admin_is_deleted', 'No');
        $this->db->where_not_in('admin_type_id', array(SUPER_ADMIN_ROLE_ID));
        $this->db->order_by('first_name', ASCENDING);
        $query = $this->db->get();
        #_e($this->db->last_query());
        return $query->result();
    }

    public function get_admin_manager()
    {
        // select admin_id, roles_id, first_name, last_name, user_name, email from admin where admin_status='Active' and admin_is_deleted='No'        
        $this->db->select('admin_id, roles_id, CONCAT(first_name," ",last_name) as full_name, user_name, email');
        $this->db->from($this->table);
        $this->db->where('admin_status', 'Active');
        $this->db->where('admin_is_deleted', 'No');
        $this->db->where('(roles_id = "2" OR roles_id = "3")');
        $query = $this->db->get();
        #_e($this->db->last_query());
        return $query->result();
    }
}

?>
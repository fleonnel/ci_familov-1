<?php //require_once('config.php'); ?>
<style>
    /**
* The CSS shown here will not be introduced in the Quickstart guide, but
* shows how you can use CSS to style your Element's container.
*/
    input,
    .StripeElement {
        height: 40px;
        padding: 10px 12px;

        color: #32325d;
        background-color: white;
        border: 1px solid transparent;
        border-radius: 4px;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    input:focus,
    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>
<script src="https://js.stripe.com/v3/"></script>

<form action="server.php" method="post" id="payment-form">
    <div class="form-row inline">
        <div class="col">
            <label for="name">
                Name
            </label>
            <input id="name" name="name" placeholder="Jane Doe" required value="Jane Doe">
        </div>
        <div class="col">
            <label for="email">
                Email Address
            </label>
            <input id="email" name="email" type="email" placeholder="jane.doe@example.com" value="jane.doe@example.com"
                   required>
        </div>
    </div>

    <div class="form-row">
        <label for="iban-element">
            IBAN
        </label>
        <input id="email" name="iban_no" type="text" placeholder="DE89370400440532013000" value="DE89370400440532013000"
               required>
        <!--<div id="iban-element">

    </div>-->
        <div id="bank-name"></div>
    </div>

    <button>Submit Payment</button>
    <button id="checkout_go" type="submit" style="margin-top: 0px;margin-bottom: -26px;" class="btn btn-green">Confirm
        the payment <i class="icon-check"></i></button>

    <!-- Used to display form errors. -->
    <div id="error-message" role="alert"></div>

    <!-- Display mandate acceptance text. -->
    <div id="mandate-acceptance">
        By providing your IBAN and confirming this payment, you are
        authorizing Rocketship Inc. and Stripe, our payment service
        provider, to send instructions to your bank to debit your account and
        your bank to debit your account in accordance with those instructions.
        You are entitled to a refund from your bank under the terms and
        conditions of your agreement with your bank. A refund must be claimed
        within 8 weeks starting from the date on which your account was debited.
    </div>
</form>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script>
    // Create a Stripe client.
    // Note: this merchant has been set up for demo purposes.
    var stripe = Stripe('pk_test_6KirUNVpAYNCSQj0o6TXY9Mz');
    //var stripe = Stripe('sk_test_PjXblK5gvDtXKbN0LAm9c5kS');
    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            },
            ':-webkit-autofill': {
                color: '#32325d',
            },
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a',
            ':-webkit-autofill': {
                color: '#fa755a',
            },
        }
    };

    // Create an instance of the iban Element.
    /* var iban = elements.create('iban', {
     style: style,
     supportedCountries: ['SEPA'],
     });

     // Add an instance of the iban Element into the `iban-element` <div>.
     iban.mount('#iban-element');

     var errorMessage = document.getElementById('error-message');
     var bankName = document.getElementById('bank-name');

     iban.on('change', function(event) {
     console.log(iban);
     // Handle real-time validation errors from the iban Element.
     if (event.error) {
     errorMessage.textContent = event.error.message;
     errorMessage.classList.add('visible');
     } else {
     errorMessage.classList.remove('visible');
     }

     // Display bank name corresponding to IBAN, if available.
     if (event.bankName) {
     bankName.textContent = event.bankName;
     bankName.classList.add('visible');

     console.log(event);
     //var form = document.getElementById('payment-form');

     *//*var hiddenInput = document.createElement('input');
     hiddenInput.setAttribute('type', 'hidden');
     hiddenInput.setAttribute('name', 'iban');
     hiddenInput.setAttribute('value', '');
     form.appendChild(hiddenInput);*//*
     //form.submit();

     } else {
     bankName.classList.remove('visible');
     }
     });*/

    // Handle form submission.
    /*var form = document.getElementById('payment-form');
     form.addEventListener('submit', function(event) {
     event.preventDefault();
     //showLoading();

     var sourceData = {
     type: 'sepa_debit',
     currency: 'eur',
     owner: {
     name: document.querySelector('input[name="name"]').value,
     email: document.querySelector('input[name="email"]').value,
     },
     mandate: {
     // Automatically send a mandate notification email to your customer
     // once the source is charged.
     notification_method: 'email',
     },
     redirect: {
     return_url: 'https://shop.example.com/crtA6B28E1',
     },
     };

     // Call `stripe.createSource` with the iban Element and additional options.
     stripe.createSource(iban, sourceData).then(function(result) {
     if (result.error) {
     // Inform the customer that there was an error.
     errorMessage.textContent = result.error.message;
     errorMessage.classList.add('visible');
     //stopLoading();
     } else {
     // Send the Source to your server to create a charge.
     errorMessage.classList.remove('visible');
     console.log(result.source);
     stripeSourceHandler(result.source);
     }
     });
     //var form = document.getElementById('payment-form');
     });*/

    function stripeSourceHandler(source) {
        // Insert the source ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeSource');
        hiddenInput.setAttribute('value', source.id);
        form.appendChild(hiddenInput);
        // Submit the form
        // form.submit();
    }
</script>
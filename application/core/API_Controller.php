<?php defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use \Firebase\JWT\JWT;

// header("Access-Control-Allow-Origin: *");
// header("Access-Control-Allow-Credentials: true");
// header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
// header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
// header("Content-Type: application/json; charset=utf-8");

class API_Controller extends REST_Controller
{
    private $_hashkey;
    private $_arrUserData = array();
    function __construct()
    {
        parent::__construct();
        $this->_hashkey = $this->config->item('rest_jwt_hash');
        $this->load->helper('constants_front_helper');
        $this->load->library('form_validation');
        $this->load->model("customer_model");
        $this->load->model("shop_model");
        $this->load->model("product_model");
    }

    public function encodeJSONData($args = array())
    {
        $tIssuedAt = time();
        $tExpire = $tIssuedAt + (7 * 24 * 60 * 60);
        $token = array(
            "iss" => $_SERVER['SERVER_NAME'],
            "iat" => isset($args['tIssuedAt']) ? $args['tIssuedAt'] : $tIssuedAt,
            "exp" => isset($args['tExpire']) ? $args['tExpire'] : $tExpire,
            "data" => isset($args['data']) ? $args['data'] : array(),
        );
        $return = $this->_encodeJsonKey($token);
        return $return;
    }

    private function _encodeJsonKey($token)
    {
        $return = JWT::encode($token, $this->_hashkey, 'HS256');
        return $return;
    }

    public function decodeJSONData() {

        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Allow-Credentials: true");
        // header("Access-Control-Allow-Methods: *");
        // header("Access-Control-Allow-Headers: Content-Type,Authorization");
        // header("Content-Type: application/json; charset=utf-8");

        // $entityBody = file_get_contents('php://input', 'r');
        // parse_str($entityBody , $post_data);

        $authHeader = $this->input->get_request_header('authorization');
        if (empty($authHeader)) {
            $authHeader = $this->input->get_post('authorization');
        }

        $isError = 2;
        if($authHeader) {
            list($token) = sscanf( $authHeader, 'Bearer %s');
            if($token) {
                $headerJsonData = $this->_decodeJsonKey($token);
                if($headerJsonData) {
                    $isError = 3;
                    $current = time();
                    if($current <= $headerJsonData->exp) {
                        $this->setTokenData($headerJsonData->data);
                        $isError = 1;
                    }
                }
            }
        }
        
        if($isError == 2) {
            $this->response([
                'status' => FALSE,
                'message' => 'Token Not Found'
            ], parent::HTTP_UNAUTHORIZED);
        }
        else if($isError == 3) {
            $this->response([
                'status' => FALSE,
                'message' => 'Token Expired'
            ], parent::HTTP_UNAUTHORIZED);
        }
        return $headerJsonData;
    }

    private function _decodeJsonKey($encodeJsonKey)
    {
        $return = JWT::decode($encodeJsonKey, $this->_hashkey, array('HS256'));
        return $return;
    }

    public function setTokenData($userData)
    {
        $this->_arrUserData = $userData;
    }

    public function getTokenData()
    {
        return $this->_arrUserData;
    }
}
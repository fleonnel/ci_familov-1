<?= doctype('html5'); ?>
<?php ob_start(); ?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE OF SITE -->
    <!--<title> Familov.com - Delivering happiness to your family at home.</title>-->
    <meta name="description"
          content="Commandez depuis l´étranger et nous livrons vos proches au pays en moins de 36 heures. Simple, éfficace, social."/>
    <meta name="keywords" content="groceries, africa, delivered,family,startup,loved, money"/>
    <meta name="Familov" content="cash to goods">
    <base href="<?= base_url() ?>"></base>
    <title><?php echo $page_title; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(ASSETS . 'common/favicon.png'); ?>" type="image/png">
    <link rel="icon" href="<?php echo base_url(ASSETS . 'common/favicon.png'); ?>" type="image/png">
    <?= $this->load->view(THEME_CSS_SECTION); ?>
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery-2.2.3.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url(ASSETS_FRONT . 'js/bootstrap.min.js'); ?>"></script>
    <!-- EXTRA JS SECTION -->
    <script src="<?php echo base_url(ASSETS_BUILD_JS . 'bootstrapValidator.js'); ?>"></script>
    <!--<script src="<?php /*echo base_url(ASSETS . 'js/validate.js'); */ ?>"></script>-->
    <script src="<?= EXTRA_PLUGIN ?>validate/jquery.validate.min.js"></script>

    <script src="<?php echo base_url(ASSETS_FRONT . 'js/sweetalert2.js'); ?>"></script>
    <script src="<?php echo base_url(ASSETS_FRONT . 'js/sweetalert2.min.js'); ?>"></script>

    <?= $this->load->view(THEME_JS_SECTION); ?>
    <script type="text/javascript">
        if (navigator.userAgent.indexOf('Safari') != -1 &&
            navigator.userAgent.indexOf('Chrome') == -1) {
            document.body.className += " safari";
        }
    </script>
</head>
<body data-spy="scroll" data-target="#main-navbar">
<div class="main-container" id="page">
    <!-- Header Section -->
    <?= $this->load->view(THEME_HEADER_SECTION); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <!-- <? /*= $this->load->view(THEME_TOP_MENU_SECTION); */ ?> -->
    <!--<div class="content-wrapper">-->
    <?php echo $the_view_content; ?>
    <!-- /.content-wrapper -->
    <!--</div>-->
    <?= $this->load->view(THEME_FOOTER_SECTION); ?>
    <!-- Control Sidebar -->
</div>
</body>
</html>
<?php ob_end_flush();?>
<!-- =========================
    FOOTER
============================== -->
<div class="row " style="margin-right:0px !important;margin-left:0px!important;background-color:#34393F">
    <div class="container-fluid container footer">
        <div class="row-padding">
            <div class="row footer-wrapper">
                <div class="col-sm-3">
                    <div class="height-15"></div>
                    <div class="footer- hidden-xs">
                         <img
                            width="100%"                                               src="<?php echo(ASSETS_URL_FRONT . 'img/images/bg/familov-award.png') ?>">
                    </div>
                    <!--<div class="footer-payoff" <p><i class="fa fa-envellope text-black"></i> hello@familov.com<br></p></div>-->
                </div>
                <div class="col-sm-3">
                    <div class="height-15"></div>
                    <a href="<?php echo base_url(MENU_HOW_IT_WORKS); ?>"><?php echo $this->lang->line('LBL_MENU_09'); ?></a>
                    <a href="<?php echo base_url(MENU_FAQ); ?>"><?php echo $this->lang->line('LBL_MENU_10') ?></a>
                    <a href="<?php echo base_url(MENU_TERMS); ?>"><?php echo $this->lang->line('LBL_MENU_11') ?></a>
                    <a href="<?php echo base_url(MENU_PRIVACY); ?>"><?php echo $this->lang->line('LBL_MENU_12') ?></a>
                </div>
                <div class="col-sm-3">
                    <div class="height-15"></div>
                    <a href="<?php echo base_url(MENU_ABOUT_US); ?>"><?php echo $this->lang->line('LBL_MENU_13') ?></a>
                    <a href="<?php echo base_url(MENU_CONTACT_US); ?>"><?php echo $this->lang->line('LBL_MENU_14') ?></a>
                    <a href="<?php echo base_url(MENU_PRESS); ?>"><?php echo $this->lang->line('LBL_MENU_15') ?></a>
                    <a href="<?php echo base_url(MENU_JOBS); ?>"><?php echo $this->lang->line('LBL_MENU_16') ?></a>
                </div>
                <div class="col-sm-3">
                    <div class="height-15"></div>
                    <div class="footer-social social-btnX ">
                        <a href="<?= SOCIAL_FACEBOOK_URL ?>" target="_blank" class="sb-dark"> <i
                                class="icon-facebook"></i><?php echo $this->lang->line('LBL_SOCIAL_FACEBOOK'); ?> </a>
                        <a href="<?= SOCIAL_TWITTER_URL ?>" target="_blank" class="sb-dark"> <i
                                class="icon-twitter"></i><?php echo $this->lang->line('LBL_SOCIAL_TWITTER') ?> </a><img
                            width=""
                                                                          src="<?php echo(ASSETS_URL_FRONT . 'img/images/ssl-and-protection.png') ?>">
                    </div>
                    <br/>
                    <div class="height-15"></div>
                </div>
            </div>
            <div class="row-padding">
                <div class="row footer-wrapper">
                    <div class="col-sm-6 col-xs-6">
                        <div class="footer-payoff"><?php echo $this->lang->line('LBL_FOOTER_01'); ?>  </div>

                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div class="footer-payoffX">
                            <div class="footer-payoffX">
                                <?php echo $this->lang->line('LBL_FOOTER_02'); ?>
                                <?php /*echo "LBL_FOOTER_MADEWITH"; */ ?><!-- <i
                                    class="fa fa-heart wow pulse" data-wow-iteration="30"
                                    style="color: rgb(236, 30, 115); visibility: visible; animation-iteration-count: 30; animation-name: pulse;"></i>
                                by Familover-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /End Main Conteiner -->
</div>
<!-- Back to Top Button -->
<a href="#" class="top" style="background-color:#525e6c;">Top</a>


<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '7pF07Z0vTy';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<!-- =========================
     SCRIPTS
============================== -->
<?= $this->load->view(THEME_JS_SECTION); ?>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery-ui-1.10.3.custom.min.js'); ?>"></script>
<!--<script src="<?php /*echo base_url(ASSETS_FRONT. 'js/bootstrap.min.js'); */ ?>"></script>-->
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery.easing.1.3.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery.countTo.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery.plainmodal.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery.jCounter-0.1.4.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery.magnific-popup.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/jquery.vide.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/spectragram.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/twitterFetcher_min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/wow.min.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/picker.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/picker.date.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/typeahead.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url(ASSETS_FRONT . 'js/background.cycle.js'); ?>"></script>

<!-- Custom Script -->
<script src="<?php echo base_url(ASSETS_FRONT . 'js/custom.js'); ?>"></script>

<script src="<?php echo base_url(ASSETS_FRONT . 'js/intlTelInput.js'); ?>"></script>
<script src="<?php echo base_url(ASSETS_FRONT . 'js/clipboard.min.js'); ?>"></script>

<script type="text/javascript">
    //for initiating calling code
    /* $("#mobile_number, #phone_number").intlTelInput({
     utilsScript: "js/utils.js"
    });
     $(".dropdown-menu li a").click(function(){
     var selText = $(this).text();
     $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
     });*/
</script>
<!-- BEGIN JIVOSITE CODE {literal} -->
<!--<script type='text/javascript'>
    (function(){ var widget_id = 'jMsZARFOXa';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>-->
<!-- {/literal} END JIVOSITE CODE -->
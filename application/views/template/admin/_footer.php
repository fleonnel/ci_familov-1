<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <p><?= _get_config('admin_version_text_one') ?> 3.0.1</p> <? /*= _get_config('admin_version_text_two') */ ?>
    </div>
   <p><!--<?= _get_config('admin_copyright_text_one') ?><!--<a
            href="<? /*= ADMIN_PANEL_FOOTER_ONE_URL */ ?>" target="_blank"><? /*= ADMIN_PANEL_FOOTER_ONE */ ?></a>-->
        Copyright &copy; 2017-2018 Familov.com LIMT</p>
</footer>

<!--<script src="<?php /*echo ASSETS_URL; */?>build/js/custom.min.js"></script>
<script src="<?/*= EX_PLUGINS */?>toastr/toastr.min.js"></script>-->
<!-- SweetAlert-->
<script src="<?= EX_PLUGINS ?>sweetalert/sweetalert.min.js"></script>
<!-- Custom Master Script Seller -->
<!--<script type="text/javascript" src='<?/*= JS_URL */?>master_script.js'></script>-->
<!-- Custom CSS which are used in every-->
<?php echo link_tag(CSS_URL . 'master_custom.css'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        /*toastr.success('Successalert', '<?=SUCCESS?>');
         toastr.error('Error alert', '<?=ERROR?>');
         toastr.warning('wadning allert', '<?=WARNING?>');
         toastr.info('info alert', '<?=INFO?>');*/
    });
    /*
     * Documentation JS script
     */
    $(function () {
        var slideToTop = $("<div />");
        slideToTop.html('<i class="fa fa-chevron-up"></i>');
        slideToTop.css({
            position: 'fixed',
            bottom: '20px',
            right: '25px',
            width: '40px',
            height: '40px',
            color: '#eee',
            'font-size': '',
            'line-height': '40px',
            'text-align': 'center',
            'background-color': '#222d32',
            cursor: 'pointer',
            'border-radius': '5px',
            'z-index': '99999',
            opacity: '.7',
            'display': 'none'
        });
        slideToTop.on('mouseenter', function () {
            $(this).css('opacity', '1');
        });
        slideToTop.on('mouseout', function () {
            $(this).css('opacity', '.7');
        });
        $('.wrapper').append(slideToTop);
        $(window).scroll(function () {
            if ($(window).scrollTop() >= 150) {
                if (!$(slideToTop).is(':visible')) {
                    $(slideToTop).fadeIn(500);
                }
            } else {
                $(slideToTop).fadeOut(500);
            }
        });
        $(slideToTop).click(function () {
            $("body").animate({
                scrollTop: 0
            }, 500);
        });
        $(".sidebar-menu li:not(.treeview) a").click(function () {
            var $this = $(this);
            var target = $this.attr("href");
            if (typeof target === 'string') {
                $("body").animate({
                    scrollTop: ($(target).offset().top) + "px"
                }, 500);
            }
        });
    });
</script>
<?php
if (PROFILER_MODE)
    _ci_profiler(PROFILER_MODE);
?>
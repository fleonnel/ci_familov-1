<!DOCTYPE html>
<html lang="en" ng-app='rsApp'>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?=$page_title;?></title>
    </head>
    <body>
        <?php echo $the_view_content;?>
    </body>
</html>